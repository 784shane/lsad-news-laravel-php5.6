<?php
namespace App\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Services\Auth\User;

class ViewHelperComposer
{
    /**
     * The json encoding options.
     *
     * @var int
     */
    protected $loggedInUser = null;

    public function __construct()
    {
        // Getting the logged in user
        $this->setLoggedInUser();
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(array(
            'BusSignupHelper' => $this
        ));
    }
}