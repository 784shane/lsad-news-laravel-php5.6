<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Deliveries extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    //protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'delivery_nonce',
        'user',
        'quote_accepted',
        'payment_made',
        'title',
        'subtitle',
        'telephone_number',
        'shipping_category',
        'collection_postcode',
        'delivery_postcode',
        'collection_address',
        'delivery_address',
        'distance_text',
        'distance_metres',
        'expiry'
    ];

    public function updateDelivery($where, $whereValue, $data){
        return $this->where($where, $whereValue)
            ->update($data);
    }

    /**
     * getDeliveriesByUserId
     * @param int
     * @return Illuminate\Database\Eloquent\Collection
     **/
    public function getDeliveriesByUserId($userId){
        return $this->where('user', '=', $userId)->get();
    }

    /**
     * getDeliveriesByUserId
     * @param int
     * @return Illuminate\Database\Eloquent\Collection
     **/
    public function getMyHomepageDeliveries($userId, $deliveriesStatus = null){
        $deliveries = $this->where('user', '=', $userId);
        $statues = \Config::get('constants.deliveries.statuses');
        if($deliveriesStatus !== null){
            switch($deliveriesStatus){
                case 'active':{
                    $deliveries->where('status', '<', $statues['EXPIRED']);
                    break;
                }
                case 'archived':{
                    $deliveries->where('status', '>', $statues['ACTIVE_PRIVATE']);
                    break;
                }
                default:{
                }
            }
        }
        return $deliveries->get();
    }

    /**
    * getDeliveriesById
    * @param int
    * @return Illuminate\Database\Eloquent\Collection
    **/
    public function getDeliveriesById($id){
        return $this->where('id', '=', $id)->get();
    }

    /**
    * getDeliveries
    * @param int
    * @return Illuminate\Database\Eloquent\Collection
    **/
    public function getDeliveries(){
        return $this->get();
    }
}
