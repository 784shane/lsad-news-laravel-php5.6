<!-- app/views/login.blade.php -->

<!doctype html>
<html>
<head>
<title>Look at me Login</title>
</head>
<body>

@if($signuptype == "")
<div>
	@if (!$userLoggedIn)
	<input type="button" value="Sign up to get Quotes" />
	@endif
	{!! Html::link('/signup/transport_providers','Sign up as Shipping company') !!}
</div>
@endif

@if($signuptype == "personal_details" || $signuptype == "transport_providers")
{!! Form::open(array('url' => $formUrl)) !!}
	<div>
		<p><b style="font-size:53px; color:#bc0;">Register</b></p>
		<p>
		    {!! $errors->first('email') !!}
		    {!! $errors->first('password') !!}
		</p>
    </div>



	@if($signuptype == "personal_details" || $signuptype == "transport_providers")
	<div>
		{!! Form::text('name', null, 
			    array('class' => 'awesome')) 
		   	!!}
		{!! Form::label('first_name', 'First Name') !!}
	    {!! 
	    Form::text('first_name', 'Shane', 
		    array('class' => 'awesome')) 
	   	!!}
    </div>
	
	<div>
		{!! Form::label('last_name', 'Last Name') !!}
	    {!! 
	    Form::text('last_name', 'Daniel', 
		    array('class' => 'awesome')) 
	   	!!}
    </div>
	
	<div>
		{!! Form::label('email', 'Email') !!}
	    {!! Form::text('email', 'xsvibes@fadmail.com',  array()) !!}
    </div>
	
	<div>
		{!! Form::label('Create your password', 'password') !!}
	    {!! 
	    Form::text('password', "password1", array('id' => 'password', 'class' => 'awesome')) 
	   	!!}
    </div>
	
	<div>
		{!! Form::label('Confirm your password', 'confirm_password') !!}
	    {!!
	    Form::text('confirm_password', "password1", array('id' => 'confirm_password', 'class' => 'awesome'))
	   	!!}
    </div>
	
	<div>
		{!! Form::label('telephone_no', 'Telephone no (Optional)') !!}
	    {!! 
	    Form::text('telephone_no', null, 
		    array('class' => 'awesome')) 
	   	!!}
    </div>

	    <br /><br /><br /><br />
    @endif


    @if($signuptype == "transport_providers")
    <div>
	
		<div>
			Business Details
	    </div>
		
		<div>
			{!! Form::label('company_name', 'Company Namexxx') !!}
		    {!! Form::text('company_name', null) !!}
	    </div>
		
		<div>
			{!! Form::label('mobile_number', 'Phone Number') !!}
		    {!! Form::text('mobile_number', null) !!}
	    </div>
		
		<div>
			{!! Form::label('postcode', 'Post Code') !!}
		    {!! Form::text('postcode', null) !!}
	    </div>

    </div>

	    <br /><br /><br /><br />
    @endif




    <!-- The form token. -->
    {!!  Form::token() !!}

    <div>
	{!! Form::submit('Send') !!}
	</div>

{!! Form::close() !!}
@endif

<p>Have you forgotten your password? <a href="/forgot">Click here.</a></p>
