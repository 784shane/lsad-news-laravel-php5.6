<?php
namespace App\ViewComposers\LocalCurrencyFormats;

class LocalCurrencyFormats
{

    public static function formatMoney($money){
        return self::moneyFormatted($money);
    }

    private static function moneyFormatted($money){
        //return "&#163".$money;
        return "&pound;".$money;
    }
}