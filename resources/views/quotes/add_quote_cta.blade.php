@if($ctaType=="no_user")
    {!! Html::link(url('login'), 'Log in to take job', array("class"=>"pure-button pure-button-primary")) !!}
    {!! Html::link(url('/signup/transport_providers'), 'Sign up to Quote for this job', array("class"=>"pure-button pure-button-primary")) !!}
@endif

@if($ctaType=="with_transport_company")
    {!! Html::link(url('/deliveries/quotes/'.$vComposerHelper->getFriendlyUrlTitle($myDeliveries[0]['subtitle']).'/'.$myDeliveries[0]['id'].'/quote'), 'Quote for this job', array("class"=>"pure-button pure-button-primary")) !!}
@endif

@if($ctaType=="no_transport_company")
    {!! Html::link(url('/signup/transport_providers'), 'Sign up to Quote for this job', array("class"=>"pure-button pure-button-primary")) !!}
@endif