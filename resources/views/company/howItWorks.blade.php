@extends('layouts.master')
@section('content')
    <h1>How It Works</h1>
    <div>
        <p>Purple Mover is a place where you can have your goods transported hassle free. A Member enters a 'job' onto the site and are then sent free quotes given by independent Transport Providers. The Member then is able to choose a quote and make arrangements with that Transport Provider to collect and deliver the item/s they require to be delivered.</p>
    </div>
@stop
