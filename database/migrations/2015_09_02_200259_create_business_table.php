<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('businesses', function($table)
	    {
	        $table->increments('id');
	        $table->integer('user',false, true)->length(11);
	        $table->string('company_email');
	        $table->string('company_name');
	        $table->string('company_phone_number');
	        $table->string('company_mobile_number');
	        $table->string('company_address_1');
	        $table->string('company_address_2');
	        $table->string('company_city');
	        $table->string('company_postcode');
			$table->string('validation_token', 60)->unique();
			$table->boolean('verified')->default(false);

			//company_mobile_number
			//company_postcode

	        //My foreign keys
	        $table->foreign('user')->references('id')->on('users');
	        $table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('business');
	}

}
