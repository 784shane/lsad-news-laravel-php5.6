@extends($layout)
@section('content')


    <h1>Forgot Password</h1>

    {!! Form::open(array(
    'url' => 'forgot',
    'id'=>"forget_form",
    'class'=>"pure-form pure-form-stacked")) !!}
    <fieldset>

        <!-- if there are login errors, show them here -->
        <p>
            {!! $errors->first('email') !!}
        </p>

        {!! Form::label('email', 'Email Address') !!}
        {!! Form::text('email', Input::old('email'), array('id'=>'email', 'placeholder' => 'email', 'required' => 'required')) !!}

        {!! Form::submit('Submit', array('class'=>'pure-button pure-button-primary narrow-submit-button')) !!}
    </fieldset>

    {!! Form::close() !!}

@stop
