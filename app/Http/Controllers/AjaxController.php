<?php
namespace App\Http\Controllers;

use App\Services\PostCodeInterpreter;
use Illuminate\Support\Facades\Input;
use Request;
use App\Services\EbayItems;

class AjaxController extends PublicController
{
    public function getPostCodeAddress($postcode)
    {
        $postcodeInterpreter = new PostCodeInterpreter();
        $address = $postcodeInterpreter->getLocationFromPostcode($postcode);
        echo $address;
    }

    public function getEbayItem($ebayitem)
    {
        $ebayItems = new EbayItems();
        $item = $ebayItems->getItemById($ebayitem);
        echo json_encode($item);
    }

    public function getQuotesInfoDetails($deliveryId)
    {
        $result = ['result' => 'false'];
        // We need to get messages from this user.
        // We also need data about this transport provider
        $messages = new \App\Services\Messages();
        //Get the message.
        $theMessageInfo = $messages->getDeliveriesSendersMessage($deliveryId);
        //exit(vde($theMessageInfo));
        if ($theMessageInfo)
        {
            //Get transport provider's details.
            $transportProvider = \App\User::find($theMessageInfo[0]['sender'])->toArray();
            //Get all messages to the user associated with this sender on this delivery.
            $messagesWithSender = $messages->getMessagesWithSender(
                $theMessageInfo[0]['receiver'],
                $theMessageInfo[0]['sender'],
                $theMessageInfo[0]['delivery']
            )->get()->toArray();
            $result['result'] = 'true';
            $result['transportProvider'] = $transportProvider;
            $result['messagesWithSender'] = $messagesWithSender;
        }
        echo json_encode($result);
    }

    public function photoUpload()
    {
        $photos = new \App\Services\Photos();
        $photos->attemptPhotoUpload('uploaded_image_');
    }

    public function calculateAdjustedQuote()
    {
        $fees = [
            'your_fee'=>500,
            'our_fee'=>100,
            'quote'=>2000
        ];
        return \Response::json( $fees );
    }
}
