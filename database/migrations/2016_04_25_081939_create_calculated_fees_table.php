<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalculatedFeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('calculated_fees', function(Blueprint $table)
		{
			$table->increments('id');
			// $table->string('register_token', 60)->unique();
			$table->integer('your_fee',false, true)->length(11);
			$table->integer('our_fee',false, true)->length(11);
			$table->integer('quote',false, true)->length(11);
			$table->string('saved_hash', 60)->unique();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('calculated_fees');
	}

}
