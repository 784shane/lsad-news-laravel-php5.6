<?php namespace App\Services;

use App\DeliveryItems as DeliveryItemsModel;

class ManageDeliveryItems extends DeliveriesItemsGatherer
{
    /**
     * addDeliveriesItems
     *
     * @param array
     * Takes care of adding items to the delivery items table.
     **/
    public function addDeliveriesItems($items, $deliveryId)
    {
        //We have items to add for this delivery.
        foreach($items as $item)
        {
            //Just put the items into the database.
            DeliveryItemsModel::create(array_merge($item, array('delivery' => $deliveryId)));
        }
    }


    /**
     * From the items_specifics table we will be adding specific data
     * For example, we have the description column
     *
     * @param array $deliveryItemSpecificsArray
     * @param array $deliveryItem
     *
     * @return array
     * Takes care of adding items to the delivery items table.
     **/
    private function addToDeliveryItemsFromSpecifics($deliveryItemSpecificsArray, $deliveryItem){
        if(count($deliveryItemSpecificsArray)>0){
            //Assign specific tables to the array
            // Assign all of the specifics to the array
            foreach($deliveryItemSpecificsArray[0] as $deliveryItemSpecificKey => $deliveryItemSpecific){
                if($deliveryItemSpecificKey == "id" || $deliveryItemSpecificKey == "item"){ continue; }
                $deliveryItem[$deliveryItemSpecificKey] = $deliveryItemSpecific;
            }
        }
        return $deliveryItem;
    }


    /**
     * getItemsForDelivery
     *
     * @param int
     *
     * @return \App\DeliveryItems
     * Takes care of adding items to the delivery items table.
     **/
    public function getItemsForDelivery($deliveryId)
    {
        $deliveryItems = \App\Items::where('delivery', '=', $deliveryId)
            ->get()->toArray();
        // Now, we will add the items to this array
        if (!empty($deliveryItems))
        {
            foreach($deliveryItems as &$deliveryItem)
            {
                // Get the item values for this item.
                $deliveryItemValues = \App\ItemValues::where('item', '=', $deliveryItem['id'])
                    ->get()->toArray();
                // Add the values to this items.
                foreach($deliveryItemValues as $deliveryItemValue)
                {
                    $deliveryItem[$deliveryItemValue['name']] = $deliveryItemValue['value'];
                }

                // Add any specific delivery items to the data
                $deliveryItemSpecificsArray = \App\ItemSpecifics::where(
                    'item', '=', $deliveryItem['id'])
                    ->get()->toArray();

                $deliveryItem = $this->addToDeliveryItemsFromSpecifics(
                    $deliveryItemSpecificsArray, $deliveryItem);

            }
        }
        return $deliveryItems;
    }
}