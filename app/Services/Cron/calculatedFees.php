<?php namespace App\Services\Cron;

class calculatedFees
{
    public function commenceCleanUp()
    {
        $this->deleteOldCalculatedFees();
    }

    private function deleteOldCalculatedFees()
    {
        $dt = new \DateTime(date('Y-m-d'));
        // @Todo - Maybe we should only look at deleting two days prior
        $timeAtMidnight = date('Y-m-d H:i:s', $dt->getTimestamp());
        \App\calculatedFees::where('created_at', '<', $timeAtMidnight)
            ->delete();
    }

    /**
     * Checks that the calculated fee and hash matches.
     *
     * @param Array $postData
     *
     * @return boolean | integer(id of the calculatedFees row)
     **/
    public function isCalculatedFeeValid($postData)
    {
        $theCalculatedFee = \App\calculatedFees::where('saved_hash', '=', $postData['saved_hash'])
            ->get();
        if ($theCalculatedFee->count())
        {
            $calculatedFeeData = $theCalculatedFee->toArray();
            if (
                $calculatedFeeData[0]['saved_hash'] === $postData['saved_hash'] && $calculatedFeeData[0]['your_fee'] == $postData['your_fee'] && $calculatedFeeData[0]['our_fee'] == $postData['our_fee'] && $calculatedFeeData[0]['quote'] == $postData['our_quote']
            )
            {
                return $calculatedFeeData[0]['id'];
            }
        }
        return false;
    }

    public function deleteCalculatedFeeById($calculatedFeeId)
    {
        return \App\calculatedFees::where('id', '=', $calculatedFeeId)
            ->delete();
    }
}