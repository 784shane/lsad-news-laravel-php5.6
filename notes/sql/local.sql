-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2016 at 09:38 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shiply`
--

-- --------------------------------------------------------

--
-- Table structure for table `businesses`
--

CREATE TABLE IF NOT EXISTS `businesses` (
  `id` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `company_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_mobile_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_address_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_address_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `validation_token` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `businesses`
--

INSERT INTO `businesses` (`id`, `user`, `company_email`, `company_name`, `company_phone_number`, `company_mobile_number`, `company_address_1`, `company_address_2`, `company_city`, `company_postcode`, `validation_token`, `verified`, `created_at`, `updated_at`) VALUES
(1, 2, 'vibingmax@fadmail.com', 'BarrelsofLaughs', '07944561578', '07944561578', '78 Sayers Place', 'Romney House', 'London', 'sw16 4uf', 'ee1a7ce9c2a26f73494b233127e9f7b6', 1, '2016-03-30 18:46:26', '2016-03-30 18:47:36'),
(2, 3, '1@2.com', 'BarrelsofLaughsX1', '07944561578', '07944561578', '78 Sayers Place', 'Romney House', 'London', 'sw16 4uf', '34322bee43c95a642d62a6ec4680fc3c', 1, '2016-03-30 18:49:39', '2016-03-30 18:49:56'),
(3, 4, '2@2.com', 'BarrelsofLaughsX3', '07944561578', '07944561578', '78 Sayers Place', 'Romney House', 'London', 'sw16 4uf', 'e2f6b4fd3a7ad13c93b156d0d05d11b9', 1, '2016-03-30 18:52:15', '2016-03-30 18:52:38');

-- --------------------------------------------------------

--
-- Table structure for table `deliveries`
--

CREATE TABLE IF NOT EXISTS `deliveries` (
  `id` int(10) unsigned NOT NULL,
  `delivery_nonce` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user` int(10) unsigned NOT NULL,
  `quote_accepted` int(10) unsigned DEFAULT NULL,
  `status` int(3) DEFAULT '1',
  `payment_made` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shipping_category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `collection_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `collection_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `distance_text` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `distance_metres` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subtitle` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `use_as_main_image` int(11) DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiry` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `deliveries`
--

INSERT INTO `deliveries` (`id`, `delivery_nonce`, `user`, `quote_accepted`, `status`, `payment_made`, `title`, `email`, `shipping_category`, `collection_postcode`, `delivery_postcode`, `collection_address`, `delivery_address`, `distance_text`, `distance_metres`, `subtitle`, `use_as_main_image`, `telephone_number`, `expiry`, `created_at`, `updated_at`) VALUES
(1, '7de71d7a2bda56d824161147e15705e7', 1, 2, 4, 1, '', '', 'cars', 'sw2 2ht', 'cr3 5lb', 'Tulse Hill, London, Greater London, United Kingdom, SW2 2HT', 'Caterham, Surrey, United Kingdom, CR3 5LB', '21.0 km', '20997', '09 59 REG NISSAN NOTE N-TEC 1.5 DCI DIESEL CAR', NULL, '07944231910', '2016-04-19 22:24:22', '2016-03-30 18:44:46', '2016-03-30 20:17:37'),
(2, '9deaa06cc00355deab14cf9d6738a661', 1, 5, 4, 1, '', '', 'cars', 'sw2 2ht', 'B4 6NQ', 'Tulse Hill, London, Greater London, United Kingdom, SW2 2HT', 'Birmingham, West Midlands, United Kingdom, B4 6NQ', '210 km', '209686', '2009 RENAULT SCENIC 1.4 DYNAMIQUE DCI DIESEL 86 ', NULL, '', '2016-04-19 22:24:22', '2016-04-01 09:23:11', '2016-04-04 15:20:34'),
(3, 'a61d4216e22a1abb26e95ce7232a910a', 1, 4, 4, 1, '', '', 'cars', 'sw2 2ht', 'B4 6NQ', 'Tulse Hill, London, Greater London, United Kingdom, SW2 2HT', 'Birmingham, West Midlands, United Kingdom, B4 6NQ', '210 km', '209686', 'Hyundai Getz 1.5 CRTD GSi 57 reg', NULL, '', '2016-04-19 22:24:22', '2016-04-02 07:06:09', '2016-04-04 14:10:02'),
(4, 'f6b357d23c221a3f5657d7b90d7dbc03', 1, NULL, 1, 0, '', '', 'moving_home', 'sw2 2ht', 'cr3 5lb', 'Tulse Hill, London, Greater London, United Kingdom, SW2 2HT', 'Caterham, Surrey, United Kingdom, CR3 5LB', '21.5 km', '21524', 'Shetland 570 cruiser with mercury 9.9hp four stroke outboard', 1, '', '2016-04-19 22:20:22', '2016-04-13 17:47:54', '2016-04-17 20:50:31'),
(5, '2ab8826d3f6e167efbf150c03ae30efe', 1, 6, 1, 0, '', '', '', 'sw3 6lz', 'sw2 2ht', 'Chelsea, London, Greater London, United Kingdom, SW3 6LZ', 'Tulse Hill, London, Greater London, United Kingdom, SW2 2HT', '7.7 km', '7651', 'name a', NULL, '', '2016-04-19 22:20:22', '2016-04-13 21:32:00', '2016-04-17 20:50:31'),
(6, 'f511f1822f6b66995f5edb3bc2d3ed71', 1, NULL, 1, 0, '', '', 'boats', 'sw3 6lz', 'sw2 2ht', 'Chelsea, London, Greater London, United Kingdom, SW3 6LZ', 'Tulse Hill, London, Greater London, United Kingdom, SW2 2HT', '7.7 km', '7651', 'shetland 570 with 1.8 desiel inboard engine with trailer', NULL, '', '2016-04-19 22:20:22', '2016-04-14 21:02:48', '2016-04-17 20:50:31'),
(7, '7e9e117c82ecd57566cef9c65c100b93', 1, NULL, 1, 0, '', '', 'boats', 'sw3 6lz', 'sw2 2ht', 'Chelsea, London, Greater London, United Kingdom, SW3 6LZ', 'Tulse Hill, London, Greater London, United Kingdom, SW2 2HT', '7.7 km', '7651', 'shetland 570 with 1.8 desiel inboard engine with trailer', NULL, '', '2016-04-19 22:20:22', '2016-04-14 21:06:04', '2016-04-17 20:50:31'),
(8, '6b293b6cd6ab778bee5671dffa2dd525', 1, NULL, 1, 0, '', '', 'boats', 'sw2 2ht', 'cr3 5lb', 'Tulse Hill, London, Greater London, United Kingdom, SW2 2HT', 'Caterham, Surrey, United Kingdom, CR3 5LB', '21.8 km', '21844', 'Fairline Corniche 31ft 1991 -  PRICED TO SELL!', NULL, '07944231910', '2016-04-19 22:20:22', '2016-04-17 10:14:39', '2016-04-17 20:50:31'),
(10, 'c420e90e6b96bd57c71bcd56109416d1', 1, NULL, 1, 0, '', '', 'boats', 'sw2 2ht', 'cr3 5lb', 'Tulse Hill, London, Greater London, United Kingdom, SW2 2HT', 'Caterham, Surrey, United Kingdom, CR3 5LB', '21.8 km', '21844', 'Colvic Sunquest 29 sailing boat', NULL, '', '2016-04-19 22:20:22', '2016-04-17 10:24:22', '2016-04-17 20:50:31');

-- --------------------------------------------------------

--
-- Table structure for table `deliveries_messages`
--

CREATE TABLE IF NOT EXISTS `deliveries_messages` (
  `id` int(10) unsigned NOT NULL,
  `delivery` int(10) unsigned NOT NULL,
  `sender` int(10) unsigned NOT NULL,
  `receiver` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `unread` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `deliveries_messages`
--

INSERT INTO `deliveries_messages` (`id`, `delivery`, `sender`, `receiver`, `message`, `unread`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 1, 'Ill do this for 900', 0, '2016-03-30 18:49:04', '2016-04-05 09:26:55'),
(2, 1, 3, 1, 'Ill try to do this for 850', 0, '2016-03-30 18:51:27', '2016-04-01 17:53:42'),
(3, 1, 4, 1, 'ill do this for even less than the other guys', 0, '2016-03-30 18:58:45', '2016-03-30 18:59:43'),
(4, 1, 1, 4, 'can you take 200 off?', 0, '2016-03-30 19:06:34', '2016-04-05 08:01:33'),
(5, 1, 1, 4, 'can you do this for even less?', 1, '2016-03-30 19:06:56', '2016-03-30 19:06:56'),
(6, 1, 1, 4, 'tell me again', 1, '2016-03-30 19:10:00', '2016-03-30 19:10:00'),
(7, 3, 3, 1, 'always in. Let me know if you accept', 1, '2016-04-02 07:09:40', '2016-04-02 07:09:40'),
(8, 2, 3, 1, 'is that enough?', 1, '2016-04-04 14:51:23', '2016-04-04 14:51:23'),
(9, 5, 2, 1, 'can i give you a call?', 1, '2016-04-13 22:13:11', '2016-04-13 22:13:11'),
(10, 1, 1, 4, 'when can we get this done?', 1, '2016-04-17 21:36:34', '2016-04-17 21:36:34'),
(11, 10, 2, 1, 'first bid', 1, '2016-04-24 17:15:05', '2016-04-24 17:15:05'),
(12, 10, 2, 1, 'addssdsd', 1, '2016-04-24 17:17:19', '2016-04-24 17:17:19'),
(13, 10, 2, 1, 'asasas', 1, '2016-04-24 17:27:43', '2016-04-24 17:27:43'),
(14, 10, 2, 1, 'ssssx', 1, '2016-04-24 18:32:50', '2016-04-24 18:32:50'),
(15, 10, 2, 1, 'tell ya', 1, '2016-04-24 18:37:12', '2016-04-24 18:37:12');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_items`
--

CREATE TABLE IF NOT EXISTS `delivery_items` (
  `id` int(10) unsigned NOT NULL,
  `delivery` int(10) unsigned NOT NULL,
  `name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `ebay_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `main_image` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `delivery_items`
--

INSERT INTO `delivery_items` (`id`, `delivery`, `name`, `ebay_id`, `main_image`, `created_at`, `updated_at`) VALUES
(1, 1, '09 59 REG NISSAN NOTE N-TEC 1.5 DCI DIESEL CAR', '262361072561', 'http://thumbs2.ebaystatic.com/m/m_YZFKyS1txYXm2GYxB-u2w/140.jpg', '2016-03-30 18:44:46', '2016-03-30 18:44:46'),
(2, 2, '2009 RENAULT SCENIC 1.4 DYNAMIQUE DCI DIESEL 86 ', '152018814725', 'http://thumbs2.ebaystatic.com/m/mypA84SOr0cjdmAe-qENt4A/140.jpg', '2016-04-01 09:23:12', '2016-04-01 09:23:12'),
(3, 3, 'Hyundai Getz 1.5 CRTD GSi 57 reg', '172152987899', 'http://thumbs4.ebaystatic.com/m/mBXCtSSVFadQYYPTR3l20fQ/140.jpg', '2016-04-02 07:06:09', '2016-04-02 07:06:09'),
(4, 4, 'Shetland 570 cruiser with mercury 9.9hp four stroke outboard', '281999690603', 'http://thumbs4.ebaystatic.com/m/mIwq07LHX29j1PLlvlH8dKw/140.jpg', '2016-04-13 17:47:55', '2016-04-13 17:47:55'),
(5, 4, 'test name', '', 'http://purplemover/photos/perm/1460573146_shrvpjw9nt3bjayj1alf3osn2wxc8dbuksemnffz_desert.jpg', '2016-04-13 17:47:55', '2016-04-13 17:47:55'),
(6, 5, 'name a', '', '', '2016-04-13 21:32:01', '2016-04-13 21:32:01'),
(7, 5, 'name b', '', '', '2016-04-13 21:32:01', '2016-04-13 21:32:01'),
(8, 6, 'shetland 570 with 1.8 desiel inboard engine with trailer', '282001103817', 'http://thumbs2.ebaystatic.com/m/mX594iDaDwSBq9CZ32tsolQ/140.jpg', '2016-04-14 21:02:48', '2016-04-14 21:02:48'),
(9, 7, 'shetland 570 with 1.8 desiel inboard engine with trailer', '282001103817', 'http://thumbs2.ebaystatic.com/m/mX594iDaDwSBq9CZ32tsolQ/140.jpg', '2016-04-14 21:06:05', '2016-04-14 21:06:05'),
(10, 8, 'Fairline Corniche 31ft 1991 -  PRICED TO SELL!', '172140462337', 'http://thumbs2.ebaystatic.com/m/mQ9a3GCbVB6rffDP8lrjbxQ/140.jpg', '2016-04-17 10:14:39', '2016-04-17 10:14:39'),
(12, 10, 'Colvic Sunquest 29 sailing boat', '322046129175', 'http://thumbs4.ebaystatic.com/m/mEJz5ot4Ol_GZtXN-3PznwA/140.jpg', '2016-04-17 10:24:22', '2016-04-17 10:24:22');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_types`
--

CREATE TABLE IF NOT EXISTS `delivery_types` (
  `id` int(10) unsigned NOT NULL,
  `status_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_value` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `delivery_types`
--

INSERT INTO `delivery_types` (`id`, `status_name`, `status_value`, `created_at`, `updated_at`) VALUES
(1, 'archived', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'expired', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'active_private', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'active_public', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE IF NOT EXISTS `enquiries` (
  `id` int(10) unsigned NOT NULL,
  `enquiry_topic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enquiry_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enquiry_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enquiry_message` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(10) unsigned NOT NULL,
  `delivery` int(10) unsigned NOT NULL,
  `item_order` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `delivery`, `item_order`) VALUES
(1, 1, 0),
(2, 2, 0),
(3, 3, 0),
(6, 5, 0),
(7, 5, 1),
(10, 4, 0),
(11, 4, 1),
(12, 6, 0),
(13, 7, 0),
(14, 8, 0),
(16, 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `item_specifics`
--

CREATE TABLE IF NOT EXISTS `item_specifics` (
  `id` int(10) unsigned NOT NULL,
  `item` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `main_image` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_specifics`
--

INSERT INTO `item_specifics` (`id`, `item`, `description`, `main_image`) VALUES
(1, 1, '59 REG NISSAN NOTE N-TEC 1.5 DCI DIESEL CAR,\r\n\r\nONE LADY OWNER FROM NEW,\r\n\r\nEXCELLENT CONDITION INSIDE AND OUT,\r\n\r\nFULL MAIN AGENT SERVICE HISTORY SERVICED AT 10K-21K-30K-39K-52K,\r\n\r\nONLY 60,000MLS WARRENTED WITH M.O.T TILL NOVEMBER 2016 WITH NO ADVISORYS,\r\n\r\nEXCELLENT SPEC,\r\n\r\nSAT NAV,\r\n\r\nELECTRIC WINDOWS/MIRRORS,\r\n\r\nPRIVACEY GLASS,\r\n\r\nREAR PARKING SENSSORS,\r\n\r\nAIR CON,\r\n\r\nDRIVING FOGS,\r\n\r\nALLOYS,\r\n\r\nFULL CENTRAL LOCKING,\r\n\r\nALARM,\r\n\r\n2 X KEYS,\r\n\r\nCAR RUNS AND DRIVES PERFECT,\r\n\r\n5 DAY AUCTION,\r\n\r\nAUCTION ENDS MONDAY NIGHT 8.30PM,\r\n\r\nNO BUY IT NOW ALL BIDS ON EBAY,\r\n\r\nCAR CAN BE VIEWED AND DRIVEN ANY INSPECTION WELCOME,\r\n\r\nWINNING BIDDER TO  MAKE CONTACT WITHIN 1HR OF AUCTION ENDING TO ARRANGE PAYMENT AND COLLECTION,\r\n\r\nNO PAY PAL ,\r\n\r\nWE ARE BASED NR KENDAL CUMBRIA M6 JUNCTION 36,\r\n\r\nNATIONWIDE DELIVERY AVAILABLE RING FOR QUOAT,\r\n\r\nFOR ANY FURTHER INFORMATION CALL OR TEXT 07751096595,\r\n\r\nGOOD LUCK AND HAPPY BIDDING.\r\n\r\n07751096595', 'http://thumbs2.ebaystatic.com/m/m_YZFKyS1txYXm2GYxB-u2w/140.jpg'),
(2, 2, '\r\n88,000 MILES FROM NEW \r\n\r\nMILEAGE IS GUARANTEED AND WARRANTIED\r\n\r\nFULL MAIN DEALER AND INDEPENDENT SPECIALIST SERVICE HISTORY\r\n\r\nLOTS OF MOT UNTIL APRIL 2017\r\n\r\n\r\nFULLY VALETTED FOR YOU UPON COLLECTION\r\n\r\nHPI CLEAR.\r\n\r\n\r\n--------------------------------------\r\n \r\nCOLLECTION FROM LEAMINGTON SPA, PLEASE LEAVE A PHONE NUMBER - \r\n\r\nIn case of auctions we require a next day token deposit to secure the car to you.\r\n\r\nWe sell quality used cars, we do not sell 10 year old brand new cars with brand new tyres all round etc,etc. Anything of note, aside of the usual age related marks and scars will be mentioned in the advert.\r\n\r\n07415 663045 for any enquires\r\n\r\nDELIVERY AVAILABLE AT £1.75 A MILE - COLLECTION FROM TRAIN STATION A PLEASURE', 'http://thumbs2.ebaystatic.com/m/mypA84SOr0cjdmAe-qENt4A/140.jpg'),
(3, 3, 'Hi thanks for looking at my auction.\r\n\r\nThe Getz is a cheap and reliable car with £30 per year tax and official figures show the 1.5 diesel engine returns 62mpg on the combined cycle.\r\n\r\nI bought this car a few months ago as a cheap run around and it has been ideal for what I needed it for, however a change of job means I now need something bigger.\r\n\r\nThe car is in excellent condition inside and out with just the usual minor age related marks.\r\n\r\nI have priced the car to sell as I have a new car arriving in a few days.\r\n\r\nThanks for looking. ', 'http://thumbs4.ebaystatic.com/m/mBXCtSSVFadQYYPTR3l20fQ/140.jpg'),
(6, 6, 'desc a', 'http://purplemover/photos/perm/1460586747_wzgcodhcobjxh4apudsm27jxrgyqpktf6gtjjka6_penguins.jpg'),
(7, 7, 'desc b', 'http://purplemover/photos/perm/1460586769_wzgcodhcobjxh4apudsm27jxrgyqpktf6gtjjka6_tulips.jpg'),
(10, 10, 'item 3', 'http://purplemover/photos/perm/1460658295_hfgskakglvytqx0vr2reiqptbjotjwrjlsueagss_tulips.jpg'),
(11, 11, 'item 4 description', 'http://purplemover/photos/perm/1460659745_ysdfk5fgbip7vr2b2vmyheeajwn9kfi2bfiohivw_penguins.jpg'),
(12, 12, 'I''m selling my Shetland 570 1.8 diesel due to illness so it looks like my boating days are over. Its 18ft long can be used on the river or sea . It was used at Hartlepool as it was used for sea fishing. \r\nIt has a 1.8 diesel ford connect engine runs good no faults.it has a bowan heat exchanger with header tank which uses the water from river or sea to cool engine. as you can see from the pics it also has a dog leg style drive.\r\nThe cabin is 2 birth .all steering and controls fitted and working fine. It also comes with canopy and trailer. Trailer has a few surface rust spots just needs tiding up to make it last .but still a good strong trailer.\r\nThe floor has been all decked out and nice timber for engine and battery covers.\r\nThis boat will make a good fishing boat or just for roaming the rivers and cannels cheap to run as of the diesel.\r\nThe boat is at boroughbridge marina and the name is sea hawk. This can be changed to whatever name you desire at any time.\r\nI''m going to start the boat at 3000 so its a barging . The light on the front and the gps  and Arial have been removed. viewing available appointment only so I can meet you there. cash on collection only please.\r\nany questions just mail me and I try to answer as best I can.\r\nthanks for your interest.\r\nthe trailer is a hydraulic braked trailer and has a hand brake. plus it also has a lighting board so all you need to do is hitch it up and your away.  easy towing rides well on the road.', 'http://thumbs2.ebaystatic.com/m/mX594iDaDwSBq9CZ32tsolQ/140.jpg'),
(13, 13, 'I''m selling my Shetland 570 1.8 diesel due to illness so it looks like my boating days are over. Its 18ft long can be used on the river or sea . It was used at Hartlepool as it was used for sea fishing. \r\nIt has a 1.8 diesel ford connect engine runs good no faults.it has a bowan heat exchanger with header tank which uses the water from river or sea to cool engine. as you can see from the pics it also has a dog leg style drive.\r\nThe cabin is 2 birth .all steering and controls fitted and working fine. It also comes with canopy and trailer. Trailer has a few surface rust spots just needs tiding up to make it last .but still a good strong trailer.\r\nThe floor has been all decked out and nice timber for engine and battery covers.\r\nThis boat will make a good fishing boat or just for roaming the rivers and cannels cheap to run as of the diesel.\r\nThe boat is at boroughbridge marina and the name is sea hawk. This can be changed to whatever name you desire at any time.\r\nI''m going to start the boat at 3000 so its a barging . The light on the front and the gps  and Arial have been removed. viewing available appointment only so I can meet you there. cash on collection only please.\r\nany questions just mail me and I try to answer as best I can.\r\nthanks for your interest.\r\nthe trailer is a hydraulic braked trailer and has a hand brake. plus it also has a lighting board so all you need to do is hitch it up and your away.  easy towing rides well on the road.', 'http://thumbs2.ebaystatic.com/m/mX594iDaDwSBq9CZ32tsolQ/140.jpg'),
(14, 14, 'The beautiful Fairline Corniche 31 was built in 1991 – one of the last to be built. 2x Volvo TAMB 200 HP inboard diesel engines on shaft drives. Only 1030 hours on the clock from new. Lying on the Medway in Kent.\r\n\r\nBolero III has two separate sleeping cabins to comfortably sleep four people plus can sleep two more in the main cabin (sofabed).  A high quality (mahogony) solid wood fold out table (with chairs hidden in its base) enables the main cabin to be easily converted to a dining area for up to 6, removing the need for a separate dinette while providing plenty of living and entertaining space. \r\n\r\nThe boat has Erberspacher heating and air-conditioning.\r\n\r\nThe boat is in lovely condition having recently been reupholstered, had new navigation equipment and radar fitted (cost £4,500), new oven, new awnings, new fly bridge upholstery and cover, new porthole cover and had the electrics checked and updated.\r\n\r\n6 berths in 3 cabins. Lovely wooden floors in main cabin with tasteful upholstery and floorings throughout.\r\n\r\nThis popular cruising boat is a soundly built and popular vessel which holds its value well and has a great heritage from a brilliant designer.\r\n\r\nComes complete with life jackets, full set of denby crockery, cutlery, pots, pans, new high quality bedding and bedsets - everything you need to just set off and go.', 'http://thumbs2.ebaystatic.com/m/mQ9a3GCbVB6rffDP8lrjbxQ/140.jpg'),
(16, 16, 'A well maintained and cared for 3/5 berth Sedan style cruiser with a self draining open cockpit and transom door leading to the bathing platform. \r\n\r\nAntifouled, hull polished and sterndrive serviced in January 2014. Re-upholstered in late 2015. \r\n\r\nThe engine is a Volvo Penta AQ151C petrol engine (150 bhp). The Volvo 290A outdrive was fully reconditioned by Coastal Rides in November 2014.\r\n\r\nWebasto diesel blown air heating fitted in 2010 along with Jabsco electric toilet/macerator to a holding tank. \r\n\r\nThe galley includes a full size cooker, fridge and filter tap. \r\n\r\nHot/cold pressurized water system heated from immersion heater and engine.\r\n\r\n240 volt with Sterling battery charger. \r\n\r\nBow thruster fitted 2010. \r\n\r\nNavico depth and speed logs. \r\n\r\n12 volt flat screen freeview T.V./DVD. \r\n\r\nRadio/cd player. \r\n\r\nExcellent access to engine which has less than 500 hrs. use. \r\n\r\nSafety Cert. valid until 20/09/2016. \r\n\r\nMooring available subject to Marina''s owners approval.', 'http://thumbs4.ebaystatic.com/m/mEJz5ot4Ol_GZtXN-3PznwA/140.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `item_values`
--

CREATE TABLE IF NOT EXISTS `item_values` (
  `id` int(10) unsigned NOT NULL,
  `item` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_values`
--

INSERT INTO `item_values` (`id`, `item`, `name`, `value`) VALUES
(1, 1, 'ebay_id', '262361072561'),
(2, 1, 'name', '09 59 REG NISSAN NOTE N-TEC 1.5 DCI DIESEL CAR'),
(3, 1, 'use_ebay', 'use_ebay'),
(4, 1, 'item_length', '234'),
(5, 1, 'item_width', '23443'),
(6, 1, 'item_height', '43435'),
(7, 1, 'item_weight', '545445'),
(8, 1, 'measured_width', 'miles'),
(9, 1, 'measured_height', 'miles'),
(10, 1, 'measured_length', 'miles'),
(11, 1, 'measured_weight', 'pounds'),
(12, 2, 'ebay_id', '152018814725'),
(13, 2, 'name', '2009 RENAULT SCENIC 1.4 DYNAMIQUE DCI DIESEL 86 '),
(14, 2, 'use_ebay', 'use_ebay'),
(15, 2, 'item_length', '8'),
(16, 2, 'item_width', '234'),
(17, 2, 'item_height', '3456'),
(18, 2, 'item_weight', '2344'),
(19, 2, 'measured_width', 'inches'),
(20, 2, 'measured_height', 'feet'),
(21, 2, 'measured_length', 'feet'),
(22, 2, 'measured_weight', 'pounds'),
(23, 3, 'ebay_id', '172152987899'),
(24, 3, 'name', 'Hyundai Getz 1.5 CRTD GSi 57 reg'),
(25, 3, 'use_ebay', 'use_ebay'),
(26, 3, 'item_length', '8'),
(27, 3, 'item_width', '400'),
(28, 3, 'item_height', '190'),
(29, 3, 'item_weight', '6541'),
(30, 3, 'measured_width', 'miles'),
(31, 3, 'measured_height', 'inches'),
(32, 3, 'measured_length', 'miles'),
(33, 3, 'measured_weight', 'pounds'),
(51, 6, 'ebay_id', ''),
(52, 6, 'name', 'name a'),
(53, 6, 'item_length', '1'),
(54, 6, 'item_width', '2'),
(55, 6, 'item_height', '3'),
(56, 6, 'item_weight', '4'),
(57, 6, 'measured_width', 'miles'),
(58, 6, 'measured_height', 'inches'),
(59, 6, 'measured_length', 'feet'),
(60, 6, 'measured_weight', 'tonnes'),
(61, 7, 'ebay_id', ''),
(62, 7, 'name', 'name b'),
(63, 7, 'item_length', '9'),
(64, 7, 'item_width', '8'),
(65, 7, 'item_height', '7'),
(66, 7, 'item_weight', '5'),
(67, 7, 'measured_width', 'feet'),
(68, 7, 'measured_height', 'inches'),
(69, 7, 'measured_length', 'feet'),
(70, 7, 'measured_weight', 'tonnes'),
(93, 10, 'name', 'Shetland 570 cruiser with mercury 9.9hp four stroke outboard'),
(94, 10, 'item_length', '1'),
(95, 10, 'item_width', '2'),
(96, 10, 'item_height', '3'),
(97, 10, 'item_weight', '4'),
(98, 10, 'measured_width', 'miles'),
(99, 10, 'measured_height', 'yards'),
(100, 10, 'measured_length', 'miles'),
(101, 10, 'measured_weight', 'stones'),
(102, 11, 'name', 'item 4'),
(103, 11, 'item_length', '5'),
(104, 11, 'item_width', '6'),
(105, 11, 'item_height', '7'),
(106, 11, 'item_weight', '8'),
(107, 11, 'measured_width', 'inches'),
(108, 11, 'measured_height', 'yards'),
(109, 11, 'measured_length', 'miles'),
(110, 11, 'measured_weight', 'pounds'),
(111, 12, 'ebay_id', '282001103817'),
(112, 12, 'name', 'shetland 570 with 1.8 desiel inboard engine with trailer'),
(113, 12, 'use_ebay', 'use_ebay'),
(114, 12, 'item_length', '12'),
(115, 12, 'item_width', '32'),
(116, 12, 'item_height', '12'),
(117, 12, 'item_weight', '22'),
(118, 12, 'measured_width', 'feet'),
(119, 12, 'measured_height', 'miles'),
(120, 12, 'measured_length', 'miles'),
(121, 12, 'measured_weight', 'tonnes'),
(122, 13, 'ebay_id', '282001103817'),
(123, 13, 'name', 'shetland 570 with 1.8 desiel inboard engine with trailer'),
(124, 13, 'use_ebay', 'use_ebay'),
(125, 13, 'item_length', '12'),
(126, 13, 'item_width', '32'),
(127, 13, 'item_height', '12'),
(128, 13, 'item_weight', '22'),
(129, 13, 'measured_width', 'feet'),
(130, 13, 'measured_height', 'miles'),
(131, 13, 'measured_length', 'miles'),
(132, 13, 'measured_weight', 'tonnes'),
(133, 14, 'ebay_id', '172140462337'),
(134, 14, 'name', 'Fairline Corniche 31ft 1991 -  PRICED TO SELL!'),
(135, 14, 'use_ebay', 'use_ebay'),
(136, 14, 'item_length', '12'),
(137, 14, 'item_width', '23'),
(138, 14, 'item_height', '34'),
(139, 14, 'item_weight', '456'),
(140, 14, 'measured_width', 'miles'),
(141, 14, 'measured_height', 'miles'),
(142, 14, 'measured_length', 'miles'),
(143, 14, 'measured_weight', 'stones'),
(154, 16, 'ebay_id', '322046129175'),
(155, 16, 'name', 'Colvic Sunquest 29 sailing boat'),
(156, 16, 'use_ebay', 'use_ebay'),
(157, 16, 'item_length', '21'),
(158, 16, 'item_width', '23'),
(159, 16, 'item_height', '32'),
(160, 16, 'item_weight', '34'),
(161, 16, 'measured_width', 'feet'),
(162, 16, 'measured_height', 'feet'),
(163, 16, 'measured_length', 'feet'),
(164, 16, 'measured_weight', 'pounds');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_08_30_221543_create_users_table', 1),
('2015_08_31_101422_create_deliveries-table', 1),
('2015_08_31_101423_create_items_table', 1),
('2015_08_31_101424_create_deliveriesMessages-table', 1),
('2015_08_31_101424_create_item_specifics_table', 1),
('2015_08_31_101424_create_item_values_table', 1),
('2015_08_31_101426_create_quotes-table', 1),
('2015_08_31_101929_create_deliveryItems_table', 1),
('2015_09_02_200259_create_business_table', 1),
('2015_09_02_200259_create_temporary_uploads_table', 1),
('2015_09_02_200359_create_transactions_table', 1),
('2015_09_02_200459_create_transaction_details_table', 1),
('2016_03_30_173904_create_delivery_types_table', 1),
('2016_04_04_124010_create_session_table', 2),
('2016_04_19_200911_create_enquiries_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE IF NOT EXISTS `quotes` (
  `id` int(10) unsigned NOT NULL,
  `quote` int(10) unsigned NOT NULL,
  `original_quote` int(11) DEFAULT NULL,
  `our_fee` int(11) DEFAULT NULL,
  `sender` int(10) unsigned NOT NULL,
  `receiver` int(10) unsigned NOT NULL,
  `delivery` int(10) unsigned NOT NULL,
  `with_message` tinyint(1) NOT NULL DEFAULT '0',
  `message` int(10) unsigned NOT NULL,
  `new_quote` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `quotes`
--

INSERT INTO `quotes` (`id`, `quote`, `original_quote`, `our_fee`, `sender`, `receiver`, `delivery`, `with_message`, `message`, `new_quote`, `created_at`, `updated_at`) VALUES
(1, 900, NULL, NULL, 2, 1, 1, 1, 1, 1, '2016-03-30 18:49:04', '2016-03-30 18:49:04'),
(2, 850, NULL, NULL, 3, 1, 1, 1, 2, 1, '2016-03-30 18:51:27', '2016-03-30 18:51:27'),
(3, 780, NULL, NULL, 4, 1, 1, 1, 3, 1, '2016-03-30 18:58:45', '2016-03-30 18:58:45'),
(4, 405, NULL, NULL, 3, 1, 3, 1, 7, 1, '2016-04-02 07:09:40', '2016-04-02 07:09:40'),
(5, 160, NULL, NULL, 3, 1, 2, 1, 8, 1, '2016-04-04 14:51:23', '2016-04-04 14:51:23'),
(6, 500, NULL, NULL, 2, 1, 5, 1, 9, 1, '2016-04-13 22:13:11', '2016-04-13 22:13:11'),
(7, 2000, NULL, NULL, 2, 1, 10, 1, 11, 1, '2016-04-24 17:15:06', '2016-04-24 17:15:06'),
(8, 2005, NULL, NULL, 2, 1, 10, 1, 12, 1, '2016-04-24 17:17:19', '2016-04-24 17:17:19'),
(9, 2006, NULL, NULL, 2, 1, 10, 1, 13, 1, '2016-04-24 17:27:43', '2016-04-24 17:27:43'),
(10, 2006, 2009, 100, 2, 1, 10, 1, 15, 1, '2016-04-24 18:37:12', '2016-04-24 18:37:12');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `payload`, `last_activity`) VALUES
('4b775e9300db281728dd9bb10bac6c3896ba01d3', 'YTo3OntzOjY6Il90b2tlbiI7czo0MDoiaG9iWG15Qmd2WWxNMjZOMVdwUmVXczUxMUdRREROMGZnVlhaamJ0VSI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czo4ODoiaHR0cDovL3B1cnBsZW1vdmVyL3F1b3Rlcy9wYXlwYWwtcmVzdWx0P1BheWVySUQ9SDM3SkVHOUpKOUxONCZ0b2tlbj1FQy02NFQ1ODE0NDRBNTg2MDEzSCI7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjY3OiJodHRwOi8vcHVycGxlbW92ZXIvcXVvdGVzL2FjY2VwdC9hNjFkNDIxNmUyMmExYWJiMjZlOTVjZTcyMzJhOTEwYS80Ijt9czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjM4OiJsb2dpbl84MmU1ZDJjNTZiZGQwODExMzE4ZjBjZjA3OGI3OGJmYyI7aToxO3M6MjU6ImV4cHJlc3NfY2hlY2tvdXRfcXVvdGVfaWQiO3M6MToiNCI7czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0NTk3NzM3MjA7czoxOiJjIjtpOjE0NTk3NzM2OTU7czoxOiJsIjtzOjE6IjAiO319', 1459773721);

-- --------------------------------------------------------

--
-- Table structure for table `temporary_uploads`
--

CREATE TABLE IF NOT EXISTS `temporary_uploads` (
  `id` int(10) unsigned NOT NULL,
  `form_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `form_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_order_no` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `temporary_uploads`
--

INSERT INTO `temporary_uploads` (`id`, `form_token`, `form_time`, `file_name`, `item_order_no`, `created_at`, `updated_at`) VALUES
(1, '7kmZY4LqU4qWUjCyuohyKBdzgwIk3KsPIu7IH62J', '2016-04-09 11:57:59', '1460203079_7kmZY4LqU4qWUjCyuohyKBdzgwIk3KsPIu7IH62J_mydeliveries.png', 0, '2016-04-09 10:59:32', '2016-04-09 11:00:24'),
(2, '7kmZY4LqU4qWUjCyuohyKBdzgwIk3KsPIu7IH62J', '2016-04-09 12:00:55', '1460203255_7kmZY4LqU4qWUjCyuohyKBdzgwIk3KsPIu7IH62J_mydeliveries.png', 0, '2016-04-09 11:01:12', '2016-04-09 11:01:12'),
(3, '7kmZY4LqU4qWUjCyuohyKBdzgwIk3KsPIu7IH62J', '2016-04-09 12:00:55', '1460203255_7kmZY4LqU4qWUjCyuohyKBdzgwIk3KsPIu7IH62J_mydeliveries.png', 1, '2016-04-09 11:01:12', '2016-04-09 11:01:12'),
(4, '7kmZY4LqU4qWUjCyuohyKBdzgwIk3KsPIu7IH62J', '2016-04-09 12:00:55', '1460203255_7kmZY4LqU4qWUjCyuohyKBdzgwIk3KsPIu7IH62J_mydeliveries.png', 2, '2016-04-09 11:01:12', '2016-04-09 11:01:12'),
(5, '7kmZY4LqU4qWUjCyuohyKBdzgwIk3KsPIu7IH62J', '2016-04-09 12:02:17', '1460203337_7kmZY4LqU4qWUjCyuohyKBdzgwIk3KsPIu7IH62J_mydeliveries.png', 0, '2016-04-09 11:02:39', '2016-04-09 11:02:49'),
(6, '7kmZY4LqU4qWUjCyuohyKBdzgwIk3KsPIu7IH62J', '2016-04-09 12:02:17', '1460203337_7kmZY4LqU4qWUjCyuohyKBdzgwIk3KsPIu7IH62J_mydeliveries.png', 1, '2016-04-09 11:02:39', '2016-04-09 11:02:39'),
(7, '7kmZY4LqU4qWUjCyuohyKBdzgwIk3KsPIu7IH62J', '2016-04-09 12:02:17', '1460203337_7kmZY4LqU4qWUjCyuohyKBdzgwIk3KsPIu7IH62J_Desert.jpg', 2, '2016-04-09 11:02:39', '2016-04-09 11:02:39'),
(8, 'EbOQ3fzGJcpRRTNcqG5yeWu5uUk1dRxd2i2jKTdN', '2016-04-09 12:29:07', '1460204947_EbOQ3fzGJcpRRTNcqG5yeWu5uUk1dRxd2i2jKTdN_Hydrangeas.jpg', 0, '2016-04-09 11:29:18', '2016-04-09 11:29:18'),
(9, 'EbOQ3fzGJcpRRTNcqG5yeWu5uUk1dRxd2i2jKTdN', '2016-04-09 12:29:07', '1460204947_EbOQ3fzGJcpRRTNcqG5yeWu5uUk1dRxd2i2jKTdN_Tulips.jpg', 1, '2016-04-09 11:29:18', '2016-04-09 11:29:18'),
(10, 'EbOQ3fzGJcpRRTNcqG5yeWu5uUk1dRxd2i2jKTdN', '2016-04-09 12:33:37', '1460205217_EbOQ3fzGJcpRRTNcqG5yeWu5uUk1dRxd2i2jKTdN_Chrysanthemum.jpg', 0, '2016-04-09 11:34:29', '2016-04-09 11:34:29'),
(11, 'EbOQ3fzGJcpRRTNcqG5yeWu5uUk1dRxd2i2jKTdN', '2016-04-09 12:33:37', '1460205217_EbOQ3fzGJcpRRTNcqG5yeWu5uUk1dRxd2i2jKTdN_Tulips.jpg', 1, '2016-04-09 11:34:29', '2016-04-09 11:34:29'),
(12, 'EbOQ3fzGJcpRRTNcqG5yeWu5uUk1dRxd2i2jKTdN', '2016-04-09 13:11:06', '1460207466_EbOQ3fzGJcpRRTNcqG5yeWu5uUk1dRxd2i2jKTdN_Tulips.jpg', 0, '2016-04-09 12:11:14', '2016-04-09 12:11:14'),
(13, 'EbOQ3fzGJcpRRTNcqG5yeWu5uUk1dRxd2i2jKTdN', '2016-04-09 13:11:06', '1460207466_EbOQ3fzGJcpRRTNcqG5yeWu5uUk1dRxd2i2jKTdN_Jellyfish.jpg', 1, '2016-04-09 12:11:14', '2016-04-09 12:11:14'),
(17, 'EbOQ3fzGJcpRRTNcqG5yeWu5uUk1dRxd2i2jKTdN', '2016-04-09 16:08:32', '1460218112_EbOQ3fzGJcpRRTNcqG5yeWu5uUk1dRxd2i2jKTdN_Penguins.jpg', 0, '2016-04-09 15:10:14', '2016-04-09 15:10:14');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(10) unsigned NOT NULL,
  `delivery` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_details`
--

CREATE TABLE IF NOT EXISTS `transaction_details` (
  `transaction` int(10) unsigned NOT NULL,
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `register_token` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `forgot_token` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `transport_company` tinyint(1) NOT NULL DEFAULT '0',
  `transport_company_verified` tinyint(1) NOT NULL DEFAULT '0',
  `forgotten_pass_token_sent` tinyint(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `user_name`, `telephone_number`, `password`, `register_token`, `forgot_token`, `transport_company`, `transport_company_verified`, `forgotten_pass_token_sent`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'vibes@fadmail.com', '784Shane', '07944231910', '$2y$10$poZPFdxXzDGwqHq.soiCY.Q8XQNYMftdgoR5AwbgRCzhLGaeAk49e', 'b1393066e0e59ed6c541001300c05d84', 'f22c645500291c8a1eb7b4611d5b78d0', 0, 0, 0, 'TxX3K1X4lwNDITlwFRBfyEpckCAV23mQAIxe4dg6ZRnXrRvMPv5EjKh73e7l', '2016-03-30 18:44:45', '2016-04-22 16:48:24'),
(2, NULL, NULL, 'vibingmax@fadmail.com', 'BarrelsofLaughs', NULL, '$2y$10$wtpG/AE39MvXn3kIpjAuTewfXkTT/UacD6sFX5gTXiioKO5NluvHK', '7dc0c46cf60b23be3ef145bf8ae72ea3', 'df318a5c3b7095d4749a64cea2bfca80', 1, 1, 0, 'rRpN0w13jvFMrTEBQ466wEW09zq1oEHPaXuaYOKtr4eGt8L6yDzvBT6Eda3M', '2016-03-30 18:46:26', '2016-04-24 16:53:19'),
(3, NULL, NULL, '1@2.com', 'BarrelsofLaughs1', NULL, '$2y$10$vo10MATLm/KuRyi/wFXZcOVyOqVGGDs4a7Y1P0YFAzEMt93x.4sEW', '24d8c8357a23b56f8d4cbdfb05a94bf5', '6ca16b28fec867cb8f4b4b862c70aa9d', 1, 1, 0, '3we51AFb05xFWJxnb1oXDyw6cxOX83ZESurizPqLwb6QAsaN1RO5lodbzMWH', '2016-03-30 18:49:39', '2016-04-05 07:58:18'),
(4, NULL, NULL, '2@2.com', 'BarrelsofLaughs2', NULL, '$2y$10$XgxJHuEJ5mN4oKQhr9xDWOYw/QMYkgtaLWYgnUSfYNDbVQGnUtxCi', 'fefff043a341a8f7c6b3036b3c1df661', '7fef649c870dc9fabba06dc36f4ae896', 1, 1, 0, 'lr9lI3ZX3hAL1R0IFGGyAMxf4Rv4ANsfFCaSuig5ruxJpkOQTrmIzKG3Ua8j', '2016-03-30 18:52:15', '2016-03-30 18:58:47'),
(5, NULL, NULL, '1@2b.com', 'hulk712', '07944231910', '$2y$10$9fVD4/8JE/1MFG4sVWuN6O37.E.8T80.IBc9KVQwCIeJgXfDsX2Ku', 'abd36afc1fbbfdd67c6b2c11d6a2dee1', 'eb79a80bdbbd4a854048c1df062aa1c9', 0, 0, 0, 'VqLPEeT0VcJKVCTBE9yTF5fC7gCRamQbqbrghtaM2nDnUN4Vew98PPMx3xT7', '2016-04-05 16:28:10', '2016-04-05 17:28:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `businesses`
--
ALTER TABLE `businesses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `businesses_validation_token_unique` (`validation_token`),
  ADD KEY `businesses_user_foreign` (`user`);

--
-- Indexes for table `deliveries`
--
ALTER TABLE `deliveries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `deliveries_delivery_nonce_unique` (`delivery_nonce`),
  ADD KEY `deliveries_user_foreign` (`user`);

--
-- Indexes for table `deliveries_messages`
--
ALTER TABLE `deliveries_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deliveries_messages_delivery_foreign` (`delivery`),
  ADD KEY `deliveries_messages_sender_foreign` (`sender`),
  ADD KEY `deliveries_messages_receiver_foreign` (`receiver`);

--
-- Indexes for table `delivery_items`
--
ALTER TABLE `delivery_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_items_delivery_foreign` (`delivery`);

--
-- Indexes for table `delivery_types`
--
ALTER TABLE `delivery_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `items_delivery_foreign` (`delivery`);

--
-- Indexes for table `item_specifics`
--
ALTER TABLE `item_specifics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_specifics_item_foreign` (`item`);

--
-- Indexes for table `item_values`
--
ALTER TABLE `item_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_values_item_foreign` (`item`);

--
-- Indexes for table `quotes`
--
ALTER TABLE `quotes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quotes_delivery_foreign` (`delivery`),
  ADD KEY `quotes_message_foreign` (`message`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `temporary_uploads`
--
ALTER TABLE `temporary_uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_delivery_foreign` (`delivery`);

--
-- Indexes for table `transaction_details`
--
ALTER TABLE `transaction_details`
  ADD KEY `transaction_details_transaction_foreign` (`transaction`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_register_token_unique` (`register_token`),
  ADD UNIQUE KEY `users_forgot_token_unique` (`forgot_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `businesses`
--
ALTER TABLE `businesses`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `deliveries`
--
ALTER TABLE `deliveries`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `deliveries_messages`
--
ALTER TABLE `deliveries_messages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `delivery_items`
--
ALTER TABLE `delivery_items`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `delivery_types`
--
ALTER TABLE `delivery_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `item_specifics`
--
ALTER TABLE `item_specifics`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `item_values`
--
ALTER TABLE `item_values`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=165;
--
-- AUTO_INCREMENT for table `quotes`
--
ALTER TABLE `quotes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `temporary_uploads`
--
ALTER TABLE `temporary_uploads`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `businesses`
--
ALTER TABLE `businesses`
  ADD CONSTRAINT `businesses_user_foreign` FOREIGN KEY (`user`) REFERENCES `users` (`id`);

--
-- Constraints for table `deliveries`
--
ALTER TABLE `deliveries`
  ADD CONSTRAINT `deliveries_user_foreign` FOREIGN KEY (`user`) REFERENCES `users` (`id`);

--
-- Constraints for table `deliveries_messages`
--
ALTER TABLE `deliveries_messages`
  ADD CONSTRAINT `deliveries_messages_delivery_foreign` FOREIGN KEY (`delivery`) REFERENCES `deliveries` (`id`),
  ADD CONSTRAINT `deliveries_messages_receiver_foreign` FOREIGN KEY (`receiver`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `deliveries_messages_sender_foreign` FOREIGN KEY (`sender`) REFERENCES `users` (`id`);

--
-- Constraints for table `delivery_items`
--
ALTER TABLE `delivery_items`
  ADD CONSTRAINT `delivery_items_delivery_foreign` FOREIGN KEY (`delivery`) REFERENCES `deliveries` (`id`);

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_delivery_foreign` FOREIGN KEY (`delivery`) REFERENCES `deliveries` (`id`);

--
-- Constraints for table `item_specifics`
--
ALTER TABLE `item_specifics`
  ADD CONSTRAINT `item_specifics_item_foreign` FOREIGN KEY (`item`) REFERENCES `items` (`id`);

--
-- Constraints for table `item_values`
--
ALTER TABLE `item_values`
  ADD CONSTRAINT `item_values_item_foreign` FOREIGN KEY (`item`) REFERENCES `items` (`id`);

--
-- Constraints for table `quotes`
--
ALTER TABLE `quotes`
  ADD CONSTRAINT `quotes_delivery_foreign` FOREIGN KEY (`delivery`) REFERENCES `deliveries` (`id`),
  ADD CONSTRAINT `quotes_message_foreign` FOREIGN KEY (`message`) REFERENCES `deliveries_messages` (`id`);

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_delivery_foreign` FOREIGN KEY (`delivery`) REFERENCES `deliveries` (`id`);

--
-- Constraints for table `transaction_details`
--
ALTER TABLE `transaction_details`
  ADD CONSTRAINT `transaction_details_transaction_foreign` FOREIGN KEY (`transaction`) REFERENCES `transactions` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
