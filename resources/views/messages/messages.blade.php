@extends($account_page_layout)
@section('content')

    <div>
        <h1>Messages</h1>

        <div>
            <div class="site-secondary-menu pure-menu pure-menu-horizontal">
                <ul class="pure-menu-list">
                    <li class="pure-menu-item">{!! Html::link('messages','My delivery Messages', array("class"=>'pure-menu-link')) !!}</li>
                    <li class="pure-menu-item">{!! Html::link('messages?tab=other-deliveries-messages','Other delivery Messages', array("class"=>'pure-menu-link')) !!}</li>
                </ul>
            </div>
        </div>

        @if(isset($messageItems))
            @foreach($messageItems as $messageItem)
                <section class="top my_message_item_section">
                    <div class="main_link">{{ $messageItem['delivery']['subtitle'] }}</div>
                    @if(count($messageItem['messages']) > 0)
                        @foreach ($messageItem['messages'] as $message)
                            @include('partials.messages.message', ['message_p' => $message['message_item']])
                        @endforeach
                    @else
                        @include('partials.messages.message', [])
                    @endif
                </section>
            @endforeach
        @endif
    </div>

@stop