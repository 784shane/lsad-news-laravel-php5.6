//Mustache tutorial
//http://coenraets.org/blog/2011/12/tutorial-html-templates-with-mustache-js

//Handlebars tutorial
//http://stackoverflow.com/questions/26643503/handlebars-loading-external-template-files

//handlebars events.tmpl -f events.tmpl.js


var colorBoxService = function () {

    this.colorBoxConfig = {
        width:'60%',
        innerWidth:'700px',
        maxWidth:'700px',
        maxHeight:false,
        inline:false,
        transition:'fade',
        speed:'50'
    };

    this.colorBox = function(settings){
        if(typeof settings != 'undefined'){
            $.extend(this.colorBoxConfig, settings);
        }
        return $.colorbox(this.colorBoxConfig);
    };



};


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//appEventsService
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//Takes care of all the events on the site
// i.e., click, mouse-over, etc.
var appEventsService = function () {
    this.setClickEvent = function (item, callback) {
        $(item).click(function (event) {
            callback(event);
        });
    };

    this.setOnChangeEvent = function (item, callback) {
        $(item).change(function (event) {
            callback(event);
        });
    };
};


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//googleMapsService
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//Takes care of all the outbound calls
//such as ajax calls to the server
//We will created both post and get calls.
var googleMapsService = function () {

    var self = this;
    this.map = map;
    this.mapTimeout = null;

    this.init = function () {
        this.map = map;
        if (this.map) {
            this.loadMap();
        } else {
            this.setGoogleMapTimer();
        }
    };

    this.setGoogleMapTimer = function () {
        clearTimeout(this.mapTimeout);
        this.mapTimeout = setTimeout(function () {
            self.init();
        }, 200);
    };

    this.loadMap = function () {

        var waypts = [];


        stop = new google.maps.LatLng(-39.419, 175.57)
        waypts.push({
            location: stop,
            stopover: true
        });

        var myLatLng1 = this.createLaTLng(coordinates.from.lat, coordinates.from.lng);
        var myLatLng2 = this.createLaTLng(coordinates.to.lat, coordinates.to.lng);


        var request = {
            origin: myLatLng1,
            destination: myLatLng2,
            //waypoints: waypts,
            //optimizeWaypoints: true,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                var route = response.routes[0];

            }
        });

        //Use the coordinates to get the latLng Coordinates.
        /*var myLatLng1 = this.createLaTLng(coordinates.from.lat, coordinates.from.lng);
         var myLatLng2 = this.createLaTLng(coordinates.to.lat, coordinates.to.lng);

         //We need two markers.
         //One for the collection point.
         //Another for the delivery point.
         var marker1 = this.createMarker(myLatLng1,'From');
         var marker2 = this.createMarker(myLatLng2,'To');

         var markers = [marker1, marker2];

         //Lets set the map to the bound.
         this.setMapBound(markers);*/

    };

    this.createLaTLng = function (lat, lng) {

        return {
            lat: parseFloat(lat),
            lng: parseFloat(lng)
        };

    };

    this.createMarker = function (latLng, title) {

        return new google.maps.Marker({
            position: latLng,
            map: this.map,
            title: title
        });

    };

    this.getMarkersBound = function (markers) {

        var bounds = new google.maps.LatLngBounds();
        for (i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].getPosition());
        }
        return bounds;

    };

    this.setMapBound = function (markersArray) {

        this.map.fitBounds(this.getMarkersBound(markersArray));

    };

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//globalHelperService
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//Takes care of all the outbound calls
//such as ajax calls to the server
//We will created both post and get calls.
var globalHelperService = function () {

    this.isClassAttributeDefined = function (element) {
        return (typeof element.attr('class') != 'undefined');
    };

    this.isIdAttributeDefined = function (element) {
        return (typeof element.attr('id') != 'undefined');
    };

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//httpService
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//Takes care of all the outbound calls
//such as ajax calls to the server
//We will created both post and get calls.
var httpService = function () {
    //We will use this function whenever we need to call the
    //server by the 'get'(ajax) method.
    this.callServerGet = function (url) {
        var defer = $.Deferred();
        var jqxhr = $.get(url)
            .done(function (result) {
                defer.resolve(result);
            })
            .fail(function (result) {
                defer.resolve(result);
            });
        return defer.promise();
    };

    //server by the 'get'(ajax) method.
    this.callServerPost = function (url, postData) {
        var defer = $.Deferred();
        var jqxhr = $.post(url, postData)
            .done(function (result) {
                defer.resolve(result);
            })
            .fail(function (result) {
                defer.resolve(result);
            });
        return defer.promise();
    };
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//postCodeService
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//Main service that handles the anything javascript
//that deals with the Post code transformation to
//addresses.
var postCodeService = function (httpService) {
    var actors = {
        'getPostCodeBaseUrl': '/get-postcode-address/',
        'httpService': httpService,
        'elements': {
            'pcAdd1': 'collection_address',
            'pcAdd2': 'delivery_address',
            'pc1': 'collection_postcode',
            'pc2': 'delivery_postcode',
            'pc1_addr_view': 'postcode1_address',
            'pc2_addr_view': 'postcode2_address'
        }
    };

    this.init = function () {
        this.setBlurEvents('#' + actors.elements.pc1);
        this.setBlurEvents('#' + actors.elements.pc2);
    };

    this.setBlurEvents = function (element) {
        $(element).blur(function (e) {
            if (typeof e.currentTarget.id != "undefined" && e.currentTarget.value != "") {

                //construct the url we need to call on the server.
                url = actors.getPostCodeBaseUrl + escape(e.currentTarget.value);

                //Go off to get the address of this post code now by calling
                //the server where this function will be performed.
                actors.httpService.callServerGet(url).done(function (address) {

                    //Decide and send this address to the view
                    //We have to show this address to the user.
                    switch (e.currentTarget.id) {
                        case actors.elements.pc1:
                        {
                            $('#' + actors.elements.pc1_addr_view).html(address);
                            $('#' + actors.elements.pcAdd1).val(address);
                            break;
                        }
                        case actors.elements.pc2:
                        {
                            $('#' + actors.elements.pc2_addr_view).html(address);
                            $('#' + actors.elements.pcAdd2).val(address);
                            break;
                        }
                    }

                });
            } else {
                //console.log('failed');
            }
        });
    }
};


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//collectionDeliveryItemsService
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
var collectionDeliveryItemsService = function () {
    var collectDeliverItems = {
        firstName: "Shane",
        'delivery_title': '',
        items_list: [
            {
                'list_no': 1,
                'the_item_list_index': 0,
                'ebay_item': false,
                'ebay_item_id': '',
                'ebay_item_name': '',
                'ebay_item_image': '',
                'firstName': 'Shane', 'lastName': 'Daniel'
            }
        ]
    };

    this.getNumberOfItems = function () {
        return collectDeliverItems.items_list.length;
    };

    this.getCollectDeliverItems = function () {
        return collectDeliverItems;
    };

    this.toggleEbayItem = function (index, value) {
        collectDeliverItems.items_list[index].ebay_item = value;
    };

    this.updateEbayItemInList = function (ebayItemIndex, ebayItemObj) {
        if (typeof ebayItemObj.ebayItem != "undefined") {
            collectDeliverItems.items_list[ebayItemIndex].ebay_item_id = ebayItemObj.ebayItem;
        }
        if (typeof ebayItemObj.title != "undefined") {
            if (ebayItemIndex == 0) {
                collectDeliverItems.delivery_title = ebayItemObj.title;
            }
            collectDeliverItems.items_list[ebayItemIndex].ebay_item_name = ebayItemObj.title;
        }
        if (typeof ebayItemObj.pic != "undefined") {
            collectDeliverItems.items_list[ebayItemIndex].ebay_item_image = ebayItemObj.pic;
        }
    };

    this.addCollectDeliverItem = function () {
        //alert('pushing another one - '+(collectDeliverItems.items_list.length + 1));
        collectDeliverItems.items_list.push({
            // Set a default for this new item list
            'list_no': (collectDeliverItems.items_list.length + 1),
            // We want the index to be recorded
            // this is just the number of items selected
            'the_item_list_index': collectDeliverItems.items_list.length
        });
    };
};

var collect_deliver_items_service = new collectionDeliveryItemsService();


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//ebayItemService
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//Main service that handles the anything javascript
//that deals with the handling getting ebay items
var ebayItemService = function (httpServiceLocal, globalHelperService) {

    var actors = {
        'getPostCodeBaseUrl': '/get-ebay-item/',
        'httpServiceLocal': httpServiceLocal,
        'globalHelperService': globalHelperService,
        'elements': {
            'use_ebay': 'use_ebay',
            'ebayItmBtn': 'get_ebay_item_button',
            'itmBlkList': 'item_block_list',
            'add_item': 'add_collection_item'
        },
        'templatesUrl': {
            'ebaySingleItems': '/templates/ebay-single-items.html'
        }
    };

    this.init = function () {

        //all the global events are set here.
        this.setUpLocalEvents();

        //We are going to populate our page.
        this.populateCollectionItems();

    }

    this.isClassAttributeDefined = function (element) {
        return (typeof element.attr('class') != 'undefined');
    };

    this.isIdAttributeDefined = function (element) {
        return (typeof element.attr('id') != 'undefined');
    };

    this.setUpClickEvents = function (element) {
        var self = this;
        $(element).click(function (e) {

            //if the element is referenced by a certain class
            if (self.isClassAttributeDefined($(this))) {
                if ($(this).attr('class').indexOf(actors.elements.use_ebay) > -1) {
                    //we know that this is the use_ebay checkbox.
                    self.performUseEbayClickActivities($(this));
                }
                if ($(this).attr('class').indexOf(actors.elements.ebayItmBtn) > -1) {
                    //we know that this is the use_ebay checkbox.
                    self.performGetEbayItemActivities($(this));
                }
            }

            //if the element is defined by an id
            if (self.isIdAttributeDefined($(this))) {
                switch ($(this).attr('id')) {

                    //the add item button
                    case actors.elements.add_item:
                    {
                        e.preventDefault();
                        self.addNewListItem();
                        break;
                    }

                }
            }


        });
    };

    this.setUpLocalEvents = function () {
        //set the use_ebay checkbox event.
        this.setUpClickEvents('#' + actors.elements.add_item);
    }

    this.performGetEbayItemActivities = function (getEbayItemElement) {
        var self = this;
        var ebayItemIndex = getEbayItemElement.data('index');
        //Lets get the input that has the proposed ebay id.
        var proposedEbayId = $('#ebayIdInput_' + ebayItemIndex).val();
        actors.httpServiceLocal.callServerGet("/get-ebay-item/" + escape(proposedEbayId)).done(function (result) {

            result = $.parseJSON(result);

            if (result.findItemsByKeywordsResponse.length > 0) {
                var searchResults = result.findItemsByKeywordsResponse[0].searchResult;
                var foundItem = false;
                var ebayItemObj = new Object;
                for (var searchResIndex in searchResults) {
                    for (var itemIndex in searchResults[searchResIndex]) {
                        var item = searchResults[searchResIndex][itemIndex];

                        //Get theebay item id.
                        if (typeof item[0].itemId != 'undefined') {
                            ebayItemObj.ebayItem = item[0].itemId[0];
                            foundItem = true;
                        }

                        //Get the gallery image.
                        if (typeof item[0].galleryURL != 'undefined') {
                            ebayItemObj.pic = item[0].galleryURL[0];
                            foundItem = true;
                        }

                        //Get the title of the item.
                        if (typeof item[0].title != 'undefined') {
                            ebayItemObj.title = item[0].title[0];
                            foundItem = true;
                        }

                        if (foundItem) {
                            break;
                        }
                    }

                    if (foundItem) {
                        break;
                    }
                }

                self.setEbayItemInList(ebayItemIndex, ebayItemObj);

            }
        });
    };

    this.setEbayItemInList = function (ebayItemIndex, ebayItemObj) {
        //Update the collection list item.item block.
        // We need to check why this ebay item is not being inserted into this
        collect_deliver_items_service.updateEbayItemInList(ebayItemIndex, ebayItemObj);
        //After updating thislist item, we shall refresh the list.
        this.populateCollectionItems();
    };

    this.performUseEbayClickActivities = function (useEbayElement) {
        var checked = useEbayElement.is(':checked');
        var checkboxIndex = useEbayElement.data('index');
        collect_deliver_items_service.toggleEbayItem(checkboxIndex, checked);
        this.toggleEbayItemBlocks(checkboxIndex, checked);
        //refresh the items list.
        //this.populateCollectionItems();
    };

    this.toggleEbayItemBlocks = function(activeIndex, state){
        //if-not-ebay-item
        // We need to hide all the blocks for we don't know which one we want to show.
        // And we certainly dont want to leave any of them to be displayed.
        if(state){
            $('#if-ebay-item_'+activeIndex).show();
            $('#if-not-ebay-item_'+activeIndex).hide();
        }else{
            $('#if-ebay-item_'+activeIndex).hide();
            $('#if-not-ebay-item_'+activeIndex).show();
        }
    }

    this.setPopulatedCollectionItemsEvents = function () {
        //set the use_ebay checkbox event.
        this.setUpClickEvents('.' + actors.elements.use_ebay);
        //set the get_ebay_item button.
        this.setUpClickEvents('.' + actors.elements.ebayItmBtn);
    };

    this.setUploadBoxTemplate = function (context) {
        var uBTemplateSource = $("#uploadBoxTemplate").html();
        var uBTemplate = Handlebars.compile(uBTemplateSource);
        var uBTemplateContext = new Object;
        uBTemplateContext.the_item_list_index = (context.items_list.length - 1) < 0 ? 0 : (context.items_list.length - 1);
        var uBTemplateHtml = uBTemplate(uBTemplateContext);
        Handlebars.registerPartial("uploadBoxTemplatePartial", uBTemplateHtml);
    };

    this.getUnitSelectBoxTemplate = function(type, numOfItemsListed){
        var htmlId = '';
        switch(type){
            case 'width':
            case 'length':
            case 'height':{
                htmlId = '#select_unit_of_measurement_length';
                break;
            }

            case 'weight':{
                htmlId = '#select_unit_of_measurement_weight';
                break;
            }

            case 'capacity':{
                htmlId = '#select_unit_of_measurement_capacity';
                break;
            }
        }

        var lengthUnitOfMeasurementHtml = $(htmlId).html();
        var lengthUnitOfMeasurementTemplate = Handlebars.compile(lengthUnitOfMeasurementHtml);
        // Attach the data needed for the template
        return lengthUnitOfMeasurementTemplate({nameOfSelect:'measured_'+type+"_"});
    };

    this.populateCollectionItems = function () {

        var source = $("#collectionItemsTemplate").html();
        var template = Handlebars.compile(source);
        var context = collect_deliver_items_service.getCollectDeliverItems();

        // Get the number of items already listed on this delivery.
        var numOfItemsListed = collect_deliver_items_service.getNumberOfItems();

        // Set the content for the select box for length unit of measurement.
        Handlebars.registerPartial('widthSelectBoxPartial', this.getUnitSelectBoxTemplate('width', numOfItemsListed));

        // Set the content for the select box for length unit of measurement.
        Handlebars.registerPartial('lengthSelectBoxPartial', this.getUnitSelectBoxTemplate('length', numOfItemsListed));

        // Set the content for the select box for the weight unit of measurement.
        Handlebars.registerPartial('heightSelectBoxPartial', this.getUnitSelectBoxTemplate('height', numOfItemsListed));

        // Set the content for the select box for the capacity unit of measurement.
        Handlebars.registerPartial('weightSelectBoxPartial', this.getUnitSelectBoxTemplate('weight', numOfItemsListed));

        this.setUploadBoxTemplate(context);

        // We are going to append to the existing blocks. We do not want to override
        // the blocks already on the page. So we will remove the equivalent of the
        // number already on the page, relative to the number in the context.
        var html = template(this.getLastCollectedItem(numOfItemsListed, context));

        //Assign this template to the block provided.
        $('#' + actors.elements.itmBlkList).append(html);

        //set up the clickEvents for the populatedItems
        this.setPopulatedCollectionItemsEvents();
    };

    this.getLastCollectedItem = function(numOfItemsListed, context){
        var arr = new Array;
        arr.push(context.items_list[(numOfItemsListed-1)]);
        return {
            'items_list':arr
        }
    };

    this.addNewListItem = function () {
        // Add a new object to our collection object
        collect_deliver_items_service.addCollectDeliverItem();
        // refresh the view to show the latest addition
        this.populateCollectionItems();

        // We need to take care of our photo uploads as well
        var upload_photo_service = new uploadPhotoService(new httpService());
        upload_photo_service.init();
    }

};


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//myAccountService
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//Main service that handles the anything javascript
//that deals with the handling getting ebay items
var myAccountService = function (httpService, globalHelperService) {

    var actors = {
        'httpService': httpService,
        'globalHelperService': globalHelperService,
        'elements': {
            'activeDelLink': 'active_deliveries_link',
            'pastDelLink': 'past_deliveries_link',
            'messageLink': 'messages_link',
            'acctLink': 'account_link'
        }/*,
         'templatesUrl':{
         'ebaySingleItems':'/templates/ebay-single-items.html'
         }*/
    };

    this.init = function () {

        //all the global events are set here.
        this.setUpLocalEvents();

    }

    //all the global events will be set up here.
    this.setUpLocalEvents = function () {

        this.setUpClickEvents('#' + actors.elements.activeDelLink);
        this.setUpClickEvents('#' + actors.elements.pastDelLink);
        this.setUpClickEvents('#' + actors.elements.messageLink);
        this.setUpClickEvents('#' + actors.elements.acctLink);

    };

    this.setUpClickEvents = function (element) {
        var self = this;
        $(element).click(function (e) {

            //if the element is referenced by a certain class
            if (actors.globalHelperService.isClassAttributeDefined($(this))) {

            }

            //if the element is defined by an id
            if (actors.globalHelperService.isIdAttributeDefined($(this))) {
                switch ($(this).attr('id')) {

                    case actors.elements.activeDelLink:
                    {
                        e.preventDefault();
                        break;
                    }

                    case actors.elements.pastDelLink:
                    {
                        e.preventDefault();
                        break;
                    }

                    case actors.elements.messageLink:
                    {
                        e.preventDefault();
                        break;
                    }

                    case actors.elements.acctLink:
                    {
                        e.preventDefault();
                        break;
                    }

                    default:
                    {

                    }
                }
            }


        });
    };

};


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//quoteDescriptionPageService
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

var quoteDescriptionPageService = function (httpService) {
    var self = this,
        actors = {
            'quotesInfoDetailsUrl': '/get-quotes-info-details/',
            'httpService': httpService,
            'elements': {
                'view_quote_info': 'view_quote_info'
            }
        };

    this.init = function () {
        // all the global events are set here.
        this.setUpLocalEvents();

    };

    //all the global events will be set up here.
    this.setUpLocalEvents = function () {
        var appEvtService = new appEventsService();
        appEvtService.setClickEvent('.' + actors.elements.view_quote_info, this.onClickedViewQuoteInfo);
    };

    // Callback when view more quote info is clicked on.
    this.onClickedViewQuoteInfo = function (evt) {
        var dataUrl = actors.quotesInfoDetailsUrl + $(evt.target).data('quote-id');
        actors.httpService.callServerGet(dataUrl)
            .done(function (result) {
                //self.populateMoreInfoSection(evt.target);
                var returnedData = $.parseJSON(result);
                self.showQuotesInfoSection(evt.target, returnedData);
            });
    };

    this.populateMoreInfoSection = function (elementToPopulate, returnedData) {

        var partial = Handlebars.templates.sj_quotes_more_info_message_insert;

        Handlebars.registerPartial("messageInsert", partial);

        template = Handlebars.templates.sj_quotes_more_info;
        var context = {
            transporterUsername: returnedData.transportProvider.user_name,
            items_list: returnedData.messagesWithSender
        };
        var html = template(context);
        $(elementToPopulate).html(html);
    };

    this.showQuotesInfoSection = function (clickedElement, returnedData) {
        // Close any opened section.
        this.toggleQuotesInfoSection('close');
        var elementToPopulate = $(clickedElement)
            .parents('.sj_quote_summary_row').find('.more_quote_info');
        this.populateMoreInfoSection(elementToPopulate, returnedData);
        this.toggleQuotesInfoSection('open', elementToPopulate);
    };

    this.toggleQuotesInfoSection = function (state, which) {
        switch (state) {
            case "open":
            {
                $(which).show();
                break;
            }
            default:
            {
                $('.sj_quote_summary_row').each(function (a, b) {
                    $(b).find('.more_quote_info').hide();
                });
            }
        }
    };

};


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//uploadPhotoService
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
var uploadPhotoService = function () {

    var actors = {
        'elements': {
            'file_upload_element': 'file-upload-input'
        }
    };

    var uploadPhotoClass = function () {
        var self = this,
            actors = {
                'quotesInfoDetailsUrl': '/get-quotes-info-details/',
                'httpService': null,
                'elements': {
                    'file_upload_name_prefix': 'uploaded_image_',
                    'file_upload_element': null,
                    'upload_group_number': null,
                    'upload_now_button': 'upload-now-button'
                }
            };

        this.reset = function () {

        };

        this.init = function (file_upload_element, httpService) {
            // Set the httpService object.
            actors.httpService = httpService;
            // Set the file_upload_element.
            actors.elements.file_upload_element = file_upload_element;
            // all the global events are set here.

            this.setItemGroupNumber();

            this.setUpLocalEvents();
        };

        this.getPresentItemBoxSurround = function () {
            // get the present item_box_surround box
            return $(actors.elements.file_upload_element)
                .parents('.item_box_surround');
        };

        // Locate the upload now button.
        this.getUploadNowButton = function () {
            // get the upload-now-button of this section
            return this.getPresentItemBoxSurround()
                .find('.' + actors.elements.upload_now_button);
        };

        // Locate the local progress bar.
        this.getLocalProgressElement = function () {
            // get the upload-now-button of this section
            return this.getPresentItemBoxSurround()
                .find('progress');
        };

        // Locate the local picture container.
        this.getLocalPictureContainerElement = function () {
            // get the upload-now-button of this section
            return this.getPresentItemBoxSurround()
                .find('.picture-show');
        };

        // Get the name of the file being uploaded
        this.getFileNameBeingUploaded = function () {
            return $(actors.elements.file_upload_element).attr('name');
        };

        // Get the name of the file being uploaded
        this.setItemGroupNumber = function () {
            actors.elements.upload_group_number = parseInt(this.getFileNameBeingUploaded()
                .replace(actors.elements.file_upload_name_prefix, ''));
        };

        // Locate the local picture name container.
        this.getLocalPictureNameContainerElement = function () {
            // get the upload-now-button of this section
            return this.getPresentItemBoxSurround()
                .find('.picture-name-show');
        };

        //all the global events will be set up here.
        this.setUpLocalEvents = function () {
            var appEvtService = new appEventsService();
            appEvtService.setOnChangeEvent(
                actors.elements.file_upload_element, this.fileInputChanged);
            // Set up click events
            /*appEvtService.setClickEvent(
             this.getUploadNowButton(),
             this.tryUpload);*/
        };

        this.everythingFromCharacterRegEx = function () {
            // Everything from the last slash or the last back slash
            // depending on the operating system.
            return /(\\|\/)[^(\\|\/)]+$/;
        };

        this.getImageUploadSource = function (sourcePath) {
            // Remove the path, we only need the file and extension.
            // *****************************************************

            // regex - selects from the final occurrence of a character.
            // In this case, we will be looking for the last occurrence
            // of a slash or a back slash. Depends on the operating system
            if (this.everythingFromCharacterRegEx().test(sourcePath)) {
                var matches = (sourcePath).match(this.everythingFromCharacterRegEx());
                if (matches !== null) {
                    //remove the front slash
                    return (matches[0]).replace(/^(\/|\\)/, '');
                }
            }
            return sourcePath;
        };

        this.fileInputChanged = function (event) {
            var imgFile = event.target.value;
            if (imgFile != "") {
                // get the image source
                var imgFile = self.getImageUploadSource(imgFile);
            }

            // Place image name into its appropriate space.
            $(event.target)
                .parents('.item_box_surround')
                .find(self.getLocalPictureNameContainerElement()).html(imgFile);

            self.tryUpload(event);
        };

        this.progressHandlingFunction = function (e) {
            if (e.lengthComputable) {
                $(self.getLocalProgressElement()).attr({value: e.loaded, max: e.total});
            }
        };

        this.afterPhotoUpload = function (result) {
            var resultObj = $.parseJSON(result);
            // get the image
            $(self.getLocalPictureContainerElement()).html(
                '<img src="' +
                site_url + '/photos/temp/' +
                resultObj.file_name + '" width="100%">'
            );

            // Now set the value off the photo_to_use hidden element.
            $('#photo_to_use_' + actors.elements.upload_group_number).val('true');

            // Now that we have uploaded the image, we will remove the form
            // and do other cleanup.
            self.cleanupAfterUploadingImage();
        };

        this.cleanupAfterUploadingImage = function(){
            // 1. We will be removing the form
            // take this object out of its parent form - this.getPresentItemBoxSurround()
            // we will embedd it right above the the parent element - the form and remove that
            // particular form
            var parentForm = $(this.getPresentItemBoxSurround()).closest('form');
            $(this.getPresentItemBoxSurround()).insertBefore(parentForm);

            // Now remove the temp 'form' element
            //$(parentForm).remove();
        };

        this.getNewUploadFormElement = function () {
            var newFormElement = $('<form id="di_photo_upload_form"></form>');
            // set the action attribute
            newFormElement.attr('action', '/photo-upload');
            // set the method attribute
            newFormElement.attr('method', 'post');
            // set the enctype attribute
            newFormElement.attr('enctype', 'multipart/form-data');
            return newFormElement;
        };//form_submitted_time

        this.addFormDependencies = function (form) {
            // adding the dependencies to the form
            form.append('<input type="hidden" ' +
                'name="form_time" ' +
                'value="' + $('#form_submitted_time').val() + '" />');

            // adding the token to the form
            form.append('<input type="hidden" ' +
                'name="_token" ' +
                'value="' + $("input[name='_token']").val() + '" />');

        };

        this.tryUpload = function (event) {
            // As soon as the file input has changed, we will
            // Set the form in place.
            $(this.getNewUploadFormElement()).insertBefore(this.getPresentItemBoxSurround());

            // Now append the upload surround div elements to the form.
            var form = $('#di_photo_upload_form');
            form.append(this.getPresentItemBoxSurround());

            // Append the other dependencies for the form submit
            this.addFormDependencies(form);

            // Set up the submit event for this form.
            form.ajaxSubmit(self.afterPhotoUpload);


        };

        this.tryUploadHold = function (event) {
            // As soon as the file input has changed, we will

            var formDatax = new FormData($('form')[0]);

            //FormData dont work in ie8
            //use this - http://malsup.com/jquery/form/
            // - ajaxForm

            formDatax.append("file_being_uploaded", self.getFileNameBeingUploaded());

            //formData.file_order_number = 12;
            $.ajax({
                url: '/photo-upload',  //Server script to process data
                type: 'POST',
                xhr: function () {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) { // Check if upload property exists
                        myXhr.upload.addEventListener('progress', self.progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                //Ajax events
                beforeSend: function (result) {
                },
                success: self.afterPhotoUpload,
                error: function (result) {
                    //console.log(result);
                },
                // Form data
                data: formDatax,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false
            });


        };

    };


    this.init = function () {
        // all the global events are set here.
        $('.' + actors.elements.file_upload_element).each(function (index, item) {
            var photoUploadClass = new uploadPhotoClass();
            photoUploadClass.init(item, httpService);
        });
    };


};


$(document).ready(function () {

    Handlebars.registerHelper('ifCond', function (v1, v2, options) {
        if (v1 !== v2) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    /*var pCodeService = new postCodeService(new httpService());
     //pCodeService.init();

     var ebayService = new ebayItemService(new httpService(), new globalHelperService());
     //ebayService.init();*/

    var mAccountService = new myAccountService(new httpService(), new globalHelperService());
    //mAccountService.init();

    /*
     /js/angular/templates/ebay-single-items.html
     /js/templates/ebay-single-items.html
     */

});


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//quoteDescriptionPageService
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

var accept_quote_service = function (httpService) {
    var self = this,
        actors = {
            'elements': {
             'paypal_radio': 'payment_by_paypal',
             'cc_radio': 'payment_by_cc'
            }
    };

    this.init = function () {
        // all the global events are set here.
        this.setUpLocalEvents();
    };

    this.setUpLocalEvents = function () {
        // We need to set up the payal/cc radio buttons.
        // When the user clicks on these buttons, they will expand to reveal
        // relevant contents.

        var self = this;
        var appEvtService = new appEventsService();
         //setOnChangeEvent
        appEvtService.setOnChangeEvent(
            '#'+actors.elements.paypal_radio, self.fileInputChanged);
        appEvtService.setOnChangeEvent(
            '#'+actors.elements.cc_radio, self.fileInputChanged);
        //cc_radio
         /*appEvtService.setClickEvent(
         '#inform', self.fileInputChanged);*/
        //payment_by_cc
        // Set up click events
    };

    this.fileInputChanged = function(evt){
        if(evt.currentTarget.checked){
            // Expand the appropriate section.
            switch(evt.target.id){
                case actors.elements.paypal_radio:{
                    self.expandPaymentChoicePockets('paypal_choice_pocket');
                    break;
                }
                case actors.elements.cc_radio:{
                    self.expandPaymentChoicePockets('cc_choice_pocket');
                    break;
                }
            }
        }
    };


    this.expandPaymentChoicePockets = function(paymentPocket){
        // close all pockets first.
        this.closeChoicePockets();
        switch(paymentPocket){
            case 'paypal_choice_pocket':{
                $('#paypal_choice_pocket').show();
                break;
            }
            case 'cc_choice_pocket':{
                $('#cc_choice_pocket').show();
                break;
            }
        }
    };


    this.closeChoicePockets = function(){
        $('#paypal_choice_pocket').hide();
        $('#cc_choice_pocket').hide();
    };

};