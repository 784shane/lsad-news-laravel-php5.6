<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TemporaryUploads extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'form_token',
        'form_time',
        'file_name',
        'item_order_no'
    ];
}
