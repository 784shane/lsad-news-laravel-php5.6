'use strict';

/* Controllers */

var shippingAppControllers = angular.module('shippingAppControllers', []);

shippingAppControllers.controller('deliveryFormInfo', 
  ['$scope',
   '$http',
   'AjaxService','ItemCollectionService', '$sce', '$element', deliveryFormInfoController]
  );

shippingAppControllers.controller('deliveryBasicInfo', 
  ['$scope',
   '$http',
   'AjaxService',
   deliveryBasicInfoController]
  );

function deliveryBasicInfoController($scope, $http, $AjaxService) {

  var callServerTimeout = null;

  $scope.postcode = '';

  $scope.processPostCode = function(elementDirective){
    var requestedPostCode = "";
    switch(elementDirective){
        case 'post-code-from':{
          requestedPostCode = $scope.postcodefrom;
          break;
        }

        case 'post-code-to':{
          requestedPostCode = $scope.postcodeto;
          break;
        }

        default:{

        }
      }
      $scope.checkPostCode(elementDirective, requestedPostCode);
  };

  $scope.checkPostCode = function(elementDirective, requestedPostCode){
    if(requestedPostCode != ""){
      
      $AjaxService.getCall("/get-postcode-address/"+escape(requestedPostCode)).then(function(response) {
        //Assign the response here so that we will update the view
        switch(elementDirective){
          case 'post-code-from':{
            $scope.postcode1 = response;
            break;
          }

          case 'post-code-to':{
            $scope.postcode2 = response;
            break;
          }

          default:{

          }
        }

      }, function(failure) {
        console.log(failure);
      });
    }
  }

}


function deliveryFormInfoController($scope, $http, AjaxService, ItemCollectionService, $sce, $element){

  $scope.collectionItemList = ItemCollectionService.getCollectionItems();
  $scope.ebaygroup = "";
  $scope.cd_date_specified = true;

  $scope.toggleEbayItem = function(ele){
    var dataListNo = ele.$index;
    console.log(ele);
    ItemCollectionService.toggleEbayItemShow(ele.xx, dataListNo);
  };

  $scope.getEbayItem = function(ele){

    var dataListNo = ele[0].attributes['data-list-no'].value;
    //We will be using the id of the textbox in the repeat iteration here.
    if( typeof ele[0].parentNode.children['ebayIdInput_'+dataListNo].value == "undefined"){
        return false;
    }

    var ebayIdInputNo = ele[0].parentNode.children['ebayIdInput_'+dataListNo].value;

    AjaxService.getCall("/get-ebay-item/"+escape(ebayIdInputNo)).then(function(result) {
      if(result.findItemsByKeywordsResponse.length > 0){
        var searchResults = result.findItemsByKeywordsResponse[0].searchResult;
        var foundItem = false;
        var ebayItemObj = new Object;
        for(var searchResIndex in searchResults){
          for(var itemIndex in searchResults[searchResIndex]){
            var item = searchResults[searchResIndex][itemIndex];

            //Get theebay item id.
            if(typeof item[0].itemId != 'undefined'){
              ebayItemObj.ebayItem = item[0].itemId[0];
              foundItem = true;
            }

            //Get the gallery image.
            if(typeof item[0].galleryURL != 'undefined'){
              ebayItemObj.pic = item[0].galleryURL[0];
              foundItem = true;
            }

            //Get the title of the item.
            if(typeof item[0].title != 'undefined'){
              ebayItemObj.title = item[0].title[0];
              foundItem = true;
            }

            if(foundItem){ break; }
          }

          if(foundItem){ break; }
        } 

          ebayItemObj.collectionNo = dataListNo; 
          ItemCollectionService.addNewCollectionItem(ebayItemObj);

      }
    }, function(failure) {
      console.log(failure);
    });
    
  };

  $scope.addNewItem = function(){
    ItemCollectionService.addNewCollectionItem({});
  };

  $scope.toggleCollectionAndDeliveryDate = function(ele){
    switch(ele.target.attributes.id.value){
      case "cd_specified_date":
      case "cd_between_dates":
      case "dd_specified_date":
      case "dd_between_dates":{
        ItemCollectionService.setCollectDeliverDateValue(ele.target.attributes.id.value, true);
        break;
      }
      default:{
        ItemCollectionService.resetCollectDeliverDateValue(ele.target.attributes.id.value);
      }
    }
    //ItemCollectionService.setCollectDeliverDateValue(ele.target.attributes.id.value, true);
  };

  $scope.show_collect_and_deliver_dates = function(collect_deliver_dates_item){
    return ItemCollectionService.getCollectDeliverDateValue(collect_deliver_dates_item);
  };

  $scope.doSomething = function(){
    //console.log('yoyu called');
  };

}