<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DeliveryTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Model::unguard();
        DB::table('delivery_types')->insert([
            [
                'status_name' => 'archived',
                'status_value' => 4
            ],
            [
                'status_name' => 'expired',
                'status_value' => 3
            ],
            [
                'status_name' => 'active_private',
                'status_value' => 2
            ],
            [
                'status_name' => 'active_public',
                'status_value' => 1
            ]
        ]);
    }
}
