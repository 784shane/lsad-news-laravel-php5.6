<div>
	
	<h1> Business Details </h1>

	<div>
		<div>
			{!! Form::label('company_user_name', 'Choose a user name') !!}
			{!! Form::text('company_user_name', null, array('required' => 'required' )) !!}
			<span class="form-errors">{!! $errors->first('company_user_name') !!}</span>
		</div>

		<div>
			{!! Form::label('company_name', 'Company Name') !!}
			{!! Form::text('company_name', $businessDetails->company_name, array('required' => 'required' )) !!}
			<span class="form-errors">{!! $errors->first('company_name') !!}</span>
		</div>

		<div>
			{!! Form::label('company_email', 'Company Email') !!}
			{!! Form::email('company_email', $businessDetails->company_email, array('required' => 'required' )) !!}
			<span class="form-errors">{!! $errors->first('company_email') !!}</span>
		</div>

		<div>
			{!! Form::label('company_phone_number', 'Phone Number') !!}
			{!! Form::text('company_phone_number', $businessDetails->company_phone_number) !!}
			<span class="form-errors">{!! $errors->first('company_phone_number') !!}</span>
		</div>

		<div>
			{!! Form::label('company_mobile_number', 'Mobile Number') !!}
			{!! Form::text('company_mobile_number', $businessDetails->company_mobile_number ) !!}
			<span class="form-errors">{!! $errors->first('company_mobile_number') !!}</span>
		</div>

		<div>
			{!! Form::label('company_address_1', 'Address Line 1') !!}
			{!! Form::text('company_address_1', $businessDetails->company_address_1) !!}
			<span class="form-errors">{!! $errors->first('company_address_1') !!}</span>
		</div>

		<div>
			{!! Form::label('company_address_2', 'Address Line 2 (optional)') !!}
			{!! Form::text('company_address_2', $businessDetails->company_address_2) !!}
			<span class="form-errors">{!! $errors->first('company_address_2') !!}</span>
		</div>

		<div>
			{!! Form::label('company_city', 'Town/City') !!}
			{!! Form::text('company_city', $businessDetails->company_city) !!}
			<span class="form-errors">{!! $errors->first('company_city') !!}</span>
		</div>

		<div>
			{!! Form::label('company_postcode', 'Post Code') !!}
			{!! Form::text('company_postcode', $businessDetails->company_postcode) !!}
			<span class="form-errors">{!! $errors->first('company_postcode') !!}</span>
		</div>
	</div>

	
	@if($businessDetails->id)
	<div>
		{!! Form::hidden('company_id', $businessDetails->id) !!}
	</div>
	@endif

	<div>
		{!! Form::hidden('sign_up_type', 'transport_providers') !!}
	</div>

</div>