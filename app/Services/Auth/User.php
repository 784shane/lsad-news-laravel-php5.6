<?php namespace App\Services\Auth;

/**
* This class takes care of the registering process.
* All the activities involving siging up.
**/


use Auth;
use Hash;


class User {

	public function getLoggedInUserObject(){

		//check to see if user is loggedIn
		if($this->isUserLoggedIn()){
			//return success
			return array('result' => 'success', 'user_object' => Auth::user()->toArray());
		}else{
			//return a fail result.
			return array('result' => 'fail');
		}

		return null;
	}

	/**
	* Checks to see if user is logged in
	* @return boolean 
	**/
	public function isUserLoggedIn(){
		return Auth::check();
	}

	/**
	* Checks to see if user is logged in
	* @return boolean 
	**/
	public function getLoggedInUsersId(){

		$loggedInUserObj = $this->getLoggedInUserObject();
		if($loggedInUserObj && $loggedInUserObj['result'] == "success"){
			return $loggedInUserObj['user_object']['id'];
		}

		return null;
	}

	/**
	* Checks to see if user is registered as transport company.
	* @return boolean 
	**/
	public function isUserATransportCompany(){

		$loggedInUserObj = $this->getLoggedInUserObject();
		if($loggedInUserObj && $loggedInUserObj['result'] == "success"){
			return (bool)$loggedInUserObj['user_object']['transport_company'];
		}

		return false;
	}

	/**
	* Checks to see if user is logged in
	* @return boolean 
	**/
	public function getLoggedInUsersEmail(){

		$loggedInUserObj = $this->getLoggedInUserObject();
		if($loggedInUserObj && $loggedInUserObj['result'] == "success"){
			return trim($loggedInUserObj['user_object']['email']);
		}

		return null;
	}

	/**
	* Checks to see if user is logged in
	* @return boolean 
	**/
	public function buildPasswordRaw($length = 10){
		//Courtesy :http://stackoverflow.com/questions/4356289/php-random-string-generator
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	/**
	* Hashes the password
	* @return String
	*
	* @return String 
	**/
	public function buildPasswordHash($password){
	    return Hash::make($password);
	}

	/**
	* Logs in the user. User is now allowed to see
	* their account.
	* @param String 
	* @return String
	*
	* @return boolean 
	**/
	public function logInUser($email, $password){
		
		if(Auth::attempt(array(
			'email'=>$email,
			'password'=>$password)))
		{
			return true;
		}

		return false;
	}

	/**
	* @param int 
	**/
	public function setuserAsTransportProvider($userId, $value){

		$user = \App\User::find($userId);
		$user->transport_company = $value;
		return $user->save();
	}

	/**
	* @param boolean 
	**/
	public function toggleTransportProviderVerified($userId, $verified){

		$user = \App\User::find($userId);
		$user->transport_company_verified = $verified;
		return $user->save();
	}

    /**
    * Checks to see if we have inputted the correct details into
    * the sign up form.
    * @return Boolean
    **/
    public function updateExistingUser($where, $whereValue, $businessData){
        $userModel = new \App\User();
        return $userModel->updateUser($where, $whereValue, $businessData);
    }
}
