<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['before' => 'home.route.filter'], function () {
	Route::get('home', array('as' => 'home', 'uses' => 'HomeController@index'));
	Route::get('messages/{userId?}', array('uses' => 'HomeController@messages'));
	Route::match(array('GET', 'POST'), 'message/{messageId}', array('uses' => 'HomeController@message'));
});

Route::group(['before' => 'getQuote.route.filter'], function () {
	Route::get('/', 'IndexController@index');
	Route::match(array('GET', 'POST'), '/deliveries/form', 'DeliveriesController@deliveryInput');
});

Route::group(['before' => 'deliveryJobs.route.filter'], function () {
	Route::get('/deliveries/jobs/{jobName?}/{jobId?}', 'DeliveriesController@liveJobs');
});

Route::group(['before' => 'signUpAsTransporter.route.filter'], function () {
	Route::match(array('GET', 'POST'), 'signup/{transport_providers?}', array(
		'as'=>'business_signup',
		'uses' => 'AuthController@signup'
	));
});

Route::group(['before' => 'login.route.filter'], function () {

	//Route::get('/login', 'AuthController@showLogin');
	//Route::post('login', array('uses' => 'AuthController@doLogin'));

	Route::match(array('GET', 'POST'), 'login', array(
		'uses' => 'AuthController@showLogin'
	));

	Route::get('logout', array('uses' => 'AuthController@logout'));
});

Route::get('/get-postcode-address/{postcode}', 'AjaxController@getPostCodeAddress');

Route::post('/photo-upload', 'AjaxController@photoUpload');

Route::get('/get-ebay-item/{ebayitem}', 'AjaxController@getEbayItem');

Route::get('/get-quotes-info-details/{deliveryId}', 'AjaxController@getQuotesInfoDetails');

Route::match(array('GET', 'POST'), '/deliveries/quotes/{jobName?}/{jobId?}/{action?}',  array('as' => 'deliveries.quotes', 'uses' => 'DeliveriesController@quote'));

Route::post('/deliveries/pre-quote/{jobName?}/{jobId?}', array('as' => 'deliveries.preQuote', 'uses' => 'DeliveriesController@preQuote'));

Route::post('/deliveries/post-quote/{jobName?}/{jobId?}', array('as' => 'deliveries.saveQuote', 'uses' => 'DeliveriesController@saveQuote'));

Route::match(array('GET', 'POST'), '/quotes/get-adjusted-quote', 'AjaxController@calculateAdjustedQuote');

Route::match(array('GET', 'POST'), '/deliveries/add', 'DeliveriesController@processDeliveriesInfo');

Route::match(array('GET', 'POST'), '/deliveries/edit/{jobId?}', array('as' => 'deliveries.edit', 'uses' => 'DeliveriesController@editDelivery'));

Route::match(array('GET', 'POST'), '/deliveries/delete/{jobId?}/{itemId?}', array('as' => 'deliveries.delete', 'uses' => 'DeliveriesController@deleteDelivery'));

Route::match(array('GET', 'POST'), '/quotes/accept/{deliveryId?}/{quoteId?}', 'QuotesController@accept');

Route::match(array('GET', 'POST'), '/quotes/process-quote-acceptance/{quoteId?}', 'QuotesController@processAcceptance');

// After Pay-pal express checkout, we will redirect to here after the transaction.
Route::get('/quotes/paypal-result', array('as' => 'paypal.express_checkout_result', 'uses' => 'QuotesController@paypalResult'));

// We will redirect to here if the Pay-pal transaction has been cancelled.
Route::get('/quotes/paypal-cancel', array('as' => 'paypal.express_checkout_cancel', 'uses' => 'QuotesController@paypalCancel'));

Route::post('/paid-by-credit-card', 'QuotesController@processCreditCardInformation');

//Login
Route::match(array('GET', 'POST'),'forgot', array('uses' => 'AuthController@forgot'));


Route::get('home/deliveries-active', array('as' => 'home.deliveries.active', 'uses' => 'HomeController@myDeliveriesActive'));
Route::get('home/deliveries-archived', array('as' => 'home.deliveries.archived', 'uses' => 'HomeController@myDeliveriesArchived'));


Route::match(array('GET', 'POST'), 'my-account/{accounttype?}', array('uses' => 'HomeController@myAccount'));

Route::get('complete-business-registration/{businessValidationToken}', array('uses' => 'AuthController@businessValidationComplete'));

Route::match(array('GET', 'POST'), 'reset-password/{forgotToken}', array('uses' => 'AuthController@userResetPassword'));

Route::match(array('GET', 'POST'), 'our-company/contact-us', array('as'=>'contact-us', 'uses' => 'CompanyController@contactUs'));

Route::get('our-company/about-us', array('as'=>'about-us', 'uses' => function ()    {
	return view('company.aboutUs');
}));

Route::get('our-company/how-it-works', array('as'=>'how-it-works', 'uses' => function ()    {
	return view('company.howItWorks');
}));

/*Route::get('/', function ()    {
	return view('greeting', ['name' => 'James']);
});*/

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);