<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    //protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'user_name',
        'telephone_number',
        'password',
        'register_token',
        'register_token_sent',
        'forgot_token',
        'forgotten_pass_token_sent'
    ];

    public function __construct(array $attributes = array())
    {
        //We want to set a default to some columns. We will do it here.
        $this->setRawAttributes([
            'forgot_token' => \md5(\Carbon\Carbon::now())
        ], true);
        parent::__construct($attributes);
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password'];

    public function getUserByUsername($userName)
    {
        $first = $this->where('user_name', '=', $userName)->first();
        if (!is_null($first))
        {
            return $first->get();
        }
        return null;
    }

    public function getUserByEmail($email)
    {
        $first = $this->where('email', '=', $email)->first();
        if (!is_null($first))
        {
            return $first->get();
        }
        return null;
    }

    public function updateUser($where, $whereValue, $data)
    {
        return $this->where($where, $whereValue)
            ->update($data);
    }

    public function getUserByForgotPasswordHash($forgotToken)
    {
        $result = $this->where('forgot_token', '=', $forgotToken)->first();
        if (!is_null($result))
        {
            return $result;
        }
        return null;
    }
}
