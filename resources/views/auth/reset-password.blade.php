@extends($layout)
@section('content')
    @if($user_found)
        {{-- User found, we will show the reset password form --}}
        {!! Form::open(array(
            'class'=>"pure-form pure-form-stacked")) !!}
        <fieldset>
            <h1>Reset password</h1>

            <div>
                {!! $errors->first('password') !!}
                {!! $errors->first('confirm_password') !!}
            </div>

            <div>
                {!! Form::label('password', 'Password') !!}
                {!! Form::password('password') !!}
            </div>

            <div>
                {!! Form::label('confirm_password', 'Confirm password') !!}
                {!! Form::password('confirm_password') !!}
            </div>

            <div>
                {!! Form::hidden('forgot_token', $forgot_token) !!}
            </div>

            <p>{!! Form::submit('Submit', array('class'=>'pure-button pure-button-primary narrow-submit-button')) !!}</p>
        </fieldset>
        {!! Form::close() !!}
    @else
        {{-- User was not found --}}
        <p>Either that hash has expired or user no longer exists here.</p>
        <p>Do you want to try again? {!! Html::link('forgot','Forgot password') !!}
        </p>
    @endif
@stop