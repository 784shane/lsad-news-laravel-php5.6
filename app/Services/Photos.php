<?php namespace App\Services;

use Input;
use PhpSpec\Exception\Exception;

class Photos extends Input
{
    /**
     * Array of all the file extensions which will be allowed.
     *
     * @var string
     */
    private static $acceptablePhotoExtensions = ['jpg', 'jpeg', 'png', 'gif', 'bmp'];
    /**
     * path to temp folder where images will be stored. Calculated from __DIR__.
     *
     * @var string
     */
    private static $temporaryPhotoDirectory = 'photos/temp';
    /**
     * Time string. - the Form's time. When the form was created.
     *
     * @var string
     */
    private $timeFormSubmitted = '';
    /**
     * Session token. - the Form's session token. When the form was created.
     *
     * @var string
     */
    private $formToken = '';
    /**
     * The file name that is in the folder. The filename that was stored.
     *
     * @var string
     */
    private $uploadedFileName = '';
    /**
     * The order of the item in the listing. Zero(0) is the first item.
     *
     * @var integer
     */
    private $item_order_number = null;

    public function attemptPhotoUpload($imagePrefix = null)
    {
        $this->seekAndUploadImageFiles($imagePrefix, self::$temporaryPhotoDirectory);
        echo json_encode([
            'result' => 'success',
            'file_name' => $this->uploadedFileName
        ]);
    }

    public function resetUploadVariables()
    {
        $this->formToken = '';
        $this->timeFormSubmitted = '';
    }

    public function uploadPhotoWhenEditing(){

        $filesUploaded = [];

        // Upload the files
        $this->seekAndUploadImageFiles('file_item_image_', self::$temporaryPhotoDirectory);

        // See if the main image file was uploaded.
        $uploadedFiles = self::file();
        if(isset($uploadedFiles['update_main_image_file'])){

            $main_imageName = $this->getNewFileName(self::file('update_main_image_file')->getClientOriginalName());
            if(file_exists(realpath(self::$temporaryPhotoDirectory.'/'.$main_imageName))){
                $filesUploaded['main_image'] = $main_imageName;
            }
        }

        return $filesUploaded;
    }

    private function seekAndUploadImageFiles($imagePrefix, $destinationDirectory)
    {
        //$fileToUpload = self::get('file_being_uploaded');
        foreach(self::file() as $elementKey => $elementValue)
        {
            //if ($fileToUpload == $elementKey)//preg_match('/^' . $imagePrefix . '/', $elementKey))
            //{
            // First of all, we will reset the variables which we will test
            // as to ensure unique upload record is maintained.
            $this->resetUploadVariables();
            // save the order number
            $this->item_order_number = $this->extractItemOrderNumber($imagePrefix, $elementKey);
            $newFileName = $this->attemptSinglePhotoUpload($elementKey, $destinationDirectory);
            if ($newFileName)
            {
                // Since the file was saved, we will make keep a record of it
                // in the database.
                $this->recordPhotoUpload();
            }
            //}
        }
    }

    private function attemptSinglePhotoUpload($fileKey, $destinationDirectory)
    {
        if (self::file($fileKey)->getClientOriginalName())
        {
            // We have an image
            $extension = $this->getFileExtension(self::file($fileKey)->getClientOriginalName());
            if (in_array($extension, self::$acceptablePhotoExtensions))
            {
                // This extension is allowed so lets go ahead and move the file
                try
                {
                    $newFileName = $this->getNewFileName(self::file($fileKey)->getClientOriginalName());
                    if($this->movePhoto(
                        $destinationDirectory,
                        $fileKey, $newFileName)){
                        return $newFileName;
                    }
                }
                catch(Exception $e)
                {
                    // Failed so we will have to show an error here.
                    //exit('Error occurred while attempting to upload file.');
                    return null;
                }
            }
            else
            {
                // @Todo: We will return the error code here
                //exit('not an acceptable extension');
                return null;
            }
        }
        // @Todo: We will return the error code here
        //exit('no file uploaded');
        return null;
    }

    private function getFileExtension($fileName)
    {
        preg_match('/\.[^\.]+$/', $fileName, $match);
        if (!empty($match))
        {
            return strtolower(preg_replace('/^\./', "", end($match)));
        }
        return "";
    }

    private function movePhoto($directory, $fileName, $newFileName)
    {
        return self::file($fileName)->move($directory, $newFileName);
    }

    private function getNewFileName($originalFileName)
    {
        // Check the posts to see if we can get the form's timestamp
        // and its token.
        $this->timeFormSubmitted = !empty(self::get('form_time'))?self::get('form_time'):'';
        $this->formToken = !empty(self::get('_token'))?self::get('_token'):'';
        $this->uploadedFileName = strtolower($this->timeFormSubmitted . '_' . $this->formToken . '_' . $originalFileName);
        return $this->uploadedFileName;
    }

    private function extractItemOrderNumber($imagePrefix, $elementKey)
    {
        $orderNumber = trim(str_replace($imagePrefix, "", $elementKey));
        if (is_numeric($orderNumber))
        {
            // just to make sure
            preg_match_all('!\d+!', $orderNumber, $matches);
            if (!empty($matches))
            {
                $lastElement = end($matches);
                return (int)$lastElement[0];
            }
            return null;
        }
        return (int)$orderNumber;
    }

    private function recordPhotoUpload()
    {
        // Let's make sure that this upload record doesn't already exist
        // If it does, we will update it for we do not want a duplicate
        // record for the same photo upload
        if ($this->formToken != '' && $this->timeFormSubmitted != '')
        {
            $timeStamp = $this->createTimeStamp($this->timeFormSubmitted);
            $tempUploadsTable = \App\TemporaryUploads::firstOrNew(array(
                'form_token' => $this->formToken,
                'form_time' => $timeStamp,
                'item_order_no' => $this->item_order_number,
            ));
            //$tempUploadsTable = new \App\TemporaryUploads();
            $tempUploadsTable->form_token = $this->formToken;
            $tempUploadsTable->form_time = $timeStamp;
            $tempUploadsTable->file_name = $this->uploadedFileName;
            $tempUploadsTable->item_order_no = $this->item_order_number;
            return $tempUploadsTable->save();
        }
        return false;
    }

    private function createTimeStamp($timeString)
    {
        return date('Y-m-d H:i:s', $timeString);
    }
}