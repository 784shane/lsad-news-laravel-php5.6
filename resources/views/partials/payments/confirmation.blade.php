<h1>Payment success</h1>
<p>Thank you for your payment. We have now displayed the transporter's details below.</p>
<p>Momentarily, you will receive an email with this information as well.</p>
<p>You can now make contact and have your job completed.</p>
<h4>Good luck</h4>

@if(isset($displayablePaymentInfo)&&!empty($displayablePaymentInfo))
<div class="description-box payment-information-box">
    <div class="description-box-inner">
        <h4>Payment information</h4>
        <div class="particulars">
            <div><b>Amount paid</b></div>
            <div>{{  $displayablePaymentInfo['amt_paid'].' '.$displayablePaymentInfo['paid_currency'] }}</div>
            <div><b>Paid via credit card ...</b></div>
            <div>{{  $displayablePaymentInfo['cc_masked_long_number'] }}</div>
            <div><b>Date paid</b></div>
            <div>{{ date('d M Y, H:i:s', $displayablePaymentInfo['paid_time'] ) }}</div>
        </div>
    </div>
</div>
@endif

@if(!empty($delivery_info))
<div class="description-box payment-information-box">
    <div class="description-box-inner">
        <h4>Delivery information</h4>
        <div class="particulars">
            <div class="picture">{!! Html::image($vComposerHelper->missingImageResolver($vComposerHelper->getIfElementExists($delivery_info[0],'overallImage')), null, array('width'=>'100%')) !!}</div>
            <div><b>Delivery:</b></div>
            <div>{{ $vComposerHelper->getIfElementExists($delivery_info[0],'subtitle') }}</div>
            <div><b>Collection from:</b></div>
            <div>{{ $vComposerHelper->getIfElementExists($delivery_info[0],'collection_address') }}</div>
            <div><b>Delivery to:</b></div>
            <div>{{ $vComposerHelper->getIfElementExists($delivery_info[0],'delivery_address') }}</div>
            <div><b>Distance:</b></div>
            <div>{{ $vComposerHelper->getReadableMileage($vComposerHelper->getIfElementExists($delivery_info[0],'distance_metres'))  }}</div>
            <div class="particulars-items-container">
                <h4>Items to be delivered:</h4>
                <div class="particulars-items">
                    @if(isset($delivery_info[0]['items'])&&!empty($delivery_info[0]['items']))
                        @for($i=0; $i<count($delivery_info[0]['items']); $i++)
                            <div class="particulars-item-block">
                                <h4>Item {{($i+1)}}</h4>
                                <div class="picture">{!! Html::image($vComposerHelper->missingImageResolver($vComposerHelper->getIfElementExists($delivery_info[0]['items'][$i],'main_image')), null, array('width'=>'100%')) !!}</div>
                                <div><b>Item name:</b></div>
                                <div>{{ $vComposerHelper->getIfElementExists($delivery_info[0]['items'][$i],'name') }}</div>
                                <div><b>Item description:</b></div>
                                <div>{{ $vComposerHelper->getIfElementExists($delivery_info[0]['items'][$i],'description') }}</div>
                            </div>
                        @endfor
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if(!empty($quote_info))
<div class="description-box payment-information-box">
    <div class="description-box-inner">
        <h4>Transporter's information</h4>
        <div class="particulars">
            <div class="picture"><img src="http://placehold.it/100x60" width="100%"></div>
            <div><b>Transport provider</b></div>
            <div>{{ $vComposerHelper->getIfElementExists($quote_info[0],'user_name') }}</div>
            <div><b>Email</b></div>
            <div>{{ $vComposerHelper->getIfElementExists($quote_info[0],'email') }}</div>
            <div><b>Telephone</b></div>
            <div>{{ $vComposerHelper->getIfElementExists($quote_info[0],'telephone_number') }}</div>
        </div>
    </div>
</div>
@endif