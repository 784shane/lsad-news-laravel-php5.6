<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryItems extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'delivery', 
        'name', 
        'ebay_id',
        'main_image'
    ];

    /**
    * getDeliveriesItemsByDeliveryId
    * @param int
    * @return Illuminate\Database\Eloquent\Collection
    **/
    public function getDeliveriesItemsByDeliveryId($deliveryId){
        return $this->where('delivery', '=', $deliveryId)->get();
    }

    /**
     * deleteDeliveryMessagesById
     * @param int
     **/
    public function deleteDeliveryItemsByDeliveryId($deliveryId){
        return $this->where('delivery', $deliveryId)->delete();
    }
}
