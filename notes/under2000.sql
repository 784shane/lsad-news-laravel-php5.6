-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2015 at 10:47 PM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `under2000`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
`id` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `post_code` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `user`, `title`, `description`, `price`, `post_code`, `category`, `created_at`, `updated_at`) VALUES
(1, 1, 'This is the title', 'This is the description of my ad', 1499, 'SW2 2HT', 'vehicle', '2015-06-08 16:32:45', '2015-06-08 16:32:45'),
(2, 1, 'This is the title1', 'This is the description of my ad', 1499, 'SW2 2HT', 'vehicle', '2015-06-08 16:33:03', '2015-06-08 16:33:03'),
(3, 1, 'This is the title2', 'This is the description of my ad', 1499, 'SW2 2HT', 'vehicle', '2015-06-08 16:33:23', '2015-06-08 16:33:23'),
(4, 1, 'This is the title4', 'This is the description of my ad', 1499, 'SW2 2HT', 'vehicle', '2015-06-08 16:33:39', '2015-06-08 16:33:39'),
(5, 1, 'This is the title5', 'This is the description of my ad', 1499, 'SW2 2HT', 'vehicle', '2015-06-08 16:33:51', '2015-06-08 16:33:51'),
(6, 1, 'This is the title6', 'This is the description of my ad', 1499, 'SW2 2HT', 'vehicle', '2015-06-08 16:34:03', '2015-06-08 16:34:03'),
(7, 1, 'This is the title6', 'This is the description of my ad', 1499, 'SW2 2HT', 'vehicle', '2015-06-08 16:34:03', '2015-06-08 16:34:03'),
(8, 1, 'This is the title6', 'This is the description of my ad', 1499, 'SW2 2HT', 'vehicle', '2015-06-08 16:34:03', '2015-06-08 16:34:03'),
(9, 1, 'This is the title6', 'This is the description of my ad', 1499, 'SW2 2HT', 'vehicle', '2015-06-08 16:34:03', '2015-06-08 16:34:03'),
(10, 1, 'This is the title6', 'This is the description of my ad', 1499, 'SW2 2HT', 'vehicle', '2015-06-08 16:34:03', '2015-06-08 16:34:03');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2015_06_03_221723_create_vehicles_table', 1),
('2015_06_03_222024_create_ads_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `register_token` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `register_token_sent` tinyint(1) NOT NULL,
  `forgotten_pass_token_sent` tinyint(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `register_token`, `register_token_sent`, `forgotten_pass_token_sent`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'John', 'Doe', 'vibes@fadmail.com', '$2y$10$TomhPHc.MG8JywxkNlNNbOTe51IZQSIijNip2VBZWw7xI8.XpcXn2', '400616c0f1aca28d13dffba15b60762a', 1, 0, 'FmiGpSXzwoNL6jiIwIp5FhMqkKfzOXiRF97CZ8K3FOrPYrI3Ygr7qWA38TBJ', '2015-06-08 16:31:43', '2015-07-16 06:19:01');

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE IF NOT EXISTS `vehicles` (
`id` int(10) unsigned NOT NULL,
  `ad` int(11) NOT NULL,
  `reg_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `make` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year` int(4) NOT NULL,
  `fuel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transmission` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `engine_size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mileage` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `ad`, `reg_no`, `model`, `make`, `year`, `fuel`, `body_type`, `transmission`, `engine_size`, `color`, `mileage`, `created_at`, `updated_at`) VALUES
(1, 1, 'WG59URT', '9-3', 'Saab', 2009, 'Petrol', 'convertible', 'manual', '2000', 'silver', 10000, '2015-06-08 16:32:45', '2015-06-08 16:32:45'),
(2, 2, 'WG59URT', '9-3', 'Saab', 2009, 'Diesel', 'convertible', 'manual', '2000', 'silver', 22, '2015-06-08 16:33:03', '2015-06-08 16:33:03'),
(3, 3, 'WG59URT', '9-3', 'Saab', 2013, 'Diesel', 'convertible', 'manual', '2000', 'silver', 83000, '2015-06-08 16:33:23', '2015-06-08 16:33:23'),
(4, 4, 'WG59URT', '9-3', 'Saab', 2009, 'Petrol', 'convertible', 'manual', '2000', 'silver', 83000, '2015-06-08 16:33:39', '2015-06-08 16:33:39'),
(5, 5, 'WG59URT', '9-3', 'Saab', 2009, 'Petrol', 'convertible', 'manual', '2000', 'silver', 83000, '2015-06-08 16:33:51', '2015-06-08 16:33:51'),
(6, 6, 'WG59URT', '9-3', 'Saab', 2012, 'Other', 'convertible', 'manual', '2000', 'silver', 183000, '2015-06-08 16:34:03', '2015-06-08 16:34:03'),
(8, 7, 'WG59URT', '9-3', 'Saab', 2004, 'Other', 'convertible', 'manual', '2000', 'silver', 183000, '2015-06-08 16:34:03', '2015-06-08 16:34:03'),
(9, 8, 'WG59URT', '9-3', 'Saab', 2006, 'Petrol', 'convertible', 'manual', '2000', 'silver', 9000, '2015-06-08 16:34:03', '2015-06-08 16:34:03'),
(10, 9, 'WG59URT', '9-3', 'Saab', 2005, 'Other', 'convertible', 'manual', '2000', 'silver', 183000, '2015-06-08 16:34:03', '2015-06-08 16:34:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
 ADD PRIMARY KEY (`id`), ADD KEY `ads_user_foreign` (`user`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`), ADD UNIQUE KEY `users_register_token_unique` (`register_token`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `vehicles_ad_unique` (`ad`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ads`
--
ALTER TABLE `ads`
ADD CONSTRAINT `ads_user_foreign` FOREIGN KEY (`user`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
