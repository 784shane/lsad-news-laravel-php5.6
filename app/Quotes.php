<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use \App\Deliveries;

class Quotes extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    //protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quote',
        'original_quote',
        'our_fee',
        'delivery',
        'with_message',
        'message',
        'sender',
        'receiver',
        'new_quote',
        'quote_accepted'
    ];

    public function delivery()
    {
        //return $this->belongsTo('Deliveries');
        return $this->hasMany('Friend');
    }

    /**
     * deleteDeliveryMessagesById
     * @param int
     **/
    public function deleteQuotesByByDeliveryId($deliveryId){
        return $this->where('delivery', $deliveryId)->delete();
    }
}
