<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A layout example that shows off a responsive product landing page.">

    <title>Landing Page &ndash; Layout Examples &ndash; Pure</title>

    <link rel="stylesheet" href="{{ asset('css/sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/jquery-colorbox/example3/colorbox.css') }}">


    <!-- <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">-->
    <link rel="stylesheet" href="{{ asset('css/pure/pure.min.css') }}">
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
    <![endif]-->
    <!--[if gt IE 8]><!-->
    <!--<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css">-->
    <link rel="stylesheet" href="{{ asset('css/pure/grids-responsive-min.css') }}">
    <!--<![endif]-->

    <!--<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">-->
    <link rel="stylesheet" href="{{ asset('css/pure/font-awesome.css') }}">

    <!--[if lte IE 8]>
    <link rel="stylesheet" href="{{ asset('css/pure-layouts/marketing-old-ie.css') }}">
    <![endif]-->
    <!--[if gt IE 8]><!-->
    <link rel="stylesheet" href="{{ asset('css/pure-layouts/marketing.css') }}">
    <!--<![endif]-->
    <link rel="stylesheet" href="{{ asset('css/overrides.css') }}">
    <!-- <link rel="stylesheet" href="{{-- asset('css/main.css') --}}"> -->
    <script>
        var site_url = '{{Request::root ()}}';
    </script>
</head>
<body>

<div class="header">
    <div class="home-menu pure-menu pure-menu-horizontal pure-menu-fixed">
        {!! Html::link(url(''), 'Purple Mover', array("class"=>"pure-menu-heading")) !!}

        <ul class="pure-menu-list">
            <li class="pure-menu-item {{ $vLayoutsHelper->setActiveClass('home') }}">
                {!! Html::link(url('home'), 'Home', array("class"=>"pure-menu-link")) !!}</li>
            <li class="pure-menu-item {{ $vLayoutsHelper->setActiveClass('get_quote') }}">
                {!! Html::link(url(''), 'Get a Quote', array("class"=>"pure-menu-link")) !!}</li>
            <li class="pure-menu-item {{ $vLayoutsHelper->setActiveClass('delivery_jobs') }}">
                {!! Html::link(url('deliveries/jobs'), 'View Delivery Jobs', array("class"=>"pure-menu-link")) !!}</li>
            <li class="pure-menu-item {{ $vLayoutsHelper->setActiveClass('sign_up_as_transporter') }}">
                {!! Html::link(url('signup'), 'Sign Up as a Transporter', array("class"=>"pure-menu-link")) !!}</li>
            <li class="pure-menu-item {{ $vLayoutsHelper->setActiveClass('login') }}">
                @if(\Auth::check())
                    {!! Html::link(url('logout'), 'Logout', array("class"=>"pure-menu-link")) !!}
                @else
                    {!! Html::link(url('login'), 'Login', array("class"=>"pure-menu-link")) !!}
                @endif
            </li>
        </ul>
    </div>
</div>

<div class="content-wrapper">
    <div class="content">
        {{-- Should we define an accounts page menu, we will place it here. --}}
        @yield('account_page_menu')
        {{-- End of accounts page menu --}}
        @yield('content')
    </div>

    <!--<div class="footer l-box is-center">-->
    <div class="footer l-box">
        <div class="pure-g">
            <div class="pure-u-13-24">
                <div class="pure-g">
                    <div class="pure-u-1-3">
                        <div class="footer-pocket">
                            <div class="footer-pocket-header">COMPANY</div>
                            <div class="footer-pocket-body">
                                <div>{!! Html::link(route('about-us'), 'About Us', array("class"=>"")) !!}</div>
                                <div>{!! Html::link(url('#'), 'Customer Services', array("class"=>"")) !!}</div>
                                <div>{!! Html::link(route('contact-us'), 'Contact Us', array("class"=>"")) !!}</div>
                                <div>{!! Html::link(url('#'), 'Enquires', array("class"=>"")) !!}</div>
                            </div>
                        </div>
                    </div>
                    <div class="pure-u-1-3">
                        <div class="footer-pocket">
                            <div class="footer-pocket-header">QUICK LINKS</div>
                            <div class="footer-pocket-body">
                                <div>{!! Html::link(url('#'), 'Help', array("class"=>"")) !!}</div>
                                <div>{!! Html::link(route('how-it-works'), 'How it Works', array("class"=>"")) !!}</div>
                                <div>{!! Html::link(url('#'), 'Frequently Asked Questions', array("class"=>"")) !!}</div>
                            </div>
                        </div>
                    </div>
                    <div class="pure-u-1-3">
                        <div class="footer-pocket">
                            <div class="footer-pocket-header">LEGAL</div>
                            <div class="footer-pocket-body">
                                <div>{!! Html::link(url('#'), 'Privacy Policy', array("class"=>"")) !!}</div>
                                <div>{!! Html::link(url('#'), 'Terms and Conditions', array("class"=>"")) !!}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pure-u-11-24">
                <div class="pure-g">
                    <div class="pure-u-3-5">
                        <div class="footer-pocket">
                            <div class="footer-pocket-header">SECURE CHECKOUT</div>
                            <div class="footer-pocket-body">
                                <a href="http://www.credit-card-logos.com"><img alt="<br />
<b>Notice</b>:  Undefined variable: alt_tag in <b>/home/ccl606/public_html/index.html</b> on line <b>119</b><br />
" title="<br />
<b>Notice</b>:  Undefined variable: alt_tag in <b>/home/ccl606/public_html/index.html</b> on line <b>119</b><br />
" src="http://www.credit-card-logos.com/images/multiple_credit-card-logos-1/paypal_mc_visa_amex_disc_210x80.gif" width="210" height="80" border="0" /></a>
                            </div>
                        </div>
                    </div>
                    <div class="pure-u-2-5">
                        <div class="footer-pocket">
                            <div class="footer-pocket-header">FOLLOW US</div>
                            <div class="footer-pocket-body">
                                <div class="social_icon"><a href="https://www.facebook.com/purplemover/" target="_blank"><img src="{{ asset('images/my-icons-collection/png/social-1.png') }}" width="100%"/></a></div>
                                <div class="social_icon"><a href="https://twitter.com/purplemover/" target="_blank"><img src="{{ asset('images/my-icons-collection/png/social.png') }}" width="100%"/></a></div>
                                <div class="social_icon"><a href="https://www.instagram.com/purplemover/" target="_blank"><img src="{{ asset('images/my-icons-collection/png/social-2.png') }}" width="100%"/></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script src="{{ asset('js/libraries/sweetalert/sweetalert.min.js') }}"></script>
<!-- Include this after the sweet alert js file -->
@include('sweet::alert')

<script type="text/javascript" src="{{ asset('js/libraries/handlebars-v4.0.5.js') }}"></script>
<script src="{{ asset('templates/todo.tpl.js') }}"></script>
<script type="text/x-mustache" src="{{ asset('templates/ebay-single-items.html') }}"></script>
<!-- <script type="text/javascript" src="{{-- asset('js/libraries/jquery_1_8_2.js') --}}"></script> -->
<!-- Courtesy the script from - http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js -->
<script src="{{ asset('js/jquery_1.7.js') }}"></script>
<script src="{{ asset('bower_components/jquery-colorbox/jquery.colorbox-min.js') }}"></script>
<!-- Courtesy the script from - http://malsup.github.com/jquery.form.js -->
<script src="{{ asset('js/jquery.form.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
{{-- Any javascripts that we will be adding. Eg - view scripts. --}}
@yield('scripts')
@yield('google_maps_scripts')


</body>
</html>

