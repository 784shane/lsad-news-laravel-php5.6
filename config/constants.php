<?php
return [
    'fees' => [
        'MINIMUM'=>'30',
        'PERCENTAGE'=>'10'
    ],

    'deliveries' => [
        'statuses' => [
            'ARCHIVED' => 4,
            'EXPIRED' => 3,
            'ACTIVE_PRIVATE' => 2,
            'ACTIVE_PUBLIC' => 1
        ],
        'expiry' => [
            'extension_reason' => [
                'DELIVERY_INSERTION' => 1,
                'QUOTE_MADE' => 2,
                'QUOTE_ACCEPTED' => 3
            ],
            'time_extension' => [
                // Add date in seconds
                'TIME_FROM_INSERTION' => 216000,
                'TIME_AFTER_QUOTE_MADE' => 172800,
                'TIME_AFTER_QUOTE_ACCEPTED' => 86400
            ],
            'expiry_checks' => [
                'EXPIRY_CHECK_INTERVAL' => 86400,
                'EXPIRY_CHECK_LIMIT' => 200
            ]
        ]
    ]
];