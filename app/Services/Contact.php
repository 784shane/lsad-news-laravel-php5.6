<?php namespace App\Services;

use SiteValidator;

class Contact
{
    public function submitEnquiry(){
        // Retrieve the data that was submitted.
        $inputs = \Request::all();
        $validatorResults = $this->validateNewDeliveriesUser($inputs);
        if ($validatorResults->fails())
        {
            $validationReturn['validatorResults'] = $validatorResults;
            $validationReturn['result'] = 'fail';
            $validationReturn['error_messages'] = $validatorResults->messages();
        }else
        {
            $validationReturn['result'] = 'success';
            // Continue saving the information
            $validationReturn['data_saved'] = $this->saveContactInfo($inputs);
        }
        return $validationReturn;
    }

    public function validateNewDeliveriesUser($data)
    {
        $contactUsValidation = SiteValidator::contactUsValidationSettings();
        return \Validator::make($data, $contactUsValidation['validation_requirements_array'],
            //insert our custom messages
            $contactUsValidation['validation_messages']
        );
    }

    private function saveContactInfo($inputs){
        return \App\Enquiries::create($inputs);
    }
}