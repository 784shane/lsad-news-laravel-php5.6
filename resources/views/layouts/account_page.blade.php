@extends('layouts.master')

@section('account_page_menu')
    <div>
        <div class="site-secondary-menu pure-menu pure-menu-horizontal">
            <ul class="pure-menu-list">
                <li class="pure-menu-item">{!! Html::link(\URL::route('home.deliveries.archived'),'Past deliveries', array('class'=>"pure-menu-link")) !!}</li>
                <li class="pure-menu-item">
                    {!! Html::link(\URL::route('home.deliveries.active'),'Active deliveries', array('class'=>"pure-menu-link")) !!}</li>
                <li class="pure-menu-item">{!! Html::link('messages','Messages ('. $vLayoutsHelper->getNewMessageCount() .')', array('class'=>"pure-menu-link")) !!}</li>
                <li class="pure-menu-item">{!! Html::link('my-account','My account', array('class'=>"pure-menu-link")) !!}</li>
            </ul>
        </div>
    </div>
@stop