var deliveriesEditService = function (httpService) {

    this.httpService = httpService;

    this.init = function () {
        this.setupClickEvents();
    };

    this.addItem = function () {

        this.addNewItem = function () {
            var existingNumberOfChildren = this.getExistingNumberOfItems();

            var itemHtml = this.getNewItemHtmlTemplate(existingNumberOfChildren, (existingNumberOfChildren + 1));

            this.appendItemHtml(itemHtml);
        };

        this.getNewItemHtmlTemplate = function(itemIndex, itemNumber){
            var source = $("#deliveryItemSingleTemplate").html();
            var template = Handlebars.compile(source);

            var itemHtml = template({
                the_item_list_index: itemIndex,
                the_item_list_number: itemNumber
            });
            return itemHtml;
        };

        this.removeItem = function (div) {
            var itemToDelete = $(div).data('item-count');
            var newCount = 0;
            $('#edit-items-container').children().each(function ($index, $item) {
                if (itemToDelete == $index) {
                    // delete the item
                    $item.remove();
                }
            });
        };

        this.appendItemHtml = function (html) {
            $('#edit-items-container').append(html);
        };

        this.getExistingNumberOfItems = function () {
            return $('#edit-items-container').children().length;
        };

        return this;
    };

    this.setupClickEvents = function () {

        var self = this;
        var addItem = new self.addItem();

        $('.delete-item-button').click(function () {
            // Looks like a request has come to delete a delivery item.
            // Lets attempt that now.
            self.proceedToConfirmDelete(addItem, this);
        });

        $('#add-item-cta').click(function (evt) {
            evt.preventDefault();
            addItem.addNewItem();
        });
    };

    this.proceedToConfirmDelete = function (addItem, deleteButton) {

        swal({
            title: "Are you sure?",
            text: "This item will be deleted. You will not be able to recover it.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }, function () {
            addItem.removeItem(deleteButton);
        });

    };

};

$(document).ready(function () {

    // Start the ebayService service.
    var editService = new deliveriesEditService(new httpService());
    editService.init();

});