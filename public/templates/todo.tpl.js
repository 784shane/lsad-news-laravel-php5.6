(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['todo'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<h3>"
    + alias4(((helper = (helper = helpers.freya || (depth0 != null ? depth0.freya : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"freya","hash":{},"data":data}) : helper)))
    + "</h3>\r\n<p>Created by: "
    + alias4(((helper = (helper = helpers.kat || (depth0 != null ? depth0.kat : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"kat","hash":{},"data":data}) : helper)))
    + "</p>";
},"useData":true});
})();