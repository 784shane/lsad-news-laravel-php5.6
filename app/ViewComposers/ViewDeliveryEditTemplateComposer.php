<?php
namespace App\ViewComposers;

use Illuminate\Contracts\View\View;

class ViewDeliveryEditTemplateComposer
{

    private $deliveryEditTemplateType = "";

    private $vComposerHelper = null;

    private $deliveryData = null;

    private $unitsOfMeasurement = null;

    private $deliveryEditIndex = "";

    public function __construct()
    {
        $this->vComposerHelper = new ViewHelperComposer();;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(array(
            'vDeliveryEditTemplateComposer' => $this
        ));
    }

    private function getCorrectIndex(){
        switch($this->deliveryEditTemplateType){
            case 'handlebars':{
                return '{{ the_item_list_index }}';
                break;
            }
            default:{
                return $this->deliveryEditIndex;
            }
        }
        return null;
    }

    private function getCorrectNumber(){
        switch($this->deliveryEditTemplateType){
            case 'handlebars':{
                return '{{ the_item_list_number }}';
                break;
            }
            default:{
                return $this->deliveryEditIndex + 1;
            }
        }
        return null;
    }

    private function getDeliveryData($dataType){
        switch($this->deliveryEditTemplateType){
            case 'php':{
                switch($dataType){
                    case "item_name_":{
                        return $this->deliveryData[0]['items'][$this->getCorrectIndex()]['name'];
                        break;
                    }
                    case "item_length_":{
                        return $this->deliveryData[0]['items'][$this->getCorrectIndex()]['item_length'];
                        break;
                    }
                    case "item_width_":{
                        return $this->deliveryData[0]['items'][$this->getCorrectIndex()]['item_width'];
                        break;
                    }
                    case "item_height_":{
                        return $this->deliveryData[0]['items'][$this->getCorrectIndex()]['item_height'];
                        break;
                    }
                    case "item_weight_":{
                        return $this->deliveryData[0]['items'][$this->getCorrectIndex()]['item_weight'];
                        break;
                    }
                    case "item_description_":{
                        return $this->deliveryData[0]['items'][$this->getCorrectIndex()]['description'];
                        break;
                    }
                    case "measured_length_":{
                        return $this->deliveryData[0]['items'][$this->getCorrectIndex()]['measured_length'];
                        break;
                    }
                    case "measured_width_":{
                        return $this->deliveryData[0]['items'][$this->getCorrectIndex()]['measured_width'];
                        break;
                    }
                    case "measured_height_":{
                        return $this->deliveryData[0]['items'][$this->getCorrectIndex()]['measured_height'];
                        break;
                    }
                    case "measured_weight_":{
                        return $this->deliveryData[0]['items'][$this->getCorrectIndex()]['measured_weight'];
                        break;
                    }
                }
                break;
            }
            default:{
                return null;
            }
        }
        return null;
    }

    private function getDeleteButton(){
        switch($this->deliveryEditTemplateType){
            case 'php':{

                //echo 'index - '.$this->deliveryEditIndex;

                if(count($this->deliveryData[0]['items']) > 1){
                    //{{ $delivery[0][\\'id\\'] }}
                    //{{ $delivery[0][\\'items\\'][$i][\\'id\\']  }}
                    return '<div class="delete-item-button" data-delivery-id=""
                                         data-item-id="" data-item-count="'.$this->deliveryEditIndex.'">delete</div>';
                }

                return '';

                return \Html::image($this->vComposerHelper->missingImageResolver($this->deliveryData[0]['items'][$this->getCorrectIndex()]['main_image']), $this->deliveryData[0]['items'][$this->getCorrectIndex()]['name'], array('width'=>'100%'));

                /*
                <div class="delete-item-button" data-delivery-id="{{ $delivery[0][\'id\'] }}"
                                         data-item-id="{{ $delivery[0][\'items\'][$i][\'id\']  }}">delete
                                    </div>

                */


                /*return \Html::image($this->vComposerHelper->missingImageResolver($this->deliveryData[0]['items'][$this->getCorrectIndex()]['main_image']), $this->deliveryData[0]['items'][$this->getCorrectIndex()]['name'], array('width'=>'100%'));*/
                break;
            }
            default:{
                return '';
            }
        }
    }

    private function getImage(){
        switch($this->deliveryEditTemplateType){
            case 'php':{
                return \Html::image($this->vComposerHelper->missingImageResolver($this->deliveryData[0]['items'][$this->getCorrectIndex()]['main_image']), $this->deliveryData[0]['items'][$this->getCorrectIndex()]['name'], array('width'=>'100%'));
                break;
            }
            default:{
                return '';
            }
        }
    }

    private function getLabel($labelType){
        $name = $value = null;
        switch($labelType){
            case "items_count_":{
                $name = 'items_count_';
                $value = 'Item no. '.$this->getCorrectNumber();
                break;
            }
            case "main_image_":{
                $name = 'main_image';
                $value = 'Item image';
                break;
            }
            case "file_item_image_":
            {
                $name = 'file_item_image_' . $this->getCorrectIndex();
                $value = 'Update item image';
                break;
            }
            case "use_as_main_image_":{
                $name = 'use_as_main_image_'.$this->getCorrectIndex();
                $value = 'Use as main image';
                break;
            }
            case "item_name_":{
                $name = 'item_name_'.$this->getCorrectIndex();
                $value = 'Item title';
                break;
            }
            case "item_description_":{
                $name = 'item_description_'.$this->getCorrectIndex();
                $value = 'Item description';
                break;
            }
            case "item_dimensions_":{
                $name = 'item_dimensions_'.$this->getCorrectIndex();
                $value = 'Item dimensions';
                break;
            }
            case "item_length_":{
                $name = 'item_length_'.$this->getCorrectIndex();
                $value = 'Item length';
                break;
            }
            case "item_width_":{
                $name = 'item_width_'.$this->getCorrectIndex();
                $value = 'Item width';
                break;
            }
            case "item_height_":{
                $name = 'item_height_'.$this->getCorrectIndex();
                $value = 'Item height';
                break;
            }
            case "item_weight_":{
                $name = 'item_weight_'.$this->getCorrectIndex();
                $value = 'Item weight';
                break;
            }
        }
        return \Form::label($name, $value);
    }

    private function getTextBox($textBoxType){
        $name = $value = null;
        switch($textBoxType){
            case "item_name_":{
                $name = 'item_name_'.$this->getCorrectIndex();
                $value = $this->getDeliveryData('item_name_');
                break;
            }
            case "item_length_":{
                $name = 'item_length_'.$this->getCorrectIndex();
                $value = $this->getDeliveryData('item_length_');
                break;
            }
            case "item_width_":{
                $name = 'item_width_'.$this->getCorrectIndex();
                $value = $this->getDeliveryData('item_width_');
                break;
            }
            case "item_height_":{
                $name = 'item_height_'.$this->getCorrectIndex();
                $value = $this->getDeliveryData('item_height_');
                break;
            }
            case "item_weight_":{
                $name = 'item_weight_'.$this->getCorrectIndex();
                $value = $this->getDeliveryData('item_weight_');
                break;
            }
        }

        return \Form::text($name, $value);
    }

    private function getTextareaBox($textBoxType){
        $name = $value = null;
        switch($textBoxType){
            case "item_description_":{
                $name = 'item_description_'.$this->getCorrectIndex();
                $value = $this->getDeliveryData('item_description_');
                break;
            }
        }

        return \Form::textarea($name, $value);
    }

    private function getFileBox($textBoxType){
        $name = null;
        switch($textBoxType){
            case "file_item_image_":{
                $name = 'file_item_image_'.$this->getCorrectIndex();
                break;
            }
        }

        return \Form::file($name);
    }

    private function getRadioBox(){
        $default = false;

        switch($this->deliveryEditTemplateType){
            case "php":{
                $default = (isset($this->deliveryData[0]['use_as_main_image']) && ($this->deliveryData[0]['use_as_main_image']==$this->getCorrectIndex()))?true:false;
                break;
            }
        }

        return \Form::radio('use_as_main_image', $this->getCorrectIndex(), $default, array('id'=>'use_as_main_image_'.$this->getCorrectIndex(), 'class'=>'use-as-main-image'));

    }

    private function getUnitsSelectBox($selectBoxType){
        $name = $units = $default = null;
        switch($selectBoxType){
            case "measured_length_":{
                $name = 'measured_length_'.$this->getCorrectIndex();
                $units = $this->unitsOfMeasurement['length']['units'];
                $default = $this->getDeliveryData('measured_length_');
                break;
            }
            case "measured_width_":{
                $name = 'measured_width_'.$this->getCorrectIndex();
                $units = $this->unitsOfMeasurement['length']['units'];
                $default = $this->getDeliveryData('measured_width_');
                break;
            }
            case "measured_height_":{
                $name = 'measured_height_'.$this->getCorrectIndex();
                $units = $this->unitsOfMeasurement['length']['units'];
                $default = $this->getDeliveryData('measured_height_');
                break;
            }
            case "measured_weight_":{
                $name = 'measured_weight_'.$this->getCorrectIndex();
                $units = $this->unitsOfMeasurement['weight']['units'];
                $default = $this->getDeliveryData('measured_weight_');
                break;
            }
        }

        return \Form::select($name, array_merge(array('make_unit_choice'=>'Choose a unit'),$units), $default, array('style'=>'width:100%;'));

    }

    //Form::select($selectName, array_merge(array('make_unit_choice'=>'Choose a unit'),$units), $default, array('style'=>'width:100%;'))

    public function getDeliveryItemsTemplate( $templateType, $unitsOfMeasurement = null, $index = null, $deliveryData = null){

        $this->deliveryEditTemplateType = $templateType;

        $this->deliveryData = $deliveryData;

        $this->unitsOfMeasurement = $unitsOfMeasurement;

        $this->deliveryEditIndex = $index;

        return '<div class="item_box_surround edit-item-single">
                <div class="edit_items_inner_surround">
                    <fieldset class="pure-group">

                        <div class="pure-g row">
                            <div class="pure-u-6-24"></div>
                            <div class="pure-u-18-24">'.$this->getDeleteButton().
                            '</div>
                        </div>

                        <div class="pure-g row">
                            <div class="pure-u-6-24">'.$this->getLabel('main_image_').'</div>
                            <div class="pure-u-18-24">
                                <div class="edit_image_box" id="edit_image_box_'.$this->getCorrectIndex().'">
                                '.$this->getImage().'
                                </div>
                                <div class="pure-g">
                                    <div class="pure-u-6-24">'.$this->getLabel('file_item_image_').'</div>
                                    <div class="pure-u-18-24">'.$this->getFileBox('file_item_image_').'</div>
                                </div>
                                <div class="pure-g">
                                    <div class="pure-u-6-24">'.$this->getLabel('use_as_main_image_').'</div>
                                    <div class="pure-u-18-24">'.$this->getRadioBox().'</div>

                                </div>
                            </div>
                        </div>

                        <div class="pure-g row">
                            <div class="pure-u-6-24">'.$this->getLabel('item_name_').'</div>
                            <div class="pure-u-18-24">'.$this->getTextBox('item_name_').'</div>
                        </div>

                        <div class="pure-g row">
                            <div class="pure-u-6-24">'.$this->getLabel('item_description_').'</div>
                            <div class="pure-u-18-24">'.$this->getTextareaBox('item_description_').'</div>
                        </div>

                        <div class="pure-g row">
                            <div class="pure-u-6-24">'.$this->getLabel('item_dimensions_').'</div>
                            <div class="pure-u-18-24">


                                <div class="pure-g row">
                                    <div class="pure-u-1-2">


                                        <div>'.$this->getLabel('item_length_').'</div>

                                        <div class="pure-g">
                                            <div class="pure-u-1-2">
                                                '.$this->getTextBox('item_length_').'
                                            </div>
                                            <div class="pure-u-1-2">
                                                '.$this->getUnitsSelectBox('measured_length_').'
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pure-u-1-2">
                                        <div>
                                            '.$this->getLabel('item_width_').'
                                        </div>
                                        <div class="pure-g">
                                            <div class="pure-u-1-2">
                                                '.$this->getTextBox('item_width_').'
                                            </div>
                                            <div class="pure-u-1-2">
                                                '.$this->getUnitsSelectBox('measured_width_').'
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="pure-g row">
                                    <div class="pure-u-1-2">
                                        <div>
                                            '.$this->getLabel('item_height_').'
                                        </div>
                                        <div class="pure-g">
                                            <div class="pure-u-1-2">
                                            '.$this->getTextBox('item_height_').'
                                            </div>
                                            <div class="pure-u-1-2">
                                                '.$this->getUnitsSelectBox('measured_height_').'
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pure-u-1-2">
                                        <div>
                                            '.$this->getLabel('item_weight_').'
                                        </div>
                                        <div class="pure-g">
                                            <div class="pure-u-1-2">
                                                '.$this->getTextBox('item_weight_').'
                                            </div>
                                            <div class="pure-u-1-2">
                                                '.$this->getUnitsSelectBox('measured_weight_').'
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>


                    </fieldset>
                </div>
            </div>';
    }
}