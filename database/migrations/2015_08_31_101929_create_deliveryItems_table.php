<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('delivery_items', function($table)
	    {
	        $table->increments('id');
	        //Column user, autoincrement, unsigned
	        $table->integer('delivery',false, true)->length(11);
	        $table->string('name',300);
	        $table->string('ebay_id',30);
	        $table->string('main_image',300);

	        //My foreign keys
	        $table->foreign('delivery')->references('id')->on('deliveries');
	        $table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('delivery_items');
	}

}
