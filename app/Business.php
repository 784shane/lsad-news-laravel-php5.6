<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user',
        'company_email',
        'company_name',
        'company_phone_number',
        'company_mobile_number',
        'company_address_1',
        'company_address_2',
        'company_city',
        'company_postcode',
        'verified'
    ];

    public function getBusinessByEmail($businessEmail){
        $businesses = $this->where('company_email', '=', $businessEmail)->get();
        if(isset($businesses[0])){
            return $businesses;
        }
        return null;
    }

    public function getVerifiedBusiness($userId){
        $businesses = $this
        ->where('user', '=', $userId)
        ->where('verified', '=', 1)->get();
        if(isset($businesses[0])){
            return $businesses;
        }
        return null;
    }

    public function getBusinessForUser($userId){
        $businesses = $this->where('user', '=', $userId)->get();
        if(isset($businesses[0])){
            return $businesses;
        }
        return null;
    }

    public function updateBusiness($where, $whereValue, $data){
        return $this->where($where, $whereValue)
        ->update($data);
    }

    public function findBusinessesByToken($businessValidationToken){
        $businesses = $this->where('validation_token', '=', $businessValidationToken)->get();
        if(isset($businesses[0])){
            return $businesses;
        }
        return null;
    }

    public function updateBusinessById($businessId, $inputs){
        $business = $this::find($businessId);
        return $business->update($inputs);
    }
}
