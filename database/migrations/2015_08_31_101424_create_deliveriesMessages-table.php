<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveriesMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('deliveries_messages', function($table)
	    {
	        $table->increments('id');
	        //Column user, autoincrement, unsigned
	        $table->integer('delivery',false, true)->length(11);
	        $table->integer('sender',false, true)->length(11);
	        $table->integer('receiver',false, true)->length(11);
	        $table->text('message');
			
	        //if this message hasnt been loaded, we will mark this as new.
	        $table->boolean('unread')->default(false);
	        
	        //My foreign keys
	        $table->foreign('delivery')->references('id')->on('deliveries');
	        $table->foreign('sender')->references('id')->on('users');
	        $table->foreign('receiver')->references('id')->on('users');
	        $table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('deliveries_messages');
	}

}
