<?php namespace App\Services;

class DeliveriesItemsGatherer{

	/**
	 * Return the deliveries of this user
	 *
	 * 
	 * @return array
	 */
	public function organiseItems($collectionArray, $dItem){
		$this->ff($collectionArray, $dItem);
		return $collectionArray;
	}

	private function ff(&$collectionArray, $dItem){
		$collectionArray['name'] = $dItem->name;
		$collectionArray['item_id'] = $dItem->id;
        $collectionArray['ebay_id'] = $dItem->ebay_id;
        $collectionArray['main_image'] = $dItem->main_image;
	}

}