<?php

namespace App\Http\Controllers;


use App\Services\Quotes\Quotes;
use Deliveries;


class QuotesController extends PrivateController
{
    public function accept($deliveryNonce, $quoteId)
    {
        // Get the quoteId that we should accept.
        $quote = new Quotes();
        // Process the quote we are about to accept.
        $quoteAccepted = $quote->acceptQuote($deliveryNonce, (int)$quoteId);

        $quoteInfo = $quote->getQuote($quoteId);

        if($quoteAccepted && $quoteInfo && isset($quoteInfo[0]['delivery'])){
            // Now get the delivery details.
            $delivery = Deliveries::getDelivery($quoteInfo[0]['delivery']);
            // Add this delivery to the view data array
            $this->viewData['delivery'] = $delivery;
            // Add the quote details to the view data array
            $this->viewData['quote'] = $quoteInfo;
        }
        return view('quotes.accepted')->with($this->viewData);
    }

    public function processCreditCardInformation()
    {
        $quote = new Quotes();
        // @Todo - On the previous page, we will have to embed within the form,
        // the decision on how much the user will pay for the release of the transporter's
        // information. Will it be based on a percentage?.
        // Mewanwhile, for this test we will propose a random figure.
        $saveQuoteInfo = \Payments::retrieveQuoteInfoAfterPayment();
        $retrievedQuoteId = $saveQuoteInfo['quote_id'];

        $charge = 19.95;
        $paymentResult = \Payments::processPaypalCreditCardPayments($this->temporaryPayPalData($charge));
        $paymentResult = \Payments::processesAfterDirectPayment($retrievedQuoteId, $paymentResult);


        $this->viewData['displayablePaymentInfo'] = \Payments::getDisplayablePaymentInfo($paymentResult);
        //exit(vde($this->viewData['displayablePaymentInfo']));
        $this->viewData['pagesInfo'] = $quote->getAfterPaymentPages($retrievedQuoteId, $paymentResult);
        return view('quotes.payments.result')->with($this->viewData);
    }

    public function processAcceptance($quoteId)
    {
        // @Todo - On the previous page, we will have to embed within the form,
        // the decision on how much the user will pay for the release of the transporter's
        // information. Will it be based on a percentage?.
        // Meanwhile, for this test we will propose a random figure.
        $temporaryPayPalData = [
            'charge' => 19.95,
            'quoteId' => $quoteId
        ];

        $pay_pal_express_checkout_URL = \Payments::processPaypalPayments($temporaryPayPalData);
        if(!empty($pay_pal_express_checkout_URL)){
            // Lets go to the paypal express page.
            return \Redirect::away($pay_pal_express_checkout_URL);
        }
        exit('pay pal express checkout failed.');
    }

    public function paypalResult()
    {
        // * We have added the AMT element temporarily to the curl result
        //   We should find a way to deal with this. Its only there temporarily
        //   in-case there is an error. The amt will be there if payment was successful.
        $saveQuoteInfo = \Payments::retrieveQuoteInfoAfterPayment();
        $retrievedQuoteId = $saveQuoteInfo['quote_id'];

        $quote = new Quotes();
        $paymentResult = \Payments::processExpressCheckoutResult();
        $paymentResult = \Payments::processesAfterDirectPayment($retrievedQuoteId, $paymentResult);

        $this->viewData['displayablePaymentInfo'] = \Payments::getDisplayablePaymentInfo($paymentResult);
        $this->viewData['pagesInfo'] = $quote->getAfterPaymentPages($retrievedQuoteId, $paymentResult);
        return view('quotes.payments.result')->with($this->viewData);
    }

    public function paypalCancel()
    {
        echo \URL::route('paypal.express_checkout_cancel');
        exit;
    }

    private function temporaryPayPalData($charge){
        return [
            'charge'=>$charge
        ];
    }
}
