@extends($account_page_layout)
@section('content')
    <div>
        <div>
            <p>
                {!! $errors->first('quote_message') !!}
            </p>
        </div>
        @if(!isset($reply_to_message))
            <div>
                {!! Form::open() !!}
                {!!Form::hidden('post_directive', 'post_reply_button')!!}
                {!! Form::submit('Reply', array('class'=>'pure-button pure-button-primary', 'name'=>'reply_to_message')) !!}
                {!! Form::close() !!}
            </div>
        @endif
        @if(isset($main_message))
            <div>
                @foreach($main_message as $messageItem)
                    @if(isset($reply_to_message))
                        <div>
                            {!! Form::open() !!}
                            <div>{!!Form::textarea('quote_message')!!}</div>
                            {!!Form::hidden('post_directive', 'post_message')!!}
                            {!! Form::submit('Send', array('class'=>'pure-button pure-button-primary')) !!}
                            {!! Form::close() !!}
                        </div>
                    @endif
                    <div>
                        <div class="pure-g">
                            <div class="pure-u-6-24">From:</div>
                            <div class="pure-u-18-24">{{ $messageItem['user_name'] }}</div>
                        </div>
                        <div class="pure-g">
                            <div class="pure-u-6-24">Sent:</div>
                            <div class="pure-u-18-24">{{ $vComposerHelper->viewDatesDisplay($messageItem['created_at'])
                                }}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div>Message:</div>
                        <div>{{ $messageItem['message'] }}</div>
                    </div>
                @endforeach
            </div>
        @endif
        @if(isset($senders_messages))
            <div>
                <h3>Communication:</h3>
                @foreach($senders_messages as $sendersMessageItem)
                    {{-- dump($sendersMessageItem) --}}
                    <div class="senders_messages_single">
                        @if($vComposerHelper->amISpeaker($sendersMessageItem['sender']))
                            {{-- You are the speaker of this message --}}
                            <div class=" text-right">{{ $vComposerHelper->viewDatesDisplay($sendersMessageItem['messages_created_at']) }}</div>
                            <div class="pure-g">
                                <div class="pure-u-18-24 delivery-message-speaker">
                                    {!! Html::link('message/'.$sendersMessageItem['message_id'],
                            $vComposerHelper->shortenDeliveriesMessageTexts($sendersMessageItem['message']), array('class' => 'text-right')) !!}
                                </div>
                                <div class="pure-u-6-24 text-right"><b>You</b></div>
                            </div>
                        @else
                            {{-- sender is the speaker of this message --}}
                            <div>{{ $vComposerHelper->viewDatesDisplay($sendersMessageItem['messages_created_at']) }}</div>
                            <div class="pure-g">
                                <div class="pure-u-6-24"><b>{{ $sendersMessageItem['user_name'] }}</b></div>
                                <div class="pure-u-18-24">{!! Html::link('message/'.$sendersMessageItem['message_id'],
                            $vComposerHelper->shortenDeliveriesMessageTexts($sendersMessageItem['message'])) !!}
                                </div>
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
        @endif
    </div>

@stop