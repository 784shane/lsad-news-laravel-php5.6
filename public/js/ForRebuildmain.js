var templateBaseUrl = '/js/angular/templates/';
var shippingApp = angular.module('shipping_app', []);


shippingApp.service('AjaxService', function($http, $q) {

    this.getCall = function(url) {
      return $q(function(resolve, reject) {
        $http.get(url)
        .success(function (response) {
          resolve(response);
          //reject('we didnt find what we were looking for');
        });
      });
    };
});

shippingApp.service('ItemCollectionService', function() {
  var collection_item = [];
  this.addNewCollectionItem = function(){

  };

  this.removeCollectionItem = function(){

  };

  this.getCollectionItems = function(){

  };
});


shippingApp.directive('listItemDetailsDirective', function() {
  return {
    templateUrl: templateBaseUrl+'ebay-single-items.html'
    //template: 'my ebay group {{ ebaygroup }}'
  };
});

/*shippingApp.directive('listOfEbayItems', function() {
  return {
    templateUrl: templateBaseUrl+'ebay-single-items.html'
    //template: 'sugar'
  };
});*/

shippingApp.directive('itemBlockListDirective', function() {
  return {
    templateUrl: templateBaseUrl+'single-items.html'
  };
});

shippingApp.directive('blurDirective', ['$animate', function($animate) {
  return function(scope, element, attrs) {
    element.on('blur', function() {
      angular.element(document.getElementById('deliveryBasicInfo'))
      .scope().processPostCode(attrs.blurDirective);
    });
  };
}]);

//---  ALL CLICK EVENTS
shippingApp.directive('clickDirective', [function() {
  return function($scope, element, attrs) {
    element.bind('click', function(e) {

    //element.on('click', function(e) {
      
      switch(attrs.clickDirective){
        case 'ebay_item_button':{
          $scope.getEbayItem();
          break;
        }
        case 'add_item_link':{
          e.preventDefault();
          break;
        }
        default:{

        }
      }

    });
  };
}]);

shippingApp.controller('deliveryFormInfo', deliveryFormInfoController);


shippingApp.controller('deliveryBasicInfo', function ($scope, $http) {

	var callServerTimeout = null;

	$scope.postcode = '';

	$scope.processPostCode = function(elementDirective){
		var requestedPostCode = "";
		switch(elementDirective){
    		case 'post-code-from':{
    			requestedPostCode = $scope.postcodefrom;
    			break;
    		}

    		case 'post-code-to':{
    			requestedPostCode = $scope.postcodeto;
    			break;
    		}

    		default:{

    		}
    	}
    	$scope.checkPostCode(elementDirective, requestedPostCode);
	};

	$scope.checkPostCode = function(elementDirective, requestedPostCode){
		if(requestedPostCode != ""){
			$http.get("/get-postcode-address/"+escape(requestedPostCode))
			.success(function (response) {
				//Assign the response here so that we will update the view
				switch(elementDirective){
		    		case 'post-code-from':{
		    			$scope.postcode1 = response;
		    			break;
		    		}

		    		case 'post-code-to':{
		    			$scope.postcode2 = response;
		    			break;
		    		}

		    		default:{

		    		}
		    	}
			});
		}
	}

});

function deliveryFormInfoController($scope, $http, AjaxService){

  $scope.collectionItemList = [{},{},{}];
  $scope.ebay_item_input = '181718417694';
  $scope.ebaygroup = "";

  $scope.getEbayItem = function(){
    //181718417694
    AjaxService.getCall("/get-ebay-item/"+escape($scope.ebay_item_input)).then(function(result) {
      if(result.findItemsByKeywordsResponse.length > 0){
        var searchResults = result.findItemsByKeywordsResponse[0].searchResult;
        var foundItem = false;
        for(var searchResIndex in searchResults){
          for(var itemIndex in searchResults[searchResIndex]){
            var item = searchResults[searchResIndex][itemIndex];

            //Get the gallery image.
            if(typeof item[0].galleryURL != 'undefined'){
              console.log(item[0].galleryURL[0]);
              foundItem = true;
            }

            //Get the title of the item.
            if(typeof item[0].title != 'undefined'){
              console.log(item[0].title[0]);
              foundItem = true;
            }

            if(foundItem){ break; }
          }

          if(foundItem){ break; }
        }      }
    }, function(failure) {
      console.log(failure);
    });
  };

  $scope.addNewItem = function(){
    this.collectionItemList.push({
      'name1':'name1d',
      'name2':'name2d'
    });
    
    //$scope.pjo = "lovely";


    /*$scope.collectionItemList = [{
      'name1':'name1a',
      'name2':'name2a',
    }];

    console.log(this.collectionItemList);*/
  };

  $scope.testme = function(){
    //$scope.ebaygroup = "aha";
    alert("vibes");
  }

}
