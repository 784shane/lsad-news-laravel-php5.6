'use strict';

/* Services */

var shippingAppServices = angular.module('shippingAppServices', []);

shippingAppServices.service('AjaxService', function($http, $q) {

    this.getCall = function(url) {
      return $q(function(resolve, reject) {
        $http.get(url)
        .success(function (response) {
          resolve(response);
          //reject('we didnt find what we were looking for');
        });
      });
    };
});

shippingAppServices.service('ItemCollectionService', function() {
  var collection_item = [{
    itemset:'set'
  }];

  var collect_deliver_dates_toggle = {
    collection_dates:{
      date_specify:false,
      between_dates:false
    },
    delivery_dates:{
      date_specify:false,
      between_dates:false
    }
  };

  this.addNewCollectionItem = function(item){

    if(typeof item.collectionNo != 'undefined'){
      //we now know which item alter so we will do that here
      var itemToAlter = parseInt(item.collectionNo);
      //collection_item[itemToAlter].itemset = 'set';
      collection_item[itemToAlter].ebay_item = true;
      collection_item[itemToAlter].ebay_item_id = item.ebayId;
      collection_item[itemToAlter].ebay_item_name = item.title;
      collection_item[itemToAlter].ebay_item_image = item.pic;
    }else{
      //otherwise we will just add a collection item
      collection_item.push({
        //itemset:item.itemset,
        ebay_item:false
      });
    }
  };

  this.removeCollectionItem = function(){

  };

  this.getCollectionItems = function(){
    return collection_item;
  };

  this.toggleEbayItemShow = function(show, itemNo){
    collection_item[itemNo].ebay_item = show;
  };

  this.getCollectDeliverDateValue = function(collect_deliver_dates_item){
    switch(collect_deliver_dates_item){
      case "cd_specified_date":{
        return collect_deliver_dates_toggle.collection_dates.date_specify;
        break;
      }
      case "cd_between_dates":{
        return collect_deliver_dates_toggle.collection_dates.between_dates;
        break;
      }
      case "dd_specified_date":{
        return collect_deliver_dates_toggle.delivery_dates.date_specify;
        break;
      }
      case "dd_between_dates":{
        return collect_deliver_dates_toggle.delivery_dates.between_dates;
        break;
      }
      default:{
        return false;
      }
    }
  };

  this.setCollectDeliverDateValue = function(collect_deliver_dates_item, value){
    this.resetCollectDeliverDateValue(collect_deliver_dates_item);
    switch(collect_deliver_dates_item){
      case "cd_specified_date":{
        collect_deliver_dates_toggle.collection_dates.date_specify = value;
        break;
      }
      case "cd_between_dates":{
        collect_deliver_dates_toggle.collection_dates.between_dates = value;
        break;
      }
      case "dd_specified_date":{
        collect_deliver_dates_toggle.delivery_dates.date_specify = value;
        break;
      }
      case "dd_between_dates":{
        collect_deliver_dates_toggle.delivery_dates.between_dates = value;
        break;
      }
    }
  };

  this.resetCollectDeliverDateValue = function(collect_deliver_dates_item){
    switch(collect_deliver_dates_item){
      case "cd_asap":
      case "cd_earliest_convenient":
      case "cd_specified_date":
      case "cd_between_dates":{
        collect_deliver_dates_toggle.collection_dates.date_specify = false;
        collect_deliver_dates_toggle.collection_dates.between_dates = false;
        break;
      }
      case "dd_asap":
      case "dd_earliest_convenient":
      case "dd_specified_date":
      case "dd_between_dates":{
        collect_deliver_dates_toggle.delivery_dates.date_specify = false;
        collect_deliver_dates_toggle.delivery_dates.between_dates = false;
        break;
      }
    }
  };

});