<?php namespace App\Services\Deliveries;

class DeliveriesDelete extends Deliveries
{
    public function attemptDelete($deliveryId, $itemId)
    {
        $returnedInfo = ['deleted'=>'false'];
        $delivery = $this->getDelivery($deliveryId);
        if ($this->isDeliveryEligibleForDeletion($delivery, [\URL::route('home'), \URL::route('deliveries.edit')]))
        {
            if ($itemId !== null && count($delivery[0]['items']) > 1)
            {
                // We will be deleting only an item on this delivery
                $this->deleteDeliveryItem($delivery, $itemId);
                $returnedInfo['deleted'] = true;
                $returnedInfo['info'] = 'item_deleted';
            }
            else
            {
                $this->deleteDelivery($delivery);
                $returnedInfo['deleted'] = true;
                $returnedInfo['info'] = 'delivery_deleted';
            }
        }
        return $returnedInfo;
    }

    private function isDeliveryEligibleForDeletion($delivery, $routesArray)
    {
        // Only deliveries with a status of ACTIVE_PUBLIC can be deleted
        // Once a delivery has a quote accepted, we can no longer it.
        $privateDeliveriesStatus = \Config::get('constants.deliveries.statuses.ACTIVE_PRIVATE');
        if ($this->isCorrectRefererRoute(\Request::server('HTTP_REFERER'), $routesArray) &&
            !empty($delivery) &&
            $this->loggedInUserOwnsDelivery($delivery[0]['id']) &&
            $delivery[0]['status'] < $privateDeliveriesStatus
        )
        {
            return true;
        }
        return false;
    }

    private function isCorrectRefererRoute($previousRoute, $routesArray)
    {
        if ($previousRoute != "")
        {
            foreach($routesArray as $route){
                $route = preg_replace('/\//', '\/', $route);
                preg_match('/^' . $route . '/', $previousRoute, $matches);
                if (!empty($matches))
                {
                    return true;
                }
            }
        }
        return false;
    }

    private function deleteDelivery($delivery){

        // delete a delivery - href="http://purplemover/deliveries/delete/5"
        // delete a delivery item -  href="http://purplemover/deliveries/delete/6/14"

        // First Of all, remove all the items
        foreach($delivery[0]['items'] as $deliveryItem){
            $this->removeDeliveryItemsById($deliveryItem['id']);
        }

        $Quotes = new \App\Quotes();
        $Quotes->deleteQuotesByByDeliveryId($delivery[0]['id']);

        // Delete the delivery messages
        $DeliveriesMessage = new \App\DeliveriesMessage();
        $DeliveriesMessage->deleteDeliveryMessagesByDeliveryId($delivery[0]['id']);

        //Remove delivery from delivery items
        $DeliveryItems = new \App\DeliveryItems();
        $DeliveryItems->deleteDeliveryItemsByDeliveryId($delivery[0]['id']);

        //Remove delivery from the database
        \App\Deliveries::find($delivery[0]['id'])->delete();
    }

    private function deleteDeliveryItem($delivery, $itemId){
        // Remove the item from the database
        $this->removeDeliveryItemsById($itemId);
        //Re-order the remaining items.
        $this->reOrderRemainingItems($delivery, $itemId);
    }

    private function reOrderRemainingItems($delivery, $deletedItemId){
        $newOrderNo = 0;
        foreach($delivery[0]['items'] as $deliveryItem){
            if($deliveryItem['id'] == $deletedItemId ){ continue; }
            $this->updateItemOrder($deliveryItem['id'], $newOrderNo);
            $newOrderNo++;
        }
    }

    private function updateItemOrder($itemId, $newOrderNo){
        $deleteItemModel = \App\Items::find($itemId);
        $deleteItemModel->item_order = $newOrderNo;
        $deleteItemModel->save();
    }
}