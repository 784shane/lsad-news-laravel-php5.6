<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transaction_details', function($table)
	    {
			$table->integer('transaction',false, true)->length(11);
			$table->string('item_name');
			$table->string('item_value');

			//My foreign keys
			$table->foreign('transaction')->references('id')->on('transactions');
			$table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transaction_details');
	}

}
