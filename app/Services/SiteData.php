<?php namespace App\Services;

use App\Services\Data\SiteTexts;

class SiteData {
	public function itemsToShipList(){
		$itemsToShip = [];
		foreach(SiteTexts::itemsToShip() as $itemsToShipKey => $itemsToShipName){
			$itemsToShip[$itemsToShipKey] = $itemsToShipName;
		}
		return $itemsToShip;
	}
}