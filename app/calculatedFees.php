<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class calculatedFees extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'saved_hash',
        'your_fee',
        'our_fee',
        'quote'
    ];

}
