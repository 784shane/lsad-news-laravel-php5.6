<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveriesMessage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'delivery',
        'sender',
        'receiver',
        'message',
        'unread'
    ];

    /**
     * deleteDeliveryMessagesById
     * @param int
     **/
    public function deleteDeliveryMessagesByDeliveryId($deliveryId){
        return $this->where('delivery', $deliveryId)->delete();
    }
}
