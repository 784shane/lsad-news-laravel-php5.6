var templateBaseUrl = '/js/angular/templates/';
//var shippingApp = angular.module('shipping_app', []);

var shippingApp = angular.module('shipping_app', [
  'shippingAppControllers',
  'shippingAppServices'
]);

shippingApp.directive('repeatDone', function() {
    return function(scope, element, attrs) {
      //console.log(attrs);
      if (scope.$last) { // all are rendered
        scope.$eval(attrs.repeatDone);
        //console.log(attrs.repeatDone);
      }
    }
  });

shippingApp.directive('itemBlockListDirective', function() {
  return {
    templateUrl: templateBaseUrl+'single-items.html'
  };
});

shippingApp.directive('blurDirective', ['$animate', function($animate) {
  return function(scope, element, attrs) {
    element.on('blur', function() {
      angular.element(document.getElementById('deliveryBasicInfo'))
      .scope().processPostCode(attrs.blurDirective);
    });
  };
}]);

shippingApp.directive('myTop',function() {
return {
    link: function (scope) {
        alert('vibes')
    }
}
})

//---  ALL CLICK EVENTS
shippingApp.directive('clickDirective', [function() {
  return function($scope, element, attrs, $document) {
    
      element.bind('click', function(e) {
        
        switch(attrs.clickDirective){
          case 'ebay_item_button':{
            $scope.getEbayItem(element);
            break;
          }
          case 'exact':{
            $scope.toggleCollectionAndDeliveryDate(element);
            break;
          }
          case 'add_item_link':{
            e.preventDefault();
            break;
          }
          default:{

          }
        }

      });

  };
}]);