var quoteRecalculation = function(){

  var self = this;

  this.init = function(){

    var adjustTimeout = null;

    $(document).bind('cbox_complete', function(){
      $( '#job_quote_amt' ).keyup(function() {
        clearTimeout(adjustTimeout);
        adjustTimeout = setTimeout(function(){ self.doQuoteAdjustment(); },300);
      });
    });

  };

  this.doQuoteAdjustment = function(){
    var _token = $('#job_quote_amt').closest('form').find( 'input[name="_token"]' ).val();
    var job_quote_amt = $( '#job_quote_amt' ).val();
    if(!isNaN(job_quote_amt)){
      var httpServ = new httpService();
      httpServ.callServerPost('/quotes/get-adjusted-quote',{
        job_quote_amt:job_quote_amt,
        _token:_token
      }).done(function(res){
        self.displayAdjustedQuoteInfo(res);
      });
    }
  };

  this.displayAdjustedQuoteInfo = function(data){
    // Apply data to the recalculation box.
    this.applyToRecalculationBox(data);
    $('.quote-recalculation-box').fadeIn(function(){
      $.colorbox.resize();
    });
  };

  this.applyToRecalculationBox = function(data){
    // Apply data to the recalculation box.
    $('#your_fee').html(data.your_fee);
    $('#our_fee').html(data.our_fee);
    $('#our_quote').html(data.quote);
  };
}

$(document).ready(function(){

  /*
  var quoteRecal = new quoteRecalculation();
  quoteRecal.init();

  var html = $('#quoteMakerTemplate').html()

  var colorBoxServ = new colorBoxService();
  colorBoxServ.colorBox({ html:html});
  */

});