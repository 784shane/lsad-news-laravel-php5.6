@extends($layout)
@section('content')

    @include('partials.forms.units.select',$unitsOfMeasurement)

    <script id="uploadBoxTemplate" type="text/x-handlebars-template">
        <div class="pure-g file-upload-surround">
            <div class="pure-u-1-5">
                <div class="fileUpload pure-button">
                    <span>Browse</span>
                    <input type="file" name="uploaded_image_@{{ the_item_list_index }}" class="file-upload-input"/>
                </div>
            </div>
            <div class="pure-u-4-5">

                <div class="pure-g upload-progress">
                    <div class="pure-u-1-5">
                        <div class="picture-show">
                            <div class="upload-place-holder"></div>
                        </div>
                    </div>
                    <div class="pure-u-4-5">
                        <div class="upload-details-box">
                            <div>
                                <div><b><i>image source</i></b></div>
                                <div class="picture-name-show">this is my image source</div>
                            </div>
                            <div><b><i>upload progress</i></b></div>
                            <progress></progress>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script id="collectionItemsTemplate" type="text/x-handlebars-template">
        @{{#each items_list}}
        <div class="item_group" style="margin:0 0 30px 0;">
            <div class="item_box_surround">
                <h3 class="section-sub-title">Item no @{{ list_no }}</h3>

                <div class="pure-g my_delivery_item_row">
                    <div class="pure-u-5-24">
                        <label for="use_ebay">Is this an ebay item?</label>
                    </div>
                    <div class="pure-u-1-24">
                        <input type="checkbox" class="use_ebay" data-index="@{{the_item_list_index}}" name="use_ebay_@{{the_item_list_index}}"
                               value="use_ebay" @{{#if ebay_item}}checked@{{/if}} />
                    </div>
                </div>

                <div id="if-ebay-item_@{{ the_item_list_index }}" class="if-ebay-item" style="@{{#unless ebay_item}}display:none;@{{/unless}}">
                    <div>
                        <label for="ebayIdInput_@{{the_item_list_index}}">Ebay item number</label>
                        <input type="text" class="" id="ebayIdInput_@{{the_item_list_index}}" name="ebay_item_number_@{{the_item_list_index}}"
                               value="@{{ ebay_item_id }}"/>
                        <button type="button" class="get_ebay_item_button pure-button-primary"
                                data-index="@{{ the_item_list_index }}">
                            Get ebay item
                        </button>
                    </div>

                    @{{#ifCond ebay_item_image ''}}
                    <div>
                        <img src="@{{ ebay_item_image }}"/>
                    </div>
                    @{{else}}
                    <div>We cannot find an image.</div>
                    @{{/ifCond}}

                    <div>
                        <label for="item_name_@{{ the_item_list_index }}">Name of item</label>
                        <input type="text" value="@{{ ebay_item_name }}" id="item_name_@{{ the_item_list_index }}"
                               name="item_name_@{{ the_item_list_index }}"/>
                    </div>
			
			<span>
			@{{! The hidden inputs }}
                    <!-- HIDDEN -->
		        <input type="hidden" name="ebay_item_image_@{{ the_item_list_index }}" value="@{{ ebay_item_image }}"/>
		    </span>
                </div>



                @{{#unless ebay_item}}
                <div class="if-not-ebay-item" id="if-not-ebay-item_@{{ the_item_list_index }}">@{{! We will show this item here if it is an ebay item }}
                <label>Upload an image for this item</label>

                @{{> uploadBoxTemplatePartial }}

                <label for="item_name_@{{ the_item_list_index }}">Name of item</label>
                <input type="text" name="item_name_@{{ the_item_list_index }}"/>


                <!-- HIDDEN -->
                @{{! We need these hidden items even if we have chosen 'not an ebay item' }}

                @{{! This is the ebay_id. We have used the input - ebay_item_number }}
                <input type="hidden" name="ebay_item_number_@{{the_item_list_index}}" value=""/>

                @{{! This is the main_image. We have used the input - ebay_item_image_ }}
                @{{! FAKE - We will remove this when we deal with placing the image into the form }}
                <input type="hidden" name="ebay_item_image_@{{the_item_list_index}}" value=""/>
                </div>
                @{{! END OF THE HIDDEN ITEMS }}
                        <!-- HIDDEN -->


                @{{/unless}} @{{! End of the unless ebay_item }}

                <div>
                    <label>Description of item</label>
                    <textarea placeholder="Enter the description here."
                              name="item_description_@{{ the_item_list_index }}"></textarea>
                    <div>
                        <label>Estimated length of item</label>
                        @{{> lengthSelectBoxPartial }}<input type="text" name="item_length_@{{ the_item_list_index }}"/>
                    </div>

                    <div>
                        <label>Estimated width of item</label>
                        @{{> widthSelectBoxPartial }}<input type="text" name="item_width_@{{ the_item_list_index }}"/>
                    </div>

                    <div>
                        <label>Estimated height of item</label>
                        @{{> heightSelectBoxPartial }}<input type="text" name="item_height_@{{ the_item_list_index }}"/>
                    </div>

                    <div>
                        <label>Estimated weight of item</label>
                        @{{> weightSelectBoxPartial }}<input type="text" name="item_weight_@{{ the_item_list_index }}"/>
                    </div>

                </div>


            </div>

            @{{! We need to tell the server which photo to use.  }}
            <input type="hidden" id="photo_to_use_@{{the_item_list_index}}" name="photo_to_use_@{{the_item_list_index}}" value=""/>
        </div>
        @{{/each}}

        <span>
            <input type="hidden" name="delivery_title" value="@{{ delivery_title }}"/>
        </span>

    </script>


    {!! Form::open(array('url' => 'deliveries/add',
        'id'=>"collect_deliveries_info_pt2",
        'class'=>"pure-form pure-form-stacked")) !!}
    {{--
    <form action="/deliveries/add" class="pure-form pure-form-stacked" id="collect_deliveries_info_pt2">
        --}}
    <p>
        {!! $errors->first('email') !!}
        {!! $errors->first('password') !!}
    </p>


    <fieldset>
        <h1>Items for collection</h1>


        <span id="item_block_list"></span>

        <div>
            {!! Html::link('#','Add another item', array('id'=>"add_collection_item")) !!}
        </div>

        <label for="collection_postcode">Collection Postcode</label>
        {!! Form::text('collection_postcode', $value = $collection_postcode,
            array('id'=>'collection_postcode', 'required' => 'required', 'placeholder' => 'Post Code' )) !!}
        <span id="postcode1_address"></span>

        <label for="delivery_postcode">Delivery Postcode</label>
        {!! Form::text('delivery_postcode', $value = $delivery_postcode,
            array('id'=>'delivery_postcode', 'required' => 'required', 'placeholder' => 'Post Code' )) !!}
        <span id="postcode2_address"></span>


        @if (!$isUserLoggedIn)
            <div id="personalInfo" style="margin:0 0 30px 0;">
                <div>
                    <div>{!!Form::label('email', 'Email address')!!}</div>
                    <div>{!!Form::text('email', $users_email )!!}</div>
                    <span class="form-errors">{!! $errors->first('email') !!}</span>
                </div>

                <div>
                    <div>{!!Form::label('user_name','User name')!!}</div>
                    <div>{!!Form::text('user_name', $users_username)!!}</div>
                </div>

                <div>
                    <div>{!!Form::label('telephone_number','Telephone number (optional)')!!}</div>
                    <div>{!!Form::text('telephone_number', $users_telephone_number)!!}</div>
                </div>
            </div>
        @endif

        <span>
	        {!!Form::hidden('shipping_category', $shipping_category)!!}
            {!!Form::hidden('collection_postcode', $collection_postcode)!!}
            {!!Form::hidden('delivery_postcode', $delivery_postcode)!!}
            {!!Form::hidden('collection_address', $collection_address)!!}
            {!!Form::hidden('delivery_address', $delivery_address)!!}
</span>

        <div class="submit_surround">
            {!! Form::submit('Continue', array('class'=>'pure-button pure-button-primary narrow-submit-button')) !!}
        </div>
    </fieldset>
    {{-- Hidden fields - Info collected for form submission --}}
    {!! Form::hidden('collection_address', null, array('id'=>'collection_address') ) !!}
    {!! Form::hidden('delivery_address', null, array('id'=>'delivery_address') ) !!}
    {!! Form::hidden('form_submitted_time', time() , array('id'=>'form_submitted_time')) !!}


    {!! Form::close() !!}

    {{--</form>--}}
@stop

@section('scripts')
    {!! Html::script('js/inline_scripts/deliveriesInput.js') !!}
@stop