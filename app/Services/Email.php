<?php namespace App\Services;

use Mail;

class Email extends Mail
{
    public function sendBusinessVerificationRequest($businessValidationData)
    {
        self::send('emails.complete_business_setup', $businessValidationData, function ($message) use ($businessValidationData)
        {
            $message->to($businessValidationData["toEmail"])
                ->subject('Please verify your business')
                ->from('admin@under2000', 'under2000');
        });
    }

    public function testing($businessValidationData)
    {
        $businessValidationData['businessValidationToken'] = "love";
        $businessValidationData['toEmail'] = "vibes@fadmail.com";
        //$businessValidationData['toEmail'] = "784shane@gmail.com";
        self::send('emails.complete_business_setup', $businessValidationData, function ($message) use ($businessValidationData)
        {
            $message->to($businessValidationData["toEmail"])
                ->subject('Please verify your business')
                ->from('admin@under2000', 'under2000');
        });
    }
}