@extends($layout)
@section('content')
    <div class="pure-g delivery-list-header">
        <div class="pure-u-2-24">&nbsp;</div>
        <div class="pure-u-7-24">Item</div>
        <div class="pure-u-3-24 text-center">Collection</div>
        <div class="pure-u-3-24 text-center">Destination</div>
        <div class="pure-u-3-24 text-center">Distance</div>
        <div class="pure-u-3-24 text-center">Date</div>
        <div class="pure-u-3-24 text-center">Quotes</div>
    </div>
    @foreach($myDeliveries as $delivery)
        <div class="pure-g delivery-list-single">
            <div class="pure-u-2-24">{!! Html::image($vComposerHelper->missingImageResolver($delivery['overallImage']), null, array('width'=>'40','height'=>'40')) !!}
            </div>
            <div class="pure-u-7-24">{!! Html::link('deliveries/jobs/'.$vComposerHelper->getFriendlyUrlTitle($delivery['subtitle']).'/'.$delivery['id'],$delivery['subtitle']) !!}</div>
            <div class="pure-u-3-24 text-center">{{$delivery['collection_postcode']}}</div>
            <div class="pure-u-3-24 text-center">{{$delivery['delivery_postcode']}}</div>
            <div class="pure-u-3-24 text-center">{{ $vComposerHelper->getReadableMileage($delivery['distance_metres'])  }}</div>
            <div class="pure-u-3-24 text-center">{{ $vComposerHelper->viewDatesDisplay($delivery['updated_at'], true)  }}</div>
            <div class="pure-u-3-24 text-center">{{ $delivery['number_of_quotes']  }}</div>
        </div>
    @endforeach
@stop