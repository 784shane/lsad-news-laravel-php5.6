<?php namespace App\Services\Deliveries;

use Request;
use Validator;
use App\User;
use App\Services\Auth\Signup;
use App\Deliveries as DeliveriesModel;
use App\Services\ManageDeliveryItems;
use App\Services\Auth\User as userService;
use App\Services\Quotes\Quotes;

class Deliveries
{

    // Bring in our trait
    use DeliveriesCommon;

    /**
     * processNewlyAddedDeliveries
     **/
    public function processNewlyAddedDeliveriesUsers()
    {
        $posts = Request::all();
        $validatorResults = $this->validateNewDeliveriesUser($posts);
        $validationReturn = array();
        if ($validatorResults->fails())
        {
            $failed = $validatorResults->failed();
            $validationReturn['result'] = 'fail';
            $validationReturn['validatorResults'] = $validatorResults;
            $validationReturn['error_messages'] = $validatorResults->messages();
            $validationReturn['numberofErrors'] = count($failed);
            //we have to be careful here because if the email is already in the
            //database, we dont want the entire process to fail.
            //Instead, we will want to still process the delivery entries to this
            //users' account.
            $validationReturn['email_exists'] = $this->findEmailError($failed);
            //Because we only have one error and its the email,
            //we will return success here because technically we still want our
            //user's entries to be inserted in their account. If we have more errors,
            //well that's a different matter.
            if ($validationReturn['email_exists'] && $validationReturn['numberofErrors'] < 2)
            {
                $validationReturn['result'] = 'success';
            }
            return $validationReturn;
        }
        else
        {
            //Incase we have a user here we will extract in order to save
            $this->extractandSaveUser($posts);
        }
    }

    private function checkPost($posts){
        // Get all the item_name elements
        $reg = '/^item_name_(\d+)$/';
        $itemNames = [];
        //$matches = [];
        foreach($posts as $postKey => $postValue){
            if(preg_match($reg,$postKey,$matches)){
                $itemNames[] = $matches[0];
            }
        }
        return $itemNames;
    }

    private function extractItemIndex($itemName){
        $reg = '/_(\d+)$/';
        if(preg_match($reg,$itemName,$matches)){
            return (int)str_replace('_','', $matches[0]);
        }
        return null;
    }

    protected function getItemsToSave(&$posts, $editing = false){

        $itemNames = $this->checkPost($posts);
        //$itemNames = $this->loopItemNames($itemNames);

        $items = [];
        $itemGroups = [];
        // Lets get the items for this delivery.
        for($itemPosition = 0; $itemPosition<count($itemNames); $itemPosition++){

            $itemIndex = $this->extractItemIndex($itemNames[$itemPosition]);
            // We might update the posts variable here. We want to make the uploaded
            // image the ebay_item_image so as to store it in the database.
            // We will pass it by reference. TAKE NOTE OF THIS.
            $items[] = $this->extractDeliveriesItems($posts, $itemIndex, $editing);
            // We will search for the item index. If the item is present,
            // we will collect that item.
            $groupOfDeliveryItems = $this->getGroupOfDeliveryItems( $posts, $itemPosition, $itemIndex );
            // These are the key and values for the item_values table
            $itemGroups[] = $groupOfDeliveryItems;
            // These are the key and values for the item_specifics table
            //$itemSpecificsGroups =  $groupOfDeliveryItems['groupOfIndexItemsSpecifics'];
        }

        return [
            'items'=>$items,
            'itemGroups'=>$itemGroups
        ];
    }

    protected function AddItemsToDelivery($itemsCountArray, $orderNo, $deliveryId){
        // Add an item to the items table for this delivery
        $itemSpecificModel = \App\Items::firstOrNew(array(
            'delivery' => $deliveryId,
            'item_order' => $orderNo
        ));
        $itemSpecificModel->save();
        return $itemSpecificModel;
    }

    protected function removeItemsFromDelivery($itemsCountArray, $deliveryId){
        $itemsModel = \App\Items::where('delivery',$deliveryId)
            ->get();
        if($itemsModel->count()>0){
            foreach($itemsModel->toArray() as $itemModelArray){
                // Is the item in the submitted items post?
                // If it isn't there, we will delete it.
                if(!in_array($itemModelArray['item_order'], $itemsCountArray)){
                    $this->removeDeliveryItemsById($itemModelArray['id']);
                }
            }
        }
    }

    protected function removeDeliveryItemsById($itemId){
        $deleteItemModel = \App\Items::find($itemId);

        // We also must remove items from the items values table
        // as well as from the items specific table.
        $this->removeItemValuesByItemID($itemId);
        $this->removeItemSpecificByItemID($itemId);

        // Now delete the item.
        return $deleteItemModel->delete();
    }

    private function removeItemValuesByItemID($itemId){
        \App\ItemValues::where('item', $itemId)->delete();
    }

    private function removeItemSpecificByItemID($itemId){
        \App\ItemSpecifics::where('item', $itemId)->delete();
    }

    /**
     * attemptToAddDeliveries
     **/
    public function attemptToAddDeliveries()
    {
        $posts = Request::all();

        $itemsToSave = $this->getItemsToSave($posts);
        $items = $itemsToSave['items'];
        $itemGroups = $itemsToSave['itemGroups'];

        //FAKE
        // We will test adding the items to the tables here.
        // PLEASE REMOVE IT WHEN DONE.
        //$this->addItemsToItemsTables($itemGroups, 2);
        /// FAKE ABOVE ***********************
        // At this point we would need to get the logged in user
        // If no user is logged in, we need to insert a false user
        // into the database and set that user as logged in.
        // Ofcourse, we will use the email that was inputted.
        $userId = $this->getUserIdFromPosts($posts);
        // If the user is in the database, we will proceed to add
        // the deliveries and its items.
        if ($userId)
        {
            $this->addDeliveriesToDb($posts, $items, $itemGroups, $userId);
        }
    }

    /**
     * getUserIdFromPosts
     *
     * @return array
     **/
    private function getUserIdFromPosts($posts)
    {
        //Now, is the user logged in?
        $userServ = new userService();
        $loggedInUsersId = $userServ->getLoggedInUsersId();
        if ($loggedInUsersId)
        {
            //If we find the Id, lets stop here
            return $loggedInUsersId;
        }
        if (is_null($loggedInUsersId))
        {
            $email = isset($posts['email'])?$posts['email']:null;
            if (is_null($email))
            {
                //We cannot find an id so forget it
                return null;
            }
            $user = new User();
            $result = $user->getUserByEmail($email);
            if ($result)
            {
                //id found from post, so we'll use it
                return $result[0]->id;
            }
        }
        //unsuccessful so forget it.
        return null;
    }

    private function addItemsToItemsTables($itemGroups, $deliveryId)
    {
        $itemsValuesTable = [];
        $itemsSpecificsTable = [];
        // Lets place the items into the appropriate tables.
        //foreach($itemGroups as $itemGroup){
        for($i = 0; $i < count($itemGroups); $i++)
        {
            // We need to get the id of the item before we can
            // post to the item_values table.
            $items = \App\Items::create([
                'delivery' => $deliveryId,
                'item_order' => $i
            ]);
            if (isset($items->id))
            {
                $itemId = $items->id;
                // We have the id, so now we will save the data to the
                // item_values table.
                foreach($itemGroups[$i]['groupOfIndexItems'] as $itemValue)
                {
                    if ($itemValue['item_name'])
                    {
                        $itemsValuesTable[] = [
                            'item' => $itemId,
                            'name' => $itemValue['item_name'],
                            'value' => $itemValue['item_value']
                        ];
                    }
                }
                // Collect data to be placed into the item_specifics table.
                foreach($itemGroups[$i]['groupOfIndexItemsSpecifics'] as $itemSpecifics)
                {
                    if ($itemSpecifics['item_name'])
                    {
                        $itemSpecifics['item_name'];
                        $iSInsert = [
                            'item' => $itemId,
                            'name' => $itemSpecifics['item_name'],
                            'value' => $itemSpecifics['item_value']
                        ];
                        if (isset($itemsSpecificsTable[$itemId]))
                        {
                            $itemsSpecificsTable[$itemId][] = $iSInsert;
                        }
                        else
                        {
                            $itemsSpecificsTable[$itemId] = [$iSInsert];
                        }
                    }
                }
            }
        }
        // Now that we have all the data for the items table, we can
        // now enter them in all at once as a batch.
        if (!empty($itemsValuesTable))
        {
            //Batch insert
            $result = \App\ItemValues::insert($itemsValuesTable);
        }
        // Now that we have all the data for the items table, we can
        // now enter them in all at once as a batch.
        if (!empty($itemsSpecificsTable))
        {
            $this->addItemsToItemSpecificsTables($itemsSpecificsTable);
        }
    }

    /**
     * Updates or inserts data into the items_specific table.
     *
     * @param array $itemSpecificsData
     * @return boolean
     **/
    private function addItemsToItemSpecificsTables($itemSpecificsData)
    {
        // We need to place the data into the item_specifics table
        // first though we must check to see if we don't find it
        $noOfSaves = 0;
        foreach($itemSpecificsData as $key => $iSData)
        {
            $saveISpecificTable = false;
            $itemSpecificModel = \App\ItemSpecifics::firstOrNew(array('item' => $key));
            // Lets set all the columns that we will be updating.
            foreach($iSData as $k)
            {
                $saveISpecificTable = true;
                $itemSpecificModel->{$k['name']} = $k['value'];
            }

            //We will not save if nothing was inserted
            if ($saveISpecificTable)
            {
                $saved = $itemSpecificModel->save();
                if($saved){
                    $noOfSaves++;
                }
            }
        }

        // Lets see if all the intended data was actually saved.
        if($noOfSaves === count($itemSpecificsData)){
            return true;
        }
        return false;
    }

    /**
     * Extracts the title from the post. Sets the title the same as
     * the first item if the delivery_title is blank or missing.
     *
     * @param array $posts
     * @return string
     **/
    private function setDeliverySubtitle($posts){
        // See if this value exists. Make sure its not blank.
        if(isset($posts['delivery_title']) && $posts['delivery_title'] != ""){
            return $posts['delivery_title'];
        }
        // Whatever the value, if it exists we will send this value back.
        if(isset($posts['item_name_0'])){
            return $posts['item_name_0'];
        }
        // Last resort. Send an empty string back
        return "";
    }

    protected function getAddressAndMileageFromPostCodes($collection_postcode, $delivery_postcode){
        $addressData = [];
        $postCodeInterpreter = new \App\Services\PostCodeInterpreter();
        $addressData['collection_address'] = $postCodeInterpreter->getLocationFromPostcode($collection_postcode);
        $addressData['delivery_address'] = $postCodeInterpreter->getLocationFromPostcode($delivery_postcode);

        // no need to run this check if we dont have one or more postcodes.
        if(!empty($collection_postcode) && !empty($delivery_postcode)){
            // Now get the distances between the two points
            $distancesBetweenPostCodes = $postCodeInterpreter->getDistanceBetweenPostCodes($collection_postcode, $delivery_postcode);

            if(property_exists($distancesBetweenPostCodes, 'text')){
                $addressData['distance_text'] = $distancesBetweenPostCodes->text;
            }
            if(property_exists($distancesBetweenPostCodes, 'value')){
                $addressData['distance_metres'] = $distancesBetweenPostCodes->value;
            }
        }

        return $addressData;
    }

    private function getInsertionExpiryDate(){
        $deliveryInsertionConstant = \Config::get('constants.deliveries.expiry.extension_reason.DELIVERY_INSERTION');
        $dm = new \App\Services\Deliveries\DeliveriesExpiryManagement();
        return $dm->getDeliveriesNewExpiryTime($deliveryInsertionConstant);
    }

    /**
     * addDeliveriesToDb
     *
     * @param array
     * @param int
     **/
    private function addDeliveriesToDb($posts, $items, $itemGroups, $userId)
    {
        $collection_postcode = isset($posts['collection_postcode'])?$posts['collection_postcode']:'';
        $delivery_postcode = isset($posts['delivery_postcode'])?$posts['delivery_postcode']:'';
        //At this point we have to take of a few things.
        //One would be to get the correct address from the postcodes given
        $addressData = $this->getAddressAndMileageFromPostCodes($collection_postcode, $delivery_postcode);

        // Set the delivery sub-title
        $subtitle = $this->setDeliverySubtitle($posts);
        $transportProvidersEmail = isset($posts['email'])?$posts['email']:'';

        // What do we need to create the delivery nonce?
        $deliveryNonce = $this->prepareDeliveriesNonce($transportProvidersEmail, $subtitle);

        // First of all, we need to create a delivery
        $delivery = DeliveriesModel::create(array(
            'delivery_nonce' => $deliveryNonce,
            'user' => $userId,
            'email' => $transportProvidersEmail,
            'subtitle' => $subtitle,
            'telephone_number' => isset($posts['telephone_number'])?$posts['telephone_number']:'',
            'shipping_category' => isset($posts['shipping_category'])?$posts['shipping_category']:'',
            'collection_postcode' => $collection_postcode,
            'delivery_postcode' => $delivery_postcode,
            'collection_address' => isset($addressData['collection_address'])?$addressData['collection_address']:"",
            'delivery_address' => isset($addressData['delivery_address'])?$addressData['delivery_address']:"",
            'distance_text' => isset($addressData['distance_text'])?$addressData['distance_text']:"",
            'distance_metres' => isset($addressData['distance_metres'])?$addressData['distance_metres']:"",
            'expiry'=>$this->getInsertionExpiryDate()
        ));
        if (isset($delivery->id))
        {
            $this->addItemsToItemsTables($itemGroups, $delivery->id);
            $deliveryItemsManagement = new ManageDeliveryItems();
            $deliveryItemsManagement->addDeliveriesItems($items, $delivery->id);
        }
    }

    private function prepareDeliveriesNonce($transportProvidersEmail, $subtitle){
        // @Todo - We might add a random string here so as to make it more unique.
        return md5($transportProvidersEmail.$subtitle.time());
    }

    private function getGroupOfDeliveryItems( $posts, $itemPosition, $itemIndex )
    {
        //Get data from the config concerning the
        $fields_db_mappings = \Config::get('local.fieldsDbMapping.itemsValues');
        $item_specifics_fields = \Config::get('local.fieldsDbMapping.itemSpecificsFields');
        $groupOfIndexItemsSpecifics = [];
        $groupOfIndexItems = [];
        foreach($fields_db_mappings as $field)
        {
            $indexItemInsert = [
                'order' => $itemPosition,
                'item_name' => isset($posts[$field['field_name'] . $itemIndex])?$field['db_name']:'',
                'item_value' => isset($posts[$field['field_name'] . $itemIndex])?$posts[$field['field_name'] . $itemIndex]:''
            ];

            // Check that it is not already set. Needed mostly for ebay item
            if (in_array($field['db_name'], $item_specifics_fields))
            {
                $groupOfIndexItemsSpecifics[] = $indexItemInsert;
                continue;
            }
            //Add to groupItems
            $groupOfIndexItems[] = $indexItemInsert;
        }
        return [
            'groupOfIndexItems' => $groupOfIndexItems,
            'groupOfIndexItemsSpecifics' => $groupOfIndexItemsSpecifics
        ];
    }

    /**
     * extractDeliveriesItems
     *
     * @param array
     * @param int
     *
     * @return array
     **/
    private function extractDeliveriesItems(&$posts, $itemIndex, $editing = false)
    {
        // We need to check for the main image here.
        // The main image could very well be an uploaded image. In this case, we will
        // not be using the ebay image.

        //we collect the list of items

        return array(
            'use_ebay' => isset($posts['use_ebay_' . $itemIndex])?
                $posts['use_ebay_' . $itemIndex]:'',
            'ebay_id' => isset($posts['ebay_item_number_' . $itemIndex])?
                $posts['ebay_item_number_' . $itemIndex]:'',
            'main_image' => $this->resolveImageForItem($posts,$itemIndex, $editing),
            'name' => isset($posts['item_name_' . $itemIndex])?
                $posts['item_name_' . $itemIndex]:''
        );
    }

    private function resolveImageForItem(&$posts, $itemIndex, $editing = false)
    {
        // Check photo_to_use_{index}. If this is true, the we will use the uploaded image.
        if($editing || (isset($posts['photo_to_use_'.$itemIndex]) && $posts['photo_to_use_'.$itemIndex] == 'true')){
            // now get the uploaded image
            $savedImage = $this->processAndSaveUploadedImage($posts, $itemIndex);
            if($savedImage){
                // Just to mke sure that the image is aligned to the posts' item,
                // we will alter the 'ebay_item_image{index}' to match the stored
                // photo.
                $posts['ebay_item_image_' . $itemIndex] = $savedImage;
            }
            return $savedImage;
        }

        if(isset($posts['ebay_item_image_' . $itemIndex])){
            return $posts['ebay_item_image_' . $itemIndex];
        }

        return '';
    }

    protected function processAndSaveUploadedImage($posts, $itemIndex)
    {
        $timestamp = date('Y-m-d H:i:s',$posts['form_submitted_time']);
        $formToken = $posts['_token'];

        return $this->moveUploadedPhotoNow($timestamp, $formToken, $itemIndex);
    }

    public function fakePhotoMove(){
        $photoUrl = $this->moveUploadedPhoto();
        exit;
    }

    private function moveUploadedPhoto(){

        $timestamp = date('Y-m-d H:i:s', '1455744179');

        $formToken  = 'ZZZ8LmhTL7b0ZDOAa8EYZf0FsMys9ixtWKRd7ydF';

        $itemOrderNo = 0;

        return $this->moveUploadedPhotoNow($timestamp, $formToken, $itemOrderNo);
    }

    private function moveUploadedPhotoNow($timestamp, $formToken, $itemOrderNo){

        // Now get the row of the record
        $uploadedPhotoData = \App\TemporaryUploads::where(
            'form_time', '=', $timestamp)
            ->where('form_token', '=', $formToken)
            ->where('item_order_no', '=', $itemOrderNo)
            ->get()->toArray();

        if(!empty($uploadedPhotoData)){
            // Get the file name.
            $filename = $uploadedPhotoData[0]['file_name'];
            // Move the photo
            if($this->moveFileToPermanentLocation($filename)){
                // Seeing that we've moved this, let's remove the record from the
                // temp photos table
                $this->removeTempPhotoRecord($uploadedPhotoData);
                // Get the url of the photo
                return Request::root().'/photos/perm/'.$filename;
            }
        }
        return null;
    }

    private function moveFileToPermanentLocation($filename){

        // Move it from the Temp directory.
        $publicPath = public_path();
        $tempDirectory = realpath($publicPath.'/photos/temp');
        $permDirectory = realpath($publicPath.'/photos/perm');
        if(file_exists(realpath($tempDirectory.'/'.$filename))){
            return rename($tempDirectory.'/'.$filename, $permDirectory.'/'.$filename);
        }

        return false;
    }

    private function removeTempPhotoRecord($uploadedPhotoData){
        // Actually remove the record. We wont need it anymore.
        $uploadedRecord = \App\TemporaryUploads::find($uploadedPhotoData[0]['id']);
        $uploadedRecord->delete();
    }

    /**
     * findEmailError
     * We will find the email error if it exists.
     * We will be looking to see if the user exists in the users database.
     * If they do we will return a note of this.
     *
     * @param array
     *
     * @return Boolean
     **/
    private function findEmailError($failedArray)
    {
        if (array_key_exists('email', $failedArray))
        {
            if (array_key_exists('Unique', $failedArray['email']))
            {
                if (in_array('users', $failedArray['email']['Unique']))
                {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * extractandSaveUser
     *
     * @param array
     **/
    private function extractandSaveUser($posts)
    {
        $email = isset($posts['email'])?$posts['email']:null;
        $userService = new userService();
        $isUserLoggedIn = $userService->isUserLoggedIn();
        //Only if a user is not already logged in.
        if (!$isUserLoggedIn)
        {
            $password = $userService->buildPasswordRaw();
            $passwordHash = $userService->buildPasswordHash($password);
            $userData = [];
            $userData['email'] = $email;
            $userData['password'] = $passwordHash;
            $userData['telephone_number'] = isset($posts['telephone_number'])?$posts['telephone_number']:null;
            $userData['user_name'] = isset($posts['user_name'])?$posts['user_name']:null;
            //Lets add the user to our database.
            $userData['register_token'] = $this->getSignupClass()->buildToken($email);
            $this->getSignupClass()->addNewUser($userData);
            //For the user that we added we will log them in now.
            $userService->logInUser($email, $password);
        }
    }

    /**
     * Gets the Signup Class' instance
     * return App\Services\Auth\Signup
     **/
    private function getSignupClass()
    {
        return Signup::getInstance();
    }

    /**
     * Checks to see if we have inputted the correct details into
     * the sign up form.
     * return Boolean
     **/
    public function validateNewDeliveriesUser($data)
    {
        $elementValidateArray = [];
        $userService = new userService();
        if (!$userService->isUserLoggedIn())
        {
            $elementValidateArray['email'] = 'required|email|unique:users';
            $elementValidateArray['user_name'] = 'required|min:5';
        }
        //This is the user we are attempting to add at deliveries insertion.
        //There are a few things we need to look for here. First of all,
        //if the user is already a part of the database we will not add them again.
        //We want to make sure this email is valid as well.
        return Validator::make($data, $elementValidateArray,
            //insert our custom messages
            $this->registerMessages()
        );
    }

    /**
     * Get the array of messages which should be applied
     * to this validation of form inputs.
     *
     * @return array
     */
    private function registerMessages()
    {
        return array(
            'email.unique' => 'Sorry, this email is already in our database.'
        );
    }

    /**
     * Return the deliveries
     *
     * @return mixed - returns array if successful, otherwise returns null
     */
    public function getDelivery($deliveryId)
    {
        $deliveryModel = new DeliveriesModel();
        $deliveriesList = $deliveryModel->getDeliveriesById($deliveryId);
        if (count($deliveriesList->toArray()) > 0)
        {
            return $this->getDeliveriesList($deliveriesList);
        }
        return null;
    }

    /**
     * Return the deliveries
     *
     * @return array
     */
    public function getPublicDeliveries()
    {
        $delModel = new DeliveriesModel();
        $deliveriesList = $delModel->getDeliveries();

        // We only want to get public deliveries
        // Any deliveries that are archived, expired or quote accepted(private),
        // shall not be shown.
        $privateDeliveriesInterger = \Config::get('constants.deliveries.statuses.ACTIVE_PUBLIC');

        return $this->getDeliveriesList($deliveriesList, $privateDeliveriesInterger);
    }

    /**
     * Return the deliveries of this user
     *
     * @return array
     */
    public function getPrivateDeliveries($deliveriesStatus = null)
    {
        $deliveryCollection = [];
        $usrService = new userService();
        $loggedInUserId = null;
        $loggedInUserObj = $usrService->getLoggedInUserObject();
        if ($loggedInUserObj && $loggedInUserObj['result'] == "success")
        {
            $loggedInUserId = $loggedInUserObj['user_object']['id'];
        }
        if ($loggedInUserId)
        {
            $delModel = new DeliveriesModel();
            $deliveriesList = $delModel->getMyHomepageDeliveries($loggedInUserId, $deliveriesStatus);

            $quote = new Quotes();
            $deliveriesList = $quote->getMyDeliveryQuotes($deliveriesList);

            $deliveryCollection = $this->getDeliveriesList($deliveriesList);
        }
        return $deliveryCollection;
    }

    /**
     * Return the deliveries of this user
     *
     * @param array $deliveriesList
     *
     * @return array
     */
    public function getDeliveriesList($deliveriesList, $statusToCheck = null)
    {
        $quotes = new Quotes();
        $deliverItemsManager = new ManageDeliveryItems();
        $deliveryCollection = [];

        $deliveryListArray = $deliveriesList->toArray();
        if(count($deliveryListArray)>0){

            foreach($deliveryListArray as $deliveryListArrayItemKey => &$deliveryListArrayItem){

                if(null !== $statusToCheck && $deliveryListArrayItem['status'] > $statusToCheck){
                    unset($deliveryListArray[$deliveryListArrayItemKey]); continue;
                }

                // Get the items for this delivery.
                $items = $deliverItemsManager->getItemsForDelivery($deliveryListArrayItem['id']);
                $deliveryListArrayItem['items'] = $items;

                // Now lets get the number of quotes made to this delivery so far.
                $deliveryListArrayItem['number_of_quotes'] = $quotes->getDeliveryQuotesNumber($deliveryListArrayItem['id']);
            }
        }

        // Set the overall image to be used on this delivery.
        $this->setImageToUseOnMainDelivery($deliveryListArray);

        return $deliveryListArray;
    }

    private function setImageToUseOnMainDelivery(&$deliveryListArray){
        foreach($deliveryListArray as &$delListArray){
            // Set the overall image here for each delivery. We might not have an image
            // to show in cases where no images are set for the items. If we find one,
            // we will set it accordingly.
            $delListArray['overallImage'] = "";
            //if($delListArray['overallImage']=="" && isset($delListArray['items'])){
            if(isset($delListArray['items'])){
                // We will loop through the items looking for an image.
                $numberOfItems = count($delListArray['items']);
                for($i= 0; $i<$numberOfItems; $i++){
                    //$delListArray['overallImage']=="" &&
                    if(isset($delListArray['items'][$i]['main_image']) && $delListArray['items'][$i]['main_image'] != ""){
                        $delListArray['overallImage'] = $delListArray['items'][$i]['main_image'];
                        if(null === $delListArray['use_as_main_image'] && $i < ($numberOfItems-$i)){
                            break;
                        }
                    }

                    if(null !== $delListArray['use_as_main_image'] &&
                        $delListArray['use_as_main_image'] == $i &&
                        isset($delListArray['items'][$i]['main_image']) &&
                        $delListArray['items'][$i]['main_image'] != ""){
                        $delListArray['overallImage'] = $delListArray['items'][$i]['main_image'];
                        //echo "it now breaks at - ".$i;
                        break;
                    }
                }
            }
        }
    }
}