<script id="select_unit_of_measurement_length" type="text/x-handlebars-template">
    <?php echo \Form::select('{{nameOfSelect}}\{{@index}}', array_merge(array('make_unit_choice'=>'Choose a unit'),$length['units']), null); ?>
</script>

<script id="select_unit_of_measurement_weight" type="text/x-handlebars-template">
    <?php echo \Form::select('{{nameOfSelect}}\{{@index}}', array_merge(array('make_unit_choice'=>'Choose a unit'),$weight['units']), null); ?>
</script>

<script id="select_unit_of_measurement_capacity" type="text/x-handlebars-template">
    <?php echo \Form::select('{{nameOfSelect}}\{{@index}}', array_merge(array('make_unit_choice'=>'Choose a unit'),$capacity['units']), null); ?>
</script>