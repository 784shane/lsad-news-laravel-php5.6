@extends($layout)
@section('content')
    @if($view_data['user_found'])
        {{-- Now, if a user was found, we will show a particular screen here. --}}
        <div>
            <h2>Success!</h2>
            <p>We have sent an email to you with instructions on logging in.</p>
            <p>To log in to the site {!! Html::link('login','click here.') !!}.</p>
        </div>
    @else
        {{-- And ofcourse if the user was not found, we show this view. --}}
        <div>
            <h2>User not found!</h2>
            <p>We are sorry, it seems that this user is not registered.</p>
            <p>please check the email entered and try again.</p>
            <p>Alternatively, you can register {!! Html::link('signup','here') !!}.</p>
        </div>
    @endif

@stop
