@extends($layout)
@section('content')
{!! Form::open(array('url' => 'deliveries/form')) !!}
<div id="deliveryBasicInfo" data-ng-controller="deliveryBasicInfo">
    <div>
        <h3>What are you moving?</h3>
        {!! Form::select('itemToShip', $itemsToShip) !!}
    </div>
    <div>
        <h3>Collection Postcode</h3>
        {!! Form::text('collection_postcode', $value = null, 
        array('data-blur-directive' => 'post-code-from',
        'data-ng-model' => 'postcodefrom',
        'placeholder' => 'Username', 
        'class'=> 'form-control', 'required' => 'required', 
        'autofocus' => 'autofocus' )) !!}
        <br />
        @{{ postcode1 }}
    </div>
    <div>
        <h3>Delivery Postcode</h3>
        {!! Form::text('delivery_postcode', $value = null, 
        array('data-blur-directive' => 'post-code-to',
        'data-ng-model' => 'postcodeto',
        'placeholder' => 'Username', 'required' => 'required' )) !!}
        <br />
        @{{ postcode2 }}
        <br /><br />
    </div>
    <div>
        {!! Form::submit('Get free Quotes') !!}
    </div>
    <div>
        {{-- Hidden fields - Info collected for form submission --}}
        {!! Form::hidden('collection_address', $value = '@{{ postcode1 }}' ) !!}
        {!! Form::hidden('delivery_address', $value = '@{{ postcode2 }}' ) !!}
    </div>
</div>
{!! Form::close() !!}
@stop