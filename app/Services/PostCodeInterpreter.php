<?php namespace App\Services;

use App\Services\CurlRequests;

class PostCodeInterpreter
{
    const GEOCODE_JSON_BASE_URL = 'http://maps.googleapis.com/maps/api';
    const GOOGLE_GEOCODE_JSON_URL = '/geocode/json';
    const GOOGLE_DISTANCE_MATRIX_JSON_URL = '/distancematrix/json';
    //https://maps.googleapis.com/maps/api/distancematrix/json?origins=Vancouver+BC|Seattle&destinations=San+Francisco|Victoria+BC&key=YOUR_API_KEY
    //https://maps.googleapis.com/maps/api/distancematrix/json?origins=cr3%205lb&destinations=sw2%202ht
    const GEOCODE_POSTAL_CODE_KEY = 'postal_code';
    const GEOCODE_NEIGHBOURHOOD_KEY = 'neighborhood';
    const GEOCODE_LOCALITY_KEY = 'locality';
    const GEOCODE_POSTAL_TOWN_KEY = 'postal_town';
    const GEOCODE_ADMINISTRATIVE_AREA_LEVEL2_KEY = 'administrative_area_level_2';
    const GEOCODE_COUNTRY_KEY = 'country';
    private $curlRequest = null;
    private $address = [
        self::GEOCODE_POSTAL_CODE_KEY => '',
        self::GEOCODE_NEIGHBOURHOOD_KEY => '',
        self::GEOCODE_LOCALITY_KEY => '',
        self::GEOCODE_POSTAL_TOWN_KEY => '',
        self::GEOCODE_ADMINISTRATIVE_AREA_LEVEL2_KEY => '',
        self::GEOCODE_COUNTRY_KEY => ''
    ];

    private function getCurlRequest()
    {
        if ($this->curlRequest === null)
        {
            $this->curlRequest = new CurlRequests();
        }
        return $this->curlRequest;
    }

    private function resetAddressArray(){
        $this->address = [
            self::GEOCODE_POSTAL_CODE_KEY => '',
            self::GEOCODE_NEIGHBOURHOOD_KEY => '',
            self::GEOCODE_LOCALITY_KEY => '',
            self::GEOCODE_POSTAL_TOWN_KEY => '',
            self::GEOCODE_ADMINISTRATIVE_AREA_LEVEL2_KEY => '',
            self::GEOCODE_COUNTRY_KEY => ''
        ];
    }

    private function createGeoAddressUrl($postCode)
    {
        return self::GEOCODE_JSON_BASE_URL . self::GOOGLE_GEOCODE_JSON_URL . '?address=' . $postCode;
    }

    /**
     * Compiles the url that will be called for the distance calculation.
     *
     * @param string $postCode1 //- url encoded postcode string
     * @param string $postCode2 //- url encoded postcode string
     *
     * @return string
     */
    public function getDistanceBetweenPostCodes($postCode1, $postCode2)
    {
        $distanceMatrixUrl = $this->createDistanceMatrixUrl(urlencode($postCode1), urlencode($postCode2));
        $addressLookupResult = $this->getCurlRequest()->distanceLookupFromPostCodes($distanceMatrixUrl);
        if($addressLookupResult['status'] === "success"){
            return $addressLookupResult['response'];
        }
        return '';
    }

    /**
     * Compiles the url that will be called for the distance calculation.
     *
     * @param string $postCode1 //- url encoded postcode string
     * @param string $postCode2 //- url encoded postcode string
     *
     * @return string
     */
    private function createDistanceMatrixUrl($postCode1, $postCode2)
    {
        return self::GEOCODE_JSON_BASE_URL . self::GOOGLE_DISTANCE_MATRIX_JSON_URL . '?origins=' . $postCode1 . '&destinations=' . $postCode2;
    }

    public function getAddressGeometry($postCode, $format = true)
    {
        $geoAddressUrl = $this->createGeoAddressUrl(urlencode($postCode));
        $addressLookupResult = $this->getCurlRequest()->addressLookupFromPostCode($geoAddressUrl);
        $geometry = $this->extractAddressGeometry($addressLookupResult);
        return $geometry;
    }

    public function getLocationFromPostcode($postCode, $format = true)
    {
        $geoAddressUrl = $this->createGeoAddressUrl(urlencode($postCode));
        $addressLookupResult = $this->getCurlRequest()->addressLookupFromPostCode($geoAddressUrl);
        // Reset the address array - So that no elements are left as rouge values.
        $this->resetAddressArray();
        $this->extractAddress($addressLookupResult);
        if ($format)
        {
            return $this->formatAddressArrayToString();
        }
        return $this->address;
    }

    private function extractAddressGeometry($addressLookupResult)
    {
        $geometry = [];
        if (isset($addressLookupResult['response']->results))
        {
            foreach($addressLookupResult['response']->results as $results)
            {
                $geometry = (array)$results->geometry;
                break;
            }
        }
        return $geometry;
    }

    private function extractAddress($addressLookupResult)
    {
        if (isset($addressLookupResult['response']->results))
        {
            foreach($addressLookupResult['response']->results as $results)
            {
                if (isset($results->address_components))
                {
                    foreach($results->address_components as $address_components)
                    {
                        $this->setAddressArray($address_components);
                    }
                }
                break;
            }
        }
    }

    private function setAddressArray($address_components)
    {
        foreach($this->address as $add => $a)
        {
            if (in_array($add, $address_components->types))
            {
                $this->address[$add] = $address_components->long_name;
            }
        }
    }

    private function formatAddressArrayToString()
    {
        return (!empty($this->address[self::GEOCODE_NEIGHBOURHOOD_KEY])?($this->address[self::GEOCODE_NEIGHBOURHOOD_KEY] . ', '):"") .
        (!empty($this->address[self::GEOCODE_POSTAL_TOWN_KEY])?($this->address[self::GEOCODE_POSTAL_TOWN_KEY] . ', '):"") .
        (!empty($this->address[self::GEOCODE_ADMINISTRATIVE_AREA_LEVEL2_KEY])?($this->address[self::GEOCODE_ADMINISTRATIVE_AREA_LEVEL2_KEY] . ', '):"") .
        (!empty($this->address[self::GEOCODE_COUNTRY_KEY])?($this->address[self::GEOCODE_COUNTRY_KEY] . ', '):"") .
        (!empty($this->address[self::GEOCODE_POSTAL_CODE_KEY])?($this->address[self::GEOCODE_POSTAL_CODE_KEY]):"");
    }
}