<?php namespace App\Services\Deliveries;


class DeliveriesEdit extends Deliveries{


    public function isDeliveryEligibleForEditing($delivery)
    {
        $privateDeliveriesStatus = \Config::get('constants.deliveries.statuses.ACTIVE_PRIVATE');
        if($delivery[0]['status'] < $privateDeliveriesStatus)
        {
            return true;
        }
        return false;
    }


    public function submitDeliveryRevision($deliveryId){
        // @Todo We must do some validation.
        // Skip this step until we do the updating.
        $posts = \Request::all();

        // other files could have been uploaded as well, but this brings back
        // the main image file for sure. We will have to check the database for
        // the image files.
        $photos = new \App\Services\Photos();
        $filesUploaded = $photos->uploadPhotoWhenEditing();

        $itemsToSave = $this->getItemsToSave($posts, true);
        $itemGroups = $itemsToSave['itemGroups'];

        $this->updateDeliveryDetails($deliveryId, $posts);

        // We will attempt to update the items.
        // ... for the items table ...
        $this->updateItemsTable($deliveryId, $itemGroups);
    }

    private function updateDeliveryDetails($deliveryId, $posts)
    {
        $collection_postcode = $posts['collection_postcode'];
        $delivery_postcode = $posts['delivery_postcode'];
        $addressData = $this->getAddressAndMileageFromPostCodes($collection_postcode, $delivery_postcode);

        $updateInfo = [
            'use_as_main_image'=>isset($posts['use_as_main_image'])?$posts['use_as_main_image']:null,
            'subtitle' => $posts['delivery_title'],
            'collection_postcode' => $collection_postcode,
            'delivery_postcode' => $delivery_postcode,
            'collection_address' => isset($addressData['collection_address'])?$addressData['collection_address']:"",
            'delivery_address' => isset($addressData['delivery_address'])?$addressData['delivery_address']:"",
            'distance_text' => isset($addressData['distance_text'])?$addressData['distance_text']:"",
            'distance_metres' => isset($addressData['distance_metres'])?$addressData['distance_metres']:""
        ];

        $delModel = new \App\Deliveries();
        $delModel->updateDelivery('id', $deliveryId, $updateInfo);
    }

    private function updateItemsTable($deliveryId, $itemGroups)
    {

        $itemsCountArray = [];

        foreach($itemGroups as $itemGroupKey => $itemGroup){
            $itemsModel = null;

            foreach($itemGroup['groupOfIndexItems'] as $groupOfIndexItem){
                // Create new items in the items table.

                if(!in_array($groupOfIndexItem['order'],$itemsCountArray)){
                    $this->AddItemsToDelivery($itemsCountArray, $groupOfIndexItem['order'], $deliveryId);

                    $itemsModel = \App\Items::where('delivery',$deliveryId)
                        ->where('item_order' , $itemGroupKey)->get();

                    $itemsCountArray[] = $groupOfIndexItem['order'];
                }

                // Add values to the items_values table
                if(!empty($groupOfIndexItem['item_name'] && null !== $itemsModel && $itemsModel->count() > 0)){
                    $itemValuesModel = \App\ItemValues::firstOrNew(array(
                        'item' => $itemsModel[0]['id'],
                        'name' => $groupOfIndexItem['item_name'],
                    ));
                    $itemValuesModel->value = $groupOfIndexItem['item_value'];
                    $itemValuesModel->save();
                }

            }

            $itemSpecificsModel = \App\ItemSpecifics::firstOrNew(array(
                'item' => $itemsModel[0]['id']
            ));

            // Add/edit information to the item specifics table.
            $addSpecifics = false;
            foreach($itemGroup['groupOfIndexItemsSpecifics'] as $groupOfIndexItemsSpecific){
                if(!empty($groupOfIndexItemsSpecific['item_name'])){
                    // Add items to the items specific table.
                    $itemSpecificsModel->{$groupOfIndexItemsSpecific['item_name']} = $groupOfIndexItemsSpecific['item_value'];
                    $addSpecifics = true;
                }
            }

            if($addSpecifics){
                $itemSpecificsModel->save();
            }

        }

        // Now lets remove items from the items table that has been deleted.
        // The user would have deleted an item so we will have to clean up as well.
        $this->removeItemsFromDelivery($itemsCountArray, $deliveryId);
    }

}