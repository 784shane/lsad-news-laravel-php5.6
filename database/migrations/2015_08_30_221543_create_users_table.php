<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table)
	    {
	        $table->increments('id');
	        $table->string('first_name')->nullable();
	        $table->string('last_name')->nullable();
	        $table->string('email')->unique();
	        $table->string('user_name')->nullable();
	        $table->string('telephone_number')->nullable();
			$table->string('password', 60);
			$table->string('register_token', 60)->unique();
			$table->string('forgot_token', 60)->unique();
			$table->boolean('transport_company')->default(false);
			$table->boolean('transport_company_verified')->default(false);
			//$table->boolean('register_token_sent');
			$table->boolean('forgotten_pass_token_sent');
			$table->rememberToken();
	        $table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
