<?php ?>
@if(isset($message_p) && isset($message_p['created_at']))
    <div class="my_message_item">
        <div class="pure-g">
            <div class="pure-u-1-3">
                <div class="pure-g">
                    <div class="pure-u-4-24"><b>From:</b></div>
                    <div class="pure-u-20-24">{{ $message_p['user_name'] }}</div>
                </div>
            </div>
            <div class="pure-u-1-3">
                <div class="pure-g">
                    <div class="pure-u-3-24"><b>Date:</b></div>
                    <div class="pure-u-21-24">{{ $vComposerHelper->viewDatesDisplay($message_p['created_at']) }}</div>
                </div>
            </div>
        </div>
        <div class="my_message_item_row">
            <div>{{ $message_p['message'] }}</div>
        </div>
        <div class="pure-g">
            <div class="pure-u-1-5">{!! Html::link('message/'.$message_p['id'],'Read more') !!}</div>
            <div class="pure-u-4-5"></div>
        </div>
    </div>
@else
    <div class="delivery-message-item">
        Messages will show up here
    </div>
@endif


{{--
<div class="delivery-message-item">
    <div>
        <div>From</div>
        <div>{{ $message['user_name'] }}</div>
    </div>
    <div>
        <div>Date</div>
        <div>{{ $vComposerHelper->viewDatesDisplay($message['created_at']) }}</div>
    </div>
    <div>
        <div>Message</div>
        <div>{{ $message['message'] }}</div>
    </div>
    <div>{!! Html::link('message/'.$message['id'],'Read') !!}
    </div>
</div>
--}}