<?php namespace App\Http\Controllers;

use Deliveries;
use Messages;
use Request;
use App\Services\Auth\Signup;
use App\Services\Auth\Business;
use App\Services\Auth\User as AuthUser;
use SiteValidator;

class HomeController extends PrivateController
{
    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */
    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {
        return $this->myDeliveriesActive();
    }

    public function myDeliveriesActive()
    {
        return $this->getMyDeliveries('active');
    }

    public function myDeliveriesArchived()
    {
        return $this->getMyDeliveries('archived');
    }

    private function getMyDeliveries($deliveriesStatus=null){
        $myDeliveries = Deliveries::getPrivateDeliveries($deliveriesStatus);
        $this->viewData['myDeliveries'] = $myDeliveries;
        return view('home')->with($this->viewData);
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function myAccount($accountType = "personal")
    {
        $authUser = new AuthUser();
        $userId = $authUser->getLoggedInUsersId();
        if (Request::isMethod('post'))
        {
            $signup = new Signup();
            if ($accountType == "personal")
            {
                $userEditedResults = $signup->processEditedPerson($userId);
                if ($userEditedResults['result'] === "fail")
                {
                    return redirect()->back()
                        ->withErrors($userEditedResults['validatorResults']);
                }
            }else
            {
                $signup->processEditedBusiness();
            }
        }
        $businessService = new Business();
        $hasBusiness = $businessService->userHasTransportProvider($userId);
        if ($accountType == "business")
        {
            //Set the business Details
            $this->setBusinessDetails($userId, $hasBusiness, $businessService);
        }else
        {
            //Set the users Details
            $this->setUsersDetails($authUser);
        }
        $this->viewData['accountType'] = $accountType;
        $this->viewData['userHasBusiness'] = $hasBusiness;
        return view('account.index')->with($this->viewData);
    }

    private function setBusinessDetails($userId, $hasBusiness, $businessService)
    {
        if ($hasBusiness)
        {
            $this->viewData['businessDetails'] = $businessService->getTransportProviderDetails($userId);
        }else
        {
            $this->viewData['businessDetails'] = $businessService->getBusinessEmptyDetails();
        }
    }

    private function setUsersDetails($authUser)
    {
        $rss = $authUser->getLoggedInUserObject();
        $s = new \stdClass();
        $s->first_name = $rss['user_object']['first_name'];
        $s->last_name = $rss['user_object']['last_name'];
        $s->email = $rss['user_object']['email'];
        $s->telephone_number = $rss['user_object']['telephone_number'];
        $s->user_name = $rss['user_object']['user_name'];
        $this->viewData['userDetails'] = $s;
        return $authUser->getLoggedInUserObject();
    }

    /**
     * Route function - calls by route /messages
     *
     * @param int $messageId
     *
     * @return Object View Object
     */
    public function messages($messageId = null)
    {
        if (is_null($messageId))
        {

            switch(Request::get('tab')){
                case "other-deliveries-messages":{
                    // We will show messages on other deliveries which concern me.
                    $messageItems = Messages::getDeliveriesWithMyMessages();
                    break;
                }

                default:{
                    //This means we will show our group of messages here.
                    //We will group the messages according by user
                    $messageItems = Messages::buildUsersDeliveriesMessages(Deliveries::getPrivateDeliveries());
                }
            }
            $this->viewData['messageItems'] = $messageItems;
            //exit(vde($this->viewData['messageItems']));
        }
        return view('messages.messages')->with($this->viewData);
    }

    /**
     * Route function - calls by route /message
     *
     * @param int $messageId
     * @return Object View Object
     */
    public function message($messageId)
    {
        $errorsArray = null;
        if (Request::isMethod('post'))
        {
            // Once we have a post we will check out the directive.
            switch(Request::get('post_directive'))
            {
                case 'post_reply_button':
                {
                    // The request is to reply to this message.
                    $this->viewData['reply_to_message'] = true;
                    break;
                }
                case 'post_message':
                {
                    $messageReply = Request::get('quote_message');
                    // At this point, we will post the reply to this message
                    $quoteSubmitAttempt = Messages::makeMessageReply(Request::all(), (int)$messageId);
                    if ($quoteSubmitAttempt['result'] === "fail")
                    {
                        // These are the errors returning from the message replying
                        $errorsArray = $quoteSubmitAttempt['validatorResults'];
                        //Send the user back to the submit form
                        $this->viewData['reply_to_message'] = true;
                    }
                    break;
                }
            }
        }
        $message = Messages::processOneDeliveryMessage($messageId);
        //Now get all messages associated with this delivery and with this sender.
        $sendersMessages = Messages::getDeliveriesSendersMessage($messageId);
        // set the message items
        $this->viewData['main_message'] = $message;
        $this->viewData['senders_messages'] = $sendersMessages;
        return view('messages.message')->with($this->viewData)->withErrors($errorsArray);
    }
}
