@extends($layout)
@section('content')

    <h1>Login</h1>

    {!! Form::open(array(
    'url' => 'login',
    'id'=>"login_form",
    'class'=>"pure-form pure-form-stacked")) !!}
    <fieldset>

        <div>
            {!! Form::label('email', 'Email Address') !!}
            {!! Form::email('email', Input::old('email'), array('id'=>'email', 'placeholder' => '', 'required' => 'required')) !!}
            <span class="form-errors">{{ $errors->first('email') }}</span>
        </div>

        <div>
            {!! Form::label('password', 'Password') !!}
            {!! Form::password('password', null, array('id'=>'password', 'required' => 'required')) !!}
            <span class="form-errors">{{ $errors->first('password') }}</span>
        </div>

        <div>
            {!! Form::submit('Submit', array('class'=>'pure-button pure-button-primary narrow-submit-button')) !!}
        </div>
    </fieldset>

    {!! Form::close() !!}


    <p>Have you forgotten your password? {!! Html::link('forgot','Click here.') !!}</p>

@stop
