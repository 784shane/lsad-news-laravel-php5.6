@extends('layouts.master')
@section('content')
    <h1>About Us</h1>
    <div>
        <p>Purple Mover was established in 2016 to enable people who have goods/items of any kind that need to be delivered locally or further afield, to find a Transport Company/individual to carry out their deliveries quickly and without any fuss.</p>

        <p>Purple Mover is a fully independent company with no connections to anyone who uses the site be it a Member or Transport Provider/Individual. We are all about providing you with a guarded, safe, individual and credible online marketplace.</p>

        <p>Learn more about us at www.purplemover.com and read our feedback page.</p>
    </div>
@stop
