<?php
namespace App\Http\Controllers;

class CompanyController extends PublicController
{
    public function contactUs()
    {
        if (\Request::isMethod('post'))
        {
            $contact = new \App\Services\Contact();
            $savedInformation = $contact->submitEnquiry();
            if ($savedInformation['result'] == "success")
            {
                return \View::make('company.contactUsThanks')->with($this->viewData);
            }
            // We want to remember this information incase we had an error
            \Request::flash();
            return redirect(route('contact-us'))
                ->withErrors($savedInformation['validatorResults']);
        }
        // We will create a contact us form so that receive user's messages etc.
        return \View::make('company.contactUs')->with($this->viewData);
    }
}