
$(document).ready(function(){

  // Start the ebayService service.
  var ebayService = new ebayItemService(new httpService(), new globalHelperService());
  ebayService.init();

  // Start the uploadPhotoService service.
  var upload_photo_service = new uploadPhotoService(new httpService());
  upload_photo_service.init();

  /*

   <h1>Handlebars JS Example</h1>
   <script id="some-template" type="text/x-handlebars-template">

   <select dropdown multiple class="dropdown" name="color">
   {{#each colors}}
   <option value="{{value}}" {{isSelected parentColor id}}>{{title}}</option>
   {{/each}}
   </select>
   </script>



   var data = {
   colors: [
   {id:1,value:1,title:"red",parentColor:2},
   {id:2,value:2,title:"green",parentColor:2},
   {id:3,value:3,title:"blue",parentColor:1}
   ]
   };


   Handlebars.registerHelper('isSelected', function (input, color) {
   return input === color ? 'selected' : '';
   });

   $('body').append(template(data));
   */
});