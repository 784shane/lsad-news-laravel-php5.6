<?php namespace App\Services\Deliveries;

use \Storage;

class DeliveriesExpiryData extends Storage
{
    const EXPIRY_DIRECTORIES = 'expiries';
    const EXPIRY_CHECKED_FILE = 'expiry_checked.txt';

    public function getStorageDirectory()
    {
        if (!self::exists(self::EXPIRY_DIRECTORIES))
        {
            // Create the storage directory
            if ($this->createExpiryLocation())
            {
                return self::EXPIRY_DIRECTORIES;
            }
            return null;
        }
        return self::EXPIRY_DIRECTORIES;
    }

    private function createExpiryLocation()
    {
        return self::makeDirectory(self::EXPIRY_DIRECTORIES);
    }

    private function createExpiryFile()
    {
        self::put($this->getStorageDirectory().'/'.self::EXPIRY_CHECKED_FILE, '');
    }

    public function getLastExpiryCheckedDate()
    {
        if (self::exists($this->getStorageDirectory().'/'.self::EXPIRY_CHECKED_FILE))
        {
            // read the file and get its date.
            return self::get($this->getStorageDirectory().'/'.self::EXPIRY_CHECKED_FILE);
        }
        $this->createExpiryFile();
        // If the file doesn't exist, we will run an expiry cleanup.
        return '';
    }

    public function setLastExpiryCheckDate()
    {
        // The last checked date has to be now
        self::put($this->getStorageDirectory().'/'.self::EXPIRY_CHECKED_FILE, time());
    }
}