@extends($layout)
@section('content')

    {!! Form::open(array('url' => 'deliveries/form',
    'id'=>"collect_deliveries_info_pt1",
    'class'=>"pure-form pure-form-stacked")) !!}
    <fieldset>
        <h1>Delivery Information</h1>

        <label for="shipping_category">What are you moving?</label>
        {!! Form::select('shipping_category', $itemsToShip, 'null', array('id'=>'shipping_category')) !!}

        <label for="collection_postcode">Collection Postcode</label>
        {!! Form::text('collection_postcode', $value = null,
            array('id'=>'collection_postcode', 'required' => 'required', 'placeholder' => 'Post Code',
            'autofocus' => 'autofocus' )) !!}
        <span id="postcode1_address"></span>

        <label for="delivery_postcode">Delivery Postcode</label>
        {!! Form::text('delivery_postcode', $value = null,
            array('id'=>'delivery_postcode', 'required' => 'required', 'placeholder' => 'Post Code' )) !!}
        <span id="postcode2_address"></span>
        <div class="submit_surround">
            {!! Form::submit('Get free Quotes', array('class'=>'pure-button pure-button-primary narrow-submit-button')) !!}
        </div>
    </fieldset>
    {{-- Hidden fields - Info collected for form submission --}}
    {!! Form::hidden('collection_address', null, array('id'=>'collection_address') ) !!}
    {!! Form::hidden('delivery_address', null, array('id'=>'delivery_address') ) !!}

    {!! Form::close() !!}
@stop

@section('scripts')
    {!! Html::script('js/inline_scripts/index.js') !!}
@stop