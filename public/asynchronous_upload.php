<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Title of the document</title>
	<script src="http://code.jquery.com/jquery-2.2.0.min.js"></script>
</head>

<body>

<form enctype="multipart/form-data">
	<input name="file" type="file" />
	<input type="button" value="Upload" />
</form>
<progress></progress>
</body>

</html>

<script>

	$(':file').change(function(){
		var file = this.files[0];
		var name = file.name;
		var size = file.size;
		var type = file.type;
		//Your validation
	});

	function progressHandlingFunction(e){
		if(e.lengthComputable){
			$('progress').attr({value:e.loaded,max:e.total});
		}
	}


	$(':button').click(function(){
		var formData = new FormData($('form')[0]);
		$.ajax({
			url: 'upload.php',  //Server script to process data
			type: 'POST',
			xhr: function() {  // Custom XMLHttpRequest
				var myXhr = $.ajaxSettings.xhr();
				if(myXhr.upload){ // Check if upload property exists
					myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
				}
				return myXhr;
			},
			//Ajax events
			beforeSend: function(result){},
			success: function(result){
				console.log(result);
			},
			error: function(result){
				console.log(result);
			},
			// Form data
			data: formData,
			//Options to tell jQuery not to process data or worry about content-type.
			cache: false,
			contentType: false,
			processData: false
		});
	});
</script>