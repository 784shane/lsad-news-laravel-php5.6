<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemSpecificsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('item_specifics', function($table)
		{
			$table->increments('id');
			$table->integer('item',false, true)->length(11);
			$table->text('description')->nullable();
			$table->text('main_image')->nullable();

			$table->foreign('item')->references('id')->on('items');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item_specifics');
	}

}
