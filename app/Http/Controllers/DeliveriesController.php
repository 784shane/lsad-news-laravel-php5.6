<?php
namespace App\Http\Controllers;

use Request;
use Deliveries;
use App\Services\Auth\User as AuthUser;
use App\Events\shane;
use Log;
use Units;
use App\Services\PostCodeInterpreter;

class DeliveriesController extends PublicController
{
    public function __construct()
    {
        // You must be logged in to see these routes.
        $this->middleware('auth', ['only' => [
            'editDelivery',
            'deleteDelivery',
            'quote'
        ]]);
        parent::__construct();
    }

    public function deliveryInput()
    {
        // Using the data that has been passed over from the
        // basic delivery collection, we will prepare the data
        // to be used in the view.
        $this->setBasicDeliveryData();
        $email = null;
        $username = null;
        $telephone_number = null;
        $authUser = new AuthUser();
        $isUserLoggedIn = $authUser->isUserLoggedIn();
        $loggedInUserObj = $authUser->getLoggedInUserObject();
        if ($loggedInUserObj && $loggedInUserObj['result'] == "success")
        {
            $email = $loggedInUserObj['user_object']['email'];
            $username = $loggedInUserObj['user_object']['user_name'];
            $telephone_number = $loggedInUserObj['user_object']['telephone_number'];
        }
        $units = Units::getUnitChoices();
        $this->viewData['unitsOfMeasurement'] = $units;
        $this->viewData['isUserLoggedIn'] = $isUserLoggedIn;
        $this->viewData['users_email'] = $email;
        $this->viewData['users_username'] = $username;
        $this->viewData['users_telephone_number'] = $telephone_number;
        return view('deliveries.input')->with($this->viewData);
    }

    private function setBasicDeliveryData()
    {
        $allPost = Request::all();
        $basicDeliveryData = [
            'shipping_category' => isset($allPost['shipping_category'])?$allPost['shipping_category']:'',
            'collection_postcode' => isset($allPost['collection_postcode'])?$allPost['collection_postcode']:'',
            'delivery_postcode' => isset($allPost['delivery_postcode'])?$allPost['delivery_postcode']:'',
            'collection_address' => isset($allPost['collection_address'])?$allPost['collection_address']:'',
            'delivery_address' => isset($allPost['delivery_address'])?$allPost['delivery_address']:''
        ];
        $this->viewData = array_merge($this->viewData, $basicDeliveryData);
    }

    public function editDelivery($jobId)
    {
        $delivery = Deliveries::getDelivery($jobId);
        if (\DeliveriesEdit::isDeliveryEligibleForEditing($delivery))
        {
            if (Request::isMethod('post'))
            {
                \DeliveriesEdit::submitDeliveryRevision($jobId);
                // Refresh this delivery since we have just edited it
                $delivery = Deliveries::getDelivery($jobId);
            }
            // Set the form time
            $this->viewData['formTime'] = time();
            // We will be editing this delivery so then we will have to get the
            // info on it
            $this->viewData['delivery'] = $delivery;
            $units = Units::getUnitChoices();
            $this->viewData['unitsOfMeasurement'] = $units;
            return view('deliveries.edit')->with($this->viewData);
        }
        \Alert::error('Sorry, you are not allowed to edit this delivery!');
        return redirect()->to('home');
    }

    public function deleteDelivery($jobId, $itemNo = null)
    {
        // Notes
        // If the deliveryId and itemNo are present, we know we must delete
        // on the item
        // If the itemNo is missing, we will remove the delivery (soft deletes only if quotes were accepted)
        $deleteResult = \DeliveriesDelete::attemptDelete($jobId, $itemNo);
        if ($deleteResult['deleted'])
        {
            if (isset($deleteResult['info']))
            {
                switch($deleteResult['info'])
                {
                    case 'item_deleted':
                    {
                        \Alert::success('Delivery item has been deleted');
                        return redirect()->to(route('deliveries.edit', ['jobId' => $jobId]));
                        break;
                    }
                    case 'delivery_deleted':
                    {
                        \Alert::success('Delivery has been deleted');
                        return redirect()->to('home');
                        break;
                    }
                }
            }
        }
        // Tell the user we wont delete this delivery.
        \Alert::error('Sorry, we wont delete this delivery!');
        return redirect()->to(route('deliveries.edit', ['jobId' => $jobId]));
    }

    public function processDeliveriesInfo()
    {
        $userAddedResults = Deliveries::processNewlyAddedDeliveriesUsers();
        if ($userAddedResults['result'] === "fail")
        {
            return redirect()->to('/deliveries/form')->withErrors($userAddedResults['validatorResults']);
        }
        //its time to input the data of deliveries
        Deliveries::attemptToAddDeliveries();
        return view('deliveries.delivery_added')->with($this->viewData);
    }

    /**
     * Route function - calls by route
     *
     * @param String $jobName
     * @param int $jobNumber
     *
     * @return View Object
     */
    public function liveJobs($jobName = null, $jobNumber = null)
    {
        //$management = new \App\Services\Deliveries\DeliveriesExpiryManagement();
        //$management->expirationCheck();
        //$podcast = "not sure";
        //$response = \Event::fire(new \App\Events\PodcastWasPurchased($podcast));
        //\Storage::disk('local')->put('file.txt', 'Contents');
        // update deliveries set expiry = `2016-04-19 23:24:22` where expiry = null;
        if (!is_null($jobNumber))
        {
            $aDelivery = Deliveries::getDelivery($jobNumber);
            if (!is_null($aDelivery))
            {
                $authUser = new AuthUser();
                $this->viewData['loggedInUser'] = $authUser->getLoggedInUsersId();
                $this->viewData['myDeliveries'] = $aDelivery;
                // Get the details of the owner of this delivery
                $this->viewData['owner'] = \App\User::find($aDelivery[0]['user'])->toArray();
                // Get quotes on this delivery.
                $quotesService = new \App\Services\Quotes\Quotes();
                $this->viewData['quotesInfo'] = $quotesService->getQuotes($jobNumber);
                $this->viewData['myDeliveries']['toAndFromGeometry'] = $this->setFromAndToGeometry($aDelivery);
                return view('deliveries.singleJob')->with($this->viewData);
            }
            exit("delivery not found");
        }
        $myDeliveries = Deliveries::getPublicDeliveries();
        $this->viewData['myDeliveries'] = $myDeliveries;
        return view('deliveries.liveJobs')->with($this->viewData);
    }

    public function preQuote($jobName = null, $jobNumber = null)
    {
        $allPosts = \Request::all();
        //Now we can re-calculate this quote and show it to the user
        $quotesService = new \App\Services\Quotes\Quotes();
        $result = $quotesService->recalculateQuote($allPosts);
        if ($result['result'] != 'success')
        {

        }

        // We should have the quote all recalculated, so we could add this
        // data to the view data array
        if (isset($result['recalculated_quote']))
        {
            $this->viewData['quote_recalculated'] = true;
            $this->viewData['your_fee'] = $result['recalculated_quote']['your_fee'];
            $this->viewData['our_fee'] = $result['recalculated_quote']['our_fee'];
            $this->viewData['our_quote'] = $result['recalculated_quote']['quote'];
            $this->viewData['saved_hash'] = $result['recalculated_quote']['saved_hash'];
        }

        $this->viewData['myDeliveries'] = Deliveries::getDelivery($jobNumber);
        $this->viewData['myLatestQuote'] = isset($allPosts['job_quote_amt'])?$allPosts['job_quote_amt']:'';
        $this->viewData['myLatestMessage'] = isset($allPosts['quote_message'])?$allPosts['quote_message']:'';
        return view('deliveries.quoteJob')->with($this->viewData);
    }

    public function saveQuote($jobName = null, $jobNumber = null){
            $quotesService = new \App\Services\Quotes\Quotes();
            $quoteSubmitAttempt = $quotesService->processQuotes($jobNumber);
            if ($quoteSubmitAttempt['result'] === "fail")
            {
                return redirect()->back()
                    ->withErrors($quoteSubmitAttempt['validatorResults']);
            }

            $aDelivery = Deliveries::getDelivery($jobNumber);
            $this->viewData['myDeliveries'] = $aDelivery;
            // Lets get the last quotation
            $this->viewData = array_merge($this->viewData, array('quoted_info' => Request::all()));
            $this->viewData['quote_status'] = 'complete';
            return view('deliveries.quoteJob')->with($this->viewData);
    }

    /**
     * Route function - calls by route /quote
     *
     * @param String $jobName
     * @param int $jobNumber
     * @param String $action
     *
     * @return View Object
     */
    public function quote($jobName = null, $jobNumber = null, $action = null)
    {
        $quotesService = new \App\Services\Quotes\Quotes();
        $aDelivery = Deliveries::getDelivery($jobNumber);
        $this->viewData['myDeliveries'] = $aDelivery;
        $quoterInfo = $quotesService->getQuotersLatestQuote($jobNumber);
        $this->viewData['myLatestQuote'] = $quoterInfo['quote'];

        //See if there was a form submission
        if (Request::isMethod('post'))
        {
            // We might post back some data to fill in the inputs here
            // Example, if a previous post was made and we need to cancel a quotation.
            $posts = \Request::all();
            if(isset($posts['your_fee'])){
                $this->viewData['myLatestQuote'] = $posts['your_fee'];
            }
            $this->viewData['myLatestMessage'] = isset($posts['quote_message'])?$posts['quote_message']:'';
        }
        return view('deliveries.quoteJob')->with($this->viewData);
    }

    /**
     * Compiles the 'from' and 'to' location data
     * into an array
     *
     * @param array $deliveryDetails
     *
     * @return array
     */
    private function setFromAndToGeometry($deliveryDetails)
    {
        $pcodeInterpreter = new PostCodeInterpreter();
        $fromGeometry = $pcodeInterpreter->getAddressGeometry($deliveryDetails[0]['collection_postcode']);
        $toGeometry = $pcodeInterpreter->getAddressGeometry($deliveryDetails[0]['delivery_postcode']);
        return ['from' => [
            'location' => [
                'lat' => isset($fromGeometry['location']->lat)?$fromGeometry['location']->lat:0,
                'lng' => isset($fromGeometry['location']->lng)?$fromGeometry['location']->lng:0
            ]
        ],
            'to' => [
                'location' => [
                    'lat' => isset($toGeometry['location']->lat)?$toGeometry['location']->lat:0,
                    'lng' => isset($toGeometry['location']->lng)?$toGeometry['location']->lng:0
                ]
            ]
        ];
    }
}
