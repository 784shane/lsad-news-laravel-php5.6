@extends($layout)
@section('content')

    <h1>Edit Delivery</h1>


    {!! Form::open(array(
        'id'=>'deliveries_edit_form',
        'class'=>"pure-form pure-form-stacked",
        'enctype'=>'multipart/form-data')) !!}

    {!! Form::submit('Update', array('class'=>'pure-button pure-button-primary narrow-submit-button')) !!}


    <fieldset class="pure-group">

        <div class="pure-g row">
            <div class="pure-u-6-24">{!!Form::label('main_image', 'Main image')!!}</div>
            <div class="pure-u-18-24">
                <div class="edit_image_box">
                {!!Html::image($vComposerHelper->missingImageResolver($delivery[0]['overallImage']), $delivery[0]['subtitle'], array('width'=>'100%'))!!}
                </div>
                <!--
                The idea here is to build a photos service and use a deliveries_photos database.
                // May not be necessary at this time. Upon revision of the site, we might implement
                // something similar. For now though, we will attain our main image from the image on the
                // item. We attain this at run time. Also, if the 'use as main image' is checked, we will
                // use the image associated with the item.
                <div class="pure-g">
                    <div class="pure-u-6-24">{{-- !!Form::label('update_main_image_file', 'Update main image')!! --}}</div>
                    <div class="pure-u-18-24">{{-- !!Form::file('update_main_image_file')!! --}}</div>
                </div>
                -->
            </div>
        </div>

        <div class="pure-g row">
            <div class="pure-u-6-24">{!!Form::label('delivery_title', 'Title of delivery')!!}</div>
            <div class="pure-u-18-24">{!!Form::text('delivery_title', $delivery[0]['subtitle'] )!!}</div>
        </div>

        <div class="pure-g row">
            <div class="pure-u-6-24">{!!Form::label('collection_postcode', 'Postcode (Collection point)')!!}</div>
            <div class="pure-u-18-24">{!!Form::text('collection_postcode', $delivery[0]['collection_postcode'] )!!}</div>
        </div>

        <div class="pure-g row">
            <div class="pure-u-6-24">{!!Form::label('delivery_postcode', 'Postcode (Destination)')!!}</div>
            <div class="pure-u-18-24">{!!Form::text('delivery_postcode', $delivery[0]['delivery_postcode'] )!!}</div>
        </div>

        <div class="pure-g row">
            <div class="pure-u-6-24">{!!Form::label('collection_address', 'Approximate address (Collection point)')!!}</div>
            <div class="pure-u-18-24">{!!Form::text('collection_address', $delivery[0]['collection_address'] , array('disabled'))!!}</div>
        </div>

        <div class="pure-g row">
            <div class="pure-u-6-24">{!!Form::label('delivery_address', 'Approximate address (Destination)')!!}</div>
            <div class="pure-u-18-24">{!!Form::text('delivery_address', $delivery[0]['delivery_address'] , array('disabled'))!!}</div>
        </div>

    </fieldset>

    <h2>Items</h2>
    {{-- */ $numberOfItems = count($delivery[0]['items']) /* --}}
    <div id="edit-items-container">
        @for($i = 0; $i<$numberOfItems; $i++)
            {!! $vDeliveryEditTemplateComposer->getDeliveryItemsTemplate('php', $unitsOfMeasurement, $i, $delivery) !!}
        @endfor
    </div>

    <div><a id="add-item-cta" href="#">+ Add item</a></div>

    {!! Form::submit('Update', array('class'=>'pure-button pure-button-primary narrow-submit-button')) !!}

    {!! Form::hidden('form_time', $formTime) !!}
    {!! Form::hidden('form_submitted_time', $formTime) !!}

    {!! Form::close() !!}

    <script id="deliveryItemSingleTemplate" type="text/x-handlebars-template">
    {!! $vDeliveryEditTemplateComposer->getDeliveryItemsTemplate('handlebars', $unitsOfMeasurement) !!}
    </script>

    @include('partials.deliveries.deliveriesEditVars')

@stop

@section('scripts')
    {!! Html::script('js/inline_scripts/deliveriesEdit.js') !!}
@stop