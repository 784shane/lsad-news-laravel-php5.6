<?php namespace App\Services;

use App\Services\Auth\User as userService;
use PhpSpec\Exception\Exception;

class Messages
{
    private function mainDeliveryMessagesColumns()
    {
        return ['deliveries_messages.*',
            'users.first_name',
            'users.last_name',
            'users.user_name'];
    }

    /**
     * Retrieves the number of unread messages for the logged in user.
     *
     * @return int
     */
    public function getNewMessageCount()
    {
        $userService = new userService();
        //Get the id of the logged in user.
        $loggedInUserId = $userService->getLoggedInUsersId();
        return \App\DeliveriesMessage::where('unread', '=', 1)
            ->where('receiver', '=', $loggedInUserId)
            ->count();
    }

    /**
     * Route function - calls by route /messages
     *
     * @param int $messageId
     * @param     Object View Object
     */
    private function getDeliveriesMessagesData($deliveryId)
    {
        return \App\DeliveriesMessage::where(
            'deliveries_messages.delivery', '=', $deliveryId)
            ->join('users', 'deliveries_messages.sender', '=', 'users.id');
    }

    /**
     * selects a unique sender's messages to be displayed on a delivery.
     *
     * @param array $myDeliveriesMessages
     *
     * @return array
     */
    private function getDeliveryContractedMgs($myDeliveriesMessages)
    {
        $userService = new userService();
        //Get the id of the logged in user.
        $loggedInUserId = $userService->getLoggedInUsersId();

        $contractedMessages = [];
        foreach($myDeliveriesMessages as $myDelMessages)
        {
            //We will not be highlighting communication from myself.
            if($loggedInUserId === $myDelMessages['sender']){continue;}
            // We only want one message to appear with the delivery
            // This is because the delivery is contracted. we will only
            // show one message per sender.
            if (!isset($contractedMessages["sender_" . $myDelMessages['sender']]))
            {
                $contractedMessages["sender_" . $myDelMessages['sender']] = [
                    "sender" => $myDelMessages['sender'],
                    'message_item' => $myDelMessages
                ];
            }else
            {
                // If this message is already set, we might have to alter it
                // For example, if we meet the same sender with an unread message,
                // we will want this cover message to be represented as unread.
                // This is the place to do this.
                // Lets look for an unread message.
                if ($myDelMessages['unread'] == 1)
                {
                    $contractedMessages["sender_" . $myDelMessages['sender']]['message_item']['unread'] = $myDelMessages['unread'];
                }
                // If we need to alter the cover message further, we can do it here...
            }
        }
        return $contractedMessages;
    }

    private function getMessageItems($myDeliveries)
    {
        //exit("<pre>".print_r($myDeliveries, true)."</pre>");
        $messageItems = [];
        foreach($myDeliveries as $myDelivery)
        {
            // If we are contracting the messages on the delivery, we will sort out which messages
            // we will be adding to the delivery
            $deliveryMessages = $this->getDeliveriesMessagesData($myDelivery['id'])
                ->orderBy('deliveries_messages.created_at', 'DESC')
                ->get($this->mainDeliveryMessagesColumns())->toArray();
            $messageItems[] = [
                'messages' => $this->getDeliveryContractedMgs($deliveryMessages),
                'delivery' => $myDelivery
            ];
        }
        return $messageItems;
    }


    public function buildUsersDeliveriesMessages($myDeliveries)
    {
        return $this->getMessageItems($myDeliveries);
    }

    public function getDeliveriesWithMyMessages()
    {
        return $this->checkForDeliveriesWithMyMessages();
    }

    /**
     * We are getting the delivery ids that have messages in association
     * with me but those deliveries are not mine.
     *
     * @return array
     */
    private function getOtherDeliveryIdsWithMyMessages()
    {
        $userService = new userService();
        //Get the id of the logged in user.
        $loggedInUserId = $userService->getLoggedInUsersId();

        $query = \App\DeliveriesMessage::where(
            'deliveries.user', '!=', 'null');
        $query->where(function()use($query, $loggedInUserId){
            $query->where('deliveries_messages.sender', '=', $loggedInUserId);
            $query->orWhere('deliveries_messages.receiver', '=', $loggedInUserId);
        });
        $query->where('deliveries.user', '!=', $loggedInUserId);
        $query->join('deliveries', 'deliveries_messages.delivery', '=', 'deliveries.id');
        $query->groupBy('deliveries_messages.delivery')->distinct();

        return $query->get(['deliveries_messages.delivery as id', 'deliveries.subtitle'])->toArray();

    }

    private function checkForDeliveriesWithMyMessages()
    {
        $otherDeliveryIds = $this->getOtherDeliveryIdsWithMyMessages();
        return $this->getMessageItems($otherDeliveryIds);
    }

    /**
     * Mark message as 'read'.
     *
     * @param int $msgId
     *
     * @return array
     */
    private function markMsgAsRead($msgId)
    {
        $deliveriesMessage = \App\DeliveriesMessage::find($msgId);
        return $deliveriesMessage->update(['unread' => 0]);
    }

    /**
     * Retrieves the message by id from the database,
     * along with the details of the sender.
     *
     * @param int $msgId
     *
     * @return Illuminate\Database\Eloquent\Builder Object
     */
    private function getDeliveryMsg($msgId)
    {
        return \App\DeliveriesMessage::where(
            'deliveries_messages.id', '=', $msgId)
            ->join('users', 'deliveries_messages.sender', '=', 'users.id');
    }

    /**
     * Retrieves the message from the database
     * i.e., if the user is meant to see it.
     *
     * @param int $msgId
     *
     * @return array
     */
    public function processOneDeliveryMessage($msgId)
    {
        $authUser = new userService();
        $loggedInUser = $authUser->getLoggedInUsersId();
        // We need to show the user an error page if this message is not theirs.
        $message = $this->getDeliveryMsg($msgId)
            ->get($this->mainDeliveryMessagesColumns())->toArray();
        if (empty($message))
        {
            // We don't have a message to show so we will let the user know.
            exit("no messages here");
        }
        // Test this message. If this message is not for the logged in user,
        // we will not allow this process to continue.
        if ($message[0]['sender'] !== $loggedInUser && $message[0]['receiver'] !== $loggedInUser)
        {
            // Throw an error here - User is not to see this message.
            exit("You cannot see this message");
        }
        // If this is unread, we need to mark it as read as we
        // are about to open it.
        if ($message[0]['unread'])
        {
            if (!$this->markMsgAsRead($msgId))
            {
                // Don't throw an error.
                //log this. We were unable to update as read
            }
        }
        return $message;
    }

    /**
     * Retrieves all messages from the sender on this delivery.
     * It makes sure as well that these messages were intended
     * for the logged in user.
     *
     * @param int $sendersId
     * @param int $deliveryId
     *
     * @return Illuminate\Database\Eloquent\Builder Object
     */
    public function getMessagesWithSender($receiversId, $sendersId, $deliveryId)
    {
        $query = \App\DeliveriesMessage::where(
            'deliveries_messages.delivery', '=', $deliveryId);

        // We want to make sure we are getting messages on this delivery
        // where the receiver and sender are corresponding only
        $query->where(function ($query) use ($receiversId, $sendersId)
        {
            $query->where(function ($query) use ($receiversId, $sendersId)
            {
                $query->where('deliveries_messages.receiver', '=', $receiversId);
                $query->where('deliveries_messages.sender', '=', $sendersId);
            });
            $query->orWhere(function ($query) use ($receiversId, $sendersId)
            {
                $query->where('deliveries_messages.receiver', '=', $sendersId);
                $query->where('deliveries_messages.sender', '=', $receiversId);
            });
        });
        $query->join('users', 'deliveries_messages.sender', '=', 'users.id');
        $query->leftJoin('quotes', 'deliveries_messages.id', '=', 'quotes.message');
        $query->get([
            'deliveries_messages.*',
            'deliveries_messages.created_at as messages_created_at',
            'deliveries_messages.id as message_id',
            'users.*',
            'quotes.quote',
            'quotes.with_message'
        ]);
        return $query;
    }

    /**
     * Retrieves a list of Senders' messages.
     * May return null if unsuccessful
     *
     * @param int $msgId
     *
     * @return mixed - returns array of senders' messages or return null
     */
    public function getDeliveriesSendersMessage($msgId)
    {
        $message = \App\DeliveriesMessage::find($msgId);
        if ($message)
        {
            $message = $message->toArray();
            $sendersId = $message['sender'];
            $receiversId = $message['receiver'];
            $deliveryId = $message['delivery'];

            return $this->getMessagesWithSender($receiversId, $sendersId, $deliveryId)
                ->orderBy('deliveries_messages.created_at', 'DESC')
                ->get($this->mainDeliveryMessagesColumns())->toArray();
        }
        return null;
    }

    private function saveNewDeliveryMessage($deliveryMessageInfo)
    {
        $deliveryMessage = \App\DeliveriesMessage::create($deliveryMessageInfo);
        if (!is_null($deliveryMessage->id))
        {
            $deliveryMessageId = $deliveryMessage->id;
        }
    }

    private function postMessageReply($inputs, $msgId)
    {
        $message = \App\DeliveriesMessage::find($msgId);
        if ($message)
        {
            $message = $message->toArray();
            // Because I am replying to a message, the sender will become
            // the receiver
            $receiverId = $message['sender'];
            // Because I am replying to a message, the receiver will become
            // the sender
            $sendersId = $message['receiver'];
            $newMessageData = [
                'sender' => $sendersId,
                'receiver' => $receiverId,
                'message' => $inputs['quote_message'],
                'delivery' => $message['delivery'],
                'unread' => 1
            ];
            $quotes = new \App\Services\Quotes\Quotes();
            $inputs = $this->addANullQuoteAmount($inputs);
            $quotes->addDeliveryMessage($newMessageData, $inputs);
        }
    }

    private function addANullQuoteAmount($inputs){
        if(!isset($inputs['job_quote_amt'])){
            $inputs['job_quote_amt'] = null;
        }
        return $inputs;
    }

    public function makeMessageReply($inputs, $msgId)
    {
        $quotesServ = new \App\Services\Quotes\QuotesProcesses();
        $validatorResults = $quotesServ->validateMessageReply($inputs);
        if ($validatorResults->fails())
        {
            $validationReturn['validatorResults'] = $validatorResults;
            $validationReturn['result'] = 'fail';
            $validationReturn['error_messages'] = $validatorResults->messages();
        }else
        {
            $validationReturn['result'] = 'success';
            $this->postMessageReply($inputs, $msgId);
        }
        return $validationReturn;
    }
}