@extends($layout)
@section('content')
    <div>
        <h2>Success!</h2>
        <p>You have updated your password.</p>
        <p>Please click here to login to the site {!! Html::link('login','here') !!}.</p>
    </div>
@stop