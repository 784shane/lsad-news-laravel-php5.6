<?php

namespace App\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Services\Auth\User;

class ViewAuthComposer
{
    /**
     * The json encoding options.
     *
     * @var int
     */
    protected $loggedInUser = null;

    /**
     * The Auth User object.
     *
     * @var object \App\Services\Auth\User
     */
    private $user;


    public function __construct()
    {
        // Getting the logged in user
        $this->setLoggedInUser();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(array(
            'vAuthComposer'=>$this
        ));
    }

    /**
     * Sets the logged in User.
     *
     * @return void
     */
    private function setLoggedInUser()
    {
        $user = new User();
        $this->user = $user;
        $this->loggedInUser = $user->getLoggedInUsersId();
    }

    public function getLoggedInUser()
    {
        return $this->loggedInUser;
    }
}