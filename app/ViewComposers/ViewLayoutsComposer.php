<?php
namespace App\ViewComposers;

use App\Http\Requests\Request;
use Illuminate\Contracts\View\View;
use App\Services\Auth\User;

class ViewLayoutsComposer
{
    /**
     * The json encoding options.
     *
     * @var int
     */
    protected $loggedInUser = null;

    public function __construct()
    {
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(array(
            'vLayoutsHelper' => $this
        ));
    }

    public function getNewMessageCount()
    {
        return \Messages::getNewMessageCount();
    }

    public function setActiveClass($linkName)
    {
        $activeMenuLinks  = \App\Services\ActiveMenuLinks::getInstance();
        $activeGroup = $activeMenuLinks->getActiveLink();
        if($activeGroup === $linkName){
            return 'pure-menu-selected';
        }
        return '';
    }
}