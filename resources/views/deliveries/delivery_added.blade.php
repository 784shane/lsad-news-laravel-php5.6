@extends($layout)
@section('content')

<p>Thank you, your delivery is now live</p>
<p>Quotes will appear here very soon. An email will also be sent to you as they arrive.</p>
<p>Where to next?</p>
<p>{!! Html::link('/home','Visit your account.', array()) !!}</p>

@stop