<?php namespace App\Services\Deliveries;

/**
 * Doctrine inflector has static methods for inflecting text.
 *
 * The methods in these classes are from several different sources collected
 * across several different php projects and several different authors. The
 * original author names and emails are not known.
 *
 * Pluralize & Singularize implementation are borrowed from CakePHP with some modifications.
 *
 * @link   www.doctrine-project.org
 * @since  1.0
 * @author Shane Daniel
 */
class DeliveriesExpiryManagement
{
    // @Todo -
    // RUN A CRON JOB ON THIS METHOD TO EXPIRE DELIVERIES.
    // This is your starting point
    public function expirationCheck()
    {
        // We are about to check to see if we should expire any deliveries
        $deliveryExpiryData = new DeliveriesExpiryData();
        $timeNow = time();
        $this->getDeliveriesForExpiration($timeNow);
        if ($this->shouldExpirationProcessProceed($timeNow, $deliveryExpiryData->getLastExpiryCheckedDate()))
        {
            $deliveries = $this->getDeliveriesForExpiration($timeNow);
            $this->expireDeliveries($deliveries, $timeNow);
            // Now set last time this check was made.
            $deliveriesExpiryDataClass = new DeliveriesExpiryData();
            $deliveriesExpiryDataClass->setLastExpiryCheckDate();
        }
    }

    private function getDeliveriesForExpiration($timeNow)
    {
        $deliveriesConstants = \Config::get('constants.deliveries');
        $deliveriesExpiredStatus = $deliveriesConstants['statuses']['EXPIRED'];
        $deliveriesCheckLimit = $deliveriesConstants['expiry']['expiry_checks']['EXPIRY_CHECK_LIMIT'];
        $deliveries = \App\Deliveries::where('expiry','<=', date('Y-m-d H:i:s', $timeNow))
        // update `deliveries` set `status` = '1', `expiry` = '2016-04-19 23:20:22' where `id` > 3
        //$deliveries = \App\Deliveries::where('expiry','<=', '2016-04-19 23:25:22')
            ->where('status','<', $deliveriesExpiredStatus)
            ->take($deliveriesCheckLimit)
            ->get();
        // As long as we get data back, lets go ahead and expire deliveries.
        if($deliveries->count()>0){
            return $deliveries->toArray();
        }
        return [];
    }

    public function expireDeliveries($deliveries, $timeNow)
    {
        // Expiring a delivery entails setting the status of the delivery to EXPIRED.
        // and of-course updating the deliveries' expiry date to now.

        $deliveriesExpiredStatus = \Config::get('constants.deliveries.statuses.EXPIRED');
        $ids = [];
        foreach($deliveries as $delivery){
            $ids[] = $delivery['id'];
        }
        \App\Deliveries::whereIn("id", $ids)->update(array(
            'status' => $deliveriesExpiredStatus,
            'expiry' => date('Y-m-d H:i:s', $timeNow)
        ));
    }

    private function shouldExpirationProcessProceed($timeNow, $lastExpirationDate)
    {
        // Is this date
        if ($lastExpirationDate)
        {
            $lastExpirationTS = date('Y-m-d H:i:s', $lastExpirationDate);
            $expiryCheckInterval = \Config::get('constants.deliveries.expiry.expiry_checks.EXPIRY_CHECK_INTERVAL');
            $nextCheckExpected = $this->getFutureTimeFromSeconds($expiryCheckInterval, $lastExpirationTS);
            if (strtotime($nextCheckExpected) <= $timeNow)
            {
                return true;
            }
            return false;
        }
        return true;
    }

    public function extendExpiryTime($deliveryId, $reason)
    {
        $newDate = $this->getDeliveriesNewExpiryTime($reason);
        if ($newDate)
        {
            $expiryData = [
                'id' => $deliveryId,
                'expiry' => $newDate
            ];
            return $this->updateDeliveryExpirationDates($expiryData);
        }
        return false;
    }

    public function getDeliveriesNewExpiryTime($reason)
    {
        // Time is extended when:
        // 1. When a delivery is sent live
        // 2. When a delivery receives a quote.
        // 3. When a quote is accepted for a delivery
        $deliveryExpiryConstants = \Config::get('constants.deliveries.expiry');
        $moreTimeSeconds = 0;
        switch($reason)
        {
            case $deliveryExpiryConstants['extension_reason']['DELIVERY_INSERTION']:
            {
                $moreTimeSeconds = $deliveryExpiryConstants['time_extension']['TIME_FROM_INSERTION'];
                break;
            }
            case $deliveryExpiryConstants['extension_reason']['QUOTE_MADE']:
            {
                $moreTimeSeconds = $deliveryExpiryConstants['time_extension']['TIME_AFTER_QUOTE_MADE'];
                break;
            }
            case $deliveryExpiryConstants['extension_reason']['QUOTE_ACCEPTED']:
            {
                $moreTimeSeconds = $deliveryExpiryConstants['time_extension']['TIME_AFTER_QUOTE_ACCEPTED'];
                break;
            }
        }
        if ($moreTimeSeconds)
        {
            return $this->getFutureTimeFromSeconds($moreTimeSeconds);
        }
        return null;
    }

    private function updateDeliveryExpirationDates($expiryData)
    {
        $theDelivery = \App\Deliveries::find($expiryData['id']);
        $theDelivery->expiry = $expiryData['expiry'];
        return $theDelivery->save();
    }

    private function getFutureTimeFromSeconds($moreTimeSeconds, $timeNow = null)
    {
        $date = new \DateTime($timeNow);
        $date->add(new \DateInterval('PT' . $moreTimeSeconds . 'S'));
        return date('Y-m-d H:i:s', $date->getTimestamp());
    }
}