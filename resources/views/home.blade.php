@extends($account_page_layout)
@section('content')



    <h1>My Deliveries</h1>

    <div>
        <div id="home-pocket">

            @foreach ($myDeliveries as $myDelivery)
                <section class="top my_delivery_item">
                    <div href="#" class="main_link">
                        {{$myDelivery['subtitle']}}
                        @unless(!$vComposerHelper->isDeliveryEditable($myDelivery['status']))
                        <div class="delivery_edit_cta">
                            {!! Html::link(url('/deliveries/edit/'.$myDelivery['id']), 'edit') !!}
                            {!! Html::link(url('/deliveries/edit/'.$myDelivery['id']), 'delete', array('class'=>'delete-delivery-button', 'data-delivery-id'=>$myDelivery['id'])) !!}
                        </div>
                        @endunless
                    </div>
                    <div class="home-delivery-info">
                        <h5 class="md-h5-label">Expires - {{ $vComposerHelper->viewDatesDisplay($myDelivery['expiry']) }}</h5>
                    </div>
                    @foreach ($myDelivery['items'] as $delItem)
                        <div class="pure-g">
                            <div class="pure-u-1-5">
                                <div class="my_delivery_item_left_insert">
                                    {{-- below: temporary issets --}}
                                    @if(isset($delItem['main_image']))
                                        {!!Html::image($vComposerHelper->missingImageResolver($delItem['main_image']), $delItem['name'], array('width'=>'100%'))!!}
                                    @endif
                                </div>
                            </div>
                            <div class="pure-u-4-5">
                                <h5 class="md-h5-label">Title</h5>
                                {{-- below: temporary issets --}}
                                <div>@if(isset($delItem['name'])){{$delItem['name']}}@endif</div>
                            </div>
                        </div>
                    @endforeach


                    @if($myDelivery['quote_info'] != null)
                        <div class="pure-g my-delivery-quote-row">
                            <div class="pure-u-5-24"><b>Amount Quoted</b></div>
                            <div class="pure-u-7-24"><b>Transporter</b></div>
                            <div class="pure-u-12-24"><b>Quote info</b></div>
                        </div>
                        @foreach($myDelivery['quote_info'] as $quoteInfo)
                            <div class="pure-g my-delivery-quote-row">
                                <div class="pure-u-5-24">{{ $quoteInfo['quote'] }}</div>
                                <div class="pure-u-7-24">{{ $quoteInfo['user_name'] }}</div>
                                <div class="pure-u-12-24">
                                    {{-- Was a quote accepted? --}}
                                    @if(null === $myDelivery['quote_accepted'])
                                        {{-- No quotes accepted as yet --}}
                                        {!! Html::link(url('/quotes/accept/'.$myDelivery['delivery_nonce'].'/'.$quoteInfo['quote_id']), 'Accept quote', array("class"=>"pure-button pure-button-primary")) !!}
                                    @else
                                        {{--
                                        A quote has been accepted, We will show no buttons here.
                                        Instead, we will check to see if payment has been made.
                                        If payment not been made, we will display a pay now button
                                        for that quote that has been accepted.
                                        --}}
                                        @if($quoteInfo['quote_id']==$myDelivery['quote_accepted'])
                                            {{-- We are looking for the quote that was accepted. --}}
                                            @if(!$myDelivery['payment_made'])
                                                {!! Html::link(url('/quotes/accept/'.$myDelivery['delivery_nonce'].'/'.$quoteInfo['quote_id']), 'Pay now', array("class"=>"pure-button pure-button-primary")) !!}
                                            @endif
                                            {{--
                                            We could/should replace this disabled button
                                            with image showing that the quote was accepted.
                                            --}}
                                            <button class="pure-button pure-button-primary" disabled="disabled">Quote accepted</button>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endif
                </section>
            @endforeach

        </div>
    </div>
    @include('partials.deliveries.deliveriesEditVars')
@stop

@section('scripts')
    {!! Html::script('js/inline_scripts/home.js') !!}
@stop
