<?php namespace App\Services;

use Curl\Curl;

class CurlRequests
{
    private $curl = null;

    protected $payPalCredentials = null;

    private function theCurlObject()
    {
        if ($this->curl === null)
        {
            $this->curl = new Curl();
        }
        return $this->curl;
    }

    private function addToErrorMsg($curlObject, $property){
        if(isset($curlObject->{$property}) && !empty($curlObject->{$property})){
            // Add this message to the error log message.
            return $property.' - '.$curlObject->{$property}.PHP_EOL;
        }
        return "";
    }

    private function setCurlErrorLog($curlObject, $url){
        $logMessage = 'Called to '.$url.'. Error returned'.PHP_EOL;
        $logMessage .= $this->addToErrorMsg($curlObject, 'curl_error_message');
        $logMessage .= $this->addToErrorMsg($curlObject, 'error_message');
        $logMessage .= $this->addToErrorMsg($curlObject, 'response');
        \Log::error($logMessage);
    }

    protected function setPaypalCredentials()
    {
        if ($this->payPalCredentials === null)
        {
            $this->payPalCredentials = $this->getPaypalCredentials();
        }
        return $this->payPalCredentials;
    }

    private function getPaypalCredentials(){
        // We dont want to expose the config details un-necessarily so we will
        // get the set environment. From this, we will get the config.
        $env = \Config::get('local.paypal.environment');
        return \Config::get('local.paypal.'.$env);
    }

    private function setPeerVerify($curlObject, $boolean = true){
        $curlObject->setopt(CURLOPT_SSL_VERIFYPEER, $boolean);
    }

    public function addressLookupFromPostCode($geoUrl)
    {
        $curlObject = $this->theCurlObject();
        $curlObject->get($geoUrl);
        $result = [];
        if ($curlObject->error || (!isset($curlObject->response)))
        {
            // We will want to know if this process was successful or not.
            // If we got a curl error, we will log it.
            $this->setCurlErrorLog($curlObject, $geoUrl);
            $result['status'] = 'fail';
            $result['failMessage'] = 'failed';
        }else
        {
            $result['status'] = 'success';
            $result['response'] = json_decode($curlObject->response);
        }
        return $result;
    }

    protected function getEbayItem($geoUrl)
    {
        $curlObject = $this->theCurlObject();
        $curlObject->get($geoUrl);
        $fail = false;
        $result = ['status' => 'success'];
        if ($curlObject->error || (!isset($curlObject->response)))
        {
            // We will want to know if this process was successful or not.
            // If we got a curl error, we will log it.
            $this->setCurlErrorLog($curlObject, $geoUrl);
            $result['messages'] = ['Internal error occurred'];
            //fail
            $fail = true;
        }else
        {
            $decodedResponse = json_decode($curlObject->response);
            if (isset($decodedResponse->findItemsByKeywordsResponse[0]->ack) &&
                in_array("Failure", $decodedResponse->findItemsByKeywordsResponse[0]->ack)
            )
            {
                //fail
                $fail = true;
            }elseif (isset($decodedResponse->findItemsByKeywordsResponse[0]->ack) &&
                in_array("Success", $decodedResponse->findItemsByKeywordsResponse[0]->ack)
            )
            {
                if (isset($decodedResponse->findItemsByKeywordsResponse[0]->searchResult[0]->{'@count'}) &&
                    ((int)$decodedResponse->findItemsByKeywordsResponse[0]->searchResult[0]->{'@count'}) > 0
                )
                {
                    //Success
                    $result['response'] = json_decode($curlObject->response);
                }else
                {
                    //fail
                    $fail = true;
                }
            }
        }
        if ($fail)
        {
            $result['status'] = 'fail';
            $result['failMessage'] = 'failed';
        }
        return $result;
    }

    public function distanceLookupFromPostCodes($distanceUrl)
    {
        $curlObject = $this->theCurlObject();
        $curlObject->get($distanceUrl);
        $fail = true;
        $result = ['status' => 'success'];
        $decodedResponse = json_decode($curlObject->response);

        if (null !== $decodedResponse && property_exists($decodedResponse, 'rows'))
        {
            foreach($decodedResponse->rows as $row)
            {
                if (property_exists($row, 'elements'))
                {
                    foreach($row->elements as $elementKey => $element)
                    {
                        if(property_exists($element, 'distance')){
                            $result['response'] = $element->distance;
                            $fail = false;
                            break;
                        }
                    }
                }
            }
        }

        if ($fail)
        {
            $result['status'] = 'fail';
            $result['failMessage'] = 'failed';
        }
        return $result;
    }

    private function setPayPalAPIVersion(){
        return [
            'VERSION' => $this->payPalCredentials['api']['version']
        ];
    }

    private function preparePaypalUserPostData(){
        return [
            'USER'=>$this->payPalCredentials['accounts']['merchant']['api_credentials']['userName'],
            'PWD'=>$this->payPalCredentials['accounts']['merchant']['api_credentials']['password'],
            'SIGNATURE'=>$this->payPalCredentials['accounts']['merchant']['api_credentials']['signature']
        ];
    }

    protected function testExpressResult(){
        $result['status'] = 'fail';
        // Get the posted data
        $allPosts = \Request::all();

        $this->setPaypalCredentials();
        $url = $this->payPalCredentials['expressCheckout']['nvpSignatureEndpoint'];

        $curlObject = $this->theCurlObject();
        $this->setPeerVerify($curlObject, false);

        $postData = [
            // @Todo - TEMPORARILY ADD AMT TO THIS POST DATA
            'AMT'=>'19.99',
            // Set the method
            'METHOD'=>'GetExpressCheckoutDetails',
            'TOKEN'=>isset($allPosts['token'])?$allPosts['token']:'',
        ];

        // Set paypal version
        $postData = array_merge(
            $postData,
            $this->setPayPalAPIVersion()
        );

        $postData = array_merge(
            $postData,
            $this->preparePaypalUserPostData()
        );

        //exit(vde($postData));

        $curlObject->post($url, $postData);

        // We will need to keep the data sent over
        $result['post_data'] = $postData;

        if ($curlObject->error || (!isset($curlObject->response)))
        {
            // We will want to know if this process was successful or not.
            // If we got a curl error, we will log it.
            $this->setCurlErrorLog($curlObject, $url);
            $result['messages'] = ['Internal error occurred'];
        }
        else
        {
            parse_str($curlObject->response, $paypalResult);
            // @Todo - TEMPORARILY ADD AMT TO THIS POST DATA
            $paypalResult['AMT']=!isset($paypalResult['AMT'])?'19.99':$paypalResult['AMT'];
            if(strtolower($paypalResult['ACK'])==="success" && isset($paypalResult['BUILD'])){
                // We should have the token
                $result['status'] = 'success';
            }
            $result['result'] = $paypalResult;
            $result['messages'] = $this->gatherPaypalErrorMessages($paypalResult);
        }

        return $result;
    }

    protected function SetExpressCheckout($transactionData){
        $result['status'] = 'fail';
        // Make sure the paypal credentials are set.
        $this->setPaypalCredentials();
        // Get the url for the
        $url = $this->payPalCredentials['expressCheckout']['nvpSignatureEndpoint'];
        $curlObject = $this->theCurlObject();

        // Turn off peer verify? Let's check our config.
        $this->setPeerVerify($curlObject, false);
        $postData = [
            // Set the method
            'METHOD'=>'SetExpressCheckout',
            'PAYMENTREQUEST_0_PAYMENTACTION'=>'SALE',
            'PAYMENTREQUEST_0_AMT'=>$transactionData['charge'],
            'PAYMENTREQUEST_0_CURRENCYCODE'=>$this->payPalCredentials['currencyCode'],
            // Set the cancel url
            'cancelUrl'=>$transactionData['cancelUrl'],
            // Set the return url
            'returnUrl'=>$transactionData['returnUrl'],
        ];

        // Set paypal version
        $postData = array_merge(
            $postData,
            $this->setPayPalAPIVersion()
        );

        // Set the API credentials
        $postData = array_merge(
            $postData,
            $this->preparePaypalUserPostData()
        );

        $curlObject->post($url, $postData);

        if ($curlObject->error || (!isset($curlObject->response)))
        {
            // We will want to know if this process was successful or not.
            // If we got a curl error, we will log it.
            $this->setCurlErrorLog($curlObject, $url);
            $result['messages'] = ['Internal error occurred'];
        }else
        {
            parse_str($curlObject->response, $paypalResult);
            if(strtolower($paypalResult['ACK'])==="success" && isset($paypalResult['TOKEN'])){
                // We should have the token
                $result['status'] = 'success';
                // We will need to keep the data sent over
                $result['post_data'] = $postData;
            }
            $result['result'] = $paypalResult;
            $result['messages'] = $this->gatherPaypalErrorMessages($paypalResult);
        }

        return $result;
    }

    protected function doDirectCreditCardPayment($transactionData){
        $result['status'] = 'fail';
        // Make sure the paypal credentials are set.
        $this->setPaypalCredentials();
        // Get the url for the
        $url = $this->payPalCredentials['expressCheckout']['nvpSignatureEndpoint'];
        $curlObject = $this->theCurlObject();

        // Turn off peer verify? Let's check our config.
        $this->setPeerVerify($curlObject, false);
        $postData = [
            // Set the method
            'METHOD'=>'DoDirectPayment',
            'PAYMENTACTION'=>'Authorization',
            'AMT'=>'19.99',
            'COUNTRYCODE'=>strtoupper($this->payPalCredentials['currencyCode'])
        ];

        // Add the credit card data to the post data.
        $postData = array_merge($postData, $transactionData);

        // Set paypal version
        $postData = array_merge(
            $postData,
            $this->setPayPalAPIVersion()
        );

        // Set the API credentials
        $postData = array_merge(
            $postData,
            $this->preparePaypalUserPostData()
        );

        $curlObject->post($url, $postData);

        // We will need to keep the data sent over
        $result['post_data'] = $postData;

        if ($curlObject->error || (!isset($curlObject->response)))
        {
            // We will want to know if this process was successful or not.
            // If we got a curl error, we will log it.
            $this->setCurlErrorLog($curlObject, $url);
            $result['messages'] = ['Internal error occurred'];
        }else
        {
            parse_str($curlObject->response, $paypalResult);
            if(strtolower($paypalResult['ACK'])==="success" && isset($paypalResult['BUILD'])){
                // We should have the token
                $result['status'] = 'success';
            }
            $result['result'] = $paypalResult;
            $result['messages'] = $this->gatherPaypalErrorMessages($paypalResult);
        }

        return $result;
    }

    private function gatherPaypalErrorMessages($paypalResult){
        $errorMessages = [];
        if(strtolower($paypalResult['ACK'])==="failure"){
            // Lets get all the long messages
            foreach($paypalResult as $paypalResultKey => $paypalResultArr){
                if(strpos(strtolower($paypalResultKey), 'longmessage') > -1){
                    $errorMessages[] = $paypalResultArr;
                }
            }
        }
        return $errorMessages;
    }
}