<script>
	$(function() {
		$.getJSON('http://barcampdeland.org/_/data/speakers.json', function(data) {
		    var template = $('#speakers-template').html();
		    var info = Mustache.to_html(template, data);
		    $('#talktitles').html(info);
		});
	});
</script>
view rawgetJSON.html hosted with ❤ by GitHub
Putting it all together
Here’s a full listing of the code:

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Mustache Sample</title>
</head>
<body>
	<h2>Titles</h2>
	<ul id="talktitles">
	</ul>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.7.0/mustache.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script id="speakers-template" type="text/template">
		{{#speakers}}
			<li>lol - {{title}}</li>
		{{/speakers}}
	</script>
	<script>
		$(function() {
			//$.getJSON('http://barcampdeland.org/_/data/speakers.json', function(data) {
			$.getJSON('sampledata.json', function(data) {
			    var template = $('#speakers-template').html();
			    var info = Mustache.to_html(template, data);
			    $('#talktitles').html(info);
			});
		});
	</script>
</body>
</html>