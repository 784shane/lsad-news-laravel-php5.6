@extends($layout)
@section('content')
    @if($pagesInfo['page'] === 'confirmation')
        @include('partials.payments.confirmation',$pagesInfo['data'])
    @else
        @include('partials.payments.failed',$pagesInfo)
    @endif
@stop