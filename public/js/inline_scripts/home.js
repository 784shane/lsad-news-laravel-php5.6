var homePageService = function(httpService){

    this.httpService = httpService;

    this.init = function(){
        this.setupClickEvents();
    };

    this.setupClickEvents = function(){

        var self = this;

        $('.delete-delivery-button').click(function(evt){
            evt.preventDefault();
            //console.log($(evt.target).data('delivery-id'));
            // Looks like a request has come to delete a delivery item.
            // Lets attempt that now.
            self.proceedToConfirmDelete(evt.target);
        });
    };

    this.proceedToConfirmDelete = function(deleteButton){

        var deliveryId = $(deleteButton).data('delivery-id');

        swal({
            title: "Are you sure?",
            text: "This delivery will be deleted. You will not be able to recover it.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }, function () {
            window.location = deliveriesDeleteBaseUrl+'/'+deliveryId;
        });

    };

};

$(document).ready(function () {

    // Start the ebayService service.
    var homeService = new homePageService(new httpService());
    homeService.init();

});