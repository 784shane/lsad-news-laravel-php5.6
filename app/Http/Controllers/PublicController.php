<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class PublicController extends BaseController
{
    protected $viewData = [];//"layout" => 'layouts.master'];

    public function __construct()
    {
        // Making sure that the layout is shared across all views.
        \Illuminate\Support\Facades\View::share("layout", 'layouts.master');
    }
}
