<div>
	
	<h1>Personal Details</h1>

	<div>
		{!! $errors->first('first_name') !!}
		{!! Form::label('first_name', 'First name') !!}
		{!! Form::text('first_name', $userDetails->first_name) !!}


		{!! $errors->first('last_name') !!}
		{!! Form::label('last_name', 'Last name') !!}
		{!! Form::text('last_name', $userDetails->last_name) !!}


		{!! $errors->first('email') !!}
		{!! Form::label('email', 'Email') !!}
		{!! Form::text('email', $userDetails->email) !!}


		{!! Form::label('user_name', 'Username') !!}
		{!! Form::text('user_name', $userDetails->user_name) !!}


		{!! $errors->first('telephone_number') !!}
		{!! Form::label('telephone_number', 'Telephone number') !!}
		{!! Form::text('telephone_number', $userDetails->telephone_number) !!}


		{!! $errors->first('password') !!}
		{!! Form::label('password', 'Password') !!}
		{!! Form::password('password') !!}


		{!! $errors->first('confirm_password') !!}
		{!! Form::label('confirm_password', 'Confirm password') !!}
		{!! Form::password('confirm_password') !!}
	</div>

</div>