<?php namespace App\Services\Auth;

use App\Business as BusinessModel;

class Business {

	/**
	* @param boolean 
	**/
	public function userHasTransportProvider($userId){

		$business = new BusinessModel();
		$businesses = $business->getVerifiedBusiness($userId);
		if($businesses){
			return true;
		}
		return false;
	}

	/**
	* @param boolean 
	**/
	public function getTransportProviderDetails($userId){
		$business = new BusinessModel();
		$businesses = $business->getBusinessForUser($userId);
		if($businesses){
			return $businesses[0];
		}
		return null;
	}

	public function getBusinessEmptyDetails(){	
		$std = new \stdClass();
		$std->id = null;
		$std->user = '';
        $std->company_email = '';
        $std->company_name = '';
        $std->company_phone_number = '';
        $std->company_mobile_number = '';
        $std->company_address_1 = '';
        $std->company_address_2 = '';
        $std->company_city = '';
        $std->company_postcode = '';
        $std->verified = 0;
		return $std;
	}
}
