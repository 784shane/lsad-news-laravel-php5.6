<?php namespace App\Services\Quotes;

use Request;
use App\Quotes As QuotesModel;
use App\Services\Auth\User as AuthUser;
use Deliveries;

class Quotes extends QuotesProcesses
{
    // Bring in our trait
    use \App\Services\Deliveries\DeliveriesCommon;

    public function processQuotes($jobNumber)
    {
        $allPosts = Request::all();
        return $this->submitQuote($allPosts, $jobNumber);
    }

    public function recalculateQuote($postData)
    {
        $validationReturn = [];
        $validatorResults = $this->quoteFormValidator($postData);
        if ($validatorResults->fails())
        {
            $validationReturn['result'] = 'fail';
        }else
        {
            $validationReturn['result'] = 'success';
            $validationReturn['recalculated_quote'] = $this->getRecalculatedQuote($postData);
        }
        return $validationReturn;
    }

    /**
     * Calculates our fee based on the fee of the transporter.
     *
     * @param Array $postData
     *
     * @return Array
     **/
    private function getRecalculatedQuote($postData)
    {
        $usersFee = isset($postData['job_quote_amt'])?$postData['job_quote_amt']:0;
        // Save this hash to the database.
        // We will be matching the calculated quote against this when we see it again.
        $saved_hash = \Hash::make(time() . (isset($postData['_token'])?$postData['_token']:md5(time())));
        $feeResult = $this->calculateFee($usersFee);
        $quote = $usersFee + $feeResult['fee'];
        $calculatedFeeData = [
            'calculated_by' => $feeResult['calculated_by'],
            'your_fee' => $usersFee,
            'saved_hash' => $saved_hash,
            'our_fee' => $feeResult['fee'],
            'quote' => $quote
        ];
        $savedCalculatedFee = $this->saveCalculatedFee($calculatedFeeData);
        $this->cleanUpCalculatedFees();
        return $calculatedFeeData;
    }

    /**
     * Saves the newly calculated fee in the database.
     * We will be checking this data against the data posted to
     * be saved as a quote.
     *
     * @param Array $calculatedFeeData
     *
     * @return void
     **/
    private function saveCalculatedFee($calculatedFeeData)
    {
        return \App\calculatedFees::create($calculatedFeeData);
    }

    /**
     * Calls the cron service to clean up old calculated fees.
     *
     * @return void
     **/
    private function cleanUpCalculatedFees()
    {
        $cronService = new \App\Services\Cron\CronService();
        $cronService->cleanUpCalculatedFees();
    }

    private function calculateFee($usersFee)
    {
        $result['calculated_by'] = 'minimum';
        $fees = \Config::get('constants.fees');
        $minimumFee = $fees['MINIMUM'];
        $feeFromPercent = floor(($fees['PERCENTAGE'] / 100) * $usersFee);
        if ($feeFromPercent > $minimumFee)
        {
            $result['calculated_by'] = 'percentage';
            $result['fee'] = $feeFromPercent;
            return $result;
        }
        $result['fee'] = $minimumFee;
        return $result;
    }

    private function submitQuote($postData, $jobNumber)
    {
        $validationReturn = [];
        $validatorResults = $this->quoteFormValidator($postData);
        if ($validatorResults->fails())
        {
            $validationReturn['validatorResults'] = $validatorResults;
            $validationReturn['result'] = 'fail';
            $validationReturn['error_messages'] = $validatorResults->messages();
        }else
        {
            $validationReturn['result'] = 'success';
            $this->saveQuote($postData, $jobNumber);
        }
        return $validationReturn;
    }

    private function saveQuote($postData, $jobNumber)
    {
        $deliveryMessageId = null;
        if (!empty($postData['quote_message']))
        {
            //Collect data to be saved
            $deliveryMessageId = $this->saveDeliveryMessageInfo($postData, $jobNumber);
        }
        //Collect data to be saved
        if ($this->checkCalculatedFee($postData))
        {
            return $this->saveQuoteInfo($postData, $jobNumber, $deliveryMessageId);
        }
        return null;
    }

    private function saveQuoteInfo($postData, $jobNumber, $deliveryMessageId)
    {
        $saveQuote = true;
        $quoteId = null;
        //Collect data to be saved
        $quoteInfo = $this->getQuoteInfo($postData, $jobNumber, $deliveryMessageId);
        $receiverAndSender = $this->getSavingMessageReceiverAndSender($jobNumber);
        $quoteInfo['sender'] = $receiverAndSender['sender'];
        $quoteInfo['receiver'] = $receiverAndSender['receiver'];
        // Should we save this quote or not?
        // If the message is null, lets just check the last quoted amount.
        // If its the same as the last previous amount, then there's no need
        // to save it.
        if (!is_null($deliveryMessageId))
        {
            $quote = \App\Quotes::create($quoteInfo);
            if (!is_null($quote->id))
            {
                $quoteId = $quote->id;
            }
        }
        return $quoteId;
    }

    /**
     * Checks and deletes the record of the saved calculated fee
     * The record if found is removed from the database.
     *
     * @param Array $postData
     *
     * @return boolean
     **/
    private function checkCalculatedFee($postData)
    {
        // Now if the calculated fee matches the one we are about to save
        // then its ok to save it to the db. If it doesn't match, we wont save it.
        $calculatedFees = new \App\Services\Cron\calculatedFees();
        $calculatedFeesId = $calculatedFees->isCalculatedFeeValid($postData);
        if($calculatedFeesId = $calculatedFees->isCalculatedFeeValid($postData)){
            // We can delete this calculatedFee
            $calculatedFees->deleteCalculatedFeeById($calculatedFeesId);
            return true;
        }
        return false;
    }

    private function saveDeliveryMessageInfo($postData, $jobNumber)
    {
        $deliveryMessageId = null;
        $deliveryMessageInfo = $this->getDeliveryMessageInfo($postData, $jobNumber);
        if (!empty($deliveryMessageInfo))
        {
            $deliveryMessageId = $this->addDeliveryMessage($deliveryMessageInfo, $postData);
        }
        return $deliveryMessageId;
    }

    private function getDeliveryMessageInfo($postData, $jobNumber)
    {
        $deliveryMessageInfo = [];
        //Get the delivery info - We have the job number.
        $delivery = Deliveries::getDelivery($jobNumber);
        //Collect data to be saved
        if (!empty($delivery))
        {
            $receiverAndSender = $this->getSavingMessageReceiverAndSender($jobNumber);
            $deliveryMessageInfo['unread'] = 1;
            $deliveryMessageInfo['delivery'] = $delivery[0]['id'];
            $deliveryMessageInfo['sender'] = $receiverAndSender['sender'];
            $deliveryMessageInfo['receiver'] = $receiverAndSender['receiver'];
            $deliveryMessageInfo['message'] = $postData['quote_message'];
        }
        return $deliveryMessageInfo;
    }

    private function getSavingMessageReceiverAndSender($jobNumber)
    {
        $delivery = Deliveries::getDelivery($jobNumber);
        //get the logged in User
        //get the logged in User
        $authUser = new AuthUser();
        $deliveryMessageInfo['sender'] = $authUser->getLoggedInUsersId();
        return [
            'receiver' => $delivery[0]['user'],
            'sender' => $authUser->getLoggedInUsersId()
        ];
    }

    private function getQuoteInfo($postData, $jobNumber, $deliveryMessageId)
    {
        //Collect data to be saved
        $quoteInfo = [];
        //see if this quote has a message.
        if (!is_null($deliveryMessageId))
        {
            $quoteInfo['with_message'] = 1;
            $quoteInfo['message'] = $deliveryMessageId;
        }
        $quoteInfo['new_quote'] = 1;
        $quoteInfo['delivery'] = $jobNumber;
        $quoteInfo['quote'] = $postData['job_quote_amt'];
        $quoteInfo['original_quote'] = $postData['your_fee'];
        $quoteInfo['our_fee'] = $postData['our_fee'];
        return $quoteInfo;
    }

    /**
     * Returns the quote information for this quote id.
     *
     * @param int $quoteId
     *
     * @return mixed [Array | null]
     **/
    public function getQuote($quoteId)
    {
        $quoteInfo = QuotesModel::where('quotes.id', $quoteId)
            ->join('deliveries_messages', 'deliveries_messages.id', '=', 'quotes.message')
            ->join('users', 'deliveries_messages.sender', '=', 'users.id')
            ->select('quotes.*', 'deliveries_messages.*', 'users.*', 'quotes.id as quote_id', 'users.id as user_id', 'deliveries_messages.id as deliveries_messages_id')->get();
        if ($quoteInfo)
        {
            return $quoteInfo->toArray();
        }
        return null;
    }

    public function getQuotes($deliveryId)
    {
        $QuotesInfo = QuotesModel::orderBy('quotes.updated_at', 'DESC')
            ->where('deliveries_messages.delivery', $deliveryId)
            ->join('deliveries_messages', 'deliveries_messages.id', '=', 'quotes.message')
            ->join('users', 'deliveries_messages.sender', '=', 'users.id')
            ->select('deliveries_messages.id as message_id', 'quotes.id', 'quotes.quote', 'quotes.updated_at', 'users.user_name')
            ->get()->toArray();
        return $QuotesInfo;
    }

    public function getQuotersLatestQuote($deliveryId)
    {
        $authUser = new AuthUser();
        $myQuotes = \App\Quotes::where('sender', '=', $authUser->getLoggedInUsersId())
            ->where('delivery', $deliveryId)
            ->orderBy('quotes.created_at', 'DESC')
            ->first();
        if ($myQuotes)
        {
            return $myQuotes->toArray();
        }
        return null;
    }

    public function addDeliveryMessage($deliveryMessageInfo, $postData = null)
    {
        // Hold on, we need to check whether this very same quote was previously saved.
        if (!$this->isThisQuoteDuplicated($deliveryMessageInfo, $postData))
        {
            // Save this delivery message
            $deliveryMessage = \App\DeliveriesMessage::create($deliveryMessageInfo);
            if (!is_null($deliveryMessage->id))
            {
                $deliveryMessageId = $deliveryMessage->id;
                return $deliveryMessageId;
            }
        }
        return null;
    }

    private function isThisQuoteDuplicated($deliveryMessageInfo, $postData = null)
    {
        // Lets look for the very last message.
        $deliveryMessage = \App\DeliveriesMessage::where('delivery', $deliveryMessageInfo['delivery'])
            ->where('sender', $deliveryMessageInfo['sender'])
            ->where('receiver', $deliveryMessageInfo['receiver'])
            // May be the user after receiving the last message wouldn't mind
            // getting the same messsage once more. We are just safeguading against
            // too many entries of the very same.
            ->where('unread', 1)
            ->orderby('id', 'DESC')
            ->first();
        // Lets now look for the last quote entered
        $quote = \App\Quotes::where('sender', $deliveryMessageInfo['sender'])
            ->where('receiver', $deliveryMessageInfo['receiver'])
            ->where('delivery', $deliveryMessageInfo['delivery'])
            ->orderby('id', 'DESC')
            ->first();
        if (!is_null($deliveryMessage) && !is_null($quote))
        {
            // Lets test the message against the one in the database.
            $saveMessage = $deliveryMessage->toArray();
            $lastQuote = $quote->toArray();
            if (
                $postData !== null &&
                $saveMessage['message'] === $postData['quote_message'] &&
                $lastQuote['quote'] === (int)$postData['job_quote_amt']
            )
            {
                return true; // Exact same message exist.
            }
        }
        return false;
    }

    public function getDeliveryQuotesNumber($deliveryId)
    {
        return \App\Quotes::where('delivery', $deliveryId)->count();
    }

    public function getMyDeliveryQuotes($deliveriesList)
    {
        foreach($deliveriesList as &$delivery)
        {
            $quoteInfo = $this->getUsersLatestQuotesOnDelivery($delivery['id']);
            $delivery['quote_info'] = $quoteInfo;
        }
        return $deliveriesList;
    }

    private function getUsersLatestQuotesOnDelivery($deliveryId)
    {
        $results = \App\Quotes::where('quotes.delivery', $deliveryId)
            ->join('users', 'quotes.sender', '=', 'users.id')
            ->select(\DB::raw("max(quotes.id) as quote_id, quotes.delivery, quotes.quote, users.*"))
            ->orderBy('quotes.quote')
            ->groupBy('users.id')->distinct()->get();
        if ($results->count() > 0)
        {
            return $results->toArray();
        }
        return null;
    }

    /**
     * Processes the quote as being accepted.
     *
     * @param int $quoteId
     *
     * @return void
     */
    public function acceptQuote($deliveryNonce, $quoteId)
    {
        // Once the user has arrived on this page, we know that they would have
        // accepted the quote. However, to be pedantic, lets make sure that the
        // logged in user is actually the one accepting this quote.
        $delivery = \App\Deliveries::where('delivery_nonce', $deliveryNonce)->get()->toArray();
        if (!empty($delivery) && $this->loggedInUserOwnsDelivery($delivery[0]['id']) && $this->quoteIdIsSetToDelivery($delivery[0]['id'], $quoteId))
        {
            // Retrieve the delivery
            $newDelivery = \App\Deliveries::find($delivery[0]['id']);
            // we need to do a few things here.
            // 1. Update the delivery status
            // This means that the delivery will be seen by the user only
            $newDelivery->status = \Config::get('constants.deliveries.statuses.ACTIVE_PRIVATE');
            // Set the quote id as the quote accepted for this delivery.
            $newDelivery->quote_accepted = $quoteId;
            return $newDelivery->save();
        }
        return null;
    }

    private function quoteIdIsSetToDelivery($deliveryId, $quoteId)
    {
        $quote = \App\Quotes::find($quoteId);
        if ($quote !== null)
        {
            // Does this quote belong to this delivery?
            if ($quote->delivery == $deliveryId)
            {
                return true;
            }
        }
        return false;
    }

    public function getAfterPaymentPages($retrievedQuoteId, $paymentResult)
    {
        // Now we have to decide on what page to show the user.
        // Did we get a fail, or a sucsess
        // even if we get a success, we still have to check to see if payment was done.
        if ($paymentResult['status'] === 'success')
        {
            return $this->getPaymentConfirmationData($retrievedQuoteId);
        }else
        {
            // Show the user the fail pages.
            return $this->getPaymentFailedData($retrievedQuoteId, $paymentResult);
        }
    }

    private function getPaymentFailedData($retrievedQuoteId, $paymentResult)
    {
        // We may have failed here. Be careful though, the failure could be
        // because of one of two things. The curl exercise failed or incorrect
        // data(such as wrong credit card number) was sent over to pay-pal.
        $failedMessages = [];
        $page = 'failed_internal_error';
        if (isset($paymentResult['messages']) && !empty($paymentResult['messages']))
        {
            $page = 'failed_wrong_info';
            $failedMessages = $paymentResult['messages'];
        }
        return [
            'page' => $page,
            'data' => [
                'failed_messages' => $failedMessages,
                'quote_data' => $this->afterPaymentQuoteAndDeliveryInfo($retrievedQuoteId)
            ]
        ];
    }

    private function afterPaymentQuoteAndDeliveryInfo($quote_id)
    {
        $quoteInfo = [];
        $deliveryInfo = [];
        if (null !== $quote_id)
        {
            $quoteInfo = $this->getQuote($quote_id);
            // We can retrieve the delivery info now.
            if (!empty($quoteInfo) && isset($quoteInfo[0]['delivery']))
            {
                $deliveryInfo = Deliveries::getDelivery($quoteInfo[0]['delivery']);
            }
        }
        return [
            'quote_info' => $quoteInfo,
            'delivery_info' => $deliveryInfo
        ];
    }

    private function getPaymentConfirmationData($retrievedQuoteId)
    {
        return [
            'page' => 'confirmation',
            'data' => $this->afterPaymentQuoteAndDeliveryInfo($retrievedQuoteId)
        ];
    }
}