<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiries extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'enquiry_topic',
        'enquiry_phone',
        'enquiry_email',
        'enquiry_message'
    ];
}
