<?php namespace App\Services\Validation;

use Validator;
use App\Services\Auth\User as AuthUser;
use \App\User;

class SiteValidator extends Validator
{
    /*
     *Array
    */
    private $loggedInUserObject = null;

    function __construct()
    {
        $authUser = new AuthUser();
        $this->loggedInUserObject = $authUser->getLoggedInUserObject();
        //Set the extra validation methods.
        $this->createExtraValidation();
        $this->validationIfUsernameAlreadyExists();
        $this->createPasswordIncorrectValidation();
    }

    /**
     * Create any extra validation rules.
     *
     * @return boolean
     */
    public function createExtraValidation()
    {
        //We need to validate an email address here.
        //The situation here is that this user must not change their
        //email to one existing in the database. They may however, use their
        //own if needed.
        self::extend('emailAlreadyInSystem', function ($attribute, $value, $parameters)
        {
            $loggedInUserEmail = null;
            if ($this->loggedInUserObject['result'] == "success")
            {
                $loggedInUserEmail = $this->loggedInUserObject['user_object']['email'];
            }
            //Allow this email if this is the user's email.
            If ($value == $loggedInUserEmail)
            {
                return true;
            }
            //If we get here, we will check the database to see if the
            //email is already registered in our system. If so, we will
            //not allow the user to have this email address.
            $userModel = new User();
            $users = $userModel->getUserByEmail($value);
            if (!is_null($users))
            {
                return false;
            }
            //All other will pass. - Success
            return true;
        });
    }

    private function validationIfUsernameAlreadyExists()
    {
        //We need to validate an email address here.
        //The situation here is that this user must not change their
        //email to one existing in the database. They may however, use their
        //own if needed.
        self::extend('user_name_exists', function ($attribute, $value, $parameters)
        {
            $loggedInUserUsername = null;
            if ($this->loggedInUserObject['result'] == "success")
            {
                $loggedInUserUsername = $this->loggedInUserObject['user_object']['user_name'];
            }
            //Allow this email if this is the user's email.
            If ($value == $loggedInUserUsername)
            {
                return true;
            }
            //If we get here, we will check the database to see if the
            //email is already registered in our system. If so, we will
            //not allow the user to have this email address.
            $userModel = new User();
            $users = $userModel->getUserByUsername($value);
            if (!is_null($users))
            {
                return false;
            }
            //All other will pass. - Success
            return true;
        });
    }

    private function createPasswordIncorrectValidation(){

        self::extend('passwordIncorrect', function ($attribute, $value, $parameters)
        {

        });
    }

    public function contactUsValidationSettings()
    {
        $enquiry_message_message = 'Please complete your message.';
        $enquiry_phone_message = 'Please enter a valid phone number.';
        $enquiry_email_message = 'Please enter an email address.';
        return [
            'validation_requirements_array' => [
                'enquiry_email' => 'required|email',
                'enquiry_message' => 'required|min:5',
                'enquiry_phone' => 'required|digits_between:6,15',
                'enquiry_topic' => 'required'
            ],
            'validation_messages' => [
                'enquiry_email.email' => $enquiry_email_message,
                'enquiry_phone.required' => $enquiry_phone_message,
                'enquiry_phone.digits_between' => $enquiry_phone_message,
                'enquiry_message.required' => $enquiry_message_message,
                'enquiry_message.min' => $enquiry_message_message,
            ]
        ];
    }
}