<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class PrivateController extends BaseController
{
	protected $viewData = [];

	public function __construct()
	{
		// Call middleware auth here as we want to make
		// sure that users must be logged in to see this
		// page.
		$this->middleware('auth');

		// Making sure that the layout is shared across all views.
		\Illuminate\Support\Facades\View::share("layout", 'layouts.master');

		// We have another layout specifically for private account Pages
		\Illuminate\Support\Facades\View::share("account_page_layout", 'layouts.account_page');
	}
}
