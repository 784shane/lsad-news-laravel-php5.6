<?php namespace App\Services;

use App\Services\CurlRequests;

class Payments extends CurlRequests
{
    const EXPRESS_CHECKOUT_QUOTE_ID = 'express_checkout_quote_id';

    public function getDisplayablePaymentInfo($paymentResult){
        // We will translate the payment result into displayable information.
        if($paymentResult['status']=="success"){
            $resultArray = [
                'amt_paid'=>$paymentResult['result']['AMT'],
                'paid_currency'=>$paymentResult['result']['CURRENCYCODE'],
                'paid_time'=>$this->convertPaypalTimeStampToFriendlyTT($paymentResult['result']['TIMESTAMP'])
            ];

            if(isset($paymentResult['post_data']) && isset($paymentResult['post_data']['ACCT'])){
                $resultArray['cc_masked_long_number'] = $this->translateViewableCreditCardInfo($paymentResult['post_data']['ACCT']);
            }
        }
        return [];
    }

    private function translateViewableCreditCardInfo($acctNumber){
        $segments = 4;
        $maskedAcctString = '';
        // Get the last four digits of the account number.
        $lastAcctDigits = substr($acctNumber,-4);
        // Get the amount of digits before the last four digits.
        $preLastAcctAmt = strlen($acctNumber)-4;
        $digitsRemaining = ($preLastAcctAmt%$segments);
        // Number of times we will loop to mask the account number
        $numberOfLoops = floor(($preLastAcctAmt/$segments));
        for($i=0; $i<$numberOfLoops; $i++){
            $maskedAcctString .= str_repeat("*", $segments).' ';
        }
        // Are there digits we need to add on to the masked cc account
        if($digitsRemaining){
            $maskedAcctString .= str_repeat("*", $digitsRemaining).' ';
        }

        // Add on the last part of the credit card
        return $maskedAcctString .= $lastAcctDigits;
    }

    private function convertPaypalTimeStampToFriendlyTT($payPalTimestamp){
        return strtotime(preg_replace('/[a-zA-Z]/',' ',$payPalTimestamp));
    }

    public function processPaypalCreditCardPayments($charge){
        // Get the posted data
        $allPosts = \Request::all();
        $allPosts = array_merge($charge, $allPosts);
        $paymentResult = $this->sendCreditCardDirectPayment($allPosts);
        return $paymentResult;

    }

    private function sendCreditCardDirectPayment($posts){

        // Make sure the pay-pal credentials are set.
        $this->setPaypalCredentials();

        // Gather what's needed for the direct payment submission.
        $postData = [
            'IPADDRESS'=>\Request::ip(),
            //'CREDITCARDTYPE'=>'VISA', //optional
            'ACCT'=>$posts['card_number'],
            'EXPDATE'=>$posts['card_exp_date_month'].$posts['card_exp_date_year'],
            'CVV2'=>$posts['card_csc'],
            'FIRSTNAME'=>$posts['card_first_name'],
            'LASTNAME'=>$posts['card_last_name'],
            'STREET'=>$posts['card_billing_address_1'],
            'STATE'=>$posts['card_city'],
            'CITY'=>$posts['card_city'],
            'COUNTRYCODE'=>'UK',
            'ZIP'=>$posts['card_zip_code']
        ];

        return $this->doDirectCreditCardPayment($postData);
    }

    public function processesAfterDirectPayment($quote_id, $paymentResult){
        // At this point, we will save data to the database.
        if(strtolower($paymentResult['status']) == "fail"){
            // Display a fail message to the user
        }else{
            // We may want to keep a record of this transaction
            // Keep as a record in the database.
            $this->databaseChangesAfterSuccessfulPayment($quote_id, $paymentResult);
        }
        return $paymentResult;
    }

    private function databaseChangesAfterSuccessfulPayment($quote_id, $paymentResult){
        // Retrieve the quote info and the delivery info
        if(null !== $quote_id){
            $quotes = new \App\Services\Quotes\Quotes();
            $quoteInfo = $quotes->getQuote($quote_id);

            //Get the Deliveries model
            $deliveryModel = \App\Deliveries::find($quoteInfo[0]['delivery']);

            // we need to do a few things here.
            // 1. Update the delivery status
            // This means that the delivery will be seen by the user and the transporter only
            $deliveryModel->status = \Config::get('constants.deliveries.statuses.ARCHIVED');

            // We need to update the payment made boolean to true.
            $deliveryModel->payment_made = true;

            // Save the changes to the deliveries table.
            $deliveryModel->save();
        }
    }

    public function processExpressCheckoutResult(){
        return $this->testExpressResult();
    }

    private function storeSessionBeforeExpressCheckout($transactionData){
        // Lets keep the quote_id
        if(isset($transactionData['quoteId'])){
            \Session::put(self::EXPRESS_CHECKOUT_QUOTE_ID, $transactionData['quoteId']);
        }
    }

    public function retrieveQuoteInfoAfterPayment(){
        return [
            'quote_id' => $this->retrieveQuoteIDAfterPayment()
        ];
    }

    private function retrieveQuoteIDAfterPayment(){
        // The quote_id could have been stored in a session or sent over
        // with a 'get' request. We will try both methods until we get the
        // quote id.
        if(\Request::get('quote_id')){
            return \Request::get('quote_id');
        }

        if(\Session::has(self::EXPRESS_CHECKOUT_QUOTE_ID)){
            $sessionQuoteId = \Session::get(self::EXPRESS_CHECKOUT_QUOTE_ID);
            // Change the session for we don't want a rogue quote id next time.
            //\Session::forget(self::EXPRESS_CHECKOUT_QUOTE_ID);
            return $sessionQuoteId;
        }

        return null;
    }

    public function processPaypalPayments($transactionData)
    {
        // Keep information needed for when we return from express checkout.
        $this->storeSessionBeforeExpressCheckout($transactionData);

        // Make sure the pay-pal credentials are set.
        $this->setPaypalCredentials();

        // Get the express checkout cancelUrl
        $transactionData['cancelUrl'] = $this->getExpressCheckoutCancelUrl();

        // Get the express checkout returnUrl
        $transactionData['returnUrl'] = $this->getExpressCheckoutReturnUrl();

        $result = $this->setExpressCheckout($transactionData);

        $payPalURL = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&useraction=commit&token=".$result['result']['TOKEN'];

        return $payPalURL;
    }

    private function getExpressCheckoutReturnUrl(){
        // Use the pre-defined express checkout result route.
        return \URL::route('paypal.express_checkout_result');
    }

    private function getExpressCheckoutCancelUrl(){
        // Use the pre-defined express checkout cancel route.
        return \URL::route('paypal.express_checkout_cancel');
    }
}