<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemSpecifics extends Model
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item',
        'description',
        'main_image'
    ];
}
