<h1>Payment failed</h1>
@if(isset($page))
    @if($page === "failed_wrong_info")
        <div>
            <p>Looks like something went wrong.</p>
            <p>Your payment attempt returned the following errors</p>
            @if(isset($data['failed_messages'])&&!empty($data['failed_messages']))
                <ul>
                    @foreach($data['failed_messages'] as $failedMessage)
                        <li>{{ $failedMessage  }}</li>
                    @endforeach
                </ul>
            @endif
        </div>
    @endif
    @if($page === "failed_internal_error")
        <div>
            <p>Something went wrong.</p>
            <p>Unfortunately your payment encountered an <b>Internal Error</b></p>
        </div>
    @endif
@endif

<div class="description-box payment-information-box">
    <div class="description-box-inner">
        <h4>Try again?</h4>
        <div class="particulars">
            <div class="try-again-inner">
                <p>If you wish to try again, you can do so here</p>
                @if(isset($data['quote_data']['quote_info']))
                    @foreach($data['quote_data']['quote_info'] as $quote_info)
                        <div class="picture"><img src="http://placehold.it/100x60" width="100%"></div>
                        <div><b>Transport provider</b></div>
                        <div>{{ $vComposerHelper->getIfElementExists($quote_info,'user_name') }}</div>
                        <br /><br />
                    @endforeach
                @endif


                @if(isset($data['quote_data']['delivery_info']))
                    @foreach($data['quote_data']['delivery_info'] as $deliveryInfo)
                        <div><b>Delivery:</b></div>
                        <div>{{ $vComposerHelper->getIfElementExists($deliveryInfo,'subtitle')
                        }}
                        </div>
                        <div class="failed-payment-delivery-details">
                            <div class="picture">{!!
                        Html::image($vComposerHelper->missingImageResolver($vComposerHelper->getIfElementExists($deliveryInfo,'overallImage')),
                        null, array('width'=>'100%')) !!}
                            </div>
                            <div><b>Collection from:</b></div>
                            <div>{{
                        $vComposerHelper->getIfElementExists($deliveryInfo,'collection_address')
                        }}
                            </div>
                            <div><b>Delivery to:</b></div>
                            <div>{{
                        $vComposerHelper->getIfElementExists($deliveryInfo,'delivery_address')
                        }}
                            </div>
                            <div><b>Distance:</b></div>
                            <div>{{
                        $vComposerHelper->getReadableMileage($vComposerHelper->getIfElementExists($deliveryInfo,'distance_metres'))
                        }}
                            </div>
                        </div>
                    @endforeach
                @endif


            </div>
            @if(isset($data['quote_data']['delivery_info'])&&isset($data['quote_data']['quote_info']))
                {!! Html::link('/quotes/accept/'.$data['quote_data']['delivery_info'][0]['delivery_nonce'].'/'.$data['quote_data']['quote_info'][0]['quote_id'],'Try paying again', array('class'=>"pure-button pure-button-primary")) !!}
            @endif
        </div>
    </div>
</div>