<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemporaryUploadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('temporary_uploads', function($table)
	    {
	        $table->increments('id');
			$table->string('form_token');
			$table->string('form_time');
			$table->string('file_name');
			$table->integer('item_order_no',false, true)->length(11);
	        $table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('temporary_uploads');
	}

}
