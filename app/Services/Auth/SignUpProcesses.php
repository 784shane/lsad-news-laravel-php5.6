<?php namespace App\Services\Auth;

/**
 * This class takes care of the registering process.
 * All the activities involving siging up.
 **/
use Mail;
use Validator;
use App\Services\Email;

Abstract class SignUpProcesses
{
    const REGISTERED_USER_EMAIL_VIEW = 'emails.registered_user';
    const FORGOT_PASSWORD_EMAIL_VIEW = 'emails.forgot_password';
    protected static $userFieldsToKeep = ['first_name', 'last_name', 'email', 'user_name', 'telephone_number', 'password'];
    protected $registerNonce = null;

    protected function sendConfirmationEmail($inputs, $emailData, $mailType)
    {
        try
        {
            $result = Mail::send($this->getCorrectMailType($mailType), $emailData, function ($message) use ($inputs)
            {
                $message->to($inputs['email'])
                    ->subject('Welcome to Under2000!')
                    ->from('admin@under2000.com', 'under2000');
            });
        }
        catch(\Exception $e)
        {
            exit($e->getMessage());
        }
    }

    private function getCorrectMailType($mailType)
    {
        switch($mailType)
        {
            case "forgot_password":
            {
                return self::FORGOT_PASSWORD_EMAIL_VIEW;
            }
            default:
            {
                return self::REGISTERED_USER_EMAIL_VIEW;
            }
        }
    }

    final function buildToken($email = "")
    {
        return md5(time() . $email);
    }

    protected function validateForgotEmail($email)
    {
        $validationReturn = array('result' => 'success');
        $validatorResults = Validator::make(['email' => $email], [
            'email' => 'required|email'
        ],
            //insert our custom messages
            array('email' => 'Please make sure you entered an email.')
        );
        if ($validatorResults->fails())
        {
            $validationReturn['result'] = 'fail';
            $validationReturn['validator'] = $validatorResults;
        }
        return $validationReturn;
    }

    protected function signUpValidator($data)
    {
        return Validator::make($data, [
            'first_name' => 'required|min:1',
            'last_name' => 'required|min:1',
            'password' => 'required|min:8',
            //Lets make sure that confirm password and password matches.
            'confirm_password' => 'required|min:8|same:password',
            //We are going to check the database here to see if the
            //user's email already exists.
            'email' => 'required|email|unique:users'
        ],
            //insert our custom messages
            $this->registerMessages()
        );
    }

    protected function editUserValidator($data)
    {
        return Validator::make($data, [
            'first_name' => 'required|min:1',
            'last_name' => 'required|min:1',
            'password' => 'required|min:8',
            //Lets make sure that confirm password and password matches.
            'confirm_password' => 'required|min:8|same:password',
            //We are going to check the database here to see if the
            //user's email already exists.
            'email' => 'required|email|emailAlreadyInSystem'
        ],
            //insert our custom messages
            $this->registerMessages()
        );
    }

    protected function passwordResetValidator($data)
    {
        return Validator::make($data, [
            'password' => 'required|min:8',
            //Lets make sure that confirm password and password matches.
            'confirm_password' => 'required|min:8|same:password',
        ],
            //insert our custom messages
            $this->registerMessages()
        );
    }

    protected function businessSignUpFormValidator($data)
    {
        return Validator::make($data, [
            'company_user_name' => 'required|user_name_exists|min:5',
            'company_email' => 'required|email',
            'company_name' => 'required|min:1',
            'company_phone_number' => 'required|numeric|min:5',
            'company_mobile_number' => 'required|numeric|min:5',
            'company_address_1' => 'required|min:1',
            'company_city' => 'required|min:1',
            'company_postcode' => 'required|min:1'
            //'required|email|emailAlreadyInSystem'
        ],
            //insert our custom messages
            $this->registerMessages()
        );
    }

    /**
     * Get the array of messages which should be applied
     * to this validation of form inputs.
     *
     * @return array
     */
    private function registerMessages()
    {
        $userNameErrorMsg = 'This user name is taken, please choose another.';
        return array(
            'email.unique' => 'Sorry, this email is already in our database.',
            'confirm_password.same' => 'Your passwords do not match. Please try again.',
            'email.emailAlreadyInSystem' => 'This email is already used by someone.',
            'user_name.user_name_exists' => $userNameErrorMsg,
            'company_user_name.user_name_exists' => $userNameErrorMsg
        );
    }

    /**
     * Get the array of messages which should be applied
     * to this validation of form inputs.
     *
     * @return array
     */
    protected function sendBusinessVerificationRequest($businessValidationToken)
    {
        //We need to get some data about this business
        $business = new \App\Business();
        $business = $business->findBusinessesByToken($businessValidationToken);
        if (count($business))
        {
            $businessValidationData = [
                'businessValidationToken' => $businessValidationToken,
                'toEmail' => $business[0]['company_email']
            ];
            $email = new Email();
            return $email->sendBusinessVerificationRequest($businessValidationData);
        }
        return null;
    }
}
