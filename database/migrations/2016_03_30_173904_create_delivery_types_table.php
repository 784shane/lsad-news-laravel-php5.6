<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * Note: to seed this table - php artisan db:seed --class=DeliveryTypesTableSeeder
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('delivery_types', function($table)
		{
			//
			$table->increments('id');
			$table->string('status_name')->nullable();
			$table->integer('status_value', false, true)->nullable()->length(3);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('delivery_types');
	}

}
