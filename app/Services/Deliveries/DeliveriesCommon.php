<?php namespace App\Services\Deliveries;

trait DeliveriesCommon
{

    /**
     * Checks to see if the logged in user owns the delivery.
     *
     * @param int $deliveryId // the id of the delivery
     *
     * @return boolean
     */
    protected function loggedInUserOwnsDelivery($deliveryId)
    {
        $delivery = \App\Deliveries::where('id', $deliveryId);
        if ($delivery)
        {
            $d = $delivery->get();
            if (isset($d[0]['user']))
            {
                $deliveryOwner = $d[0]['user'];
                $authUser = new \App\Services\Auth\User();
                // Now see if the logged in user and the own are the same person.
                if ($deliveryOwner == $authUser->getLoggedInUsersId())
                {
                    return true;
                }
            }
        }
        return false;
    }
}