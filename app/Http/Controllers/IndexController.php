<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use SiteData;


class IndexController extends PublicController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
    	$this->viewData['itemsToShip'] = SiteData::itemsToShipList();
        return view('index')->with($this->viewData);
    }
}
