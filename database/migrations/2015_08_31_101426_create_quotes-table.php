<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('quotes', function($table)
	    {
	        $table->increments('id');
	        //Column user, autoincrement, unsigned
			$table->integer('quote',false, true)->length(11);
			$table->integer('original_quote',false, true)->length(11);
			$table->integer('our_fee',false, true)->length(11);
	        
	        $table->integer('sender',false, true)->length(11);
	        $table->integer('receiver',false, true)->length(11);

	        //Column user, autoincrement, unsigned
	        $table->integer('delivery',false, true)->length(11);

	        //If this quote was received with a message, we will
	        //switch a boolean - whether it is true or false.
			$table->boolean('with_message')->default(false);
			
	        //If this quote was received with a message we will
	        //make a note of the message it is linked to here.
	        $table->integer('message',false, true)->length(11);
			
	        //If this is unseen, ie. at login or when the messages page
	        //hasnt been loaded, we will mark this as new.
	        $table->boolean('new_quote')->default(false);

	        //My foreign keys
	        $table->foreign('delivery')->references('id')->on('deliveries');
	        $table->foreign('message')->references('id')->on('deliveries_messages');
	        $table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('quotes');
	}

}
