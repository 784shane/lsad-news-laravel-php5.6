<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Gather the constant saved in the config.
		$activePublicInteger = \Config::get('constants.deliveries.statuses.ACTIVE_PUBLIC');

		Schema::create('deliveries', function($table) use($activePublicInteger)
		{
			$table->increments('id');
			$table->string('title')->length(300);
			$table->string('email');
			$table->string('shipping_category');
			$table->string('collection_postcode');
			$table->string('delivery_postcode');
			$table->string('collection_address');
			$table->string('delivery_address');
			$table->string('distance_text')->length(60)->nullable();
			$table->string('distance_metres')->length(60)->nullable();
			$table->string('subtitle')->length(300);
			$table->integer('use_as_main_image', false, true)->nullable()->length(11);

			$table->string('telephone_number');
			$table->string('delivery_nonce', 60)->unique()->nullable();
			//Column user, autoincrement, unsigned
			$table->integer('user',false, true)->length(11);
			// Placing the quote id that was accepted
			$table->integer('quote_accepted', false, true)->nullable()->length(11);
			// Set the status for the delivery.
			$table->integer('status', false, true)->nullable()->default($activePublicInteger)->length(3);
			// Identify whether or not payment has been made for this delivery.
			$table->boolean('payment_made')->default(false);

			$table->timestamp('expiry')->nullable();

			//My foreign keys
			$table->foreign('user')->references('id')->on('users');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('deliveries');
	}

}