@extends($layout)
@section('content')
    <h1>Contact Us</h1>
{!! Form::open(array(
'id'=>'collect_deliveries_info_pt2',
'class'=>"pure-form pure-form-stacked")) !!}
    <div>
        <div>{!!Form::label('enquiry_topic', 'Choose a topic for your message')!!}</div>
        <div>{!!Form::select('enquiry_topic', [
        'problems'=>'Problems using the website',
        'complaints'=>'Complaints',
        'general'=>'General Enquiry'
        ], 'null', array('id'=>'shipping_category'))!!}</div>
    </div>
    <div>
        <div>{!!Form::label('enquiry_phone', 'Your contact phone number')!!}</div>
        <div>{!!Form::text('enquiry_phone',  null, array('placeholder'=>'phone number.', 'required' => 'required') )!!}</div>
        <span class="form-errors">{{ $errors->first('enquiry_phone') }}</span>
    </div>
    <div>
        <div>{!!Form::label('enquiry_email', 'Your email address')!!}</div>
        <div>{!!Form::email('enquiry_email',  null, array('placeholder'=>'email address.', 'required' => 'required') )!!}</div>
        <span class="form-errors">{{ $errors->first('enquiry_email') }}</span>
    </div>
    <div>
        <div>{!!Form::label('enquiry_message', 'Enter your message.')!!}</div>
        <div>{!!Form::textarea('enquiry_message',  null, array('placeholder'=>'Enter your message.', 'required' => 'required') )!!}</div>
        <span class="form-errors">{{ $errors->first('enquiry_message') }}</span>
    </div>
    <div class="submit_surround">
        {!! Form::submit('Submit Enquiry', array('class'=>'pure-button pure-button-primary narrow-submit-button')) !!}
    </div>

{!! Form::close() !!}
@stop
