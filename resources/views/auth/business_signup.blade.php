@extends($layout)
@section('content')
    @if(isset($business_registration_complete))
        @if(empty($loggedInEmail))
            <div>
                Thank you for registering your business.
                We have sent you an email with instructions on how
                to confirm. {!! Html::link('login','Login') !!}
            </div>
        @else
            <div>
                Thank you for registering your business.
                Please click on the link in the email we've sent you to confirm your business.
                You can see your account here - {!! Html::link('my-account','My account') !!}
            </div>
            @endif
            @else



            {!! Form::open(array('url' => $formUrl,
                'id'=>"business_sign_up_form",
                'class'=>"pure-form pure-form-stacked")) !!}

            @include('partials.business.business_signup')

                    <!-- The form token. -->
            {!!  Form::token() !!}

            <div>
                {!! Form::submit('Send', array('class'=>'pure-button pure-button-primary narrow-submit-button')) !!}
            </div>
            {!! Form::close() !!}


        @endif
@stop
