<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SiteValidatorProvider extends ServiceProvider {

	private $siteValidator = '';

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//initiate the SiteValidator
		new \App\Services\Validation\SiteValidator();
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
        $this->app->bind('extra_validation_class', function(){
        	return new \App\Services\Validation\SiteValidator;
        });
	}

}
