@extends($layout)
@section('content')
    <h1>Thank you - Message submitted!</h1>
    <p>Your message has been submitted. Should a reply be required, someone on our team will be in contact with you shortly</p>
@stop
