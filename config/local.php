<?php
return [
    // Ebay app config data
    'fieldsDbMapping' => [
        // For the items_values table
        'itemsValues' => [
            ['db_name' => 'ebay_id', 'field_name' => 'ebay_item_number_'],
            ['db_name' => 'main_image', 'field_name' => 'ebay_item_image_'],
            ['db_name' => 'name', 'field_name' => 'item_name_'],
            ['db_name' => 'use_ebay', 'field_name' => 'use_ebay_'],
            ['db_name' => 'description', 'field_name' => 'item_description_'],
            ['db_name' => 'item_length', 'field_name' => 'item_length_'],
            ['db_name' => 'item_width', 'field_name' => 'item_width_'],
            ['db_name' => 'item_height', 'field_name' => 'item_height_'],
            ['db_name' => 'item_weight', 'field_name' => 'item_weight_'],
            // Measured units specified by the user.
            ['db_name' => 'measured_width', 'field_name' => 'measured_width_'],
            ['db_name' => 'measured_height', 'field_name' => 'measured_height_'],
            ['db_name' => 'measured_length', 'field_name' => 'measured_length_'],
            ['db_name' => 'measured_weight', 'field_name' => 'measured_weight_'],
            // Whether we should use one photo as our main photo or not.
            ['db_name' => 'use_item_image', 'field_name' => 'use_as_main_image_'],
            //['db_name' => 'main_image', 'field_name' => 'ebay_item_image_']
        ],

        // For the item_specifics table
        'itemSpecificsFields' => [
            'description',
            'main_image'
        ],
    ],
    // Ebay app config data
    'ebay' => [
        'app_id' => 'ShaneDan-7e74-4a55-9dbe-c0da8f0dcd4e',
        'global_ids' => [
            'EBAY-GB',
            'EBAY-US',
            'EBAY-IE',
            'EBAY-AT',
            'EBAY-AU',
            'EBAY-CH',
            'EBAY-DE',
            'EBAY-ENCA',
            'EBAY-ES',
            'EBAY-FR',
            'EBAY-FRBE',
            'EBAY-FRCA',
            'EBAY-HK',
            'EBAY-IN',
            'EBAY-IT',
            'EBAY-MY',
            'EBAY-NL',
            'EBAY-NLBE',
            'EBAY-PH',
            'EBAY-PL',
            'EBAY-SG'
        ]
    ],
    // Google app config data
    'google' => [
        'maps' => [
            //Keys at - https://console.developers.google.com/project/transport-service-provider/apiui/credential
            'browser_key' => 'AIzaSyBKtiJ0dBkYar-pDi_LEUvzTFNJsqZ4kio',
            'server_key' => 'AIzaSyBTyNgno0aHsgrhtVXwJhhqY8mF_vnsV-Y'
        ]
    ],

    'unitsOfMeasurement' => [
        'default'=>'imperial',
        'metric' => [
            'length' => [
                'metres' => [
                    'long_name' => 'metres',
                    'short_name' => 'm',
                ],
                'kilometres' => [
                    'long_name' => 'kilometres',
                    'short_name' => 'km',
                ],
                'centimetres' => [
                    'long_name' => 'centimetres',
                    'short_name' => 'cm',
                ]
            ],
            'weight' => [
                'grams' => [
                    'long_name' => 'grams',
                    'short_name' => 'g',
                ],
                'kilograms' => [
                    'long_name' => 'kilograms',
                    'short_name' => 'kg',
                ]
            ],
            'capacity' => [
                'litres' => [
                    'long_name' => 'litres',
                    'short_name' => 'l',
                ],
                'millilitres' => [
                    'long_name' => 'millilitres',
                    'short_name' => 'ml',
                ]
            ]
        ],

        'imperial' => [
            'length' => [
                'feet' => [
                    'long_name' => 'feet',
                    'short_name' => 'ft',
                ],
                'miles' => [
                    'long_name' => 'miles',
                    'short_name' => 'm',
                ],
                'inches' => [
                    'long_name' => 'inches',
                    'short_name' => 'in',
                ],
                'yards' => [
                    'long_name' => 'yards',
                    'short_name' => 'yds',
                ]
            ],
            'weight' => [
                'tonnes' => [
                    'tonnes' => 'tonnes',
                    'short_name' => 'tonnes',
                ],
                'stones' => [
                    'stones' => 'stones',
                    'short_name' => 'stones',
                ],
                'pounds' => [
                    'long_name' => 'pounds',
                    'short_name' => 'lbs',
                ]
            ],
            'capacity' => [
                'litres' => [
                    'long_name' => 'litres',
                    'short_name' => 'l',
                ],
                'millilitres' => [
                    'long_name' => 'millilitres',
                    'short_name' => 'ml',
                ]
            ]
        ]


    ],

    'paypal' => [
        'environment' => 'sandbox',
        // Sandbox Paypal credentials
        'sandbox'=>[
            'accounts'=>[
                'buyer'=>[
                    'emailId'=>'vibes-buyer@fadmail.com',
                    'password'=>'schooldays',
                    'dateOfBirth'=>'01/01/1980',
                    'phone'=>'0356933289',
                    'accountType'=>'personal',
                    'accountNumber'=>'64175753',
                    'RoutingNumber'=>'700709',
                    'CreditCardNumber'=>'4137352300903856',
                    'CreditCardType'=>'VISA',
                    'billingAddressLine1'=>'1 Main Terrace',
                    'billingAddressLine2'=>'Wolverhampton, West Midlands',
                    'billingAddressPostCode'=>'W12 4LQ',
                    'expiryDateMonth'=>'04',
                    'expiryDateYear'=>'2021',
                    'creditCardCSC'=>'434',
                    'CreditCardExpiryDate'=>'04/2021',
                ],
                'merchant'=>[
                    'emailId'=>'vibes-facilitator@fadmail.com',
                    'password'=>'password1',
                    'phone'=>'0359291293',
                    'accountType'=>'Business',
                    'accountNumber'=>'89928798',
                    'RoutingNumber'=>'700709',
                    'CreditCardNumber'=>'4137352104370179',
                    'CreditCardType'=>'VISA',
                    'CreditCardExpiryDate'=>'04/2021',
                    'api_credentials'=>[
                        //'userName'=>'vibes-facilitator_api1.fadmail.com',
                        //'password'=>'7DBUWCDTKRRVYPLU',
                        //'signature'=>'AQU0e5vuZCvSg-XJploSa.sGUDlpAEe20PAeBlfb.EFZanlUC-iq4t5P',
                        'userName'=>'santafe_api1.fadmail.com',
                        'password'=>'F4DE8PRLAKAV65JT',
                        'signature'=>'AFcWxV21C7fd0v3bYYYRCpSSRl31AHS6X4k.wP2Cm7HFp2r8ijzoN-kh',
                    ]
                ]
            ],

            'currencyCode'=>'USD',

            'api'=>[
                'version'=>'78'
            ],

            'expressCheckout'=>[
                'nvpSignatureEndpoint'=>'https://api-3t.sandbox.paypal.com/nvp'
            ]
        ],// End of sandbox

        // Live Paypal credentials
        'live'=>[
            'accounts'=>[
                'buyer'=>[
                    'emailId'=>'vibes-buyer@fadmail.com',
                    'password'=>'password1',
                    'phone'=>'0356933289',
                    'accountType'=>'personal',
                    'accountNumber'=>'64175753',
                    'RoutingNumber'=>'700709',
                    'CreditCardNumber'=>'4137352300903856',
                    'CreditCardType'=>'VISA',
                    'CreditCardExpiryDate'=>'04/2021',
                ],
                'merchant'=>[
                    'emailId'=>'vibes-facilitator@fadmail.com',
                    'password'=>'password1',
                    'phone'=>'0359291293',
                    'accountType'=>'Business',
                    'accountNumber'=>'89928798',
                    'RoutingNumber'=>'700709',
                    'CreditCardNumber'=>'4137352104370179',
                    'CreditCardType'=>'VISA',
                    'CreditCardExpiryDate'=>'04/2021',
                    'api_credentials'=>[
                        'userName'=>'vibes-facilitator_api1.fadmail.com',
                        'password'=>'7DBUWCDTKRRVYPLU',
                        'signature'=>'AQU0e5vuZCvSg-XJploSa.sGUDlpAEe20PAeBlfb.EFZanlUC-iq4t5P'
                    ]
                ]
            ],

            'currencyCode'=>'USD',

            'api'=>[
                'version'=>'78'
            ],

            'expressCheckout'=>[
                'nvpSignatureEndpoint'=>'https://api-3t.sandbox.paypal.com/nvp'
            ]
        ]// End of live

    ]
];