<?php namespace App\Services;

class ActiveMenuLinks
{
    const MAIN_MENU_ACTIVE = 'routes.main_nav.active';
    /*
    * @var class self
    */
    private static $_instance = null;

    /**
     * @name getInstance
     * Class is constructed once. This function provides the same _instance of
     * the class at all times.
     * @return self
     **/
    public static function getInstance()
    {
        if (null === self::$_instance)
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __construct()
    {

    }

    public function setActiveLink($groupName = null)
    {
        // Set the active link for the menu.
        \Config::set(self::MAIN_MENU_ACTIVE, $groupName);
    }

    public function getActiveLink($groupName = null)
    {
        // Set the active link for the menu.
        return \Config::get(self::MAIN_MENU_ACTIVE);
    }
}