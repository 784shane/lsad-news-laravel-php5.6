<?php namespace App\Services\Data;

abstract class SiteTexts {

	public static function itemsToShip(){
		return [
		"moving_home"=>"Moving home",
		"furniture"=>"Furniture",
		"cars"=>"Cars",
		"vehicles"=>"Other vehicles",
		"boats"=>"Boats",
		"pets_animals"=>"Pets/Animals",
		"general"=>"General items",
		"other"=>"Other",
		];
	}
}