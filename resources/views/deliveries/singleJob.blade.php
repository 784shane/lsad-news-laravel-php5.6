@extends($layout)
@section('content')


    <div>

        <h1>Delivery details</h1>

        {{-- Allow others to quote for this job but not the owner. --}}
        @if($owner['id']!==$loggedInUser)
            @include('quotes.add_quote_cta', array('ctaType'=>$vComposerHelper->jobQuoteCtaType()))
        @endif

        <section class="top my_delivery_item">
            <div id="map"></div>
        </section>
        <section class="top my_delivery_item">
            <a class="main_link">{{$myDeliveries[0]['subtitle']}}</a>
            <div class="pure-g my-delivery-items-details">
                <div class="pure-u-1-5">
                    <div class="my_delivery_item_left_insert">
                        {!!Html::image($vComposerHelper->missingImageResolver($myDeliveries[0]['overallImage']),
                        $myDeliveries[0]['subtitle'], array('width'=>'100%'))!!}
                    </div>
                </div>
                <div class="pure-u-4-5">


                    <div class="pure-g">
                        <div class="pure-u-1-2">
                            <div><b>Title</b></div>
                            <div>{{$myDeliveries[0]['subtitle']}}</div>
                        </div>
                        <div class="pure-u-1-2">
                            <div><b>Number of Quotes</b></div>
                            <div>{{ $myDeliveries[0]['number_of_quotes'] }}</div>
                        </div>
                    </div>


                    <div class="pure-g">
                        <div class="pure-u-1-2">
                            <div><b>move from:</b></div>
                            <div>{{$myDeliveries[0]['collection_address']}}</div>
                        </div>
                        <div class="pure-u-1-2">
                            <div><b>Distance</b></div>
                            <div>{{ $vComposerHelper->getReadableMileage($myDeliveries[0]['distance_metres'])  }}</div>
                        </div>
                    </div>


                    <div class="pure-g">
                        <div class="pure-u-1-2">
                            <div><b>move to:</b></div>
                            <div>{{$myDeliveries[0]['delivery_address']}}</div>
                        </div>
                        <div class="pure-u-1-2">
                            <div><b>Distance</b></div>
                            <div>{{ $vComposerHelper->getReadableMileage($myDeliveries[0]['distance_metres'])  }}</div>
                        </div>
                    </div>



                </div>
            </div>
        </section>

        <section class="top my_delivery_item">
            <div class="pure-g my_delivery_item_row">
                <div class="pure-u-1-3">
                    <div class="pure-g">
                        <div class="pure-u-7-24"><b>Owner:</b></div>
                        <div class="pure-u-16-24">{{$owner['user_name']}}</div>
                    </div>
                </div>
                <div class="pure-u-1-3">
                    <div class="pure-g">
                        <div class="pure-u-7-24"><b>Date listed:</b></div>
                        <div class="pure-u-16-24">{{$vComposerHelper->viewDatesDisplay($myDeliveries[0]['created_at'])}}</div>
                    </div>
                </div>
                <div class="pure-u-1-3">
                    <div class="pure-g">
                        <div class="pure-u-7-24"><b>Expires:</b></div>
                        <div class="pure-u-16-24">{{ $vComposerHelper->viewDatesDisplay($myDeliveries[0]['expiry']) }}</div>
                    </div>
                </div>
            </div>
        </section>

        <section class="top my_delivery_item">


            @for($i = 0; $i < count($myDeliveries[0]['items']); $i++)
                <div class="item_box_surround">
                    <div class="pure-g">
                        <div class="pure-u-3-24">
                            <div class="my-delivery-item-picture-pocket">
                                {!!Html::image($vComposerHelper->missingImageResolver($myDeliveries[0]['items'][$i]['main_image']),
                        $myDeliveries[0]['subtitle'], array('width'=>'100%'))!!}
                            </div>
                        </div>
                        <div class="pure-u-21-24">
                            <div class="my_delivery_item_row">
                                <div class="section-sub-title">Delivery item {{ ($i+1) }}</div>
                                <div><b>Title:</b></div>
                                <div>{{ $myDeliveries[0]['items'][$i]['name'] }}</div>
                                <div><b>Description:</b></div>
                                <div>{{ isset($myDeliveries[0]['items'][$i]['description'])?$myDeliveries[0]['items'][$i]['description']:"Not provided"  }}</div>
                            </div>

                            <div class="pure-g my_delivery_item_row">
                                <div class="pure-u-1-3">
                                    <div class="pure-g">
                                        <div class="pure-u-7-24"><b>Length:</b></div>
                                        <div class="pure-u-16-24">{{  $vComposerHelper->getUnitFriendlyString($myDeliveries[0]['items'], $i, 'item_length') }} {{  isset($myDeliveries[0]['items'][$i]['measured_width'])?$myDeliveries[0]['items'][$i]['measured_length']:"" }} </div>
                                    </div>
                                </div>
                                <div class="pure-u-1-3">
                                    <div class="pure-g">
                                        <div class="pure-u-7-24"><b>Width:</b></div>
                                        <div class="pure-u-16-24">{{  $vComposerHelper->getUnitFriendlyString($myDeliveries[0]['items'], $i, 'item_width') }} {{  isset($myDeliveries[0]['items'][$i]['measured_width'])?$myDeliveries[0]['items'][$i]['measured_width']:"" }} </div>
                                    </div>
                                </div>
                                <div class="pure-u-1-3">
                                    <div class="pure-g">
                                        <div class="pure-u-7-24"><b>Height:</b></div>
                                        <div class="pure-u-16-24">{{  $vComposerHelper->getUnitFriendlyString($myDeliveries[0]['items'], $i, 'item_height') }} {{  isset($myDeliveries[0]['items'][$i]['measured_width'])?$myDeliveries[0]['items'][$i]['measured_height']:"" }} </div>
                                    </div>
                                </div>
                            </div>

                            <div class="pure-g my_delivery_item_row">
                                <div class="pure-u-1-3">
                                    <div class="pure-g">
                                        <div class="pure-u-7-24"><b>Weight:</b></div>
                                        <div class="pure-u-16-24">{{  $vComposerHelper->getUnitFriendlyString($myDeliveries[0]['items'], $i, 'item_weight') }} {{  isset($myDeliveries[0]['items'][$i]['measured_width'])?$myDeliveries[0]['items'][$i]['measured_weight']:"" }} </div>
                                    </div>
                                </div>
                                <div class="pure-u-1-3">
                                    <div class="pure-g">
                                        <div class="pure-u-7-24"><b></b></div>
                                        <div class="pure-u-16-24"></div>
                                    </div>
                                </div>
                                <div class="pure-u-1-3">
                                    <div class="pure-g">
                                        <div class="pure-u-5-24"><b></b></div>
                                        <div class="pure-u-1-24"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endfor
        </section>

        <section class="top my_delivery_item">
            <div class="my_delivery_item_row">
                <div><b>Quotes on this item:</b></div>
                <div class="pure-g">
                    <div class="pure-u-1-3">Amount quoted</div>
                    <div class="pure-u-1-3">Transport Provider</div>
                    <div class="pure-u-1-3">More Info</div>
                </div>
                @foreach($quotesInfo as $qInfo)
                    <div class="sj_quote_summary_row">
                    <div class="pure-g">
                        <div class="pure-u-1-3"><b>£{{$qInfo['quote']}}</b></div>
                        <div class="pure-u-1-3"><a href="#">{{$qInfo['user_name']}}</a></div>
                        <div class="pure-u-1-3">

                            <div class="pure-g">
                                <div class="pure-u-1-2">More Info</div>
                                <div class="pure-u-1-2">{!! Form::Button('About Quote', array(
                            'class'=>'right view_quote_info pure-button pure-button-primary',
                            'data-quote-id' => $qInfo["message_id"]
                            )) !!}</div>
                            </div>
                        </div>
                    </div>
                    <span class="more_quote_info"></span>
                    </div>
                @endforeach
            </div>
        </section>


        <div class="job_quotes_section">
        </div>
    </div>
    {{-- handlebars templates --}}
    @include('partials.deliveries.moreQuoteInfo')
@stop

@section('google_maps_scripts')
    {{-- {!! Html::script('https://maps.googleapis.com/maps/api/js?key=AIzaSyBTyNgno0aHsgrhtVXwJhhqY8mF_vnsV-Y&callback=initMap') !!} --}}
    {!! Html::script('http://maps.google.com/maps/api/js?sensor=false&callback=initMap') !!}
@stop

@section('scripts')
    <script>
        var coordinates = {
            'from': {
                'lat': '<?php echo $myDeliveries["toAndFromGeometry"]["from"]["location"]["lat"]; ?>',
                'lng': '<?php echo $myDeliveries["toAndFromGeometry"]["from"]["location"]["lng"]; ?>'
            },
            'to': {
                'lat': '<?php echo $myDeliveries["toAndFromGeometry"]["to"]["location"]["lat"]; ?>',
                'lng': '<?php echo $myDeliveries["toAndFromGeometry"]["to"]["location"]["lng"]; ?>'
            }
        };
    </script>

    {!! Html::script('templates/sj_quotes_more_info_message_insert.tpl.js') !!}
    {!! Html::script('templates/sj_quotes_more_info.tpl.js') !!}
    {!! Html::script('js/inline_scripts/singleJob.js') !!}
@stop