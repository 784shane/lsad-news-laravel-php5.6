<?php namespace App\Http\Controllers;

use Carbon\Carbon;
use Validator;
use Redirect;
use Request as Req;
use Auth;
use App\Services\Auth\Signup;
use App\Services\Auth\User;
use App\Services\Auth\Business as BusinessService;
use Mail;

class AuthController extends PublicController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller hallows users to login into the software.
    |
    */
    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard $auth
     * @param  \Illuminate\Contracts\Auth\Registrar $registrar
     *
     * @return void
     */
    public function __construct()//Guard $auth, Registrar $registrar)
    {
        parent::__construct();
        /*$this->auth = $auth;
        $this->registrar = $registrar;

        $this->middleware('guest', ['except' => 'getLogout']);*/
    }

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard $auth
     * @param  \Illuminate\Contracts\Auth\Registrar $registrar
     *
     * @return void
     */
    public function showLogin()
    {
        /*$remember = "vihffndlfdd";
            if (Auth::attempt(['email' => 'vibes@fadmail.com', 'password' => 'password1'], $remember))
        */
        // show the form
        //return View::make('auth.login');

        if (Req::isMethod('post')){

            // process the form
            // validate the info, create rules for the inputs
            $rules = array(
                'email' => 'required|email', // make sure the email is an actual email
                'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Req::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails())
            {
                \Request::flash();
                return Redirect::to('login')
                    ->withErrors($validator); // send back all errors to the login form
                //->withInput(Request::except('password')); // send back the input (not the password) so that we can repopulate the form
            }
            else
            {
                // create our user data for the authentication
                $userdata = $this->buildLoggingInData(
                    Req::get('password'),
                    Req::get('email')
                );
                // attempt to do the login
                if (Auth::attempt($userdata))
                {
                    // validation successful!
                    // redirect them to the secure section or whatever
                    // return Redirect::to('secure');
                    // for now we'll just echo success (even though echoing in a controller is bad)
                    return Redirect::to('/home');
                }
                else
                {
                    // validation not successful, send back to form
                    return Redirect::to('login');
                }
            }

        }

        return view('auth.login');//->with($this->viewData);

    }

    public function doLogin()
    {
        // process the form
        // validate the info, create rules for the inputs
        $rules = array(
            'email' => 'required|email', // make sure the email is an actual email
            'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );
        // run the validation rules on the inputs from the form
        $validator = Validator::make(Req::all(), $rules);
        // if the validator fails, redirect back to the form
        if ($validator->fails())
        {
            \Request::flash();
            return Redirect::to('login')
                ->withErrors($validator); // send back all errors to the login form
            //->withInput(Request::except('password')); // send back the input (not the password) so that we can repopulate the form
        }
        else
        {
            // create our user data for the authentication
            $userdata = $this->buildLoggingInData(
                Req::get('password'),
                Req::get('email')
            );
            // attempt to do the login
            if (Auth::attempt($userdata))
            {
                // validation successful!
                // redirect them to the secure section or whatever
                // return Redirect::to('secure');
                // for now we'll just echo success (even though echoing in a controller is bad)
                return Redirect::to('/home');
            }
            else
            {
                // validation not successful, send back to form
                return Redirect::to('login');
            }
        }
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('login');
    }

    private function buildLoggingInData($password, $email = null, $username = null)
    {
        $userData = array('password' => $password);
        if ($email != null)
        {
            $userData['email'] = $email;
        }
        if ($username != null)
        {
            $userData['username'] = $username;
        }
        return $userData;
    }

    public function forgot()
    {
        if (Req::isMethod('post'))
        {
            $signup = new Signup();
            $processingResults = $signup->processForgotForm();
            if ($processingResults['result'] == "fail")
            {
                return view('auth.forgot')->withErrors($processingResults['validator']);
            }
            //check forgot and render view
            return view('partials.auth.sign_up_confirmation',
                array(
                    'type' => 'forgot_password',
                    'view_data' => $processingResults
                )
            );
        }
        return view('auth.forgot');
    }

    public function signup($signUpType = "")
    {
        //As long as the user arrives on this page, lets log the user out
        //because we don't want to update the logged in user's details.
        //We will call this same form and processes for updating account.
        //Auth::logout();
        $usr = new User();
        $loggedInUser = $usr->getLoggedInUserObject();
        $loggedInEmail = null;
        $loggedInUsername = null;
        if($loggedInUser['result']=="success"){
            $loggedInUsername = $loggedInUser['user_object']['user_name'];
            $loggedInEmail = $loggedInUser['user_object']['email'];
        }
        //See if there was a form submission
        $businessService = new BusinessService();
        $this->viewData['formUrl'] = '/signup/' . $signUpType;
        $this->viewData['loggedInEmail'] = $loggedInEmail;
        $this->viewData['loggedInUsername'] = $loggedInUsername;
        $this->viewData['businessDetails'] = $businessService->getBusinessEmptyDetails();
        if (Req::isMethod('post'))
        {
            $signUpType = $this->testForSignUpType();
            $this->viewData['formUrl'] = '/signup/' . $signUpType;
            switch($signUpType)
            {
                case "transport_providers":
                {
                    $processResult = $this->processBusinessSignUpForm($signUpType, $usr);
                    if ($processResult['result'] == "success")
                    {
                        // All went well. Show success screen
                        $this->viewData['business_registration_complete'] = true;
                        return view('auth.business_signup')->with($this->viewData);
                    }
                    else
                    {
                        // We want to remember this information incase we had an error
                        // Send the user back to the business sign up page with the errors
                        // and data they've inputted.
                        \Request::flash();
                        return redirect(route('business_signup'))
                            ->withErrors($processResult['validatorResults']);
                    }
                    break;
                }
            }
        }
        return view('auth.business_signup')->with($this->viewData);
    }

    private function testForSignUpType()
    {
        if ($sign_up_type = Req::get('sign_up_type'))
        {
            return $sign_up_type;
        }
        return "";
    }

    private function processBusinessSignUpForm($signUpType, $usr)
    {
        //Retrieve the input values.
        $inputs = Req::all();
        $signUp = new Signup();
        // @Todo - We must validate this form
        return $signUp->attemptToRegisterBusiness($inputs);
    }

    /**
     * Validates the business added earlier and allows the user to add
     * it to their account.
     *
     * @param  String $businessValidationToken
     *
     * @return Object View Object
     */
    public function businessValidationComplete($businessValidationToken)
    {
        $signUp = new Signup();
        $validationResults = $signUp->processBusinessValidation($businessValidationToken);
        $this->viewData['viewType'] = $validationResults['msg'];
        return view('business.completevalidation')->with($this->viewData);
    }

    /**
     * Validates the business added earlier and allows the user to add
     * it to their account.
     *
     * @param String $forgotToken
     *
     * @return Object View Object
     */
    public function userResetPassword($forgotToken)
    {
        $signUp = new Signup();
        $this->viewData['user_found'] = true;
        $this->viewData['forgot_token'] = $forgotToken;
        if (Req::isMethod('post'))
        {
            // We will now validate the reset password form.
            // If everything goes well, we will update the password,
            // log out the user then present the log in screen.
            $result = $signUp->attemptPasswordUpdate(Req::all());
            if ($result['result'] == "success")
            {
                // All went well. You can now login
                return view('auth.reset-password-success');
            }
            else
            {
                //Obviously, if validation fails, we will show the errors
                return view('auth.reset-password')->with($this->viewData)->withErrors($result['validatorResults']);
            }
        }
        // We collect the forgot token hash.
        // With this, we will check to see if this hash is associated with
        // an account. If it is, we will present the user with an opportunity
        // to change their password.
        $userFound = $signUp->checkUserExistByForgotHash($forgotToken);
        // the user doesn't exist, we will let the user know.
        if (!$userFound)
        {
            //show the user an error screen. i.e. no user found.
            $this->viewData['user_found'] = false;
            return view('auth.reset-password')->with($this->viewData);
        }
        return view('auth.reset-password')->with($this->viewData);
    }
}
