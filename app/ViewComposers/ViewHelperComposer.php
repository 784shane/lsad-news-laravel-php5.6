<?php
namespace App\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Services\Auth\User;

class ViewHelperComposer
{
    /**
     * The json encoding options.
     *
     * @var int
     */
    protected $loggedInUser = null;

    public function __construct()
    {
        // Getting the logged in user
        $this->setLoggedInUser();
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(array(
            'vComposerHelper' => $this
        ));
    }

    /**
     * Sets the logged in User.
     *
     * @return void
     */
    private function setLoggedInUser()
    {
        $user = new User();
        $this->loggedInUser = $user->getLoggedInUsersId();
    }

    /**
     * Gets the id of the logged in User.
     *
     * @return int
     */
    public function getLoggedInUser()
    {
        return $this->loggedInUser;
    }

    public function getIfElementExists($container, $element)
    {
        return isset($container[$element])?$container[$element]:'';
    }

    public function shortenDeliveriesMessageTexts($txt)
    {
        $your_desired_width = 26;//121;
        $parts = preg_split('/([\s\n\r]+)/', $txt, null, PREG_SPLIT_DELIM_CAPTURE);
        $parts_count = count($parts);
        $length = 0;
        $last_part = 0;
        for(; $last_part < $parts_count; ++$last_part)
        {
            $length += strlen($parts[$last_part]);
            if ($length > $your_desired_width)
            {
                break;
            }
        }
        $shortendedString = implode(array_slice($parts, 0, $last_part));
        if($your_desired_width < strlen($txt)){
            $shortendedString = $shortendedString."...";
        }
        return $shortendedString;
    }

    /**
     * Determines if the sender is the logged in user.
     *
     * @param int $senderId
     *
     * @return boolean
     */
    public function amISpeaker($senderId)
    {
        if ($this->loggedInUser == $senderId)
        {
            return true;
        }
        return false;
    }

    /**
     * Returns a formatted date string.
     *
     * @param string $timeString
     * @param boolean $dateOnly
     *
     * @return string
     */
    public function viewDatesDisplay($timeString, $dateOnly = false)
    {
        return date(!$dateOnly?'d M, Y H:i':'d M, Y', strtotime($timeString));
    }

    /**
     * Returns unit if available or N/A string if not.
     *
     * @param array $array
     * @param int $index
     *
     * @return mixed - int or string
     */
    public function getUnitFriendlyString($array, $index, $unitType)
    {
        if (isset($array[$index][$unitType]))
        {
            return $array[$index][$unitType];
        }
        return "Not provided.";
    }

    /**
     * Checks if the item is set, if not returns empty string.
     *
     * @param string $imgUrl
     *
     * @return string
     */
    public function missingImageResolver($imgUrl)
    {
        if (isset($imgUrl))
        {
            return $imgUrl;
        }
        return "";
    }

    public function jobQuoteCtaType()
    {
        $authUser = new User();
        if ($authUser->isUserLoggedIn())
        {
            //does this user have a business?
            $userObject = $authUser->getLoggedInUserObject();
            if ($userObject['user_object']['transport_company'])
            {
                return 'with_transport_company';
            }else
            {
                return 'no_transport_company';
            }
        }
        return 'no_user';
    }

    public function getReadableMileage($distance_metres)
    {
        $distance = $this->getDistance($distance_metres);
        if (!empty($distance))
        {
            return number_format($distance, 2) . " miles";
        }
        return "";
    }

    private function getDistance($distance_metres)
    {
        if ($distance_metres)
        {
            return $this->convertMToMiles($distance_metres);
        }
        return "";
    }

    private function convertMToMiles($distance_metres)
    {
        return ($distance_metres * 0.00062137);
    }

    public function getUnitsSelect($unitsMeasurementsArray, $unit, $selectBoxName)
    {
        if (isset($unitsMeasurementsArray[$unit]))
        {
            return \Form::select($selectBoxName, $unitsMeasurementsArray[$unit]['units']);

        }
        return null;
    }

    public function isDeliveryEditable($deliveryStatus){
        $privateStatus = \Config::get('constants.deliveries.statuses.ACTIVE_PRIVATE');
        if($deliveryStatus < $privateStatus){
            return true;
        }
        return false;
    }

    public function getFriendlyUrlTitle($title){
        if($title != ""){
            return str_replace(" ","-",substr($title,0,100));
        }
        return 'delivery-title';
    }

    public function getLocalCurrencyFormat($money){
        return FrontendHelpers::convertToCurrencyFormat($money);
    }
}