<?php namespace App\Services\Auth;

use App\User as UserModel;
use App\Business as BusinessModel;
use App\Services\Auth\User as AuthUser;
use Request;
use Validator;

class Signup extends SignUpProcesses
{
    /*
    * @var class self
    */
    private static $_instance = null;

    /**
     * @name getInstance
     * Class is constructed once. This function provides the same _instance of
     * the class at all times.
     * @return self
     **/
    public static function getInstance()
    {
        if (null === self::$_instance)
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Checks to see if we have inputted the correct details into
     * the sign up form.
     *
     * @return Boolean
     **/
    public function addNewUser($userData)
    {
        return UserModel::create($userData);
    }

    /**
     * Checks to see if we have inputted the correct details into
     * the sign up form.
     *
     * @return Boolean
     **/
    public function addNewBusiness($businessData)
    {
        return BusinessModel::create($businessData);
    }

    /**
     * Checks to see if we have inputted the correct details into
     * the sign up form.
     *
     * @return Boolean
     **/
    public function updateExistingBusiness($where, $whereValue, $businessData)
    {
        $businessModel = new BusinessModel();
        return $businessModel->updateBusiness($where, $whereValue, $businessData);
    }

    public function processForgotForm()
    {
        $result = array('user_found' => '0', 'result' => 'success');
        //Get the email entered
        $emailEntered = Request::get('email');
        //Lets validate this email. make sure it is an email.
        $validateResults = $this->validateForgotEmail($emailEntered);
        if ($validateResults['result'] == "fail")
        {
            return $validateResults;
        }
        //Now check to see if this email exists in the database.
        $userCount = UserModel::where('email', '=', $emailEntered)->count();
        if ($userCount > 0)
        {
            // Now if we get more than zero result, we will send this user
            // an email so that the user can update their password after
            // checking and clicking in their em ail.
            $returnedUser = UserModel::where('email', '=', $emailEntered)->first()->toArray();
            // What's needed here - The token sent to the user by email must be saved to the
            // database. When this token is retrieved upon the user returning to the site,
            // we will check if the token exists. If the token exists, we will show the user
            // a screen where he/she may update their password. Only when that password is updated,
            // will we remove this token or if a new token and email is sent out.
            $forgot_token = $this->buildToken($emailEntered);
            // 1. save the forgot token now.
            $this->setUserForgotToken($forgot_token, $returnedUser);
            $emailData = array('forgot_token' => $forgot_token);
            //Send the email.
            $this->sendConfirmationEmail($returnedUser, $emailData, 'forgot_password');
            $result['user_found'] = '1';
            $result['result'] = 'success';
        }
        return $result;
    }

    /**
     * we will attempt to register a business to a user
     * In some cases, no user may exist. So in this case, we will
     * have to set it up so that the user will be added after
     * email confirmation.
     *
     * @param Array $inputs
     *
     * @return Mixed array or boolean
     **/
    public function registerBusinessAfterValidating($inputs)
    {
        $usrModel = new UserModel();
        $usr = new AuthUser();
        $userId = null;
        $userExists = $usrModel->getUserByEmail(trim($inputs['company_email']));
        //If the user is logged in
        if ($usr->isUserLoggedIn())
        {
            //scenario 1 - If email are the same
            $loggedInUsersId = $usr->getLoggedInUsersId();
            //set this user as a transport provider.
            $userSetAsTransportProvider =
                $usr->setuserAsTransportProvider($loggedInUsersId, true);
            if ($usr->getLoggedInUsersEmail() === strtolower(trim($inputs['company_email'])))
            {
                //ADD business and align it to this user
                $usr->toggleTransportProviderVerified($loggedInUsersId, true);
                return $this->addBusinessToUser($inputs, $loggedInUsersId, true);
            }
            else
            {
                //scenario 2 - If the emails are not the same
                $usr->toggleTransportProviderVerified($loggedInUsersId, false);
                return $this->addBusinessToUser($inputs, $loggedInUsersId, false);
                //NB: let the user click on link in email.
            }
        }
        else
        {
            //If they are not logged in
            if ($userExists)
            {
                //scenario 3 - If not logged in and the email already exist
                //set this user as a transport provider.
                $userSetAsTransportProvider =
                    $usr->setuserAsTransportProvider($userExists[0]->id, true);
                //Add the business still to the existing user
                //but the user must verify their email
                $usr->toggleTransportProviderVerified($userExists[0]->id, false);
                return $this->addBusinessToUser($inputs, $userExists[0]->id, false);
            }
            else
            {
                //scenario 4 - If not logged in and the email doesn't exist
                //Ad the business and add the user a user as well.
                //And of-course the user must verify their email
                return $this->addTotallyNewBusinessAndUser($inputs);
            }
        }
    }

    /**
     * Validates the registration form then pass it on to registration.
     *
     * @param Array $inputs
     *
     * @return Mixed array or boolean
     **/
    public function attemptToRegisterBusiness($inputs)
    {
        $result = ['result' => 'fail'];
        $validationResults = $this->businessSignUpFormValidator($inputs);
        if ($validationResults->fails())
        {
            $result['validatorResults'] = $validationResults;
            $result['error_messages'] = $validationResults->messages();
        }
        else
        {
            $businessInsertionResult = $this->registerBusinessAfterValidating($inputs);
            if ($businessInsertionResult)
            {
                $result['result'] = 'success';
            }
        }
        return $result;
    }

    /**
     * Construct from the array, the business data to be inputted into the database.
     *
     * @param Array $inputs
     *
     * @return Array
     **/
    private function getBusinessOnlyData($inputs)
    {
        return [
            'company_user_name' => $inputs['company_user_name'],
            'company_name' => $inputs['company_name'],
            'company_phone_number' => $inputs['company_phone_number'],
            'company_mobile_number' => $inputs['company_mobile_number'],
            'company_address_1' => $inputs['company_address_1'],
            'company_address_2' => $inputs['company_address_2'],
            'company_city' => $inputs['company_city'],
            'company_email' => $inputs['company_email'],
            'company_postcode' => $inputs['company_postcode']
        ];
    }

    /**
     * Construct from the array, the user data to be inputted into the database.
     *
     * @param Array $inputs
     *
     * @return Array
     **/
    private function getUserOnlyData($inputs)
    {
        foreach($inputs as $elementKey => $element)
        {
            if (!in_array($elementKey, self::$userFieldsToKeep))
            {
                //remove unnecessary fields
                unset($inputs[$elementKey]);
            }
        }
        return $inputs;
    }

    /**
     * From the form post, we will get the personal details.
     * return Boolean
     **/
    private function addBusinessToUser($inputs, $userId = null, $verify = false)
    {
        $businessAdded = false;
        // $verify = false means that email-verified in business table
        // will be true only when user clicks on link in his email
        // - the company email provided.
        $businessModel = new \App\Business();
        $businesses = $businessModel->getBusinessForUser($userId);
        //make sure the user id is added to the data.
        $data = $this->getBusinessOnlyData($inputs);
        $data['user'] = $userId;
        $data['verified'] = $verify;
        $newBusinessId = null;
        if (is_null($businesses))
        {
            //create new business
            $businessAdded = $this->addNewBusiness($data);
            $newBusinessId = isset($businessAdded['id'])?$businessAdded['id']:null;
        }
        else
        {
            //update the business
            $newBusinessId = $businesses[0]->id;
            $businessAdded = $this->updateExistingBusiness('id', $businesses[0]->id, $data);
        }
        if (!$verify && $businessAdded)
        {
            //We will send an email out to the user.
            //User must click on the link to complete the
            //verification process
            $businessValidationToken = $this->prepareBusinessValidationToken($data['company_email']);
            $this->setBusinessValidationToken($newBusinessId, $businessValidationToken);
            $this->sendBusinessVerificationRequest($businessValidationToken);
        }
        return $businessAdded;
    }

    /**
     * From the form post, we will get the personal details.
     * return Boolean
     **/
    private function addTotallyNewBusinessAndUser($inputs)
    {
        $usr = new AuthUser();
        $businessData = $this->getBusinessOnlyData($inputs);
        //Lets create the necessary user data information.
        $userData = [];
        $userData['user_name'] = $businessData['company_user_name'];
        $userData['email'] = $businessData['company_email'];
        //We need to create a fake user for this user.
        $userData['password'] = $usr->buildPasswordHash($usr->buildPasswordRaw());
        //Lets add the user to our database.
        $userData['register_token'] = $this->buildToken($businessData['company_email']);
        $n_usr = $this->addNewUser($userData);
        if (isset($n_usr['id']))
        {
            $businessData['user'] = $n_usr['id'];
            $businessData['verified'] = false;
            $businessAdded = $this->addNewBusiness($businessData);
            $newBusinessId = isset($businessAdded['id'])?$businessAdded['id']:null;
            //We will send an email out to the user.
            //User must click on the link to complete the
            //verification process
            $businessValidationToken = $this->prepareBusinessValidationToken($businessData['company_email']);
            $this->setBusinessValidationToken($newBusinessId, $businessValidationToken);
            $this->sendBusinessVerificationRequest($businessValidationToken);
            return true;
        }
        return false;
    }

    /**
     * Checks to see if we have inputted the correct details into
     * the sign up form.
     * return Boolean
     **/
    public function attemptToRegister($signuptype, $inputs)
    {
        $validationReturn = [];
        $validatorResults = $this->signUpValidator($inputs);
        if ($validatorResults->fails())
        {
            $validationReturn['result'] = 'fail';
            $validationReturn['error_messages'] = $validatorResults->messages();
        }
        else
        {
            $validationReturn['result'] = 'success';
        }
        return $validationReturn;
    }

    private function prepareBusinessValidationToken($email)
    {
        return md5(time() . $email);
    }

    private function setUserForgotToken($forgot_token, $returnedUser)
    {
        $user = UserModel::find($returnedUser['id']);
        $user->forgot_token = $forgot_token;
        $user->save();
    }

    private function setBusinessValidationToken($newBusinessId, $businessValidationToken)
    {
        //save the validation token.
        $business = \App\Business::find($newBusinessId);
        $business->validation_token = $businessValidationToken;
        return $business->save();
    }

    public function processBusinessValidation($businessValidationToken)
    {
        $result = ['result' => 'success'];
        $result['msg'] = 'business_validated';
        //First we need to find out if this token is valid
        //if businesses are comnected to it
        //and get the user that is....
        $businessModel = new BusinessModel();
        $businesses = $businessModel->findBusinessesByToken($businessValidationToken);
        if (is_null($businesses))
        {
            $result['result'] = 'fail';
            $result['msg'] = 'no_token_match';
        }
        else
        {
            $businessId = $businesses[0]['id'];
            $userId = $businesses[0]['user'];
            $user = UserModel::find($userId);
            if (is_null($user))
            {
                $result['result'] = 'fail';
                $result['msg'] = 'business_matches_no_user';
            }
            else
            {
                //All good we have a business which matches
                //the user.
                //1. Therefore we must mark that business as verified.
                $busN = $businessModel::find($businessId);
                // We must change this validation token. I suggest changing it
                // by appending the now time on the business email, then create the hash
                // That way, it will still be unique.
                $busN->validation_token = $this->prepareBusinessValidationToken($businesses[0]['company_email'] . \time());
                $busN->verified = true;
                $busN->save();
                //2. Therefore we must mark that user as transport_company_verfied.
                $user->transport_company_verified = true;
                $user->transport_company = true;
                $user->save();
            }
        }
        return $result;
    }

    public function processEditedBusiness()
    {
        $inputs = Request::all();
        $result = ['result' => 'fail'];
        $validationResults = $this->businessSignUpFormValidator($inputs);
        if ($validationResults->fails())
        {
            $result['error_messages'] = $validationResults->messages();
        }
        else
        {
            if (isset($inputs['company_id']))
            {
                $businessModel = new BusinessModel();
                $updated = $businessModel->updateBusinessById((int)$inputs['company_id'], $inputs);
                if ($updated)
                {
                    $result['result'] = 'success';
                }
            }
        }
        return $result;
    }

    public function processEditedPerson($userId)
    {
        $inputs = Request::all();
        $validationReturn = ['result' => 'fail'];
        $validationResults = $this->editUserValidator($inputs);
        if ($validationResults->fails())
        {
            $validationReturn['validatorResults'] = $validationResults;
            $validationReturn['error_messages'] = $validationResults->messages();
        }
        else
        {
            $authUser = new AuthUser();
            //remove unnecessary input fields
            $dataToUpdate = $this->getUserOnlyData($inputs);
            //if password is blank we do not want it updated.
            if ($dataToUpdate['password'] === "")
            {
                unset($dataToUpdate['password']);
            }
            if (isset($dataToUpdate['password']))
            {
                //Hash the password
                $dataToUpdate['password'] = $authUser->buildPasswordHash($dataToUpdate['password']);
            }
            //Update the existing user
            $updated = $authUser->updateExistingUser('id', $userId, $dataToUpdate);
            if ($updated)
            {
                $validationReturn['result'] = 'success';
            }
        }
        return $validationReturn;
    }

    /**
     * Checks to see a user exists with this forgot_password hash.
     *
     * @param Array $resetInputs
     *
     * @return Array
     **/
    public function attemptPasswordUpdate($resetInputs)
    {
        $validationReturn = ['result' => 'fail'];
        $validationResults = $this->passwordResetValidator($resetInputs);
        if ($validationResults->fails())
        {
            $validationReturn['validatorResults'] = $validationResults;
            $validationReturn['error_messages'] = $validationResults->messages();
        }
        else
        {
            if (isset($resetInputs['forgot_token']))
            {
                //Get the user
                $usrModel = new UserModel();
                $user = $usrModel->getUserByForgotPasswordHash($resetInputs['forgot_token']);
                //Update the user's password and change the forgot_token hash.
                $authUser = new AuthUser();
                $newPassword = $authUser->buildPasswordHash($resetInputs['password']);
                $forgotTokenHash = $this->buildToken($resetInputs['forgot_token']);
                $updatingData = [
                    'password' => $newPassword,
                    'forgot_token' => $forgotTokenHash
                ];
                $updated = $authUser->updateExistingUser('id', $user['id'], $updatingData);
                if ($updated)
                {
                    $validationReturn['result'] = 'success';
                }
            }
        }
        return $validationReturn;
    }

    /**
     * Checks to see a user exists with this forgot_password hash.
     *
     * @param String $forgotToken
     *
     * @return Boolean
     **/
    public function checkUserExistByForgotHash($forgotToken)
    {
        $usrModel = new UserModel();
        $user = $usrModel->getUserByForgotPasswordHash($forgotToken);
        if ($user)
        {
            return true;
        }
        return false;
    }
}