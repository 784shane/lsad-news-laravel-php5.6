@extends($account_page_layout)
@section('content')
    {!! Form::open(array(
                'class'=>"pure-form pure-form-stacked")) !!}

    <fieldset>
        <div>

            <div>
                <div class="site-secondary-menu pure-menu pure-menu-horizontal">
                    <ul class="pure-menu-list">
                        <li class="pure-menu-item">
                            {!! Html::link('my-account','Edit Personal information', array('class'=>"pure-menu-link")) !!}</li>
                        <li class="pure-menu-item">{!! Html::link('my-account/business','Edit Business Details', array('class'=>"pure-menu-link")) !!}</li>
                    </ul>
                </div>
            </div>
            <div>
                @if($accountType == "personal")
                    @include('partials.business.personal_signup')
                @else
                    @include('partials.business.business_signup')
                @endif
            </div>
        </div>


        <!-- The form token. -->
        {!!  Form::token() !!}

        <div>
            {!! Form::submit('Send', array('class'=>'pure-button pure-button-primary narrow-submit-button')) !!}
        </div>

    </fieldset>
    {!! Form::close() !!}
@stop