@extends($layout)
@section('content')

    @if(isset($quote_status) && $quote_status == "complete")

        <section>
            <div class="delivery-list-header">Quote results</div>
            <p><b>You have made a quote. Thank you!</b></p>

            <div class="pure-g">
                <div class="pure-u-4-24">Amount quoted:</div>
                <div class="pure-u-20-24">{{ $quoted_info['job_quote_amt'] }}</div>
                <div class="pure-u-4-24">Last Message sent:</div>
                <div class="pure-u-20-24">{{ $quoted_info['quote_message'] }}</div>
            </div>
        </section>

        <section>
            <div class="delivery-list-header">Delivery details</div>
            <div>{!! Html::image($vComposerHelper->missingImageResolver($myDeliveries[0]['overallImage']), null, array('width'=>'40','height'=>'40')) !!}</div>
            <div><b>Title of delivery</b></div>
            <div>{{$myDeliveries[0]['subtitle']}}</div>
            <div><b>From</b></div>
            <div>{{$myDeliveries[0]['collection_address']}}</div>
            <div><b>To</b></div>
            <div>{{$myDeliveries[0]['delivery_address']}}</div>
            <div><b>Date posted</b></div>
            <div>{{ $vComposerHelper->viewDatesDisplay($myDeliveries[0]['updated_at'], true)  }}</div>
        </section>
    @else

        <h1>Make a quote</h1>

        @if(isset($quote_recalculated))
            <div style="background-color:#f0f0f0; padding:30px;">
                <h3>We have added our fee to your quote. Take a look.</h3>
                <div class="quote-recalculation-box">
                    <div class="pure-g">
                        <div class="pure-u-8-24">Your fee -</div>
                        <div class="pure-u-1-24" id="your_fee">{{ $your_fee }}</div>
                    </div>
                    <div class="pure-g">
                        <div class="pure-u-8-24">Our fee -</div>
                        <div class="pure-u-1-24" id="our_fee">{{ $our_fee }}</div>
                    </div>
                    <div class="pure-g">
                        <div class="pure-u-8-24">Quote to be submitted -</div>
                        <div class="pure-u-1-24 calculated-amount" id="our_quote">{{ $our_quote }}</div>
                    </div>
                </div>
                <div>
                    <h4>Your message</h4>
                    <div>{{ isset($myLatestMessage)?$myLatestMessage:""  }}</div>
                </div>

                <h3>If everything is acceptable, go ahead and agree and submit your quote.</h3>
                {!! Form::open(array('url'=>\route('deliveries.saveQuote', ['jobName' => $vComposerHelper->getFriendlyUrlTitle($myDeliveries[0]['subtitle']), 'jobId'=>$myDeliveries[0]['id']]),
                                   'id'=>"quote_job_form",
                                   'class'=>"pure-form pure-form-stacked")) !!}
                {!! Form::submit('Agree and Send Quote', array('class'=>'pure-button pure-button-primary narrow-submit-button')) !!}
                {!! Form::hidden('job_quote_amt',$our_quote) !!}
                {!! Form::hidden('saved_hash',$saved_hash) !!}
                {!! Form::hidden('quote_message',$myLatestMessage) !!}
                {!! Form::hidden('your_fee',$your_fee) !!}
                {!! Form::hidden('our_fee',$our_fee) !!}
                {!! Form::hidden('our_quote',$our_quote) !!}
                {!! Form::close() !!}

                {!! Form::open(array('url'=>\route('deliveries.quotes', ['jobName' => $vComposerHelper->getFriendlyUrlTitle($myDeliveries[0]['subtitle']), 'jobId'=>$myDeliveries[0]['id']]),
                                   'id'=>"quote_job_form",
                                   'class'=>"pure-form pure-form-stacked")) !!}
                {!! Form::submit('Cancel and redo quote', array('class'=>'pure-button button-error narrow-submit-button')) !!}
                {!! Form::hidden('quote_message',$myLatestMessage) !!}
                {!! Form::hidden('your_fee',$your_fee) !!}
                {!! Form::hidden('our_fee',$our_fee) !!}
                {!! Form::hidden('our_quote',$our_quote) !!}
                {!! Form::close() !!}
            </div>

        @else

            {!! Form::open(array('url'=>\route('deliveries.preQuote', ['jobName' => $vComposerHelper->getFriendlyUrlTitle($myDeliveries[0]['subtitle']), 'jobId'=>$myDeliveries[0]['id']]),
                               'id'=>"quote_job_form",
                               'class'=>"pure-form pure-form-stacked")) !!}

            <fieldset>
                <div>
                    <p>
                        {!! $errors->first('job_quote_amt') !!}
                        {!! $errors->first('quote_message') !!}
                    </p>
                </div>

                <div>
                    {!! Form::label('job_quote_amt', 'Quote Amount') !!}
                    {!! Form::text('job_quote_amt', isset($myLatestQuote)?$myLatestQuote:'', array('required' => 'required' )) !!}
                </div>

                <div>
                    {!! Form::label('quote_message', 'Add a message') !!}
                    {!! Form::textarea('quote_message', isset($myLatestMessage)?$myLatestMessage:"") !!}
                </div>

            </fieldset>
            {!! Form::submit('Agree and Send Quote', array('class'=>'pure-button pure-button-primary narrow-submit-button')) !!}
            {!! Form::close() !!}

        @endif

        <div>
            <h3>You are about to quote for the following job:</h3>
        </div>

        <div>
            <section class="top my_delivery_item">
                <a class="main_link">{{$myDeliveries[0]['subtitle']}}</a>
                <div class="pure-g my-delivery-items-details">
                    <div class="pure-u-1-5">
                        <div class="my_delivery_item_left_insert">
                            {!!Html::image($vComposerHelper->missingImageResolver($myDeliveries[0]['overallImage']),
                            $myDeliveries[0]['subtitle'], array('width'=>'100%'))!!}
                        </div>
                    </div>
                    <div class="pure-u-4-5">


                        <div class="pure-g">
                            <div class="pure-u-1-2">
                                <div><b>Title</b></div>
                                <div>{{$myDeliveries[0]['subtitle']}}</div>
                            </div>
                            <div class="pure-u-1-2">
                                <div><b>Number of Quotes</b></div>
                                <div>{{ $myDeliveries[0]['number_of_quotes'] }}</div>
                            </div>
                        </div>


                        <div class="pure-g">
                            <div class="pure-u-1-2">
                                <div><b>move from:</b></div>
                                <div>{{$myDeliveries[0]['collection_address']}}</div>
                            </div>
                            <div class="pure-u-1-2">
                                <div><b>Distance</b></div>
                                <div>{{ $vComposerHelper->getReadableMileage($myDeliveries[0]['distance_metres'])  }}</div>
                            </div>
                        </div>


                        <div class="pure-g">
                            <div class="pure-u-1-2">
                                <div><b>move to:</b></div>
                                <div>{{$myDeliveries[0]['delivery_address']}}</div>
                            </div>
                            <div class="pure-u-1-2">
                                <div><b>Distance</b></div>
                                <div>{{ $vComposerHelper->getReadableMileage($myDeliveries[0]['distance_metres'])  }}</div>
                            </div>
                        </div>


                    </div>
                </div>
            </section>
        </div>

        <script id="quoteMakerTemplate" type="text/x-handlebars-template">

        </script>

    @endif
@stop

@section('scripts')
    {!! Html::script('js/inline_scripts/quoteJob.js') !!}
@stop