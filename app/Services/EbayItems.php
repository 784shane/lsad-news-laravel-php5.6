<?php namespace App\Services;

use Config;
use App\Services\CurlRequests;

class EbayItems extends CurlRequests {

	const EBAY_CALL_ENDPOINT = 'http://svcs.ebay.com/services/search/FindingService/v1';

	const EBAY_API_VERSION = '1.0.0';

	public function getItemById($ebayItemId = null){
		return $this->findItemOnEbaySites($ebayItemId);
	}

	private function getEbayCallUrl(){
		return self::EBAY_CALL_ENDPOINT
			.'?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION='
			.self::EBAY_API_VERSION
			.'&SECURITY-APPNAME='
			.$this->getEbayAppId()
			.'&GLOBAL-ID=%s'
			.'&RESPONSE-DATA-FORMAT=JSON'
			.'&paginationInput.entriesPerPage=1'
			.'&keywords=%s';
	}

	private function getEbayAppId(){
		$appId = Config::get('local.ebay.app_id');
		return $appId?$appId:"";
	}

	private function getEbaySites(){
		$globalIds = Config::get('local.ebay.global_ids');
		return $globalIds?$globalIds:[];
	}

	private function findItemOnEbaySites($ebayItemId){

		$reponse = (object)[];
		$success = false;
		$callUrl = $this->getEbayCallUrl();
		foreach($this->getEbaySites() as $ebaySite){
			$ebayResults = $this->getEbayItem(sprintf($callUrl, $ebaySite, $ebayItemId));
			if($ebayResults['status']=="success"){
				$success = true;
				$reponse = $ebayResults['response'];
				break;
			}
		}
		return $reponse;
	}
}