<?php namespace App\Services;

class Units
{
    public function getUnitChoices(){
        $unitsOfMeasurement = \Config::get('local.unitsOfMeasurement');
        // We now have the config of measurements the site is using. Lets now check the default.
        if($unitsOfMeasurement){
            return $this->createUnitSelectElement($unitsOfMeasurement[$unitsOfMeasurement['default']]);
            return $unitsOfMeasurement[$unitsOfMeasurement['default']];
        }
    }

    public function createUnitSelectElement($unitsOfMeasurements){
        $arrays = [];
        foreach($unitsOfMeasurements as $unitsOfMeasurementKey1 => $unitsOfMeasurement){
            $array=[$unitsOfMeasurementKey1];
            foreach($unitsOfMeasurement as $unitsOfMeasurementKey => $singleUnitOfMeasurement){
                $array['units'][$unitsOfMeasurementKey] = $singleUnitOfMeasurement['short_name'];
            }
            $arrays[$unitsOfMeasurementKey1] = $array;
        }
        return $arrays;
    }
}