@extends($layout)
@section('content')

{{--
https://demo.paypal.com/gb/demo/navigation?device=desktop
https://developer.paypal.com/docs/classic/express-checkout/gs_expresscheckout/
https://www.sandbox.paypal.com/signin/?country.x=GB&locale.x=en_GB

https://www.paypal-techsupport.com/
--}}

        <!--
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="MA75KQ6ZKM5VU">
        <input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online.">
        <img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
    </form>
    -->
<div xmlns="http://www.w3.org/1999/html">
    <h1>Quote accepted</h1>
    <h4>Thank you for accepting this quote of {{ $vComposerHelper->getLocalCurrencyFormat($quote[0]['quote']) }}</h4>
    <div class="pure-g accepted-quote-details-surround">
        <div class="pure-u-1-2">
            <div class="description-box description-box-left">
                <div class="description-box-inner">
                    <h4>Quote details</h4>
                    <div class="particulars">
                        <div class="picture"></div>
                        <div><b>Amount quoted</b></div>
                        <div>{{ $vComposerHelper->getLocalCurrencyFormat($quote[0]['quote']) }}</div>
                        <div><b>Transport provider</b></div>
                        <div>{{ $quote[0]['user_name'] }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pure-u-1-2">
            @foreach($delivery as $theDelivery)
                <div class="description-box description-box-right">
                    <div class="description-box-inner">
                        <h4>Delivery details</h4>
                        <div class="particulars">
                            <div class="picture">{!! Html::image($vComposerHelper->missingImageResolver($theDelivery['overallImage']), null, array('width'=>'100%')) !!}</div>
                            <div><b>Title</b></div>
                            <div>{{ $theDelivery['subtitle'] }}</div>
                            <div><b>From:</b></div>
                            <div>{{ $theDelivery['collection_address'] }}</div>
                            <div><b>To:</b></div>
                            <div>{{ $theDelivery['delivery_address'] }}</div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>


    </div>

    <div class="pure-g">
        <div class="pure-u-15-24">
            <div>
                <div>
                    <span class="pay-now-instructions"><p>Upon payment, we will provide you the contact details of your transport provider - <span
                                    class="pay-now-tp-username">{{ $quote[0]['user_name'] }}</span>
                        </p></span>
                    <h3>To pay a deposit of <span class="header-deposit">{{ $vComposerHelper->getLocalCurrencyFormat($quote[0]['our_fee'])  }}</span> now! Choose one of the following
                        options</h3>
                </div>
                <div><h4>Payment options</h4></div>
                <div>
                    {!! Form::radio('payment_choice', null, null, array('id'=>'payment_by_paypal')) !!}
                    {!! Form::label('payment_by_paypal', 'Pay by Payal') !!}
                </div>

            <span id="paypal_choice_pocket">
                <div><p>Log into your paypal account to complete the transaction.</p></div>
                <div>
                    {!! Form::open(array('url' => '/quotes/process-quote-acceptance/'.$quote[0]['quote_id'], 'class'=>"pure-form pure-form-aligned", 'id'=>'express_checkout_paypal_form')) !!}
                    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/x-click-but6.gif"
                           alt="PayPal Click Here to Pay"/>
                    {!! Form::close() !!}
                </div>
            </span>

                <div>
                    {!! Form::radio('payment_choice', null, null, array('id'=>'payment_by_cc')) !!}
                    {!! Form::label('payment_by_cc', 'Pay by Debit / Credit card') !!}
                </div>

            <span id="cc_choice_pocket" style="display:none;">
                <div class="cc_surround">
                    <div><p>Enter your credit card details to complete the transaction.</p></div>
                    {!! Form::open(array('url' => '/paid-by-credit-card', 'class'=>"pure-form pure-form-aligned")) !!}
                    <fieldset>
                        <div class="cc_image_representation">
                            <!-- PayPal Logo -->
                            <a href="https://www.paypal.com/uk/webapps/mpp/paypal-popup" title="How PayPal Works"
                               onclick="javascript:window.open('https://www.paypal.com/uk/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img
                                        src="https://www.paypalobjects.com/webstatic/mktg/Logo/AM_SbyPP_mc_vs_ms_ae_UK.png"
                                        width="100%"/></a>
                            <!-- PayPal Logo -->
                        </div>
                        <div class="pure-g">
                            <div class="pure-u-8-24">{!! Form::label('card_type', 'Card type') !!}</div>
                            <div class="pure-u-16-24">{!! Form::select('card_type', array( 'select_card'=>'Select Card', 'MasterCard'=>'MasterCard/Eurocard', 'Visa'=>'Visa/Delta/Electron', 'Discover'=>'Discover', 'Amex'=>'American Express', 'Maestro'=>'Maestro'), 'null') !!}</div>
                        </div>

                        <div class="pure-g">
                            <div class="pure-u-8-24">{!! Form::label('card_number', 'Card number') !!}</div>
                            <div class="pure-u-16-24">{!! Form::text('card_number','4137352300903856') !!}</div>
                        </div>
                        <div class="pure-g">
                            <div class="pure-u-8-24">{!! Form::label('xxx', 'Expiry date') !!}</div>
                            <div class="pure-u-16-24">
                                <div class="pure-g">
                                    <div class="pure-u-1-2">
                                        <div class="cc_expiry_box">
                                            <div class="cc_expiry_box_m">
                                                <div class="cc_expiry_box_inputs_surround">
                                                    <div>{!! Form::label('card_exp_date_month', 'M') !!}</div>
                                                    <div>{!! Form::text('card_exp_date_month','04') !!}</div>
                                                </div>
                                            </div>
                                            <div class="cc_expiry_box_slash">/</div>
                                            <div class="cc_expiry_box_y">
                                                <div class="cc_expiry_box_inputs_surround">
                                                    <div>{!! Form::label('card_exp_date_year', 'Y') !!}</div>
                                                    <div>{!! Form::text('card_exp_date_year','2021') !!}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pure-u-1-2">
                                        <div class="cc_csc_container">
                                            <div class="pure-g">
                                                <div class="pure-u-8-24">{!! Form::label('card_csc', 'CSC') !!}</div>
                                                <div class="pure-u-16-24">{!! Form::text('card_csc','434') !!}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pure-g">
                            <div class="pure-u-8-24">{!! Form::label('card_first_name', 'First name') !!}</div>
                            <div class="pure-u-16-24">{!! Form::text('card_first_name','test') !!}</div>
                        </div>
                        <div class="pure-g">
                            <div class="pure-u-8-24">{!! Form::label('card_last_name', 'Last name') !!}</div>
                            <div class="pure-u-16-24">{!! Form::text('card_last_name','buyer') !!}</div>
                        </div>
                        <div class="pure-g">
                            <div class="pure-u-8-24">{!! Form::label('card_billing_address_1', 'Billing address Line 1') !!}</div>
                            <div class="pure-u-16-24">{!! Form::text('card_billing_address_1','1 Main Terrace') !!}</div>
                        </div>
                        <div class="pure-g">
                            <div class="pure-u-8-24">{!! Form::label('card_billing_address_2', 'Billing address Line 2') !!}</div>
                            <div class="pure-u-16-24">{!! Form::text('card_billing_address_2','Wolverhampton, West Midlands') !!}</div>
                        </div>
                        <div class="pure-g">
                            <div class="pure-u-8-24">{!! Form::label('card_zip_code', 'Zip code') !!}</div>
                            <div class="pure-u-16-24">{!! Form::text('card_zip_code','W12 4LQ') !!}</div>
                        </div>
                        <div class="pure-g">
                            <div class="pure-u-8-24">{!! Form::label('card_city', 'City') !!}</div>
                            <div class="pure-u-16-24">{!! Form::text('card_city','Wolverhampton') !!}</div>
                        </div>
                        <div class="pure-g">
                            <div class="pure-u-8-24">{!! Form::label('card_state', 'State') !!}</div>
                            <div class="pure-u-16-24">{!! Form::text('card_state') !!}</div>
                        </div>
                        <div class="pure-g">
                            <div class="pure-u-8-24">{!! Form::label('card_country', 'Country') !!}</div>
                            <div class="pure-u-16-24">{!! Form::text('card_country','United Kingdom') !!}</div>
                        </div>
                        <div class="pure-g">
                            <div class="pure-u-8-24">{!! Form::label('card_phone', 'Phone') !!}</div>
                            <div class="pure-u-16-24">{!! Form::text('card_phone','0356933289') !!}</div>
                        </div>
                        <div class="pure-g">
                            <div class="pure-u-8-24">{!! Form::label('card_email_address', 'Email address') !!}</div>
                            <div class="pure-u-16-24">{!! Form::text('card_email_address','vibes-buyer@fadmail.com') !!}</div>
                        </div>
                        <button type="submit" class="pure-button pure-button-primary">Pay now</button>
                    </fieldset>

                    {!! Form::hidden('quote_id',$quote[0]['quote_id']) !!}

                    {!! Form::close() !!}
                </div>

            </span>


            </div>
        </div>

        <div class="pure-u-9-24">
            <div class="shopping-basket-surround">
                <div class="shopping-basket-title">Checkout Details</div>

                <div class="shopping-basket-row">
                    <div class="basket-item-name"><p>Deposit to be paid now</p></div>
                    <div class="basket-amount">{{ $vComposerHelper->getLocalCurrencyFormat($quote[0]['our_fee'])  }}</div>
                </div>

                <div class="shopping-basket-row">
                    <div class="basket-item-name">
                        <p>Amount to be paid directly to the Transport provider</p>
                        <div>
                            <span style="font-size:120%; color:#fff; font-weight:bold;">{{ $vComposerHelper->getLocalCurrencyFormat(($quote[0]['quote'])-($quote[0]['our_fee'])) }}</span><span
                                    style="font-size:80%;"> (not collected now)</span></div>
                    </div>
                    <div class="basket-amount"></div>
                </div>

                <div class="shopping-basket-row">
                    <div class="basket-item-name basket-total-text"><p>Total payable now</p></div>
                    <div class="basket-amount basket-total">{{ $vComposerHelper->getLocalCurrencyFormat($quote[0]['our_fee'])  }}</div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
    {!! Html::script('js/inline_scripts/accepted_quote.js') !!}
@stop