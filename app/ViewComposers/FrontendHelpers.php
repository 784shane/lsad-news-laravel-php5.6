<?php
namespace App\ViewComposers;

use App\ViewComposers\LocalCurrencyFormats\LocalCurrencyFormats;

class  FrontendHelpers
{
    static $chosenCurrency = "pound";

    public static function convertToCurrencyFormat($money){
        // Get the default cuurency format. call its converter.
        // The pound just happens to be LocalCurrencyFormats
      return LocalCurrencyFormats::formatMoney($money);
    }

}