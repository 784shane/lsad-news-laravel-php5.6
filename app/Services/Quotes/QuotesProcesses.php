<?php namespace App\Services\Quotes;

use App\Services\Validation\SiteValidator;


class QuotesProcesses extends SiteValidator{

	protected function quoteFormValidator($data){

		return self::make($data, [

			'job_quote_amt' => 'numeric',
			'quote_message' => 'required|min:1',
			// @Todo - We will create custom validation rules here
			// 1 User should not be able to submit any resemblance
			// of email or
		],
			//insert our custom messages
			$this->registerMessages()
		);
	}

	public function validateMessageReply($data){

		return self::make($data, [

			'quote_message' => 'required|min:1',
		],
			//insert our custom messages
			$this->registerMessages()
		);
	}

	/**
	 * Get the array of messages which should be applied
	 * to this validation of form inputs.
	 *
	 * @return array
	 */
	private function registerMessages(){
		return array(
			'quote_message.required' => 'At least please enter something',
			'job_quote_amt.numeric' => 'Make sure you have entered a number. '
		);
	}

}