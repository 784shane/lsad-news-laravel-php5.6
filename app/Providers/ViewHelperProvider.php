<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewHelperProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
        $this->app->bind('view_helper', function(){
            return new \App\Helpers\ViewHelper;
        });
	}

}
