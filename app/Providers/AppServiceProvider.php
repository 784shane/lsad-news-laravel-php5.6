<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $this->defineFilters();

        // Add a view composer for our layouts
        view()->composer(
        //Add the views we need to use this
            [
                'layouts.master',
                'layouts.account_page'
            ],
            'App\ViewComposers\ViewLayoutsComposer'
        );

        view()->composer(
        //Add the views we need to use this
            [
                'messages.message',
                'partials.messages.message',
                'deliveries.singleJob',
                'deliveries.liveJobs',
                'deliveries.quoteJob',
                'deliveries.input',
                'deliveries.edit',
                'home',
                'layouts.master',
                'quotes.accepted',
                'quotes.payments.result'
            ],
            'App\ViewComposers\ViewHelperComposer'
        );

        view()->composer(
        //Add the views we need to use this
            [
                'layouts.master'
            ],
            'App\ViewComposers\ViewAuthComposer'
        );

        view()->composer(
        //Add the views we need to use this
            [
                'deliveries.edit'
            ],
            'App\ViewComposers\ViewDeliveryEditTemplateComposer'
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('site_data', function(){
            return new \App\Services\SiteData;
        });

        $this->app->bind('deliveries_class', function(){
            return new \App\Services\Deliveries\Deliveries;
        });

        $this->app->bind('deliveries_edit_class', function(){
            return new \App\Services\Deliveries\DeliveriesEdit;
        });

        $this->app->bind('deliveries_delete_class', function(){
            return new \App\Services\Deliveries\DeliveriesDelete;
        });

        $this->app->bind('message_management', function(){
            return new \App\Services\Messages;
        });

        $this->app->bind('site_units', function(){
            return new \App\Services\Units;
        });

        $this->app->bind('site_payments', function(){
            return new \App\Services\Payments;
        });
    }

    private function defineFilters(){

        $activeMenuLinks  = \App\Services\ActiveMenuLinks::getInstance();
        \Route::filter('home.route.filter', function() use($activeMenuLinks)
        {
            $activeMenuLinks->setActiveLink('home');
        });
        \Route::filter('getQuote.route.filter', function() use($activeMenuLinks)
        {
            $activeMenuLinks->setActiveLink('get_quote');
        });
        \Route::filter('deliveryJobs.route.filter', function() use($activeMenuLinks)
        {
            $activeMenuLinks->setActiveLink('delivery_jobs');
        });
        \Route::filter('signUpAsTransporter.route.filter', function() use($activeMenuLinks)
        {
            $activeMenuLinks->setActiveLink('sign_up_as_transporter');
        });
        \Route::filter('login.route.filter', function() use($activeMenuLinks)
        {
            $activeMenuLinks->setActiveLink('login');
        });

    }
}
